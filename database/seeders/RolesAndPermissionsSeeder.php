<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\SystemModule;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();

        $role = Role::create([
            'name' => 'super_admin',
            'display_name' => 'Super Admin',
            'description' => 'Super Admin'
        ]);

        Role::create([
            'name' => 'campus_head',
            'display_name' => 'Campus Head',
            'description' => 'Campus Head'
        ]);

        Role::create([
            'name' => 'admission_advisor',
            'display_name' => 'Admission Advisor',
            'description' => 'Admission Advisor'
        ]);

        Role::create([
            'name' => 'social_media_manager',
            'display_name' => 'Social Media Manager',
            'description' => 'Social Media Manager'
        ]);

        Role::create([
            'name' => 'admin_officer',
            'display_name' => 'Admin Officer',
            'description' => 'Admin Officer'
        ]);

        Role::create([
            'name' => 'reviewer',
            'display_name' => 'Reviewer',
            'description' => 'Reviewer'
        ]);

        DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id,
            'user_type' => 'App\Models\User'
        ]);

        $systemModule = SystemModule::create([
            'name' => 'Roles and Permissions',
            'description' => 'This module is related to roles & permissions'
        ]);

        DB::table('permissions')->insert(array(
            [
                'name' => 'show-roles-and-permissions',
                'display_name' => 'Role and Permissions',
                'description' => 'Can add role',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'add-role',
                'display_name' => 'Add Role',
                'description' => 'Can add role',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'edit-role',
                'display_name' => 'Edit Role',
                'description' => 'Can edit role',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'delete-role',
                'display_name' => 'Delete Role',
                'description' => 'Can delete role',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'add-permission',
                'display_name' => 'Add Permission',
                'description' => 'Can add permission',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'edit-permission',
                'display_name' => 'Edit Permission',
                'description' => 'Can edit permission',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'delete-permission',
                'display_name' => 'Delete Permission',
                'description' => 'Can delete permission',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'add-user',
                'display_name' => 'Add User',
                'description' => 'Can add user',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'edit-user',
                'display_name' => 'Edit User',
                'description' => 'Can edit user',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'delete-user',
                'display_name' => 'Delete User',
                'description' => 'Can delete user',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'add-system-module',
                'display_name' => 'Add System Module',
                'description' => 'Can add system module',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'edit-system-module',
                'display_name' => 'Edit System Module',
                'description' => 'Can edit system module',
                'system_module_id' => $systemModule->id
            ],
            [
                'name' => 'delete-system-module',
                'display_name' => 'Delete System Module',
                'description' => 'Can delete system module',
                'system_module_id' => $systemModule->id
            ]
        ));

        $systemModule = SystemModule::create([
            'name' => 'Branches',
            'description' => 'This module is related to branches'
        ]);

        DB::table('permissions')->insert(array(
            [
                'name' => 'add-branch',
                'display_name' => 'Add Branch',
                'description' => 'Can add branch',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-branch',
                'display_name' => 'Edit Branch',
                'description' => 'Can edit branch',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-branch',
                'display_name' => 'Delete Branch',
                'description' => 'Can delete branch',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'add-employee',
                'display_name' => 'Add Employee',
                'description' => 'Can add employee',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-employee',
                'display_name' => 'Edit Employee',
                'description' => 'Can edit employee',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-employee',
                'display_name' => 'Delete Employee',
                'description' => 'Can delete employee',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-branches',
                'display_name' => 'Show Branches',
                'description' => 'Can view branches',
                'system_module_id' => $systemModule->id
            ]
        ));

        /*creating source module*/
        $systemModule = SystemModule::create([
            'name' => 'Sources',
            'description' => 'This module is related to sources'
        ]);

        /*adding permissions of source module*/
        DB::table('permissions')->insert(array(
            [
                'name' => 'add-source',
                'display_name' => 'Add Source',
                'description' => 'Can add source',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-source',
                'display_name' => 'Edit Source',
                'description' => 'Can edit source',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-source',
                'display_name' => 'Delete Source',
                'description' => 'Can delete source',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-sources',
                'display_name' => 'Show Sources',
                'description' => 'Can view sources',
                'system_module_id' => $systemModule->id
            ]
        ));

        /*creating course module*/
        $systemModule = SystemModule::create([
            'name' => 'Courses',
            'description' => 'This module is related to courses'
        ]);

        /*adding permissions of course module*/
        DB::table('permissions')->insert(array(
            [
                'name' => 'add-course',
                'display_name' => 'Add Course',
                'description' => 'Can add course',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-course',
                'display_name' => 'Edit Course',
                'description' => 'Can edit course',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-course',
                'display_name' => 'Delete Course',
                'description' => 'Can delete course',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-courses',
                'display_name' => 'Show Courses',
                'description' => 'Can view courses',
                'system_module_id' => $systemModule->id
            ]
        ));

        /*creating class module*/
        $systemModule = SystemModule::create([
            'name' => 'Classes',
            'description' => 'This module is related to classes'
        ]);

        /*adding permissions of class module*/
        DB::table('permissions')->insert(array(
            [
                'name' => 'add-class',
                'display_name' => 'Add Class',
                'description' => 'Can add class',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-class',
                'display_name' => 'Edit Class',
                'description' => 'Can edit class',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-class',
                'display_name' => 'Delete Class',
                'description' => 'Can delete class',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-classes',
                'display_name' => 'Show Classes',
                'description' => 'Can view classes',
                'system_module_id' => $systemModule->id
            ]
        ));

        /*creating lead module*/
        $systemModule = SystemModule::create([
            'name' => 'Leads',
            'description' => 'This module is related to leads'
        ]);

        /*adding permissions of lead module*/
        DB::table('permissions')->insert(array(
            [
                'name' => 'add-lead',
                'display_name' => 'Add Lead',
                'description' => 'Can add lead',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-lead',
                'display_name' => 'Edit Lead',
                'description' => 'Can edit lead',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-lead',
                'display_name' => 'Delete Lead',
                'description' => 'Can delete lead',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-leads',
                'display_name' => 'Show Leads',
                'description' => 'Can view leads',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'add-lead-type',
                'display_name' => 'Add Lead Type',
                'description' => 'Can add lead type',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-lead-type',
                'display_name' => 'Edit Lead Type',
                'description' => 'Can edit lead type',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-lead-type',
                'display_name' => 'Delete Lead Type',
                'description' => 'Can delete lead type',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'add-lead-status',
                'display_name' => 'Add Lead Status',
                'description' => 'Can add lead status',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-lead-status',
                'display_name' => 'Edit Lead Status',
                'description' => 'Can edit lead status',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-lead-status',
                'display_name' => 'Delete Lead Status',
                'description' => 'Can delete lead status',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-lead-type',
                'display_name' => 'Show Lead Type',
                'description' => 'Can view lead types',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-lead-status',
                'display_name' => 'Show Lead Status',
                'description' => 'Can view lead statuses',
                'system_module_id' => $systemModule->id
            ]
        ));

        /*creating region management module*/
        $systemModule = SystemModule::create([
            'name' => 'Region Management',
            'description' => 'This module is related to region management'
        ]);

        /*adding permissions of region management module*/
        DB::table('permissions')->insert(array(
            [
                'name' => 'add-country',
                'display_name' => 'Add Country',
                'description' => 'Can add country',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-country',
                'display_name' => 'Edit Country',
                'description' => 'Can edit country',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-country',
                'display_name' => 'Delete Country',
                'description' => 'Can delete country',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-region-management',
                'display_name' => 'Show Region Management',
                'description' => 'Can view region management',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'add-state',
                'display_name' => 'Add State',
                'description' => 'Can add state',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-state',
                'display_name' => 'Edit State',
                'description' => 'Can edit state',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-state',
                'display_name' => 'Delete State',
                'description' => 'Can delete state',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'add-city',
                'display_name' => 'Add City',
                'description' => 'Can add city',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-city',
                'display_name' => 'Edit City',
                'description' => 'Can edit city',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-city',
                'display_name' => 'Delete City',
                'description' => 'Can delete city',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'add-contact-code',
                'display_name' => 'Add Contact Code',
                'description' => 'Can add contact code',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-contact-code',
                'display_name' => 'Edit Contact Code',
                'description' => 'Can edit contact code',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-contact-code',
                'display_name' => 'Delete Contact Code',
                'description' => 'Can delete contact code',
                'system_module_id' => $systemModule->id
            ]
        ));

        /*creating dashboard module*/
        $systemModule = SystemModule::create([
            'name' => 'Dashboard',
            'description' => 'This module is related to dashboard'
        ]);

        /*adding permissions of dashboard module*/
        DB::table('permissions')->insert(array(
            [
                'name' => 'show-leads-widget',
                'display_name' => 'Show Leads Widget',
                'description' => 'Can view lead widget',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-branches-widget',
                'display_name' => 'Show Branches Widget',
                'description' => 'Can view branches widget',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-users-widget',
                'display_name' => 'Show Users Widget',
                'description' => 'Can view users widget',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-employees-widget',
                'display_name' => 'Show Employees Widget',
                'description' => 'Can view employees widget',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'show-lead-by-source',
                'display_name' => 'Show Lead By Source',
                'description' => 'Can view lead by source chart',
                'system_module_id' => $systemModule->id
            ]
        ));

        /*creating region management module*/
        $systemModule = SystemModule::create([
            'name' => 'Call Center',
            'description' => 'This module is related to call center'
        ]);

        /*adding permissions of region management module*/
        DB::table('permissions')->insert(array(
            [
                'name' => 'add-outreach-data',
                'display_name' => 'Add Outreach Data',
                'description' => 'Can add outreach data',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'edit-country',
                'display_name' => 'Edit Outreach Data',
                'description' => 'Can edit outreach data',
                'system_module_id' => $systemModule->id
            ], [
                'name' => 'delete-country',
                'display_name' => 'Delete Outreach Data',
                'description' => 'Can delete outreach data',
                'system_module_id' => $systemModule->id
            ]
        ));

        $permissions = Permission::all();
        if (count($permissions) > 0) {
            foreach ($permissions as $permission) {
                DB::table('permission_role')->insert([
                    'permission_id' => $permission->id,
                    'role_id' => $role->id
                ]);
            }
        }
    }
}
