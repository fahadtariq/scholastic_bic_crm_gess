<?php

namespace Database\Seeders;

use App\Models\LeadStatus;
use App\Models\LeadType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeadTypeAndStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeadType::create([
            'name' => '80%',
            'status_id' => 1
        ]);

        LeadType::create([
            'name' => '50%',
            'status_id' => 1
        ]);

        LeadType::create([
            'name' => '20%',
            'status_id' => 1
        ]);

        LeadType::create([
            'name' => '100%',
            'status_id' => 1
        ]);

        LeadType::create([
            'name' => '0%',
            'status_id' => 1
        ]);

        LeadStatus::create([
            'name' => 'Pending',
            'status_id' => 1
        ]);

        LeadStatus::create([
            'name' => 'Assigned',
            'status_id' => 1
        ]);

        LeadStatus::create([
            'name' => 'Processing',
            'status_id' => 1
        ]);

        LeadStatus::create([
            'name' => 'Dropped',
            'status_id' => 1
        ]);

        LeadStatus::create([
            'name' => 'Registered',
            'status_id' => 1
        ]);

        LeadStatus::create([
            'name' => 'Paid',
            'status_id' => 1
        ]);

        LeadStatus::create([
            'name' => 'Not Interested',
            'status_id' => 1
        ]);
        LeadStatus::create([
            'name' => 'Withdrawals',
            'status_id' => 1
        ]);

        DB::table('courses')->insert(array([
            'name' => 'BSc Business Administration',
            'status_id' => 1
        ], [
            'name' => 'BSc Computer Science',
            'status_id' => 1
        ], [
            'name' => 'LLB (Hons)',
            'status_id' => 1
        ], [
            'name' => 'Pearson BTEC Extended National Diploma in Business',
            'status_id' => 1
        ], [
            'name' => 'BTEC Higher National Diploma',
            'status_id' => 1
        ], [
            'name' => 'Cert HE  Business Administration',
            'status_id' => 1
        ], [
            'name' => 'BSc Psychology',
            'status_id' => 1
        ], [
            'name' => 'BEng (Hons) Mechanical Engineering',
            'status_id' => 1
        ], [
            'name' => 'BEng (Hons) Electrical and Electronic Engineering',
            'status_id' => 1
        ], [
            'name' => 'BA (Hons) International Relations and Politics',
            'status_id' => 1
        ], [
            'name' => 'MA International Relations',
            'status_id' => 1
        ]));

        DB::table('countries')->insert(array([
            'name' => 'Pakistan',
            'status_id' => 1
        ]));

        DB::table('states')->insert(array([
            'country_id' => 1,
            'name' => 'Punjab',
            'status_id' => 1
        ], [
            'country_id' => 1,
            'name' => 'Sindh',
            'status_id' => 1
        ], [
            'country_id' => 1,
            'name' => 'Khyber Pakhtunkhwa',
            'status_id' => 1
        ], [
            'country_id' => 1,
            'name' => 'Balochistan',
            'status_id' => 1
        ]));

        DB::table('cities')->insert(array([
            'country_id' => 1,
            'state_id' => 1,
            'name' => 'Lahore',
            'status_id' => 1
        ], [
            'country_id' => 1,
            'state_id' => 1,
            'name' => 'Faisalabad',
            'status_id' => 1
        ], [
            'country_id' => 1,
            'state_id' => 1,
            'name' => 'Islamabad',
            'status_id' => 1
        ]));

        DB::table('class_grades')->insert(array([
            'name' => 'Year-1',
            'beams_id' => 80,
            'status_id' => 1
        ], [
            'name' => 'Year-2',
            'beams_id' => 81,
            'status_id' => 1
        ], [
            'name' => 'Year-3',
            'beams_id' => 82,
            'status_id' => 1
        ]));

        DB::table('contact_codes')->insert(array([
            'country_id' => 1,
            'code' => '+92',
            'status_id' => 1
        ]));

        DB::table('sources')->insert(array([
            'name' => 'BSS Matric',
            'status_id' => 1
        ], [
            'name' => 'Chat Box',
            'status_id' => 1
        ], [
            'name' => 'Campus Outreach',
            'status_id' => 1
        ], [
            'name' => 'Social Media',
            'status_id' => 1
        ], [
            'name' => 'Walk-In',
            'status_id' => 1
        ], [
            'name' => 'Call Center',
            'status_id' => 1
        ]));

        DB::table('reasons')->insert(array([
            'name' => 'Due to illness/death',
            'status_id' => 1
        ], [
            'name' => 'Disciplinary Action by college',
            'status_id' => 1
        ], [
            'name' => 'Due to conveyance problem/change in residence/long distance',
            'status_id' => 1
        ], [
            'name' => 'Non-acceptance by foreign awarding body',
            'status_id' => 1
        ], [
            'name' => 'Transfer to another branch',
            'status_id' => 1
        ], [
            'name' => 'Moved to another College',
            'status_id' => 1
        ], [
            'name' => 'Financial Issues',
            'status_id' => 1
        ], [
            'name' => 'Personal Issues',
            'status_id' => 1
        ], [
            'name' => 'Going for subject not offered / change of board',
            'status_id' => 1
        ], [
            'name' => 'Prolonged Absence / Non-payment of Fee',
            'status_id' => 1
        ], [
            'name' => 'Privately appeared in examinations',
            'status_id' => 1
        ], [
            'name' => 'Parent (Staff) resignation',
            'status_id' => 1
        ], [
            'name' => 'Unreachable',
            'status_id' => 1
        ], [
            'name' => 'Dissatisfied with quality of teaching',
            'status_id' => 1
        ], [
            'name' => 'Dissatisfied with Management/ Administration',
            'status_id' => 1
        ], [
            'name' => 'Dissatisfied with Infrastructure/facilities',
            'status_id' => 1
        ], [
            'name' => 'Dissatisfied with staff attitude',
            'status_id' => 1
        ], [
            'name' => 'Dissatisfied with college discipline',
            'status_id' => 1
        ]));

    }
}
