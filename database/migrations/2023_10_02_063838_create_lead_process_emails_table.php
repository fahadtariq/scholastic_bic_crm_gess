<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_process_emails', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("lead_id")->nullable();
            $table->string("to")->nullable();
            $table->string("subject")->nullable();
            $table->string("type")->nullable();
            $table->mediumText("message")->nullable();
            $table->text("remarks")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_process_emails');
    }
};
