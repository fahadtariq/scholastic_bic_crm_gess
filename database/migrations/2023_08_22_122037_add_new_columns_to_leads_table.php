<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->dateTime('beams_paid_date')->nullable();
            $table->dateTime('beams_registration_date')->nullable();
            $table->dateTime('sync_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->dropColumn('beams_paid_date');
            $table->dropColumn('beams_registration_date');
            $table->dropColumn('sync_date');
        });
    }
};
