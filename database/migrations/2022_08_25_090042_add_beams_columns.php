<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
        });

        Schema::table('states', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
        });

        Schema::table('cities', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
        });

        Schema::table('class_grades', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
        });

        Schema::table('sources', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
        });

        Schema::table('courses', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
        });

        Schema::table('branches', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
        });

        Schema::table('reasons', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
        });

        Schema::table('leads', function(Blueprint $table) {
            $table->string('beams_id', 255)->nullable()->after('id');
            $table->tinyInteger('is_sync')->default(0)->after('beams_id')->comment('0 = No, 1 = Yes');
            $table->tinyInteger('is_confirmed')->default(0)->after('is_sync')->comment('0 = No, 1 = Yes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function(Blueprint $table) {
            $table->dropColumn('beams_id');
        });

        Schema::table('states', function(Blueprint $table) {
            $table->dropColumn('beams_id');
        });

        Schema::table('cities', function(Blueprint $table) {
            $table->dropColumn('beams_id');
        });

        Schema::table('class_grades', function(Blueprint $table) {
            $table->dropColumn('beams_id');
        });

        Schema::table('sources', function(Blueprint $table) {
            $table->dropColumn('beams_id');
        });

        Schema::table('courses', function(Blueprint $table) {
            $table->dropColumn('beams_id');
        });

        Schema::table('branches', function(Blueprint $table) {
            $table->dropColumn('beams_id');
        });

        Schema::table('reasons', function(Blueprint $table) {
            $table->dropColumn('beams_id');
        });

        Schema::table('leads', function(Blueprint $table) {
            $table->dropColumn(['beams_id','is_sync','is_confirmed']);
        });
    }
};
