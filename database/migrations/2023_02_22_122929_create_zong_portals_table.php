<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zong_portals', function (Blueprint $table) {
            $table->id();
            $table->string('phone')->nullable();
            $table->string('client_ext')->nullable();
            $table->dateTime('on_date')->nullable();
            $table->integer('duration')->nullable();
            $table->string('status')->nullable();
            $table->tinyInteger('type')->nullable()->comment('incoming => 1, outgoing => 0');
            $table->string('recording')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zong_portals');
    }
};
