<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1 => In-Progress, 2 => Completed');
            $table->tinyInteger('type')->nullable()->comment('1 => Email, 2 => Sms');
            $table->longText('recipients')->nullable();
            $table->longText('email_content')->nullable();
            $table->boolean('is_scheduled')->default(0);
            $table->string('scheduled_date_time')->nullable();
            $table->unsignedBigInteger('email_template_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
};
