<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->string('code', 255);
            $table->unsignedBigInteger('user_id')->nullable()->comment('Lead created by');
            $table->unsignedBigInteger('student_id')->nullable();
            $table->unsignedBigInteger('source_id')->nullable();
            $table->unsignedBigInteger('lead_type_id')->nullable();
            $table->unsignedBigInteger('lead_status_id')->nullable();
            $table->unsignedBigInteger('reason_id')->nullable()->comment('reason id on drop');
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->unsignedBigInteger('course_id')->nullable();
            $table->unsignedBigInteger('employee_id')->nullable()->comment('Assigning to employee');
            $table->tinyInteger('is_eligible')->default(0)->comment('0 = No, 1 = Yes, 2 = Not Sure');
            $table->tinyInteger('is_referred')->default(0)->comment('0 = No, 1 = Yes');
            $table->string('advisor_name', 255)->nullable();
            $table->string('advisor_campus_name', 255)->nullable();
            $table->string('previous_class', 255)->nullable();
            $table->string('previous_branch', 255)->nullable();
            $table->tinyInteger('is_important')->default(0)->comment('0 = No, 1 = Yes');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('source_id')->references('id')->on('sources');
            $table->foreign('lead_type_id')->references('id')->on('lead_types');
            $table->foreign('lead_status_id')->references('id')->on('lead_statuses');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('reason_id')->references('id')->on('reasons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
};
