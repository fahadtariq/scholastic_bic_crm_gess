<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('previous_leads', function (Blueprint $table) {
            $table->id();
            $table->string('region')->nullable();
            $table->string('school_id')->nullable();
            $table->string('student_id')->nullable();
            $table->string('student_name')->nullable();
            $table->string('student_gender')->nullable();
            $table->string('student_class')->nullable();
            $table->string('student_section')->nullable();
            $table->string('student_father_cnic')->nullable();
            $table->string('student_mother_cnic')->nullable();
            $table->string('admission_fee')->nullable();
            $table->string('branch')->nullable();
            $table->string('total')->nullable();
            $table->string('period')->nullable();
            $table->string('admission_date')->nullable();
            $table->string('paid_date')->nullable();
            $table->string('registration_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('previous_leads');
    }
};
