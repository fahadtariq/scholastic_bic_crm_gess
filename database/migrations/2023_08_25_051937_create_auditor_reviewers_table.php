<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_reviewers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->comment('Auditor/Reviewer ID');
            $table->date('review_date')->nullable();
            $table->boolean('lead_count_acc')->nullable()->comment('0 => No, 1 => Yes');
            $table->boolean('lead_assignment_acc')->nullable()->comment('0 => No, 1 => Yes');
            $table->boolean('review_acc')->nullable()->comment('0 => No, 1 => Yes');
            $table->boolean('admission_acc')->nullable()->comment('0 => No, 1 => Yes');
            $table->boolean('outbound_call_acc')->nullable()->comment('0 => No, 1 => Yes');
            $table->text('review_remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_reviewers');
    }
};
