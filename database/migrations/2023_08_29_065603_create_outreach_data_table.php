<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outreach_data', function (Blueprint $table) {
            $table->id();
            $table->integer('city_id')->nullable();
            $table->integer('source_id')->nullable();
            $table->string('city_name', 255)->nullable();
            $table->string('student_id')->nullable();
            $table->string('student_name', 255)->nullable();
            $table->string('school', 255)->nullable();
            $table->string('class', 255)->nullable();
            $table->string('section', 255)->nullable();
            $table->string('contact_number', 255)->nullable();
            $table->string('address')->nullable();
            $table->string('email', 255)->nullable();
            $table->string('year')->nullable();
            $table->string('session')->nullable();
            $table->string('status')->nullable();
            $table->string('source')->nullable();
            $table->date('received_date')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outreach_data');
    }
};
