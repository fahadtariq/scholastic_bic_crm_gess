<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_trackings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lead_id')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->unsignedBigInteger('lead_type_id')->nullable();
            $table->unsignedBigInteger('lead_status_id')->nullable();
            $table->tinyInteger('type')->default(0)->comment('0 = lead type , 1 = lead status, 2 = employee, 3 = branch');
            $table->text('reason')->nullable();
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('lead_id')->references('id')->on('leads');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('lead_type_id')->references('id')->on('lead_types');
            $table->foreign('lead_status_id')->references('id')->on('lead_statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_trackings');
    }
};
