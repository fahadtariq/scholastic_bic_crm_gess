<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_guardians', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('smart_card')->nullable();
            $table->unsignedBigInteger('student_id')->nullable();
            $table->bigInteger('employee_id')->default(0);
            $table->string('employee_beams_id')->nullable();
            $table->string('occupation')->nullable();
            $table->string('designation')->nullable();
            $table->string('organisation')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->unsignedBigInteger('contact_code_id')->nullable();
            $table->string('contact_number');
            $table->tinyInteger('guardian_type')->default(1)->comment('1 = Father, 2 = Mother');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('contact_code_id')->references('id')->on('contact_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_guardians');
    }
};
