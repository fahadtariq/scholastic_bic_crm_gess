<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_follow_ups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->comment('follow up created by');
            $table->unsignedBigInteger('lead_id')->nullable();
            $table->unsignedBigInteger('source_id')->nullable();
            $table->dateTime('follow_up_date')->nullable();
            $table->string('follow_up_by')->nullable()->comment('the person took follow up');
            $table->tinyInteger('follow_up_duration')->default('0');
            $table->text('follow_up_remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('lead_id')->references('id')->on('leads');
            $table->foreign('source_id')->references('id')->on('sources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_follow_ups');
    }
};
