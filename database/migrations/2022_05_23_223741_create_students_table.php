<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->unsignedBigInteger('class_grade_id')->nullable();
            $table->tinyInteger('gender')->default(1)->comment('1 = Male, 2 = Female, 3 = Other');
            $table->date('date_of_birth')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('nationality')->nullable();
            $table->string('smart_card')->nullable();
            $table->string('passport')->nullable();
            $table->string('address')->nullable();
            $table->unsignedBigInteger('contact_code_id')->nullable();
            $table->string('contact_number');
            $table->string('email')->nullable();
            $table->string('city')->nullable();
            $table->string('illness_allergies')->nullable();
            $table->string('physical_impairment')->nullable();
            $table->string('profile_image')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('contact_code_id')->references('id')->on('contact_codes');
            $table->foreign('class_grade_id')->references('id')->on('class_grades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
};
