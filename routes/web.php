<?php

use App\Http\Controllers\Backend\ActivityController;
use App\Http\Controllers\Backend\AuditorReviewerController;
use App\Http\Controllers\Backend\BranchController;
use App\Http\Controllers\Backend\CampaignController;
use App\Http\Controllers\Backend\CityController;
use App\Http\Controllers\Backend\ClassGradeController;
use App\Http\Controllers\Backend\CommonController;
use App\Http\Controllers\Backend\ContactCodeController;
use App\Http\Controllers\Backend\CountryController;
use App\Http\Controllers\Backend\CourseController;
use App\Http\Controllers\Backend\EmployeeController;
use App\Http\Controllers\Backend\IndexController;
use App\Http\Controllers\Backend\LeadController;
use App\Http\Controllers\Backend\LeadStatusController;
use App\Http\Controllers\Backend\LeadTypeController;
use App\Http\Controllers\Backend\OutReachDataController;
use App\Http\Controllers\Backend\PermissionController;
use App\Http\Controllers\Backend\PromotionalAdmissionController;
use App\Http\Controllers\Backend\ReportController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\SourceController;
use App\Http\Controllers\Backend\StateController;
use App\Http\Controllers\Backend\SystemModuleController;
use App\Http\Controllers\Backend\TagController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\ZongPortalController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

// Check authenttication
Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('dashboard');
    } else {
        return view('auth.login');
    }
})->name('index');

Route::get('/test',[ZongPortalController::class,'test']);

// QRCode Scanner
Route::get('/generate-lead',[LeadController::class,'generateLead'])->name('generate.lead');
Route::post('/generate-lead/store', [LeadController::class, 'generateLeadStore'])->name('generate.lead.store');
// Social Lead Generate
Route::get('/generate-lead-facebook',[LeadController::class,'generateLeadFacebook'])->name('generate.lead.facebook');
Route::get('/generate-lead-instagram',[LeadController::class,'generateLeadInstagram'])->name('generate.lead.instagram');
Route::get('/generate-lead-website',[LeadController::class,'generateLeadWebsite'])->name('generate.lead.website');

// Routes for QR leads
Route::post('qr-load-courses', [BranchController::class, 'loadCourses'])->name('qrlead.load-courses');
Route::post('qr-load-employees', [BranchController::class, 'loadEmployees'])->name('qrlead.load-employees');
Route::post('qr-branch-sessions', [BranchController::class, 'sessions'])->name('qrlead.sessions');
Route::post('get-funnel-data', [IndexController::class, 'funnelData'])->name('funnel.data');
Route::post('filter-funnel-data', [IndexController::class, 'filterFunnelData'])->name('funnel.data.filter');
Route::get('/get-previous-data-report', [IndexController::class, 'getPreviousDataReport'])->name('previous.data.report');

/*Authentication Middleware*/
Route::group(['middleware' => ['auth']], function () {
    /*Lock Screen*/
    Route::prefix('lock')->group(function () {
        Route::get('screen', [HomeController::class, 'lockScreen'])->name('lock.screen');
        Route::post('update', [HomeController::class, 'lockScreenUpdate'])->name('lock.screen.update');
    });

    /*Lock Screen Middleware*/
    Route::middleware('lockScreen')->group(function () {
        /*Dashboard*/
        // Route::get('/dashboard', [IndexController::class, 'index'])->name('dashboard');
        Route::get('/dashboard', [IndexController::class, 'salesFunnel'])->name('dashboard');

        Route::post('/dashboard/load-content', [IndexController::class, 'loadContent'])->name('dashboard.content.load');

        Route::group(['middleware' => ['role:super_admin']], function () {
            /*SYSTEM MODULE*/
            Route::resources(['system-modules' => SystemModuleController::class]);

            /*PERMISSION MODULE*/
            Route::resources(['permissions' => PermissionController::class]);

            /*ROLES MODULE*/
            Route::resources(['roles' => RoleController::class]);

            /*Promotional Admission*/
            Route::resource('promotional-admissions', PromotionalAdmissionController::class);
            Route::get('toggle-promotional-lead/{promotional_admission}/{is_add}', [PromotionalAdmissionController::class, 'togglePromotionalLead'])->name('promotional-admissions.toggle-promotional-lead');

            /*USER MODULE*/
            Route::prefix('user')->group(function () {
                Route::get('list', [UserController::class, 'index'])->name('users.index');
                Route::get('create', [UserController::class, 'create'])->name('user.create');
                Route::post('show', [UserController::class, 'show'])->name('user.show');
                Route::post('store', [UserController::class, 'store'])->name('user.store');
                Route::get('edit/{id}', [UserController::class, 'edit'])->name('user.edit');
                Route::post('update', [UserController::class, 'update'])->name('user.update');
                Route::get('profile', [UserController::class, 'editProfile'])->name('user.profile.edit');
                Route::post('profile/update', [UserController::class, 'updateProfile'])->name('user.profile.update');
                Route::patch('toggle/{user}', [UserController::class, 'toggleUserActivation'])->name('user.toggle-activation');
            });

            /*Country Module*/
            Route::prefix('country')->group(function () {
                Route::get('/list',  [CountryController::class, 'index'])->name('country.index');
                Route::post('/store', [CountryController::class, 'store'])->name('country.store');
                Route::get('/edit/{id}', [CountryController::class, 'edit'])->name('country.edit');
                Route::post('/update', [CountryController::class, 'update'])->name('country.update');
                Route::post('/delete', [CountryController::class, 'destroy'])->name('country.destroy');
            });

            /*State Module*/
            Route::prefix('state')->group(function () {
                Route::get('/list',  [StateController::class, 'index'])->name('state.index');
                Route::post('/store', [StateController::class, 'store'])->name('state.store');
                Route::get('/edit/{id}', [StateController::class, 'edit'])->name('state.edit');
                Route::post('/update', [StateController::class, 'update'])->name('state.update');
                Route::post('/delete', [StateController::class, 'destroy'])->name('state.destroy');
            });

            /*City Module*/
            Route::prefix('city')->group(function () {
                Route::get('/list',  [CityController::class, 'index'])->name('city.index');
                Route::post('/store', [CityController::class, 'store'])->name('city.store');
                Route::get('/edit/{id}', [CityController::class, 'edit'])->name('city.edit');
                Route::post('/update', [CityController::class, 'update'])->name('city.update');
                Route::post('/delete', [CityController::class, 'destroy'])->name('city.destroy');
            });

            /*Contact Codes Module*/
            Route::prefix('contact-code')->group(function () {
                Route::get('/list',  [ContactCodeController::class, 'index'])->name('contact-code.index');
                Route::post('/store', [ContactCodeController::class, 'store'])->name('contact-code.store');
                Route::get('/edit/{id}', [ContactCodeController::class, 'edit'])->name('contact-code.edit');
                Route::post('/update', [ContactCodeController::class, 'update'])->name('contact-code.update');
                Route::post('/delete', [ContactCodeController::class, 'destroy'])->name('contact-code.destroy');
            });
        });

        Route::group(['middleware' => ['role:super_admin|campus_head']], function () {
            /*BRANCH MODULE*/
            Route::prefix('branch')->group(function () {
                Route::get('list', [BranchController::class, 'index'])->name('branches.index');
                Route::get('create', [BranchController::class, 'create'])->name('branch.create');
                Route::post('store', [BranchController::class, 'store'])->name('branch.store');
                Route::get('edit/{id}', [BranchController::class, 'edit'])->name('branch.edit');
                Route::post('update', [BranchController::class, 'update'])->name('branch.update');
                Route::post('delete', [BranchController::class, 'delete'])->name('branch.delete');
                Route::post('country-states', [BranchController::class, 'countryStates'])->name('branch.country.states');
                Route::post('state-cities', [BranchController::class, 'stateCities'])->name('branch.state.cities');
                Route::post('targets', [BranchController::class, 'targets'])->name('branch.targets');
                Route::post('target/store', [BranchController::class, 'targetStore'])->name('branch.target.store');
                Route::post('target/edit', [BranchController::class, 'editTarget'])->name('branch.target.edit');
                Route::post('target/delete', [BranchController::class, 'deleteTarget'])->name('branch.target.delete');
                Route::post('course/target/save', [BranchController::class, 'saveCourseTarget'])->name('branch.course.target.save');
                Route::post('target/status/update', [BranchController::class, 'updateTarget'])->name('branch.target.default.status');
                Route::post('session/load-courses', [BranchController::class, 'sessionCourses'])->name('branch.session.load-courses');
            });

            /*EMPLOYEE MODULE*/
            Route::prefix('employee')->group(function () {
                Route::get('/list', [EmployeeController::class, 'index'])->name('employees.index');
                Route::get('/create', [EmployeeController::class, 'create'])->name('employee.create');
                Route::post('/store', [EmployeeController::class, 'store'])->name('employee.store');
                Route::get('/edit/{id}', [EmployeeController::class, 'edit'])->name('employee.edit');
                Route::post('/update', [EmployeeController::class, 'update'])->name('employee.update');
                Route::post('/delete', [EmployeeController::class, 'delete'])->name('employee.delete');
            });



            /*Zong Portal*/
            Route::prefix('zong-portal')->group(function () {
                Route::get('/list', [ZongPortalController::class, 'index'])->name('zong-portal.index');
                Route::get('/live-list', [ZongPortalController::class, 'liveZongPortal'])->name('zong-portal.live');
                Route::get('/get-today-data', [ZongPortalController::class, 'getTodayData'])->name('zong-portal.today-data');
            });
        });

        Route::group(['middleware' => ['role:super_admin|campus_head|reviewer']], function () {
        /*Report MODULE*/
            Route::prefix('report')->group(function () {
                Route::get('/withdrawals', [ReportController::class, 'withdrawalsReport'])->name('reports.withdrawals');
                Route::get('/walk-in-sources', [ReportController::class, 'walkInSourcesReport'])->name('reports.walk-in-sources');
                Route::get('/mis', [ReportController::class, 'misReport'])->name('reports.mis');
            });
        });
        Route::group(['middleware' => ['role:super_admin|social_media_manager|campus_head']], function () {
            Route::post('load-courses', [BranchController::class, 'loadCourses'])->name('branch.load-courses');
            Route::post('load-employees', [BranchController::class, 'loadEmployees'])->name('branch.load-employees');
        });

        Route::group(['middleware' => ['role:super_admin|reviewer']], function (){
            Route::resource('auditor-review', AuditorReviewerController::class);
        });

        Route::post('/branch-sessions', [BranchController::class, 'sessions'])->name('branch.sessions');

        /*State and City List*/
        Route::get('/list-states', [CommonController::class, 'listStates'])->name('list-states');
        Route::get('/list-cities', [CommonController::class, 'listCities'])->name('list-cities');

        Route::group(['middleware' => ['role:super_admin']], function () {
            /*SOURCE MODULE*/
            Route::prefix('source')->group(function () {
                Route::get('list', [SourceController::class, 'index'])->name('sources.index');
                Route::get('create', [SourceController::class, 'create'])->name('source.create');
                Route::post('store', [SourceController::class, 'store'])->name('source.store');
                Route::get('edit/{id}', [SourceController::class, 'edit'])->name('source.edit');
                Route::post('update', [SourceController::class, 'update'])->name('source.update');
                Route::post('delete', [SourceController::class, 'delete'])->name('source.delete');
            });

            /*TAG MODULE*/
            Route::prefix('tag')->group(function () {
                Route::get('list', [TagController::class, 'index'])->name('tags.index');
                Route::get('create', [TagController::class, 'create'])->name('tags.create');
                Route::post('store', [TagController::class, 'store'])->name('tags.store');
                Route::get('edit/{id}', [TagController::class, 'edit'])->name('tags.edit');
                Route::post('update', [TagController::class, 'update'])->name('tags.update');
                Route::post('delete', [TagController::class, 'delete'])->name('tags.delete');
            });

            /*Courses Module*/
            Route::prefix('courses')->group(function () {
                Route::get('/list', [CourseController::class, 'index'])->name('course.index');
                Route::get('/create', [CourseController::class, 'create'])->name('course.create');
                Route::post('/store', [CourseController::class, 'store'])->name('course.store');
                Route::get('/edit/{id}', [CourseController::class, 'edit'])->name('course.edit');
                Route::post('/update', [CourseController::class, 'update'])->name('course.update');
                Route::post('/delete', [CourseController::class, 'destroy'])->name('course.destroy');
            });

            /*CLASS GRADE MODULE*/
            Route::prefix('class')->group(function () {
                Route::get('/list', [ClassGradeController::class, 'index'])->name('class-grades.index');
                Route::post('/store', [ClassGradeController::class, 'store'])->name('class-grade.store');
                Route::get('/edit/{id}', [ClassGradeController::class, 'edit'])->name('class-grade.edit');
                Route::post('/update', [ClassGradeController::class, 'update'])->name('class-grade.update');
                Route::post('/delete', [ClassGradeController::class, 'delete'])->name('class-grade.delete');
            });

            /*Courses Module*/
            Route::prefix('activities')->group(function () {
                Route::get('/list', [ActivityController::class, 'index'])->name('activities.index');
                Route::get('/detail', [ActivityController::class, 'detail'])->name('activity.detail');
            });

            /*LEAD TYPES MODULE*/
            Route::prefix('lead-type')->group(function () {
                Route::get('/list', [LeadTypeController::class, 'index'])->name('lead-type.index');
                Route::post('/store', [LeadTypeController::class, 'store'])->name('lead-type.store');
                Route::get('/edit/{id}', [LeadTypeController::class, 'edit'])->name('lead-type.edit');
                Route::post('/update', [LeadTypeController::class, 'update'])->name('lead-type.update');
                Route::post('/delete', [LeadTypeController::class, 'delete'])->name('lead-type.delete');
            });

            /*LEAD TYPES MODULE*/
            Route::prefix('lead-status')->group(function () {
                Route::get('/list', [LeadStatusController::class, 'index'])->name('lead-status.index');
                Route::post('/store', [LeadStatusController::class, 'store'])->name('lead-status.store');
                Route::get('/edit/{id}', [LeadStatusController::class, 'edit'])->name('lead-status.edit');
                Route::post('/update', [LeadStatusController::class, 'update'])->name('lead-status.update');
                Route::post('/delete', [LeadStatusController::class, 'delete'])->name('lead-status.delete');
            });
        });

        /*Sales Insights (Data Stories)*/

        Route::get('data-insights', [UserController::class, 'showSalesInsights'])->name('data-insights');
        Route::get('call-center-insights', [UserController::class, 'showCallCenterInsights'])->name('call-center-insights');

        /*LEAD MODULE*/
        Route::prefix('lead')->group(function () {
            Route::get('/list', [LeadController::class, 'index'])->name('leads.index');
            Route::get('/create', [LeadController::class, 'create'])->name('lead.create');
            Route::post('/store', [LeadController::class, 'store'])->name('lead.store');
            Route::get('/edit/{id}', [LeadController::class, 'edit'])->name('lead.edit');
            Route::post('/show', [LeadController::class, 'show'])->name('lead.show');
            Route::post('/show-email-body', [LeadController::class, 'showEmailBody'])->name('lead.showemailbody');
            Route::post('/p/sendemail', [LeadController::class, 'sendPEmailBody'])->name('lead.p.sendemail');
            Route::post('/w/sendemail', [LeadController::class, 'sendWEmailBody'])->name('lead.w.sendemail');
            Route::post('/update', [LeadController::class, 'update'])->name('lead.update');
            Route::post('/delete', [LeadController::class, 'delete'])->name('lead.delete');
            Route::post('/change-info', [LeadController::class, 'changeInfo'])->name('lead.info.change');
            Route::post('/change-info/store', [LeadController::class, 'changeInfoStore'])->name('lead.info.change-store');
            Route::post('follow-up/create', [LeadController::class, 'createFollowUp'])->name('lead.follow-up.create');
            Route::post('follow-up/store', [LeadController::class, 'storeFollowUp'])->name('lead.follow-up.store');
            Route::post('load/employees', [LeadController::class, 'loadEmployees'])->name('load.employees');
            Route::post('multiple-assign', [LeadController::class, 'multipleAssignToEmployee'])->name('lead.multiple-assign');
            Route::post('import', [LeadController::class, 'import'])->name('lead.import');
            Route::get('export/email', [LeadController::class, 'exportEmail'])->name('lead.export.email');
            Route::get('export/leads', [LeadController::class, 'exportLeads'])->name('lead.export.excel');
            Route::post('import-fall', [LeadController::class, 'importFall'])->name('leads.fall.store');
            Route::post('assign-by-range', [LeadController::class, 'assignByRange'])->name('lead.assign-by-range');
            Route::post('reminders', [LeadController::class, 'reminders'])->name('lead.reminders');
            Route::post('reminder/store', [LeadController::class, 'reminderStore'])->name('lead.reminder.store');
            Route::post('reminder/delete', [LeadController::class, 'deletereminder'])->name('lead.reminder.delete');
            Route::post('/message', [LeadController::class, 'message'])->name('lead.message');
            Route::post('message/send', [LeadController::class, 'messageSend'])->name('lead.message.send');
            // Beams Integration Crons
            Route::group(['middleware' => ['role:super_admin']], function () {
                Route::get('/store-leads-beams', [LeadController::class, 'leadStoreBeams'])->name('lead.store.beams');
                Route::get('/get-status-leads-beams', [LeadController::class, 'leadGetStatusBeams'])->name('lead.get.status.beams');
                Route::get('/get-fall-leads-beams', [LeadController::class, 'leadGetFallDataBeams'])->name('lead.get.fall.data.beams');
                Route::get('/get-visitors-leads-beams', [LeadController::class, 'leadGetVistorLeadBeams'])->name('lead.get.visitors.leads.beams');
            });
        });
        // Route for duplicate leads
        Route::get('list-duplicate-leads', [LeadController::class, 'listDuplicateLeads'])->name('leads.duplicate-leads');
        // Route for campaigns
        Route::prefix('campaign')->group(function () {
            Route::get('list', [CampaignController::class, 'index'])->name('campaign.index');
            Route::get('create', [CampaignController::class, 'create'])->name('campaign.create');
            Route::post('store', [CampaignController::class, 'store'])->name('campaign.store');
            Route::get('edit', [CampaignController::class, 'edit'])->name('campaign.edit');
            Route::post('recipients/show', [CampaignController::class, 'showRecipients'])->name('campaign.recipients.show');

            Route::prefix('templates')->group(function () {
                Route::get('/', [CampaignController::class, 'campaignTemplates'])->name('campaign.template.index');
                Route::get('/show/{slug}/{campaign_id?}', [CampaignController::class, 'showCampaignTemplate'])->name('campaign.template.show');
            });
        });
        //Route for whatsapp chat
        Route::get('/whatsapp/chat',  [\App\Http\Controllers\Backend\WhatsappChatController::class, 'index'])->name('whatsapp.chat.index');
        Route::post('/whatsapp/chat/messages',  [\App\Http\Controllers\Backend\WhatsappChatController::class, 'chat'])->name('whatsapp.chat.list');
        Route::post('/whatsapp/chat/send',  [\App\Http\Controllers\Backend\WhatsappChatController::class, 'send'])->name('whatsapp.chat.send');

        /*Out Reach Data Module*/
        Route::prefix('call-center')->group(function () {
            Route::prefix('outreach-data')->group(function () {
                Route::get('/list',  [OutReachDataController::class, 'index'])->name('outreach.index');
                Route::get('/create',  [OutReachDataController::class, 'create'])->name('outreach.create');
                Route::post('/store', [OutReachDataController::class, 'store'])->name('outreach.store');
                Route::get('/edit/{id}', [OutReachDataController::class, 'edit'])->name('outreach.edit');
                Route::post('/update', [OutReachDataController::class, 'update'])->name('outreach.update');
                Route::post('/delete', [OutReachDataController::class, 'destroy'])->name('outreach.delete');
                Route::post('/import-outreach', [OutReachDataController::class, 'importOutReachData'])->name('import.outreach');
            });
        });
    });
});
