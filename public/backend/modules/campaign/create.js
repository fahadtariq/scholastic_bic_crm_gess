$(document).ready(function () {

    $('#scheduled_date_time').flatpickr({
        enableTime: true,
        time_24hr: true,
        minDate: new Date()
    });

    if ($("#snow-editor").length) {
        var quill_snow;
        quill_snow = new Quill("#snow-editor", {
            modules: {
                toolbar: [
                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['blockquote', 'code-block'],

                    // custom button values
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                    [{ 'direction': 'rtl' }],                         // text direction

                    // [{ 'size': ['small', false, 'large', 'huge'] }],
                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                    [ 'link'],
                    [{ 'color': [] }, { 'background': [] }],
                    // [{ 'font': [] }],
                    [{ 'align': [] }],
                ],
            },
            theme: "snow",
        });
        quill_snow.on("text-change", function (delta, oldDelta, source) {
            $("#email_content").val(quill_snow.root.innerHTML);
        });
    }

})

$(document).on('click', '#is_scheduled', function () {
    if ($(this).val() == 1) {
        $('#schedule_date_wrapper').css('display', 'block')
        $('#scheduled_date_time').attr('required', true)
    } else {
        $('#scheduled_date_time').val('')
        $('#schedule_date_wrapper').css('display', 'none')
        $('#scheduled_date_time').attr('required', false)
    }
})

var fileInput = document.getElementById('email_list');

fileInput.addEventListener('change', function() {
    var file = fileInput.files[0];
    var allowedExtensions = /(\.xlsx|.csv)$/i;

    if (!allowedExtensions.test(file.name)) {
        $('#email_list').siblings('.invalid-tooltip').text('Please select an excel or csv file')
        $('#campaign-form').addClass('was-validated')
        fileInput.value = ''; // Reset the file input
    }
});

function updateEditorInput(el) {
    let editorContent = $(el).text()
    if (editorContent.length == 0) {
        $('#email_content').val('')
    }
}

function isSchedule(el) {
    let is_schedule = $(el).val()
    if (is_schedule == 1) {
        $('#scheduled_date_time').attr('required', true)
    } else {
        $('#scheduled_date_time').attr('required', false)
    }
}
