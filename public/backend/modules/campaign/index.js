$(document).ready(function () {
    var route = $("#ajaxRoute").val();
    $("#campaign-data-table").DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'All'],
        ],
        scrollX: true,
        language: {
            search: "",
            searchPlaceholder: "Search...",
        },
        ajax: route,
        columns: [
            {
                data: "id",
                name: "id",
                width: "5%",
            },
            {
                data: "title",
                name: "title",
            },
            {
                data: "email_template_id",
                name: "Email Template",
                width: "15%",
            },
            {
                data: "recipients",
                name: "Recipients",
                width: "15%",
            },
            {
                data: "status",
                name: "status",
                width: "15%",
            },
            {
                data: "scheduled_date_time",
                name: "schedule_at",
                width: "15%",
            },
            {
                data: "created_at",
                name: "created_at",
                width: "15%",
            },
            // {
            //     data: "action",
            //     name: "action",
            //     orderable: false,
            //     searchable: false,
            //     width: "5%",
            //     sClass: "text-center",
            // },
        ],
        order: [[0, 'desc']]
    });

})

function showRecipients(url) {
    $.ajax({
        type : 'POST',
        url : url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success : function (response) {
            let html = `<div class="d-flex flex-wrap gap-2">`
            $(response.data).each(function (i, email) {
                html += `<span class="badge badge-soft-primary badge-border">${email}</span>`
            })
            html += `</div>`;
            $('#recipients-body').html(html)
            $('.recipientsModal').modal('show')
        }
    })
}
