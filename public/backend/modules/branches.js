$(document).ready(function() {
    $('.select2').select2();

    let branch_list_url = $('#branch_list_url').val();
    $('#branches-table').dataTable({
        searching: true,
        processing: true,
        serverSide: true,
        responsive: true,
        ordering: true,
        bLengthChange: true,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'All'],
        ],
        scrollX: true,
        language: {search: "", searchPlaceholder: "Search..."},
        ajax:
            {
                url: branch_list_url,
            },
        columns: [
            {   data: 'id', name: 'id'    },
            {   data: 'name', name: 'name'  },
            {   data: 'status', name: 'status'  },
            {   data: 'action', name: 'action', orderable: false, searchable: false,  width: "5%",  sClass: 'text-center'   }
        ]
    });
});

/**
 * @description function to load states according to selected country
 * */
$(document).on('change', '.countries', function () {
    let country_id = $(this).val();
    let url = $(this).attr('data-url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type : 'POST',
        url : url,
        data : {
            country_id
        },
        success:function (response) {
            $('.states').html(response);
        }
    })
})

/**
 * @description function to load cities according to selected state
 */
$(document).on('change', '.states', function () {
    let country_id = $(this).find(':selected').attr('data-country-id');
    let state_id = $(this).val();
    let url = $(this).attr('data-url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type : 'POST',
        url : url,
        data : {
            country_id,
            state_id
        },
        success:function (response) {
            $('.cities').html(response);
        }
    })
})

/**
 * @description function to delete branch
 */
$(document).on('click', '.delete-branch', function () {
    let id = $(this).attr('data-id');
    let url = $(this).attr('data-url');

    Swal.fire({
        html: '<div class="mt-3">' +
            '<lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>' +
            '<div class="mt-4 pt-2 fs-15 mx-5">' +
            '<h4>Are you sure?</h4>' +
            '<p class="text-muted mx-4 mb-0">Are you Sure You want to Delete this Record ?</p>' +
            '</div>' +
            '</div>',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-primary w-xs me-2 mb-1',
        confirmButtonText: 'Yes, Delete It!',
        cancelButtonClass: 'btn btn-danger w-xs mb-1',
        buttonsStyling: false,
        showCloseButton: true
    }).then(function(result) {

        if (result.isConfirmed) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : 'POST',
                url : url,
                data : {
                    id
                },
                success:function () {
                    Swal.fire({
                        icon: 'success',
                        title: 'Done',
                        text: 'Branch removed successfully',
                        allowOutsideClick: false
                    })
                    $('#branches-table').DataTable().ajax.reload(null, false);
                }
            })
        }
    });
});

$(document).on('click', '.branch_targets-btn', function () {
    let url = $(this).attr('data-route');
    let id = $(this).attr('data-branch-id');
    let edit_target_route = $(this).attr('data-edit-target-route');
    let target_default_route = $(this).attr('data-target-default-route');

    $.ajax({
        type : 'POST',
        url : url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            id
        },
        success:function (response) {
            appendBranchTargetListings(response, edit_target_route,target_default_route);
            $('.branch-target-form').append("<input type='hidden' name='branch_id' value='" + id + "'>");
            $('#branchTargetsModal').modal('show');
        }
    })

});

$(document).on('submit', '.branch-target-form', function (e) {
    let form = $(this);
    let url = form.attr('data-route');
    let edit_target_route = form.attr('data-edit-target-route');
    let target_default_route = $(this).attr('data-target-default-route');

    $.ajax({
        type : 'POST',
        url : url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : form.serialize(),
        success:function (response) {
            appendBranchTargetListings(response, edit_target_route,target_default_route);
            form.removeClass('was-validated');
            $('.clear_target_btn').click();
        },
        error:function (response) {
            let errors = response.responseJSON.errors;
            $.each(errors, function(i) {
                $('.' + i).addClass('is-invalid');
            })
        }
    })
})

// $(document).on('click', '.delete-branch-target', function (e) {
//     e.preventDefault();
//     var url = $(this).attr('data-route');
//     var edit_target_route = $(this).attr('data-route');
//     var rowID = $(this).data('rowid');
//
//     Swal.fire({
//         html: '<div class="mt-3">' +
//             '<lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>' +
//             '<div class="mt-4 pt-2 fs-15 mx-5">' +
//             '<h4>Are you sure?</h4>' +
//             '<p class="text-muted mx-4 mb-0">Are you Sure You want to Delete this Record ?</p>' +
//             '</div>' +
//             '</div>',
//         showCancelButton: true,
//         confirmButtonClass: 'btn btn-primary w-xs me-2 mb-1',
//         confirmButtonText: 'Yes, Delete It!',
//         cancelButtonClass: 'btn btn-danger w-xs mb-1',
//         buttonsStyling: false,
//         showCloseButton: true
//     }).then(function(result) {
//
//         if (result.isConfirmed) {
//
//             $.ajax({
//
//                 url: url,
//                 type: "POST",
//                 data: {
//                     'id': rowID
//                 },
//                 headers: {
//                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                 },
//                 cache: false,
//                 success: function(response) {
//                     appendBranchTargetListings(response, edit_target_route);
//                 }
//             });
//         }
//     });
//
// })

$(document).on('click', '.edit-branch-target', function (e) {
    e.preventDefault();
    var url = $(this).attr('data-route');
    // var edit_target_route = $(this).attr('data-route');
    var rowID = $(this).data('rowid');


    $.ajax({

        url: url,
        type: "POST",
        data: {
            'id': rowID
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache: false,
        success: function(response) {
            $('.branch-target-form').find('#name').val(response.name);
            $('.branch-target-form').find('#target').val(response.target);
            $('.branch-target-form').find('#date_range').flatpickr({
                mode: "range",
                defaultDate: [response.from, response.to]
            });
            $('.branch-target-form').find('#target_id').val(rowID);
            $('.target_submit_btn').text('Save');
        }
    });
})

function appendBranchTargetListings(response, edit_target_route,target_default_route) {
    let branch_target_listings = ``;
    $.each(response, function (i, row) {
        branch_target_listings += `<tr><td>${row.name}</td><td>${row.from}</td><td>${row.to}</td><td>${row.target}</td>
                                            <td>
                                            <a href="javascript:;" data-route="${edit_target_route}" data-rowid="${row.id}"
                                            class="btn btn-sm btn-primary btn-icon waves-effect waves-light edit-branch-target">
                                            <i class="ri-pencil-line"></i>
                                            </a>
                                           <a href="javascript:;" class="form-check form-switch form-switch-md mt-1 float-start" dir="ltr">
                                           <input type="checkbox" data-url="${target_default_route}" data-rowid="${row.id}"
                                           class="form-check-input change-status" ${row.is_default == 1 ? 'checked' : ''} >
                                           </a>
                                           </td>
                                           </tr>`;
    })
    $('#branch_targets_listing').html(branch_target_listings);
}

if($('#date_range').length > 0) {
    $('#date_range').flatpickr({
        mode: "range"
    });
}

$(document).on('click', '.clear_target_btn', function () {
    $('.branch-target-form')[0].reset();
    $('.branch-target-form').find('#target_id').val('');
    $('.target_submit_btn').text('Add');
})

$(document).on('click', '.save_course_target_btn', function () {
    let branch_id = $(this).attr('data-branch-id');
    let course_id = $(this).attr('data-course-id');
    let session_id = $('.session_select').val();
    let url = $(this).attr('data-route');
    let target = $(this).siblings('#course_target').val();

    if (target >= 0 && target != '') {
        $.ajax({
            url: url,
            type: "POST",
            data: {
                branch_id,
                course_id,
                session_id,
                target
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            success: function() {

            }
        });
    }
})

$(document).on('change', '.session_select', function () {
    let branch_id = $(this).attr('data-branch-id');
    let session_id = $(this).val();
    let url = $(this).attr('data-route');

    $.ajax({
        url: url,
        type: "POST",
        data: {
            branch_id,
            session_id,
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache: false,
        success: function(response) {
            $('#session-course-wrapper').html(response);
        }
    });
})

$(document).on('click', '.change-status', function () {
    $('.change-status').not(this).prop('checked', false);
    let is_active = $(this).is(':checked') ? 1 : 0;
    var url = $(this).data('url');
    var rowID = $(this).data('rowid');
    $.ajax({
        url: url,
        type: "POST",
        data: {
            'id': rowID,
            is_active
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
        },
        cache: false,
        success: function (data) {
        }
    });
});
