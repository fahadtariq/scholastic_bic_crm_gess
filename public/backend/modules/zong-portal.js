$(document).ready(function () {
    var startDate = new Date();
    var endDate = new Date();
    endDate.setDate(endDate.getDate() - 7);
    $('#date_range').flatpickr({
        mode: "range",
        defaultDate: [startDate, endDate]
    });
    $('#date_range_live').flatpickr({
        mode: "range",
    });

    $('.custom-select2').select2();
});

function changeType(){
    val = $('#call_type').val();
    if(val == 1){
        $('.incoming').css('display', 'block');
        $('.outgoing').css('display', 'none');
    }
    else{
        $('.incoming').css('display', 'none');
        $('.outgoing').css('display', 'block');
    }
}

function initializeDatatable() {
    let scrollX = $('#zong-data-table').attr("data-scrollX");
    if (scrollX == '0') {
        scrollX = false;
    } else {
        scrollX = true;
    }
    $("#zong-data-table").DataTable({
        lengthChange: false,
        ordering: false,
        searching: false,
        responsive: true,
        scrollX: scrollX,
        paging: false,
        bInfo: false
    });
}

function destroyDatatable() {
    $("#zong-data-table").DataTable().destroy();
}

initializeDatatable();

function filterDataTable(url) {
    var current_url = document.location.href;
    var params = current_url.substring(current_url.indexOf('?') + 1);
    if(params.includes('page=')){
        url += `?${params}`;
    }
    $.ajax({
        url: url,
        data: $('#filter-form').serialize(),
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            $('#custom-datatable-wrapper').html(data.view);
            $('#custom-pagination-wrapper').html(data.pagination);
            $('#dbCounter').text(`(Count: ${data.dbCounter})`);
            destroyDatatable();
            initializeDatatable();
            window.history.pushState("", "", url);
        }
    });
}

$(document).on('click', '.page-link', function(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    if (url == 'javascript: void(0);') {
        return;
    }
    var type = e.target.getAttribute("data-type");
    var module = e.target.getAttribute("data-module");
    getRecords(url, type, module);
    window.history.pushState("", "", url);
});

function getRecords(url, type, module) {
    $.ajax({
        url: url,
    }).done(function(data) {
        $('#custom-datatable-wrapper').html(data.view);
        $('#custom-pagination-wrapper').html(data.pagination);
        destroyDatatable();
        initializeDatatable();
    }).fail(function() {});
}
