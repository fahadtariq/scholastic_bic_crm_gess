$(document).ready(function () {

    if ($("#snow-editor").length) {
        var quill_snow;
        quill_snow = new Quill("#snow-editor", {
            modules: {
                toolbar: [
                    [{ header: [1, 2, 3, 4, 5, 6, false] }],
                    ["bold", "italic", "underline", "strike"],
                    ["code-block"],
                    ["link"],
                    [{ script: "sub" }, { script: "super" }],
                    [{ list: "ordered" }, { list: "bullet" }],
                    ["clean"],
                ],
            },
            theme: "snow",
        });
        quill_snow.on("text-change", function(delta, oldDelta, source) {
            $("#remarks").val(quill_snow.root.innerHTML);
        });
    }

    $('#received_date').flatpickr({
        maxDate : new Date()
    });

    $('#year').flatpickr({
        enableDate : true,
        dateFormat : "Y"
    });

    $('#date').flatpickr({
        mode : "range"
    })
})

function filterDataTable(url) {
    var current_url = document.location.href;
    var params = current_url.substring(current_url.indexOf('?') + 1);
    if(params.includes('page=')){
        url += `?${params}`;
    }
    $.ajax({
        url: url,
        data: $('#filter-form').serialize(),
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            $('#custom-datatable-wrapper').html(data.view);
            $('#custom-pagination-wrapper').html(data.pagination);
            $('#dbCounter').text(`(Count: ${data.dbCounter})`);
            destroyDatatable();
            initializeDatatable();
            window.history.pushState("", "", url);
        }
    });
}

function destroyDatatable() {
    $("#outreach-data-table").DataTable().destroy();
}

initializeDatatable();

function initializeDatatable() {
    let scrollX = $('#outreach-data-table').attr("data-scrollX");
    if (scrollX == '0') {
        scrollX = false;
    } else {
        scrollX = true;
    }
    $("#outreach-data-table").DataTable({
        lengthChange: false,
        ordering: false,
        searching: false,
        responsive: true,
        scrollX: scrollX,
        paging: false,
        bInfo: false,
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ]
    });
}

$(document).on('click', '.delete-outreach', function(e) {
    e.preventDefault();

    var url = $(this).attr('href');
    var rowID = $(this).data('rowid');

    Swal.fire({
        html: '<div class="mt-3">' +
            '<lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>' +
            '<div class="mt-4 pt-2 fs-15 mx-5">' +
            '<h4>Are you sure?</h4>' +
            '<p class="text-muted mx-4 mb-0">Are you Sure You want to Delete this Record ?</p>' +
            '</div>' +
            '</div>',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-primary w-xs me-2 mb-1',
        confirmButtonText: 'Yes, Delete It!',
        cancelButtonClass: 'btn btn-danger w-xs mb-1',
        buttonsStyling: false,
        showCloseButton: true
    }).then(function(result) {

        if (result.isConfirmed) {

            $.ajax({

                url: url,
                type: "POST",
                data: {
                    'id': rowID
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,
                success: function() {
                    $('.filter-outreach-table-btn').click();
                }
            });
        }
    });
});
