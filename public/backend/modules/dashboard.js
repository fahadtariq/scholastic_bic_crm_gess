$(document).ready(function () {
    loadContent();
});

function loadContent() {
    let url = $('#dashboard_content_url').val();
    let branch_id = $('#branch_id').val();
    let session_id = $('.session_filter').val();

    $.ajax({
        type : 'POST',
        url : url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            branch_id,
            session_id
        },
        success:function (response) {
            $('#load-content').html(response);
        }
    })
}

$(document).on('change', '#branch_id, .session_filter', function () {
    loadContent();
})

$('#branch_id').select2();

$(document).on('change', '.branch_filter', function () {
    let branch_id = $(this).val();
    let url = $(this).attr('data-session-route');

    if (branch_id.length == 1) {
        $.ajax({
            url: url,
            data: {
                branch_id
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
            },
            type: 'POST',
            success: function (response) {
                let options = ``;
                $.each(response, function (i, val) {
                    options += `<option value="${val.id}">${val.name}</option>`;
                })
                let html = `<div class="form-label-group in-border">
                                <select class="session_filter form-select custom-select2" name="session_id" placeholder="Session">
                                <option value="">Select Session</option>
                                    ${options}
                                </select>
                                <label for="branch_ids" class="form-label">Session</label>
                            </div>`;
                $('.session_select').html(html);
            }
        });
    } else {
        $('.session_select').html(``);
    }
})
