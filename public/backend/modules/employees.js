$(document).ready(function() {
    let employees_list_url = $('#employees_list_url').val();

    $.extend($.fn.dataTableExt.oStdClasses, {
        "sFilterInput": "form-control",
        "sLengthSelect": "form-control"
    });

    $('#employees-table').dataTable({
        searching: false,
        processing: true,
        serverSide: true,
        responsive: true,
        ordering: true,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'All'],
        ],
        scrollX: true,
        language: {search: "", searchPlaceholder: "Search..."},
        ajax:
            {
                url: employees_list_url,
                data: function ( d ) {
                    d.branch_id = $('#branch_id').val();
                    d.role_id = $('#role_id').val();
                    d.mySearch = $('#mySearch').val();
                }
            },
        columns: [
            {   data: 'branch', name: 'branch'    },
            {   data: 'first_name', name: 'first_name'  },
            {   data: 'last_name', name: 'last_name'  },
            {   data: 'email', name: 'email'  },
            {   data: 'role', name: 'role'  },
            {   data: 'status', name: 'status'  },
            {   data: 'action', name: 'action', orderable: false, searchable: false,  width: "5%",  sClass: 'text-center'   }
        ]
    });
});

$(document).on('change', '.filter', function() {
    $('#employees-table').DataTable().ajax.reload(null, false);
});

$(document).on("keyup",'#mySearch', function() {
    var value = $(this).val().toLowerCase();
    if(value.length > 0 || value.length == 0){
        $('#employees-table').DataTable().search($(this).val()).draw() ;
    }
});
