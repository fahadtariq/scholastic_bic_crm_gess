$(document).ready(function () {
    /*$('#admission_date').flatpickr({
        enable: [new Date()],
        defaultDate: new Date(),
    });*/

    $('#admission_date').val(new Date().toJSON().slice(0, 10));

    $('.select2').select2();

    const omit_promotional_admission = $('#omit_promotional_admission').val();

    $('.select2').select2();

    const promotional_admissions_url = $('#ajaxRoute').val();
    $('#promotional-admissions_table').dataTable({
        searching: true,
        processing: true,
        serverSide: true,
        responsive: true,
        ordering: true,
        bLengthChange: true,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'All'],
        ],
        scrollX: true,
        language: {search: "", searchPlaceholder: "Search..."},
        ajax:
            {
                url: promotional_admissions_url,
                data: function (d) {
                    d.omit_promotional_admission = omit_promotional_admission;
                }
            },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'branch.name', name: 'branch.name'},
            {data: 'no_of_promotional_admissions', name: 'no_of_promotional_admissions'},
            {data: 'admission_date', name: 'admission_date'},
            // {
            //     data: 'is_add_to_paid_lead',
            //     name: 'is_add_to_paid_lead',
            //     render: function (data, type, row) {
            //         if (row.is_add_to_paid_lead == 1) {
            //             return 'Yes';
            //             return '<span class="badge badge-success">Yes</span>';
            //         } else {
            //             return 'No';
            //             return '<span class="badge badge-danger">No</span>';
            //         }
            //     },
            // },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                width: "5%",
                sClass: 'text-center'
            }
        ]
    });

    $('.promotional_admission_form').on('submit', promotionalAdmissionHandler);

    function promotionalAdmissionHandler(e) {
        //validate the admission date
        e.preventDefault();
        let is_validated = validateAdmissionDate();
        if (is_validated) {
            return false;
        }
        $(this).off().submit();
    }

    const admissions_date = $('#admission_date');

    function validateAdmissionDate() {
        //validate the admission date
        if (admissions_date.val() == '') {
            admissions_date.addClass('is-invalid');
            return true;
        } else {
            admissions_date.removeClass('is-invalid');
            return false;
        }
    }

});
