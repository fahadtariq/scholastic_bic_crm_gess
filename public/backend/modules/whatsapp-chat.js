$(document).on('click', '.chat_people', function (e) {

    let form = $(this);

    let chat_id = form.attr('data-id');
    let url = $('#chat_history_route').val();
    //Send whatsapp message
    sendWhatsappChatRequest(url, chat_id);
});

$(document).on('submit', '.whatsapp-full-chat-form', function (e) {
    let form = $(this);
    let url = form.attr('data-route');

    $.ajax({
        type : 'POST',
        url : url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : form.serialize(),
        beforeSend: function() {
            //$('.lead-send-error-message-container').html('');
        },
        success:function (response) {
            $(".whatsapp_messages_history_container").animate({ scrollTop: $(".whatsapp_messages_history_container")[0].scrollHeight }, 1000);


            form[0].reset();
            //Refresh chat window to get updated chat
            $(".chat_people").trigger("click");
        },
        error:function (response) {
            //console.log(response.responseJSON);
            //$('.lead-send-error-message-container').html('<div class="alert alert-danger" role="alert">'+response.responseJSON.message+'</div>');
        }
    });

    // Get the <div> element by its ID
    var chatContainer = $('.msg_history');

    // Calculate the maximum scroll height
    var maxScrollHeight = chatContainer.prop('scrollHeight') - chatContainer.innerHeight();

    // Scroll the div to the bottom with animation
    chatContainer.animate({ scrollTop: maxScrollHeight }, 500); // Adjust the animation speed (500 milliseconds in this example)
});

$(document).on('click', '.whatsapp-chat-refresh', function (e) {
    //$(".chat_people").trigger("click");

    let chat_id = $('#chat_id').val();
    let url = $('#chat_history_route').val();
    //Send whatsapp message
    sendWhatsappChatRequest(url, chat_id);
});

function sendWhatsappChatRequest(url, chat_id){
    $.ajax({
        type : 'POST',
        url : url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            chat_id: chat_id
        },
        beforeSend: function() {
            //$('.lead-send-error-message-container').html('');
        },
        success:function (response) {

            $('.whatsapp_messages_history_container').html(response);

            // Get the <div> element by its ID
            var chatContainer = $('.msg_history');

            // Calculate the maximum scroll height
            var maxScrollHeight = chatContainer.prop('scrollHeight') - chatContainer.innerHeight();

            // Scroll the div to the bottom with animation
            chatContainer.animate({ scrollTop: maxScrollHeight }, 500); // Adjust the animation speed (500 milliseconds in this example)
        },
        error:function (response) {
            //console.log(response.responseJSON);
            $('.msg_history').html('<div class="alert alert-danger" role="alert">Error getting chat history.</div>');
        }
    });
}

