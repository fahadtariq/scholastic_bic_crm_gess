$(document).ready(function() {
    let tag_list_url = $('#tag_list_url').val();
    $('#tags-table').dataTable({
        searching: true,
        processing: true,
        serverSide: true,
        responsive: true,
        ordering: true,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'All'],
        ],
        scrollX: true,
        language: {search: "", searchPlaceholder: "Search..."},
        ajax:
            {
                url: tag_list_url,
            },
        columns: [
            {   data: 'id', name: 'id'    },
            {   data: 'name', name: 'name'  },
            {   data: 'abbreviation', name: 'abbreviation'  },
            {   data: 'status', name: 'status'  },
            {   data: 'action', name: 'action', orderable: false, searchable: false,  width: "5%",  sClass: 'text-center'   }
        ]
    });
});
