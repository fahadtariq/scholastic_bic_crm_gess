$(document).ready(function () {
    $('.select2').select2();

    let defaultDate = null;
    defaultDate = $('#review_date').data('old-value') == '' ? new Date() : $('#review_date').data('old-value');

    $('#review_date').flatpickr({
        defaultDate: defaultDate,
    });

    let auditor_review_list_url = $('#auditor-review_list_url').val();

    $('#auditor-review-table').dataTable({
        searching: true,
        processing: true,
        serverSide: true,
        responsive: true,
        ordering: true,
        bLengthChange: true,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'All'],
        ],
        scrollX: true,
        language: {search: "", searchPlaceholder: "Search..."},
        ajax:
            {
                url: auditor_review_list_url,
            },
        columns: [
            {
                data: 'id',
                name: 'id'
            },
            {
                data: 'user.name',
                name: 'user.name'
            },
            {
                data: 'review_date',
                name: 'review_date',
            },
            {
                data: 'lead_count_acc',
                name: 'lead_count_acc',
                render:  function render(data) {
                    return data ? 'Yes' : 'No';
                },
            },
            {
                data: 'lead_assignment_acc',
                name: 'lead_assignment_acc',
                render: function render(data) {
                    return data ? 'Yes' : 'No';
                },
            },
            {
                data: 'review_acc',
                name: 'review_acc',
                render: function render(data) {
                    return data ? 'Yes' : 'No';
                },
            },
            {
                data: 'admission_acc',
                name: 'admission_acc',
                render: function render(data) {
                    return data ? 'Yes' : 'No';
                },
            },
            {
                data: 'outbound_call_acc',
                name: 'outbound_call_acc',
                render: function render(data) {
                    return data ? 'Yes' : 'No';
                },
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
                width: "5%",
                sClass: 'text-center'
            }
        ]
    });

});
