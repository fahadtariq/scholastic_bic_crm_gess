$(document).ready(function () {
    var route = $("#ajaxRoute").val();
    $("#country-data-table").DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'All'],
        ],
        scrollX: true,
        language: {
            search: "",
            searchPlaceholder: "Search...",
        },
        ajax: route,
        columns: [
            {
                data: "id",
                name: "id",
                width: "5%",
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "abbreviation",
                name: "abbreviation",
                width: "15%",
            },
            {
                data: "status",
                name: "status",
                width: "15%",
            },
            {
                data: "created_at",
                name: "created_at",
                width: "15%",
            },
            {
                data: "action",
                name: "action",
                orderable: false,
                searchable: false,
                width: "5%",
                sClass: "text-center",
            },
        ],
    });
});
