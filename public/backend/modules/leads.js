$(document).ready(function() {

    let date_after_tomorrow = new Date()
    date_after_tomorrow.setDate(date_after_tomorrow.getDate() + 2)
    date_after_tomorrow = date_after_tomorrow.getFullYear() + '-' + (date_after_tomorrow.getMonth() + 1) + '-' + date_after_tomorrow.getDate()

    if ($('#create_lead_page').length <= 0) {
        date_after_tomorrow = ''
    }
    $('#date_of_birth').flatpickr();
    $('#follow_up_date').flatpickr({
        enableTime: true,
        time_24hr: true,
        defaultDate: date_after_tomorrow
    });
    $('#date_range').flatpickr({
        mode: "range"
    });

    $('#follow_up_by_employee_date').flatpickr();

    $('.courses').select2();


    if ($("#snow-editor").length) {
        var quill_snow;
        quill_snow = new Quill("#snow-editor", {
            modules: {
                toolbar: [
                    [{ header: [1, 2, 3, 4, 5, 6, false] }],
                    ["bold", "italic", "underline", "strike"],
                    ["code-block"],
                    ["link"],
                    [{ script: "sub" }, { script: "super" }],
                    [{ list: "ordered" }, { list: "bullet" }],
                    ["clean"],
                ],
            },
            theme: "snow",
        });
        quill_snow.on("text-change", function(delta, oldDelta, source) {
            $("#follow_up_remarks").val(quill_snow.root.innerHTML);
        });
    }
})

function initializeDatatable() {
    let scrollX = $('#leads-data-table').attr("data-scrollX");
    if (scrollX == '0') {
        scrollX = false;
    } else {
        scrollX = true;
    }
    $("#leads-data-table").DataTable({
        lengthChange: false,
        ordering: false,
        searching: false,
        responsive: true,
        scrollX: scrollX,
        paging: false,
        bInfo: false,
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ]
    });
}

function destroyDatatable() {
    $("#leads-data-table").DataTable().destroy();
}

initializeDatatable();

function filterDataTable(url) {
    var current_url = document.location.href;
    var params = current_url.substring(current_url.indexOf('?') + 1);
    if(params.includes('page=')){
        url += `?${params}`;
    }
    $.ajax({
        url: url,
        data: $('#filter-form').serialize(),
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            $('#custom-datatable-wrapper').html(data.view);
            $('#custom-pagination-wrapper').html(data.pagination);
            $('#dbCounter').text(`(Count: ${data.dbCounter})`);
            destroyDatatable();
            initializeDatatable();
            window.history.pushState("", "", url);
        }
    });
}

$(document).on('click', '.page-link', function(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    if (url == 'javascript: void(0);') {
        return;
    }
    var type = e.target.getAttribute("data-type");
    var module = e.target.getAttribute("data-module");
    getRecords(url, type, module);
    window.history.pushState("", "", url);
});

function getRecords(url, type, module) {
    $.ajax({
        url: url,
    }).done(function(data) {
        $('#custom-datatable-wrapper').html(data.view);
        $('#custom-pagination-wrapper').html(data.pagination);
        destroyDatatable();
        initializeDatatable();
    }).fail(function() {});
}

// $(document).on('change', '.filter', function() {
//     $('.filter-lead-table-btn').click();
// });
//
// $(document).on("keyup", '#mySearch', function() {
//     var value = $(this).val().toLowerCase();
//     if (value.length > 0 || value.length == 0) {
//         $('#leads-data-table').DataTable().search($(this).val()).draw();
//     }
// });


$(document).on('change', '#branch_id', function() {
    let id = $(this).val();
    let courses_url = $(this).attr('data-route-courses');
    let employees_url = $(this).attr('data-route-employees');
    let session_url = $(this).attr('data-route-sessions');
    let data_filter = $(this).attr('data-filter');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: courses_url,
        data: {
            id
        },
        success: function(response) {
            $('.courses').html(response);
        }
    })

    $.ajax({
        type: 'POST',
        url: employees_url,
        data: {
            id
        },
        success: function(response) {
            $('.employees').html(response);
            if ($('#create_lead_page').length) {
                appendEmployeeName()
            }
        }
    })

    $.ajax({
        type: 'POST',
        url: session_url,
        data: {
            branch_id:id,
            data_filter:data_filter
        },
        success: function(response) {
            let options = ``;
            $.each(response, function (i, val) {
                options += `<option value="${val.id}">${val.name}</option>`;
            })
            let html = `<div class="form-label-group in-border">
                                <select class="session_filter form-select custom-select2" name="session_id" placeholder="Session">
                                <option value="">Select Session</option>
                                    ${options}
                                </select>
                                <label for="branch_ids" class="form-label">Session</label>
                            </div>`;
            $('.session_select').html(html);
        }
    })
})

function appendEmployeeName() {
    let employee_name = $('.employees option:selected').text().split("--");
    $('#follow_up_by').val(employee_name[0])
}

$(document).on('change', '.is_referred', function() {
    if ($(this).val() == 1) {
        $('.referral-input').css('display', 'inline-block');
    } else {
        $('.referral-input').css('display', 'none');
    }
})

$(document).on('click', '.show-lead', function() {
    let id = $(this).attr('data-id');
    let url = $(this).attr('data-route');


    $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            id
        },
        success: function(response) {
            $('#lead-modal-content').html(response);
            $('#leadInfoModal').modal('show');
        }
    })

});

$(document).on('click', '.show-lead-email', function() {
    let id = $(this).attr('data-id');
    let url = $(this).attr('data-route');
    let type = $(this).attr('data-type');

    $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            id: id,
            type: type
        },
        success: function(response) {
            $('#lead-modal-content').html(response);
            $('#leadInfoModal').modal('show');
        }
    })

});

$(document).on('click', '.change-info', function() {
    let id = $('#lead_id').val();
    let url = $(this).attr('data-route');
    let action = $(this).attr('data-action');

    $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            id,
            action
        },
        success: function(response) {
            $('.change-info-modal-area').html(response);
            $('#changeInfoModal').modal('show');
        }
    })
});

$(document).on('submit', '.change-info-form', function(e) {
    e.preventDefault();
    let form = $(this);
    let url = form.attr('data-route');

    Swal.fire({
        html: '<div class="mt-3">' +
            '<lord-icon\n' +
            '    src="https://cdn.lordicon.com/nxaaasqe.json"\n' +
            '    trigger="loop"\n' +
            '    style="width:250px;height:250px">\n' +
            '</lord-icon>' +
            '<div class="mt-4 pt-2 fs-15 mx-5">' +
            '<h4>Are you sure?</h4>' +
            '<p class="text-muted mx-4 mb-0">Are you Sure You want to perform this action to lead?</p>' +
            '</div>' +
            '</div>',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-primary w-xs me-2 mb-1',
        confirmButtonText: 'Yes, Perform It!',
        cancelButtonClass: 'btn btn-danger w-xs mb-1',
        buttonsStyling: false,
        showCloseButton: true
    }).then(function(result) {

        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: form.serialize(),
                success: function(res) {
                    let lead_id = $('#lead_id').val();
                    let lead_info_url = $('#lead_info_url').val();
                    $.ajax({
                        type: 'POST',
                        url: lead_info_url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: lead_id
                        },
                        success: function(response) {
                            $('#lead-modal-content').html(response);
                            $('.filter-lead-table-btn').click();
                        }
                    })

                    if (res.close) {
                        $('#leadInfoModal').modal('toggle');
                    }
                }
            })
        }
        $('#changeInfoModal').modal('toggle');
    })
});

$(document).on('click', '.add-follow-up', function() {
    let lead_id = $('#lead_id').val();
    let url = $(this).attr('data-route');

    $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            id: lead_id
        },
        success: function(response) {
            $('.add-follow-up-modal-area').html(response);
            // $('.filter-lead-table-btn').click();
            $('#addFollowUpModal').modal('show');
        }
    })
})

$(document).on('submit', '.store-lead-follow-up-form', function() {
    let form = $(this);
    let url = form.attr('data-route');

    $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: form.serialize(),
        success: function(response) {
            let lead_id = $('#lead_id').val();
            let lead_info_url = $('#lead_info_url').val();
            $.ajax({
                type: 'POST',
                url: lead_info_url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    id: lead_id
                },
                success: function(response) {
                    $('#lead-modal-content').html(response);
                    $('.filter-lead-table-btn').click();
                    $('#addFollowUpModal').modal('toggle');
                }
            })
        },
        error: function(response) {
            let errors = response.responseJSON.errors;
            $.each(errors, function(i) {
                $('.' + i).addClass('is-invalid');
            })

        }
    })

})

$(document).on('click', '.lead-check-parent', function() {
    let el = $(this);
    if (el.is(':checked')) {
        $('#leads-data-table').find('.lead-check-child').prop('checked', true);
    } else {
        $('#leads-data-table').find('.lead-check-child').prop('checked', false);
    }
})

$(document).on('click', '.lead-check-child, .lead-check-parent', function() {
    let checked = false;
    $('.lead-check-child').each(function(i, el) {
        if ($(el).is(':checked')) {
            checked = true;
            return false;
        }
    });

    if (checked) {
        let url = $('.lead-check-parent').attr('data-route');
        $.ajax({
            type: 'POST',
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                $('.loaded-employees').html(response);
                $('.employees_assigned_wrapper').css('display', 'block');
            }
        });

    } else {
        $('.lead-check-parent').prop('checked', false);
        $('.loaded-employees').html('');
        $('.employees_assigned_wrapper').css('display', 'none');
    }
})

$(document).on('change', '#assigned_employee_id', function() {
    let employee_id = $(this).val();
    let lead_ids = [];
    let url = $('.lead-check-parent').attr('data-assign-lead-route');

    if (employee_id != null && employee_id != '') {

        Swal.fire({
            html: '<div class="mt-3">' +
                '<lord-icon\n' +
                '    src="https://cdn.lordicon.com/nxaaasqe.json"\n' +
                '    trigger="loop"\n' +
                '    style="width:250px;height:250px">\n' +
                '</lord-icon>' +
                '<div class="mt-4 pt-2 fs-15 mx-5">' +
                '<h4>Are you sure?</h4>' +
                '<p class="text-muted mx-4 mb-0">Are you Sure You want to assign leads to this employee ?</p>' +
                '</div>' +
                '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-primary w-xs me-2 mb-1',
            confirmButtonText: 'Yes, Assign It!',
            cancelButtonClass: 'btn btn-danger w-xs mb-1',
            buttonsStyling: false,
            showCloseButton: true
        }).then(function(result) {

            if (result.isConfirmed) {

                $('.lead-check-child').each(function(i, el) {
                    if ($(el).is(':checked')) {
                        lead_ids.push($(el).attr('data-lead-id'));
                    }
                });


                $.ajax({

                    url: url,
                    type: "POST",
                    data: {
                        employee_id,
                        lead_ids
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    cache: false,
                    success: function() {
                        $('.filter-lead-table-btn').click();
                        Swal.fire({
                            'icon': 'success',
                            'text': 'Assigned Successfully!'
                        })
                    }
                });
            }

            $('.employees_assigned_wrapper').css('display', 'none');
            $('.lead-check-parent, .lead-check-child').prop('checked', false);
        });
    }
});

$(document).on('click', '.import-leads-btn', function() {
    $('.import-leads-form').removeClass('was-validated');
    $('#importLeadsModal').modal('show');
    $('.import-leads-form')[0].reset();
    $('.error-row').remove();
});

$(document).on('click', '.assign-by-range-btn', function() {

    $('.assign-by-range-leads-form').removeClass('was-validated');
    $('#assignByRangeLeadsModal').modal('show');
    $('.assign-by-range-leads-form')[0].reset();
    $('.error-row').remove();
});

$(document).on('change', '.change-status', function () {
    let val = $(this).val();
    if (val == 4 || val == 7) {
        $('.remarks-box').css('display', 'none');
        $('.remarks-textarea').prop('disabled', true);
        $('.reason-dropdown').removeAttr('disabled');
        $('.reason-dropdown-area').css('display', 'block');
    } else {
        $('.remarks-box').css('display', 'block');
        $('.remarks-textarea').prop('disabled', false);
        $('.reason-dropdown').attr('disabled');
        $('.reason-dropdown-area').css('display', 'none');
    }
});

$(document).on('keyup', '#min', function () {
    let val = $(this).val();
    let max = $('#max');
    if (val > 0) {
        max.prop('disabled', false);
        max.attr('min', val);
        if (max.val() < val) {
            max.val('');
        }
    } else {
        max.prop('disabled', true);
    }
});

$(document).on('submit', '.assign-by-range-leads-form', function() {
    let url = $(this).attr('data-route');
    $.ajax({

        url: url,
        type: "POST",
        data: $(this).serialize(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache: false,
        success: function(data) {
            if (data.errors != undefined && data.errors.length > 0) {
                $('.append-errors').css('display', 'block');
                $.each(data.errors, function(key, value){
                    console.log(value);
                    $('.append-errors').append('<p>'+value+'</p>');
                });
            } else {
                $('#assignByRangeLeadsModal').modal('hide');
                Swal.fire({
                    'icon': 'success',
                    'text': 'Assigned Successfully!'
                })
                $('.filter-lead-table-btn').click();

            }
            $("button[type='submit']").prop('disabled', false);
        }
    });
});

$(document).on('click', '.delete-lead', function(e) {
    e.preventDefault();

    var url = $(this).attr('href');
    var rowID = $(this).data('rowid');

    Swal.fire({
        html: '<div class="mt-3">' +
            '<lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>' +
            '<div class="mt-4 pt-2 fs-15 mx-5">' +
            '<h4>Are you sure?</h4>' +
            '<p class="text-muted mx-4 mb-0">Are you Sure You want to Delete this Record ?</p>' +
            '</div>' +
            '</div>',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-primary w-xs me-2 mb-1',
        confirmButtonText: 'Yes, Delete It!',
        cancelButtonClass: 'btn btn-danger w-xs mb-1',
        buttonsStyling: false,
        showCloseButton: true
    }).then(function(result) {

        if (result.isConfirmed) {

            $.ajax({

                url: url,
                type: "POST",
                data: {
                    'id': rowID
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,
                success: function() {
                    $('.filter-lead-table-btn').click();
                }
            });
        }
    });
});

$(document).on('change', '.branch_filter', function () {
    let branch_id = $(this).val();
    let url = $(this).attr('data-session-route');
    let data_filter = $(this).attr('data-filter');

    if (branch_id.length == 1) {
        $.ajax({
            url: url,
            data: {
                branch_id,data_filter
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
            },
            type: 'POST',
            success: function (response) {
                let options = ``;
                $.each(response, function (i, val) {
                    options += `<option value="${val.id}">${val.name}</option>`;
                })
                let html = `<div class="form-label-group in-border">
                                <select class="session_filter form-select custom-select2" name="session_id" placeholder="Session">
                                <option value="">Select Session</option>
                                    ${options}
                                </select>
                                <label for="branch_ids" class="form-label">Session</label>
                            </div>`;
                $('.session_select').html(html);
            }
        });
    } else {
        $('.session_select').html(`<select class="session_filter form-select custom-select2" name="session_id" placeholder="Session"><option value="">Select Session</option></select>`);
    }
})

$(document).on('change', '.branch_import_filter', function () {
    let branch_id = $(this).val();
    let url = $(this).attr('data-session-route');

    if (branch_id.length == 1) {
        $.ajax({
            url: url,
            data: {
                branch_id
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
            },
            type: 'POST',
            success: function (response) {
                let options = ``;
                $.each(response, function (i, val) {
                    options += `<option value="${val.id}">${val.name}</option>`;
                })
                let html = `<div class="form-label-group in-border">
                                <select class="session_filter form-select custom-select2" name="session_id" placeholder="Session">
                                <option value="">Select Session</option>
                                    ${options}
                                </select>
                                <label for="branch_ids" class="form-label">Session</label>
                            </div>`;
                $('.session_import_select').html(html);
            }
        });
    } else {
        $('.session_import_select').html(`<select class="session_filter form-select custom-select2" name="session_id" placeholder="Session"><option value="">Select Session</option></select>`);
    }
})

$(document).on('click', '.lead_reminders-btn', function () {
    let url = $(this).attr('data-route');
    let id = $(this).attr('data-lead-id');
    let delete_reminder_route = $(this).attr('data-delete-reminder-route');

    $.ajax({
        type : 'POST',
        url : url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : {
            id
        },
        success:function (response) {
            appendLeadReminderListings(response, delete_reminder_route);
            $('#lead_id_form').val(id);
            $('.lead-reminder-form').removeClass('was-validated');
            $('#leadRemindersModal').modal('show');
        }
    })

});

$(document).on('submit', '.lead-reminder-form', function (e) {
    let form = $(this);
    let url = form.attr('data-route');
    let delete_reminder_route = form.attr('data-delete-reminder-route');

    $.ajax({
        type : 'POST',
        url : url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : form.serialize(),
        success:function (response) {
            appendLeadReminderListings(response, delete_reminder_route);
            $('.lead-reminder-form').removeClass('was-validated');
            form[0].reset();
        },
        error:function (response) {
            let errors = response.responseJSON.errors;
            $.each(errors, function(i) {
                $('.' + i).addClass('is-invalid');
            })
        }
    })
})

$(document).on('click', '.delete-lead-reminder', function (e) {
    e.preventDefault();
    var url = $(this).attr('data-route');
    var delete_reminder_route = $(this).attr('data-route');
    var rowID = $(this).data('rowid');

    Swal.fire({
        html: '<div class="mt-3">' +
            '<lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>' +
            '<div class="mt-4 pt-2 fs-15 mx-5">' +
            '<h4>Are you sure?</h4>' +
            '<p class="text-muted mx-4 mb-0">Are you Sure You want to Delete this Record ?</p>' +
            '</div>' +
            '</div>',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-primary w-xs me-2 mb-1',
        confirmButtonText: 'Yes, Delete It!',
        cancelButtonClass: 'btn btn-danger w-xs mb-1',
        buttonsStyling: false,
        showCloseButton: true
    }).then(function(result) {

        if (result.isConfirmed) {

            $.ajax({

                url: url,
                type: "POST",
                data: {
                    'id': rowID
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,
                success: function(response) {
                    appendLeadReminderListings(response, delete_reminder_route);
                }
            });
        }
    });

})

function appendLeadReminderListings(response, delete_reminder_route) {
    let lead_reminder_listings = ``;
    $.each(response, function (i, row) {
        var is_executed = 'No';
        if(row.is_executed == 1){
            is_executed = 'Yes';
        }
        lead_reminder_listings +=   `<tr>
                                        <td>${row.message}</td>
                                        <td>${row.date_time}</td>
                                        <td>${is_executed}</td>
                                        <td>
                                            <a href="javascript:;" data-route="${delete_reminder_route}" data-rowid="${row.id}" class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-lead-reminder">
                                                <i class="ri-delete-bin-5-line"></i>
                                            </a>
                                        </td>
                                    </tr>`;
    })
    $('#lead_reminders_listing').html(lead_reminder_listings);
}

$('#date_time').flatpickr({
    enableTime: true,
});

$(document).on('change', '#follow_up_employee_id', function () {
    let val = $(this).val();
    if (val.length > 0) {
        $('#follow_up_by_employee_date').prop('disabled', false);
        $('.follow_up_date_wrapper').css('display', 'block');
    } else {
        $('#follow_up_by_employee_date').val('');
        $('#follow_up_by_employee_date').prop('disabled', false);
        $('.follow_up_date_wrapper').css('display', 'none');
    }
})
// Show lead generation modal
$(document).on('click', '.show-lead-msg-modal', function () {
    let id = $(this).attr('data-id');
    let url = $(this).attr('data-route');
    $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            id
        },
        success: function (response) {
            $('#lead-msg-modal-content').html(response);
            $('#leadMessageModal').modal('show');
        }
    })

});
$(document).on('submit', '.lead-message-form', function (e) {
    let form = $(this);
    let url = form.attr('data-route');
    console.log(url);
    $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: form.serialize(),
        beforeSend: function () {
            $('.lead-send-error-message-container').html('');
        },
        success:function (response) {
            console.log(response);
            if(response.success){
                $('.lead-send-error-message-container').html('<div class="alert alert-success" role="alert">'+response.message+'</div>');
                //$('#leadMessageModal').modal('hide');
            } else {
                $('.lead-send-error-message-container').html('<div class="alert alert-danger" role="alert">' + response.message + '</div>');
            }

            //form[0].reset();
        },
        error: function (response) {
            //console.log(response.responseJSON);
            $('.lead-send-error-message-container').html('<div class="alert alert-danger" role="alert">' + response.responseJSON.message + '</div>');
        }
    })
});

$(document).on('submit', '.lead-p-email-form', function (e) {
    let form = $(this);
    let url = form.attr('data-route');
    console.log(url);
    $.ajax({
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: form.serialize(),
        beforeSend: function () {
            
        },
        success:function (response) {
            console.log(response);
            if(response.success){
                $('.lead-send-error-message-container').html('<div class="alert alert-success" role="alert">'+response.message+'</div>');
                //$('#leadMessageModal').modal('hide');
            } else {
                $('.lead-send-error-message-container').html('<div class="alert alert-danger" role="alert">' + response.message + '</div>');
            }

            //form[0].reset();
        },
        error: function (response) {
            //console.log(response.responseJSON);
            $('.lead-send-error-message-container').html('<div class="alert alert-danger" role="alert">' + response.responseJSON.message + '</div>');
        }
    })
});
