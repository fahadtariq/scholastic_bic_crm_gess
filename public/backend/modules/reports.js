$(document).ready(function () {
    $('#date_range').flatpickr({
        mode: "range"
    });

    $('.custom-select2').select2();
});

var addContinue = "no";
$(document).on('click', '.add_continue', function () {
    if(addContinue == "no"){
        addContinue = "yes";
        $("#add_continue").removeClass("btn-primary");
        $("#add_continue").addClass("btn-danger");
        $("#add_continue").html('<i class="ri-add-box-line label-icon align-middle rounded-pill fs-16 me-2"></i>Sub Continuing Data');
    }else{
        addContinue = "no";
        $("#add_continue").removeClass("btn-danger");
        $("#add_continue").addClass("btn-primary");
        $("#add_continue").html('<i class="ri-add-box-line label-icon align-middle rounded-pill fs-16 me-2"></i>Add Continuing Data');
       
    }
    $('.branch_filter').trigger('change');
});

$(document).on('change', '#date_range, .branch_filter, .course_filter', function () {
    let date_range = $('#date_range').val();
    let url = $(this).attr('data-route');
    let table_wrapper = $(this).attr('data-table-wrapper');
    let branch_ids = $('.branch_filter').val();
    let course_ids = $('.course_filter').val();
    $.ajax({
        url: url,
        data: {
            date_range,
            branch_ids,
            course_ids,
            addContinue
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
        },
        type: 'GET',
        success: function (response) {
            $('#' + table_wrapper).html(response.view);
            let html = ``;
            const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            let between_dates = response.between_dates;
            let on_date = response.on_date;
            if (between_dates != null && between_dates != '') {
                let date_from = new Date(between_dates[0]);
                let date_to = new Date(between_dates[1]);
                html = `${months[date_from.getMonth()]} ${date_from.getDate()}, ${date_from.getFullYear()} to ${months[date_to.getMonth()]} ${date_to.getDate()}, ${date_to.getFullYear()}`;
            } else if (on_date != null && on_date != '') {
                let date = new Date(on_date);
                html = `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
            } else {
                let current_date = new Date();
                html = `January ${current_date.getFullYear()}<br> As of ${months[current_date.getMonth()]} ${current_date.getDate()}, ${current_date.getFullYear()}`;
            }
            $('#date-changer').html(html);
        }
    })
})
$(document).on('change', '.session_filter', function () {
    let date_range = $('#date_range').val();
    let url = $(this).attr('data-route');
    let table_wrapper = $(this).attr('data-table-wrapper');
    let branch_ids = $('.branch_filter').val();
    let course_ids = $('.course_filter').val();
    let session_ids_dropped_report = [];
    let session_ids_not_interested_report = [];
    let session_ids_withdrawal_report = [];
    let session_ids_bss_matric = [];
    let session_ids_target_data = [];
    let session_ids_joining_probablity = [];
    let session_ids_business = [];
    let session_ids_business_part2 = [];
    $(".session_filter_dropped").each(function ()
    {
        let dropped = ($(this).val().split(","));
        session_ids_dropped_report[dropped[0]] = dropped[1];
    })
    $(".session_filter_not_interested").each(function ()
    {
        let notIntrested = ($(this).val().split(","));
        session_ids_not_interested_report[notIntrested[0]] = notIntrested[1];
    })
    $(".session_filter_withdrawal").each(function ()
    {
        let withdarawal = ($(this).val().split(","));
        session_ids_withdrawal_report[withdarawal[0]] = withdarawal[1];
    })
    $(".session_filter_bss_matric").each(function ()
    {
        let bssMatric = ($(this).val().split(","));
        session_ids_bss_matric[bssMatric[0]] = bssMatric[1];
    })
    $(".session_filter_target_data").each(function ()
    {
        let targetData = ($(this).val().split(","));
        session_ids_target_data[targetData[0]] = targetData[1];
    })
    $(".session_filter_joining_probablity").each(function ()
    {
        let joiningProbablity = ($(this).val().split(","));
        session_ids_joining_probablity[joiningProbablity[0]] = joiningProbablity[1];
    })
    $(".session_filter_business").each(function ()
    {
        let business = ($(this).val().split(","));
        session_ids_business[business[0]] = business[1];
    })
    $(".session_filter_business_part2").each(function ()
    {
        let business = ($(this).val().split(","));
        session_ids_business_part2[business[0]] = business[1];
    })

    $.ajax({
        url: url,
        data: {
            date_range,
            branch_ids,
            course_ids,
            session_ids_dropped_report,
            session_ids_withdrawal_report,
            session_ids_not_interested_report,
            session_ids_bss_matric,
            session_ids_target_data,
            session_ids_joining_probablity,
            session_ids_business,
            session_ids_business_part2,
            addContinue
        },
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
        },
        type: 'GET',
        success: function (response) {
            $('#' + table_wrapper).html(response.view);
            let html = ``;
            const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            let between_dates = response.between_dates;
            let on_date = response.on_date;
            if (between_dates != null && between_dates != '') {
                let date_from = new Date(between_dates[0]);
                let date_to = new Date(between_dates[1]);
                html = `${months[date_from.getMonth()]} ${date_from.getDate()}, ${date_from.getFullYear()} to ${months[date_to.getMonth()]} ${date_to.getDate()}, ${date_to.getFullYear()}`;
            } else if (on_date != null && on_date != '') {
                let date = new Date(on_date);
                html = `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
            } else {
                let current_date = new Date();
                html = `January ${current_date.getFullYear()}<br> As of ${months[current_date.getMonth()]} ${current_date.getDate()}, ${current_date.getFullYear()}`;
            }
            $('#date-changer').html(html);
        }
    })
})

$(document).on('change', '.source_branch_filter', function () {
    let branch_id = $(this).val();
    let url = $(this).attr('data-session-route');
    let table_wrapper = $(this).attr('data-table-wrapper');

    if (branch_id.length == 1) {
        $.ajax({
            url: url,
            data: {
                branch_id,
                addContinue
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
            },
            type: 'POST',
            success: function (response) {
                let options = ``;
                $.each(response, function (i, val) {
                    options += `<option value="${val.id}">${val.name}</option>`;
                })
                let html = `<div class="form-label-group in-border">
                                <select class="session_filter form-select" data-table-wrapper="${table_wrapper}" name="session_id" placeholder="Session">
                                <option value="">Select Session</option>
                                    ${options}
                                </select>
                                <label for="branch_ids" class="form-label">Session</label>
                            </div>`;
                $('.session_select').html(html);
            }
        });
    } else {
        $('.session_select').html('');
    }
});
