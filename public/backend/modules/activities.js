$(document).ready(function () {

    $('#date_range').flatpickr({
        mode: "range"
    });

    var route = $("#ajaxRoute").val();
    console.log(route);
    $("#activities-data-table").DataTable({
        searching: false,
        processing: true,
        serverSide: true,
        responsive: true,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, 'All'],
        ],
        scrollX: true,
        language: {
            search: "",
            searchPlaceholder: "Search...",
        },
        ajax: {
            url: route,
            data: function ( d ) {
                d.event = $('#event').val();
                d.modal_search = $('#modal_search').val();
                d.date_range = $('#date_range').val();
            }
        },
        columns: [
            {
                data: "id",
                name: "id",
                width: "5%",
            },
            {
                data: "performed_on",
                name: "performed_on",
            },
            {
                data: "performed_by",
                name: "performed_by",
                width: "15%",
            },
            {
                data: "event",
                name: "performed_action",
                width: "15%",
            },
            {
                data: "created_at",
                name: "performed_date",
                width: "15%",
            },
            {
                data: "action",
                name: "action",
                orderable: false,
                searchable: false,
                width: "5%",
                sClass: "text-center",
            },
        ],
    });
});

$(document).on('change', '.filter', function() {
    $('#activities-data-table').DataTable().ajax.reload(null, false);
});

$(document).on("keyup",'#modal_search', function() {
    var value = $(this).val().toLowerCase();
    if(value.length > 0 || value.length == 0){
        $('#activities-data-table').DataTable().search($(this).val()).draw() ;
    }
});

// Detail Activity Log
function detailActivityLog(id, url)
{
    var id = id;
    var url = url;
    $('#activity-log-body').html('');
    $('#id-span-activity-log').text(id)
    ;
    $.ajax({
        url: url,
        data: {id: id},
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            if(data.status == 1){
                if(data.activityLog.properties != '' && (data.activityLog.event == 'created' || data.activityLog.event == 'updated')){
                    var attributes = JSON.parse(data.activityLog.new_values);
                    var html = `<h5 class="mt-0">Attributes</h5>`;
                    html += `<div class="row">`;
                    $.each(attributes, function (k, v) {
                        html += `<div class="col-6">`;
                        html += `<strong>${k}: </strong> <p class="mb-1">${v}</p>`;
                        html += `</div>`;
                    });
                    html += `</div>`;

                    if(data.activityLog.event == 'updated'){
                        var oldAttributes = JSON.parse(data.activityLog.old_values);
                        html += `<hr class="mt-1 mb-1">`;
                        html += `<h5>Old</h5>`;
                        html += `<div class="row">`;
                        $.each(oldAttributes, function (k, v) {
                            html += `<div class="col-6">`;
                            html += `<strong>${k}: </strong> <p class="mb-1">${v}</p>`;
                            html += `</div>`;
                        });
                        html += `</div>`;
                    }

                    $('#activity-log-body').html(html);
                    $('#activityModal').modal('show');
                }
            }
        }
    });
}
