<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsappChatMessage extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'chat_id',
        'sender_phone_number',
        'text',
        'status'
    ];
    public function chat()
    {
        return $this->belongsTo(WhatsappChat::class, 'chat_id', 'id');
    }
}
