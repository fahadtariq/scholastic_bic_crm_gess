<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class LeadTracking extends Model implements Auditable
{
    use HasFactory, SoftDeletes, AuditableTrait;
    protected $guarded = [];

    public function leadStatus()
    {
        return $this->belongsTo(LeadStatus::class, 'lead_status_id', 'id');
    }

    public function leadType()
    {
        return $this->belongsTo(LeadType::class, 'lead_type_id', 'id');
    }

    public function leadEmployee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function leadBranch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    public function reason()
    {
        return $this->belongsTo(Reason::class, 'reason', 'id');
    }
}
