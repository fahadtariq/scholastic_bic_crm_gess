<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Source extends Model implements Auditable
{
    use HasFactory, SoftDeletes, AuditableTrait;
    protected $guarded = [];

    public function leads()
    {
        return $this->hasMany(Lead::class, 'source_id', 'id');
    }

    public function leadFollowUps()
    {
        return $this->hasMany(LeadFollowUp::class, 'source_id', 'id');
    }
}
