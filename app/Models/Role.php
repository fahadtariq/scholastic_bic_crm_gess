<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Role extends LaratrustRole implements Auditable
{
    use AuditableTrait;
    public $guarded = [];
}
