<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Student extends Model implements Auditable
{
    use HasFactory, SoftDeletes, AuditableTrait;
    protected $guarded = [];

    public function guardians()
    {
        return $this->hasMany(StudentGuardian::class);
    }

    public function emergencyContact()
    {
        return $this->hasOne(StudentEmergencyContact::class);
    }

    public function classGrade()
    {
        return $this->belongsTo(ClassGrade::class);
    }

    public function contactCode()
    {
        return $this->belongsTo(ContactCode::class);
    }
}
