<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditorReviewer extends Model
{
    use HasFactory, softDeletes;

    protected $fillable = [
        'user_id',
        'review_date',
        'lead_count_acc',
        'lead_assignment_acc',
        'review_acc',
        'admission_acc',
        'outbound_call_acc',
        'review_remarks',
    ];

    protected $dates = [
        'created_at',
        'review_date',
    ];

    protected $casts = [
        'review_date' => 'date:d M, Y H:i'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
