<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Lead extends Model implements Auditable
{
    use HasFactory, SoftDeletes, AuditableTrait;
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    protected $casts = [
        'created_at' => 'date:d M, Y H:i',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    public function followUps()
    {
        return $this->hasMany(LeadFollowUp::class);
    }

    public function status()
    {
        return $this->belongsTo(LeadStatus::class, 'lead_status_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(LeadType::class, 'lead_type_id', 'id');
    }

    public function source()
    {
        return $this->belongsTo(Source::class, 'source_id', 'id');
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function typeTrack()
    {
        return $this->hasMany(LeadTracking::class)->where('type', 0)->orderBy('id', 'desc');
    }

    public function statusTrack()
    {
        return $this->hasMany(LeadTracking::class)->where('type', 1)->orderBy('id', 'desc');
    }

    public function employeeTrack()
    {
        return $this->hasMany(LeadTracking::class)->where('type', 2)->orderBy('id', 'desc');
    }

    public function branchTrack()
    {
        return $this->hasMany(LeadTracking::class)->where('type', 3)->orderBy('id', 'desc');
    }

    public function trackings()
    {
        return $this->hasMany(LeadTracking::class);
    }

    /**
     * @return BelongsTo
     */
    public function session(): BelongsTo
    {
        return $this->belongsTo(BranchTarget::class, 'session_id', 'id');
    }

    public static function employeeAutoAssign($branchId)
    {
        $assignedEmployeeId = null;
        /** Get lead selected branch active employee ids those have admission advisor role*/
        $employeeIds = Employee::where(['branch_id' => $branchId, 'status_id' => 1])->get()->map(function ($query) {
            if ($query->user->hasRole(['admission_advisor'])) {
                return $query;
            }
        })->filter()->pluck('id')->toArray();

        /** Check if branch has any employees**/
        if (count($employeeIds) > 0) {
            $assignedEmployeeId = $employeeIds[0];
            /** Get latest lead having an employee id within today's date**/
            $lead = Lead::where(['branch_id' => $branchId])->where('employee_id', '!=', null)
                ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
                ->latest()->first();

            if (!empty($lead)) {
                $existingEmployeeIndex = array_search($lead->employee_id, $employeeIds);
                if (isset($employeeIds[$existingEmployeeIndex + 1])) {
                    $assignedEmployeeId = $employeeIds[$existingEmployeeIndex + 1];
                }
            }
        }
        return $assignedEmployeeId;
    }
}
