<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class SystemModule extends Model implements Auditable
{
    use HasFactory, SoftDeletes, AuditableTrait;
    protected $fillable = [
        'name',
        'description',
        'parent_id',
    ];
    protected $dates = [

        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'created_at' => 'date:d M, Y H:i',
    ];

    protected $parentColumn = 'parent_id';

    public function parent()
    {
        return $this->belongsTo(SystemModule::class,$this->parentColumn);
    }

    public function children()
    {
        return $this->hasMany(SystemModule::class, $this->parentColumn);
    }

    public function allChildren()
    {
        return $this->children()->with('allChildren');
    }

    public function modules_permission()
    {
        return $this->hasMany(Permission::class, 'system_module_id','id');
    }
}
