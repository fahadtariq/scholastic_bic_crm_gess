<?php

namespace App\Models;

use Laratrust\Models\LaratrustPermission;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Permission extends LaratrustPermission implements Auditable
{
    use AuditableTrait;
    public $guarded = [];
    protected $fillable = [
        'system_module_id',
        'name',
        'display_name',
        'description'

    ];

    public function systemModules()
    {
        return $this->belongsTo(SystemModule::class, 'system_module_id', 'id');
    }
}
