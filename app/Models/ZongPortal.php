<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZongPortal extends Model
{
    use HasFactory;

    protected $table = 'zong_portals';
    protected $guarded = [];
}
