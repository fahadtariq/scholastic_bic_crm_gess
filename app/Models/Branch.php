<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Branch extends Model implements Auditable
{
    use HasFactory, SoftDeletes, AuditableTrait;
    protected $guarded = [];

    public function state()
    {
        return $this->BelongsTo(State::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function leads()
    {
        return $this->hasMany(Lead::class);
    }

    public function targets()
    {
        return $this->hasMany(BranchTarget::class);
    }

    public function promotion()
    {
        return $this->belongsTo(PromotionalAdmission::class,"id","branch_id");
    }
}
