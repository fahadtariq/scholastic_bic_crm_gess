<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsappChat extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'lead_id',
        'user1_phone_number',
        'user2_phone_number'
    ];
    public function messages()
    {
        return $this->hasMany(WhatsappChatMessage::class, 'chat_id', 'id');
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class, 'lead_id', 'id');
    }
}
