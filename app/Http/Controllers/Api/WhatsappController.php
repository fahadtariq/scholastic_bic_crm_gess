<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\TwiML\MessagingResponse;

class WhatsappController extends Controller
{
    public function listenToReplies(Request $request)
    {
        $from = $request->input('From');
        $body = $request->input('Body');
        $messageSid = $request->input('MessageSid');

        // Now you can process the message or store it in your database
        //$senderNumber = env('SENDER_WHATSAPP_NUMBER');
        //Get lead details if number is associated with any lead
        $from = str_replace('whatsapp:','', $from);
        $businessNumber = env('SENDER_WHATSAPP_NUMBER'); // Replace with actual phone number

        //Send whatsapp message
        saveWhatsappMessage( $from, $businessNumber, '0', $body, 'Received' );

        // Send a response
        $response = new MessagingResponse();
        $response->message('Your message has been received.');

        header('Content-Type: text/xml');
        return $response;
    }
}
