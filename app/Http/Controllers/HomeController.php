<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Redirect;
use Cookie;
use Auth;
use Session;

class HomeController extends Controller
{
    // Lock Screen by Usman 20/05/2022
    public function lockScreen(Request $request){
        $userId = Auth::user()->id;
        $cookie = Cookie::queue(Cookie::make('_lock', $userId, 36000000));
        return view('auth.lock');
    }

    // Lock Screen Authenticate by Usman 20/05/2022
    public function lockScreenUpdate(Request $request){
        $inputs = $request->all();
        $messsages = array('password.required' => 'Password is must required');
        $rules = array(
            'password' => 'required'
        );
        $validator = Validator::make(request()->all(), $rules,$messsages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput();
        }
        $cookie = Cookie::has('_lock');
        if($cookie == true) {
            $getCookie = Cookie::get('_lock');
            $user = User::select('email')->where('id',$getCookie)->first();
            if($user == '' || $user == null) {
                return Redirect::back()->with('error', 'User Not Found.');
            }

            $auth = Auth::attempt(array(
                'email'     => $user->email,
                'password'  => $inputs['password']
            ));
            if ($auth == true) {
                $usr = Auth::loginUsingId($getCookie);
                Cookie::queue(Cookie::forget('_lock'));
                $intended_url = Session::get('intended_url');
                if($intended_url != null && $intended_url != ''){
                    Session::forget('intended_url');
                    return Redirect::to($intended_url);
                }
                return redirect()->route('dashboard');
            } 
            else {
                return Redirect::back()->with('error', 'Invalid password. Try again with correct password');
            }
        } 
        else {
            return redirect()->route('dashboard');
        }
    }
}
