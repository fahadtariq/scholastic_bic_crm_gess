<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs\MarketingCampaignJob;
use App\Models\Campaign;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class CampaignController extends Controller
{
    public function index(Request $request)
    {
        $statuses = Status::get();
        if ($request->ajax()) {
            $data = Campaign::orderBy('id', 'DESC')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('email_template_id', function ($row) {
                    $slug = "basic";
                    if ($row->email_template_id == 2) {
                        $slug = "uae";
                    } else if ($row->email_template_id == 3) {
                        $slug = "akhuwat";
                    }
                    return "<a href='" . route('campaign.template.show', ['slug' => $slug, 'campaign_id' => $row->id]) ."' target='_blank'>" . strtoupper($slug) . "</a>";
                })
                ->addColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return '<span class="badge bg-info">In-Progress</span>';
                    } else if ($row->status == 3) {
                        return '<span class="badge bg-warning">Scheduled</span>';
                    }
                    return '<span class="badge bg-success">Completed</span>';
                })
                ->addColumn('recipients', function ($row) {
                    $route = route("campaign.recipients.show", ['id' =>$row->id]);
                    return "<a href='javascript:;' onClick=showRecipients('$route')
                                class='btn btn-sm btn-warning btn-icon waves-effect waves-light'>
                                <i class='mdi mdi-eye'></i>
                            </a>";
                })
                ->addColumn('scheduled_date_time', function ($row) {
                    if (filled($row->scheduled_date_time)) {
                        return date('d F Y (h:i A)', strtotime($row->scheduled_date_time));
                    }
                    return "------";
                })
                ->addColumn('created_at', function ($row) {
                    return date('d F Y (h:i A)', strtotime($row->created_at));
                })
//                ->addColumn('action', function ($row) {
//                    return view('backend.campaigns.actions', ['row' => $row]);
//                })
                ->rawColumns(['action', 'status', 'created_at', 'email_template_id', 'scheduled_date_time', 'recipients'])
                ->make(true);
        }
        return view('backend.campaigns.index', get_defined_vars());
    }

    public function create()
    {
        return view('backend.campaigns.create');
    }

    public function store(Request $request)
    {
        $this->validateCampaignFields($request);
        $status = 1;
        if ($request->hasFile('email_list')) {
            $leadsData = Excel::toArray([], $request->file('email_list'))[0];

            $recipients = [];
            foreach ($leadsData as $key => $data) {
                if ($key == 0 || !filter_var($data[0], FILTER_VALIDATE_EMAIL))
                continue;

                array_push($recipients, $data[0]);
            }

            if (count($recipients) == 0) {
                return back()->with('error', 'Please enter the email address properly. no email found in the file');
            }

            $isSchedule = 0;
            $scheduleDateTime = null;
            if ($request->is_scheduled) {
                $isSchedule = 1;
                $scheduleDateTime = $request->scheduled_date_time;
                $status = 3;
                $scheduleDateAndTime = Carbon::parse($request->scheduled_date_time, env('DEFAULT_TIMEZONE', 'Asia/Karachi'))->setTimezone('UTC');
                $delayInMinutes = Carbon::now(env('DEFAULT_TIMEZONE', 'Asia/Karachi'))->diffInMinutes($scheduleDateAndTime);
            }

            $campaign = Campaign::create([
                'title' => $request->title,
                'status' => $status,
                'type' => 1,
                'email_template_id' => $request->email_template_id,
                'recipients' => $recipients,
                'email_content' => $request->email_content,
                'is_scheduled' => $isSchedule,
                'scheduled_date_time' => $scheduleDateTime
            ]);

            if (!$request->is_scheduled) {
                MarketingCampaignJob::dispatch($campaign);
            } else {
                MarketingCampaignJob::dispatch($campaign)->delay(now(env('DEFAULT_TIMEZONE', 'Asia/Karachi'))->addMinutes($delayInMinutes));
            }

            return Redirect()->route('campaign.index')->with('success', 'Campaign has been started.');
        }
        return back()->with('error', 'Please upload a email list');
    }

    public function campaignTemplates()
    {
        return view('backend.campaign_templates.index');
    }

    public function showCampaignTemplate($title, $campaignId = null)
    {
        $campaign = Campaign::find($campaignId);
        if (!empty($campaign)) {
            $content = $campaign->email_content;
        }
        if ($title == "basic") {
            return view('emails.campaigns.basic', get_defined_vars());
        } else if ($title == "uae") {
            return view('emails.campaigns.uae', get_defined_vars());
        } else if ($title == "akhuwat") {
            return view('emails.campaigns.akhuwat', get_defined_vars());
        }
        return redirect()->route('campaign.template.index');
    }

    public function showRecipients(Request $request)
    {
        $recipients = [];
        $campaign = Campaign::find($request->id);
        if (!empty($campaign)) {
            $recipients = $campaign->recipients;
        }
        return Response()->json([
            'data' => $recipients
        ]);
    }

    protected function validateCampaignFields($request)
    {
        $rules = [
            'title' => 'required',
            'email_template_id' => 'required',
            'email_content' => 'required',
            'is_scheduled' => 'required',
            'email_list' => 'required'
        ];

        $messages = [
            'title.required' => 'Campaign title is required!',
            'email_template_id.required' => 'Select the email template!',
            'is_scheduled.required' => 'Select the option!',
            'email_content.required' => 'Enter the email content!',
            'email_list.required' => 'Upload a email list!'
        ];

        if ($request->is_scheduled) {
            $rules['scheduled_date_time'] = "required";
            $messages['scheduled_date_time.required'] = "Select the scheduled date & time!";
        }
        $request->validate($rules, $messages);
    }
}
