<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Status;
use App\Models\Tag;

class TagController extends Controller
{
    /**
     * @description function to show tag listing view, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function index(Request $request)
    {
        $statuses = Status::all();
        if ($request->ajax()) {
            $data = Tag::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.tags.actions', compact('row'));
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('backend.tags.index', compact('statuses'));
    }

    /**
     * @description function to store new tag record, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|max:15', 'status_id' => 'required|integer'], ['name.required' => 'Tag name is required!', 'status_id.required' => 'Select the status!']);
        Tag::create($request->except(['_token','beams_id']));
        return redirect()->route('tags.index')->with('success', 'Tag is created successfully');
    }

    /**
     * @description function to edit tag view, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function edit($id)
    {
        $tag = Tag::find($id);
        if (!empty($tag)) {
            $statuses = Status::all();
            return view('backend.tags.index', compact('tag', 'statuses'));
        }
        return redirect()->route('tags.index');
    }

    /**
     * @description function to update tag record, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function update(Request $request)
    {
        $request->validate(['name' => 'required|max:15', 'status_id' => 'required|integer'], ['name.required' => 'Tag name is required!', 'status_id.required' => 'Select the status!']);
        Tag::find($request->id)->update(['name' => $request->name, 'abbreviation' => $request->abbreviation]);
        return redirect()->route('tags.index')->with('success', 'Tag is updated successfully');
    }

    /**
     * @description function to delete tag record, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function delete(Request $request)
    {
        $tag = Tag::find($request->id);
        try {
            $tag->leads()->delete();
            $tag->leadFollowUps()->delete();
            $tag->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
