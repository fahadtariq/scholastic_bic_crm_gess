<?php

namespace App\Http\Controllers\Backend;

use App\Models\ZongPortal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Artisan;

class ZongPortalController extends Controller
{
    public function getTodayData()
    {
        set_time_limit(120);
        $res = Artisan::call('zongportal:report');
        return redirect()->route('zong-portal.index')->with('success', $res.' Records Found.');
    }

    public function liveZongPortal(Request $request)
    {
        set_time_limit(120);
        $inputs = $request->all();
        $apiToken = 'brEejfgLPu7x1HQuwQkgEEW7CevyEggRpwUMr4nkeupRwZR6KKhCsfkRJ7m9';
        $callUrl = 'https://zong-cap.com.pk:93/api/customer/outgoing-calls';
        $callType = 0;
        if (isset($inputs['call_type']) && $inputs['call_type'] != '' && $inputs['call_type'] == 1) {
            $callType = 1;
            $callUrl = 'https://zong-cap.com.pk:93/api/customer/incoming-calls';
        }

        $paramArr = array(
            'api_token' => $apiToken,
        );

        if (isset($inputs['date_range']) && $inputs['date_range'] != '') {
            $dates = explode(" to ", $inputs['date_range']);
            if (count($dates) > 1) {
                $paramArr['date_from'] = $dates[0];
                $paramArr['date_to'] = $dates[1];
            } else {
                $paramArr['date_from'] = $dates[0];
                $paramArr['date_to'] = $dates[0];
            }
        } else {
            $paramArr['date_from'] = date('Y-m-d');
            $paramArr['date_to'] = date('Y-m-d');
        }

        $callCounts = 0;
        $calls = [];

        $response = Http::post($callUrl, $paramArr);
        if ($response->successful()) {
            $records = $response->object();
            if (isset($records->Total)) {
                $calls = collect($records->calls);
                foreach ($calls as $call) {
                    $checData = ZongPortal::where('on_date', $call->datetime)->first();
                    if (is_null($checData)) {
                        $callData = [
                            'phone' => $call->phone ? substr($call->phone, 0) : '',
                            'client_ext' => is_null($call->ext) ? 'IVR' : ($call->ext == '' ? 'Queue' : $call->ext),
                            'on_date' => $call->datetime ? $call->datetime : '',
                            'duration' => $call->duration ? $call->duration : 0,
                            'status' => $call->status ? $call->status : '',
                            'type' => isset($inputs['call_type']) && $inputs['call_type'] != '' && $inputs['call_type'] == 1 ? 1 : 0,
                            'recording' => isset($call->recording) ? $call->recording : '',
                        ];
                        ZongPortal::create($callData);
                    }
                }
                if (auth()->user()->hasRole('campus_head')) {
                    $branchId = auth()->user()->employee->branch_id;
                    if ($branchId == 1) {
                        $calls = $calls->whereIn('ext', ['03178222171', '03178222172']);
                    } elseif ($branchId == 2) {
                        $calls = $calls->whereIn('ext', ['03178222177']);
                    } elseif ($branchId == 3) {
                        $calls = $calls->whereIn('ext', ['03178222173', '03178222174']);
                    } elseif ($branchId == 4) {
                        $calls = $calls->whereIn('ext', ['03178222175', '03178222176']);
                    }
                }
                if (isset($inputs['call_status']) && $inputs['call_status'] != '') {
                    $calls = $calls->where('status', $inputs['call_status']);
                }
                if (isset($inputs['duration']) && $inputs['duration'] != '' && $inputs['duration'] >= 0) {
                    $seconds = $inputs['duration'] * 60;
                    if ($inputs['duration_filter'] == 1) {
                        $cond = '=';
                    } else if ($inputs['duration_filter'] == 2) {
                        $cond = '<';
                    } else {
                        $cond = '>=';
                    }
                    $calls = $calls->where('duration', $cond, $seconds);
                }
                if (isset($inputs['client_no']) && $inputs['client_no'] != '') {
                    $phone = '77' . $inputs['client_no'];
                    if ($callType == 1) {
                        $phone = $inputs['client_no'];
                    }
                    $calls = $calls->where('phone', $phone);
                }
                if (isset($inputs['agent_no']) && $inputs['agent_no'] != '') {
                    $calls = $calls->whereIn('ext', $inputs['agent_no']);
                }
                $callCounts = count($calls);
            }
        }

        if ($request->ajax()) {
            $view = view('backend.live-zong-portals.partial', ['calls' => $calls, 'callType' => $callType])->render();
            return response()->json(['view' => $view, 'dbCounter' => $callCounts]);
        }

        return view('backend.live-zong-portals.index')->with([
            'calls' => $calls,
            'callCounts' => $callCounts,
            'callType' => $callType
        ]);
    }

    public function index(Request $request)
    {
        $inputs = $request->all();
        $pageLength = 10;
        $calls = [];

        $calls = ZongPortal::orderBy('on_date', 'desc');
        if (isset($inputs['page_length']) && $inputs['page_length'] != '') {
            $pageLength = $inputs['page_length'];
            if ($pageLength == -1) {
                $pageLength = $calls->count();
            }
        }
        if (isset($inputs['date_range']) && $inputs['date_range'] != '') {
            $dates = explode(" to ", $inputs['date_range']);
            if (count($dates) > 1) {
                $paramArr['date_from'] = $dates[0];
                $paramArr['date_to'] = $dates[1];
            } else {
                $paramArr['date_from'] = $dates[0];
                $paramArr['date_to'] = $dates[0];
            }
        } else {
            $paramArr['date_from'] = date('Y-m-d', strtotime("-1 week"));
            $paramArr['date_to'] = date('Y-m-d');
        }

        $callCounts = 0;
        $calls = $calls->whereBetween('on_date', [$paramArr['date_from'] . ' 00:00:00', $paramArr['date_to'] . ' 23:59:59']);

        if (isset($inputs['call_type']) && $inputs['call_type'] != '') {
            $calls = $calls->where('type', $inputs['call_type']);
        }
        if (auth()->user()->hasRole('campus_head')) {
            $branchId = auth()->user()->employee->branch_id;
            // dd($branchId);
            if ($branchId == 1) {
                $calls = $calls->whereIn('client_ext', ['03178222171', '03178222172']);
            } elseif ($branchId == 2) {
                $calls = $calls->whereIn('client_ext', ['03178222177']);
            } elseif ($branchId == 3) {
                $calls = $calls->whereIn('client_ext', ['03178222173', '03178222174']);
            } elseif ($branchId == 4) {
                $calls = $calls->whereIn('client_ext', ['03178222175', '03178222176']);
            }
        }
        if (isset($inputs['call_status']) && $inputs['call_status'] != '') {
            $calls = $calls->where('status', $inputs['call_status']);
        }
        if (isset($inputs['duration']) && $inputs['duration'] != '' && $inputs['duration'] >= 0) {
            $seconds = $inputs['duration'] * 60;
            if ($inputs['duration_filter'] == 1) {
                $cond = '=';
            } else if ($inputs['duration_filter'] == 2) {
                $cond = '<';
            } else {
                $cond = '>=';
            }
            $calls = $calls->where('duration', $cond, $seconds);
        }
        if (isset($inputs['client_no']) && $inputs['client_no'] != '') {
            $phone = '77' . $inputs['client_no'];
            if (isset($inputs['call_type']) && $inputs['call_type'] != '' && $inputs['call_type'] == 1) {
                $phone = $inputs['client_no'];
            }
            $calls = $calls->where('phone', $phone);
        }
        if (isset($inputs['agent_no']) && $inputs['agent_no'] != '') {
            $calls = $calls->whereIn('client_ext', $inputs['agent_no']);
        }
        $callCounts = $calls->count();
        $calls = $calls->paginate($pageLength);
        $calls->appends($request->all())->render();
        if ($request->ajax()) {
            $view = view('backend.zong-portals.partial', ['calls' => $calls])->render();
            $pagination = view('layouts.pagination_common', ['pagination' => $calls])->render();
            return response()->json(['view' => $view,'pagination' => $pagination, 'dbCounter' => $callCounts]);
        }

        return view('backend.zong-portals.index')->with([
            'calls' => $calls,
            'callCounts' => $callCounts,
            // 'callType' => $callType
        ]);
    }


    public function test()
    {
        date_default_timezone_set("Asia/Karachi");
        $currentDate = date('Y-m-d');
        $outgoings = $incomings = [];
        $apiToken = 'brEejfgLPu7x1HQuwQkgEEW7CevyEggRpwUMr4nkeupRwZR6KKhCsfkRJ7m9';
        $outgoingCallUrl = 'https://zong-cap.com.pk:93/api/customer/outgoing-calls';
        $incomingCallUrl = 'https://zong-cap.com.pk:93/api/customer/incoming-calls';
        $paramArr = array(
            'api_token' => $apiToken,
            'date_from' => $currentDate,
            'date_to' => $currentDate,
        );

        $outgoingResponse = Http::post($outgoingCallUrl, $paramArr);
        if ($outgoingResponse->successful()) {
            $outgoingRecords = $outgoingResponse->object();
            if (isset($outgoingRecords->Total)) {
                $outgoings = collect($outgoingRecords->calls);
            }
        }

        $incomingResponse = Http::post($incomingCallUrl, $paramArr);
        if ($incomingResponse->successful()) {
            $incomingRecords = $incomingResponse->object();
            if (isset($incomingRecords->Total)) {
                $incomings = collect($incomingRecords->calls);
            }
        }
        return $incomings;
    }
}
