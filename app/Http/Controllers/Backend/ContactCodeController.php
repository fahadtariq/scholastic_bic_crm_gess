<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ContactCode;
use App\Models\Country;
use App\Models\Status;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DataTables;

class ContactCodeController extends Controller
{

    //it returns main page and list of contact codes  19/05/2022 by Jan Muhammad 
    public function index(Request $request)
    {
        $statuses = Status::get();
        $countries = Country::where('status_id', 1)->get();
        if ($request->ajax()) {
            $data = ContactCode::with(['country'])->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.contact_codes.actions', ['row' => $row]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }

        return view('backend.contact_codes.index', compact('statuses', 'countries'));
    }

    //it returns main page and store new contact code  19/05/2022 by Jan Muhammad 
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|max:255|unique:cities,name',
            'status_id' => 'required|integer',
            'country_id' => 'required|integer',
        ]);

        ContactCode::create($request->all());

        return redirect()->route('contact-code.index')
            ->with('success', 'Contatct Code created successfully.');
    }


    //it returns edit page  19/05/2022 by Jan Muhammad 
    public function edit($id)
    {
        $conatct_code = ContactCode::find($id);
        $statuses = Status::get();
        $countries = Country::where('status_id', 1)->get();
        return view('backend.contact_codes.index', ['conatct_code' => $conatct_code, 'statuses' => $statuses, 'countries' => $countries]);
    }

    //it returns main page and update contact code  19/05/2022 by Jan Muhammad 
    public function update(Request $request)
    {
        $conatct_code = ContactCode::find($request->id);
        $request->validate([
            'code' => 'required|max:255|unique:conatct_codes,name,' . $request->id,
            'status_id' => 'required|integer',
            'country_id' => 'required|integer',
        ]);

        $conatct_code->update($request->all());

        return redirect()->route('contact-code.index')
            ->with('success', 'Contact Code updated successfully.');
    }

    //it returns main page and delete contact code  19/05/2022 by Jan Muhammad 
    public function destroy(Request $request)
    {
        // dd($request->id);
        $conatct_code = ContactCode::find($request->id);
        try {
            return $conatct_code->delete();
        } catch (QueryException $e) {
            print_r($e->errorInfo);
        }
    }
}
