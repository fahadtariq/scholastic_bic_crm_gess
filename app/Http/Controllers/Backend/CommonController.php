<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\City;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function listStates(Request $request)
    {
        $data = State::where('country_id', $request->id)->get();
        return response()->json($data);
    }

    public function listCities(Request $request)
    {
        $data = City::where('state_id', $request->id)->get();
        return response()->json($data);
    }
}
