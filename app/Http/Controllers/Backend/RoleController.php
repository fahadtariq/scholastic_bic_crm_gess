<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Models\SystemModule;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Role::with('permissions')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    return view('backend.roles.actions', compact('row'));
                })
                ->addColumn('permissions', function($row){
                    $count = ($row->permissions->count());
                    return $count;
                })
                ->rawColumns(['action', 'permissions'])
                ->make(true);
        }
        return view('backend.roles.index');

    }

    public function create()
    {
        $permissions = Permission::all();
        return view('backend.roles.create', [ 'permissions', $permissions]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|max:32',
            'description' => 'nullable|min:2|max:128',
            'display_name' => 'nullable|min:2|max:32',
        ]);

        try {
            $role = Role::create([
                'name' => $request->name,
                'display_name' => $request->display_name,
                'description' => $request->description,
            ]);

            if ($role->wasRecentlyCreated) {
                return redirect('roles')->with('success', 'Role is created!');
            }else{
                return redirect('roles')->withErrors($request)->withInput();
            }

        } catch (QueryException $exception) {
            throw new \InvalidArgumentException($exception->getMessage());
        }
    }

    public function show(Role $role)
    {

    }

    public function edit(Role $role)
    {
        $role_permissions = $role->permissions()->get()->pluck('id')->toArray();
        // $permissions = Permission::all(['id', 'name', 'display_name' ,'description'])
        //     ->map(function ($permission) use ($role) {
        //         $permission->assigned = $role->permissions
        //             ->pluck('id')
        //             ->contains($permission->id);

        //         return $permission;
        //     });

        $system_modules = SystemModule::where('parent_id', '<=>')->with('modules_permission')->orderBy('name')
            ->get();
        foreach ($system_modules as $key => $system_module) {

            $system_module->modules_permission->map(function ($permission) use ($role) {
                $permission->assigned = $role->permissions->pluck('id')->contains($permission->id);
                return $permission;
            });
        }

        return view('backend.roles.edit', [
            'role'=> $role,
            'permissions'=> $system_modules,
            'role_permissions'=> $role_permissions,
        ]);
    }

    public function update(Request $request,Role $role)
    {
        $request->validate([
            'description' => 'nullable|min:2|max:128',
            'display_name' => 'nullable|min:2|max:32',
        ]);

        $role->update([
            'display_name' => $request->display_name,
            'description' => $request->description,
        ]);

        if($role->save()){
            //if($request->input('permissions')){
            $role->syncPermissions($request->input('permissions') ?? []);
            //}
        }

        return redirect(route('roles.index'))->with('success', 'Role has been updated!');
    }

    public function destroy(Role $role)
    {
        try {
            return $role->delete();
            //return redirect('roles')->with('success', 'Role record has been deleted');

        } catch (QueryException $e) {
            print_r($e->errorInfo);
        }
    }
}
