<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\BranchTarget;
use App\Models\Course;
use App\Models\Lead;
use App\Models\LeadStatus;
use App\Models\LeadType;
use App\Models\PromotionalAdmission;
use App\Models\Reason;
use App\Models\Source;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function withdrawalsReport(Request $request)
    {
        $betweenDates = $onDate = null;
        $branches = Branch::where('status_id', 1);
        $reasons = Reason::where('status_id', 1)->get();
        if (auth()->user()->hasRole('campus_head')) {
            $branchID = auth()->user()->employee->branch_id;
            $branches = $branches->where('id', $branchID);
        } else {
            if ($request->has('branch_ids')) {
                if (!empty($request->branch_ids)) {
                    $branches = $branches->whereIn('id', $request->branch_ids);
                }
            }
        }

        if ($request->has('date_range')) {
            $dates = explode(" to ", $request->date_range);
            if (count($dates) > 1) {
                $betweenDates = $dates;
            } else {
                $onDate = $dates[0];
            }
        }

        $branches = $branches->get(); // loop
        foreach ($branches as $branch) {
            if (isset($request->session_ids_dropped_report) && !empty($request->session_ids_dropped_report)) {
                $branch->session_id_dropped = $request->session_ids_dropped_report[$branch->id];
            }
            if (isset($request->session_ids_not_interested_report) && !empty($request->session_ids_not_interested_report)) {
                $branch->session_id_not_interested = $request->session_ids_not_interested_report[$branch->id];
            }
            if (isset($request->session_ids_withdrawal_report) && !empty($request->session_ids_withdrawal_report)) {
                $branch->session_id_withdrawal = $request->session_ids_withdrawal_report[$branch->id];
            }
        }


        if ($request->ajax()) {
            $view = view('backend.reports.withdrawals.partial', compact('branches', 'betweenDates', 'onDate', 'reasons'))->render();
            return Response()->json(['view' => $view, 'between_dates' => $betweenDates, 'on_date' => $onDate]);
        }
        return view('backend.reports.withdrawals.index', compact('branches', 'reasons'));
    }

    public function walkInSourcesReport(Request $request)
    {
        $betweenDates = $onDate = null;
        $branches = Branch::where('status_id', 1);
        if (auth()->user()->hasRole('campus_head')) {
            $branchID = auth()->user()->employee->branch_id;
            $branches = $branches->where('id', $branchID);
        } else {
            if ($request->has('branch_ids')) {
                if (!empty($request->branch_ids)) {
                    $branches = $branches->whereIn('id', $request->branch_ids);
                }
            }
        }

        if ($request->has('date_range')) {
            if (!empty($request->date_range)) {
                $dates = explode(" to ", $request->date_range);
                if (count($dates) > 1) {
                    $betweenDates = $dates;
                } else if (!empty($dates[0])) {
                    $onDate = $dates[0];
                }
            }
        }


        $branches = $branches->get(); // loop
        $newAllBranchData = [];
        foreach ($branches as $branch) {
            if (isset($request->session_ids_bss_matric) && !empty($request->session_ids_bss_matric)) {
                $branch->session_id_bss_matric = $request->session_ids_bss_matric[$branch->id];
            }
            if (isset($request->session_ids_target_data) && !empty($request->session_ids_target_data)) {
                $branch->session_id_target_data = $request->session_ids_target_data[$branch->id];
            }
            if (isset($request->session_ids_joining_probablity) && !empty($request->session_ids_joining_probablity)) {
                $branch->session_id_joining_probablity = $request->session_ids_joining_probablity[$branch->id];
            }
        }


        $sources = Source::where('status_id', 1)->get();
        $leadStatuses = LeadStatus::where('status_id', 1)->get();
        $leadTypes = LeadType::where('status_id', 1)->get();

        if ($request->ajax()) {
            $view = view('backend.reports.walk-in-sources.partial', compact('branches', 'sources', 'leadStatuses', 'leadTypes', 'betweenDates', 'onDate'))->render();
            return Response()->json(['view' => $view, 'between_dates' => $betweenDates, 'on_date' => $onDate]);
        }

        return view('backend.reports.walk-in-sources.index', compact('branches', 'sources', 'leadStatuses', 'leadTypes'));
    }

    public function misReport(Request $request)
    {
        $betweenDates = $onDate = null;
        $branches = Branch::with('targets')->where('status_id', 1);
        $branch_ids = $request->branch_ids;
        if (auth()->user()->hasRole('campus_head')) {
            $branchID = auth()->user()->employee->branch_id;
            $branches = $branches->where('id', $branchID);
        } else {
            if ($request->has('branch_ids')) {
                if (!empty($request->branch_ids)) {
                    $branches = $branches->whereIn('id', $request->branch_ids);
                }
            }
        }

        if ($request->has('date_range')) {
            if (!empty($request->date_range)) {
                $dates = explode(" to ", $request->date_range);
                if (count($dates) > 1) {
                    $betweenDates = $dates;
                } else if (!empty($dates[0])) {
                    $onDate = $dates[0];
                }
            }
        }
        $branches = $branches->get(); // loop
        $newAllBranchData = [];
        foreach ($branches as $branch) {
            if (isset($request->session_ids_business) && !empty($request->session_ids_business)) {
                $branch->session_id_business = $request->session_ids_business[$branch->id];
            }
            if (isset($request->session_ids_business_part2) && !empty($request->session_ids_business_part2)) {
                $branch->session_id_business_part2 = $request->session_ids_business_part2[$branch->id];
            }
        }
        $courses = Course::where('status_id', 1)->get();
        $totalPromotionAdmissionCount = PromotionalAdmission::sum('no_of_promotional_admissions');
        $addContinueVal = $request->addContinue;
        if ($request->ajax()) {
            $selectedCourseIDs = [];
            if (!empty($request->course_ids)) {
                $selectedCourseIDs = $request->course_ids;
            }
            $view = view('backend.reports.mis.partial', compact('branches', 'betweenDates', 'onDate', 'selectedCourseIDs', 'courses', 'addContinueVal', 'totalPromotionAdmissionCount', 'branch_ids'))->render();
            return Response()->json(['view' => $view, 'between_dates' => $betweenDates, 'on_date' => $onDate]);
        }
        return view('backend.reports.mis.index', compact('branches', 'courses', 'totalPromotionAdmissionCount'));
    }
}
