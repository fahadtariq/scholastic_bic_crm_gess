<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Imports\OutReachImport;
use App\Models\OutreachData;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class OutReachDataController extends Controller
{
    //it returns main page and list of states  19/05/2022 by Jan Muhammad
    public function index(Request $request)
    {
        $inputs = $request->all();
        $pageLength = 10;
        $outReachData = OutreachData::orderBy('id', 'DESC');

        if (isset($inputs['page_length']) && $inputs['page_length'] != '') {
            $pageLength = $inputs['page_length'];
            if ($pageLength == -1) {
                $pageLength = $outReachData->count();
            }
        }

        if (!empty($inputs['student_id'])) {
            $outReachData->where('student_id', $inputs['student_id']);
        }

        if (!empty($inputs['source'])) {
            $outReachData->where('source', $inputs['source']);
        }

        if (!empty($inputs['student_name'])) {
            $outReachData->where('student_name', 'LIKE', '%' . $inputs['student_name'] . '%');
        }

        if (!empty($inputs['year'])) {
            $outReachData->where('year', $inputs['year']);
        }

        if (isset($inputs['date_range']) && $inputs['date_range'] != '') {
            $dates = explode(" to ", $inputs['date_range']);
            if (count($dates) > 1) {
                $outReachData = $outReachData->whereDate('received_date', '>=', $dates[0])
                    ->whereDate('received_date', '<=', $dates[1]);
            } else {
                $outReachData = $outReachData->whereDate('received_date', $dates[0]);
            }
        }

        $outReachDataCounts = $outReachData->count();
        $outReachData = $outReachData->paginate($pageLength);
        $outReachData->appends($request->all())->render();

        if ($request->ajax()) {
            $view = view('backend.outreach.partial', ['outReachData' => $outReachData])->render();
            $pagination = view('layouts.pagination_common', ['pagination' => $outReachData])->render();
            return response()->json(['view' => $view, 'pagination' => $pagination, 'dbCounter' => $outReachDataCounts]);
        }
        return view('backend.outreach.index', compact('outReachData', 'outReachDataCounts'));
    }

    public function create()
    {
        return view('backend.outreach.create');
    }

    //it returns main page and store new states  19/05/2022 by Jan Muhammad
    public function store(Request $request)
    {
        $request->validate([
            'student_id' => 'required',
            'student_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'contact_number' => 'required|digits_between:8,15',
            'school' => 'required|max:255',
            'class' => 'required|max:255',
            'section' => 'required|max:255',
            'session_info' => 'required|max:255',
            'city' => 'nullable|max:255',
            'address' => 'nullable',
            'source' => 'required',
            'year' => 'required|integer',
            'received_date' => 'required'
        ]);

        $inputs = [
            'city_name' => $request->city,
            'student_id' => $request->student_id,
            'student_name' => $request->student_name,
            'school' => $request->school,
            'class' => $request->class,
            'section' => $request->section,
            'session' => $request->session_info,
            'contact_number' => $request->contact_number,
            'address' => $request->address,
            'email' => $request->email,
            'year' => $request->year,
            'source' => $request->source,
            'received_date' => $request->received_date,
            'remarks' => $request->remarks
        ];

        OutreachData::create($inputs);
        return redirect()->route('outreach.index')
            ->with('success', 'Out Reach Data submitted successfully.');
    }


    //it returns edit page  19/05/2022 by Jan Muhammad
    public function edit($id)
    {
        $outReachData = OutreachData::findOrFail($id);
        return view('backend.outreach.edit', compact('outReachData'));
    }

    //it returns main page and update state  19/05/2022 by Jan Muhammad
    public function update(Request $request)
    {
        $request->validate([
            'student_id' => 'required',
            'student_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'contact_number' => 'required|digits_between:8,15',
            'school' => 'required|max:255',
            'class' => 'required|max:255',
            'section' => 'required|max:255',
            'session_info' => 'required|max:255',
            'city' => 'nullable|max:255',
            'address' => 'nullable',
            'source' => 'required',
            'year' => 'required|integer',
            'received_date' => 'required'
        ]);

        $inputs = [
            'city_name' => $request->city,
            'student_id' => $request->student_id,
            'student_name' => $request->student_name,
            'school' => $request->school,
            'class' => $request->class,
            'section' => $request->section,
            'session' => $request->session_info,
            'contact_number' => $request->contact_number,
            'address' => $request->address,
            'email' => $request->email,
            'year' => $request->year,
            'source' => $request->source,
            'received_date' => $request->received_date,
            'remarks' => $request->remarks
        ];

        $outReachData = OutreachData::find($request->id);
        $outReachData->update($inputs);
        return redirect()->route('outreach.index')
            ->with('success', 'Out Reach Data updated successfully.');
    }

    //it returns main page and delete country  19/05/2022 by Jan Muhammad
    public function destroy(Request $request)
    {
        $outReachData = OutreachData::findOrFail($request->id);
        try {
            return $outReachData->delete();
        } catch (QueryException $e) {
            print_r($e->errorInfo);
        }
    }

    public function importOutReachData(Request $request)
    {
        $validations = [
            'file' => 'required|file|max:40000|mimes:xlsx,xls'
        ];
        $validator = Validator::make($request->all(), $validations);
        if($validator->fails()){
            return redirect()->back()->with('error', $validator->getMessageBag()->first());
        }
        Excel::import(new OutReachImport($request), $request->file('file'));
        return back()->with('success', 'Data Imported Successfully.');
    }
}
