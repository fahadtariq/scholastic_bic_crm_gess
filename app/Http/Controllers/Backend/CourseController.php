<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Status;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DataTables;

class CourseController extends Controller
{

    //it returns main page and list of courses  20/05/2022 by Jan Muhammad
    public function index(Request $request)
    {
        $statuses = Status::get();
        if ($request->ajax()) {
            $data = Course::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.courses.actions', ['row' => $row]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('backend.courses.index', compact('statuses'));
    }

    //it returns create page to add new course  20/05/2022 by Jan Muhammad
    public function create()
    {
        $statuses = Status::get();
        return view('backend.courses.create', compact('statuses'));
    }

    //it returns main page and store new course  20/05/2022 by Jan Muhammad
    public function store(Request $request)
    {
        $request->validate([
            'beams_id' => 'nullable|max:255',
            'name' => 'required|max:255|unique:courses,name',
            'status_id' => 'required|integer',
        ]);

        Course::create($request->all());
        return redirect()->route('course.index')
            ->with('success', 'Course created successfully.');
    }


    //it returns edit page  20/05/2022 by Jan Muhammad
    public function edit($id)
    {
        $course = Course::find($id);
        $statuses = Status::get();
        return view('backend.courses.edit', ['course' => $course, 'statuses' => $statuses]);
    }

    //it returns main page and update course  20/05/2022 by Jan Muhammad
    public function update(Request $request)
    {
        $course = Course::find($request->id);
        $request->validate([
            'beams_id' => 'nullable|max:255',
            'name' => 'required|max:255|unique:courses,name,' . $request->id,
            'status_id' => 'required|integer',
        ]);

        $course->update($request->all());
        return redirect()->route('course.index')
            ->with('success', 'Course updated successfully.');
    }

    //it returns main page and delete course  20/05/2022 by Jan Muhammad
    public function destroy(Request $request)
    {
        $course = Course::find($request->id);
        try {
            $course->leads()->delete();
            $course->delete();

        } catch (QueryException $e) {
            print_r($e->errorInfo);
        }
    }
}
