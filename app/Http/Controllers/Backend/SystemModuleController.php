<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\SystemModule;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SystemModuleController extends Controller
{
    /**
     * @description function to show systems listings, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = SystemModule::with(['parent'])->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    return view('backend.settings.system_modules.actions', ['row' => $row]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $parents = SystemModule::where('parent_id', '<=>')->orderBy('name')->get(['id','name']);
        return view('backend.settings.system_modules.system_modules',['parents' => $parents]);
    }

    /**
     * @description function to store new system module record, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        SystemModule::create($request->only('name', 'parent_id', 'description'));

        return redirect()->route('system-modules.index')
            ->with('success', 'System Module created successfully.');
    }

    /**
     * @description function to show edit system module view, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function edit(SystemModule $systemModule)
    {
        $parents = SystemModule::where('parent_id', '<=>')->orderBy('name')->get(['id','name']);
        return view('backend.settings.system_modules.system_modules', ['systemModule' => $systemModule,'parents' => $parents]);
    }

    /**
     * @description function to update system module record, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function update(Request $request, SystemModule $systemModule)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $systemModule->update($request->all());

        return redirect()->route('system-modules.index')
            ->with('success', 'System Modules updated successfully.');
    }

    /**
     * @description function to delete system module record, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function destroy(SystemModule $systemModule)
    {
        try {
            return $systemModule->delete();
        } catch (QueryException $e) {
            print_r($e->errorInfo);
        }
    }
}
