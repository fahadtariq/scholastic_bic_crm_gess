<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\PromotionalAdmission;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Type\Integer;

class PromotionalAdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::orderBy('name', 'desc')->get();
        if (\request()->ajax()) {
            $promotional_admissions = isset(\request()->omit_promotional_admission) ? PromotionalAdmission::with('branch')->where('id', '!=', \request()->omit_promotional_admission) : PromotionalAdmission::with('branch')->select('*');
            return datatables()->of($promotional_admissions)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    return view('backend.promotional_admissions.actions', ['row' => $row]);
                })->rawColumns(['action'])
                ->make(true);
        }
        return view('backend.promotional_admissions.index', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validated = $this->validateRequest(Request()->all());
        try {
            PromotionalAdmission::create($validated);
            return redirect()->back()->with('success', 'Promotional Admission added successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong! Please try again later :(');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\PromotionalAdmission $promotionalAdmission
     * @return \Illuminate\Http\Response
     */
    public function show(PromotionalAdmission $promotionalAdmission)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\PromotionalAdmission $promotionalAdmission
     * @return \Illuminate\Http\Response
     */
    public function edit(PromotionalAdmission $promotionalAdmission)
    {
        $branches = Branch::orderBy('name', 'desc')->get();
        return view('backend.promotional_admissions.index', compact('promotionalAdmission', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\PromotionalAdmission $promotionalAdmission
     * @return \Illuminate\Http\Response
     */
    public function update(PromotionalAdmission $promotionalAdmission)
    {
        $this->validateRequest(\request()->all());
        try {
            $promotionalAdmission->update(\request()->all());
            return redirect()->route('promotional-admissions.index')->with('success', 'Promotional Admission updated successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong! Please try again later :(');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\PromotionalAdmission $promotionalAdmission
     * @return \Illuminate\Http\Response
     */
    public function destroy(PromotionalAdmission $promotionalAdmission)
    {
        try {
            $promotionalAdmission->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param PromotionalAdmission $promotionalAdmission
     * @param Integer $is_add
     * @return void
     */
    public function togglePromotionalLead(PromotionalAdmission $promotionalAdmission, $is_add)
    {
//        dd($promotionalAdmission, $is_add);
        try {
            $promotionalAdmission->update(['is_add_to_paid_lead' => $is_add]);

            if($is_add == 1)
                return redirect()->back()->with('success', 'Promotional Admission added to paid funnel successfully!');
            else
                return redirect()->back()->with('success', 'Promotional Admission removed from paid funnel successfully!');

        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong! Please try again later :(');
        }
    }

    protected function validateRequest($data)
    {
        $rules = [
            'branch_id' => 'required|max:255',
            'no_of_promotional_admissions' => 'required|max:255',
            'admission_date' => 'required|date'
        ];
        return Validator::validate($data, $rules);
    }
}
