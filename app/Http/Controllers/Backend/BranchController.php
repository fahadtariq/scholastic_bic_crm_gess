<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\BranchCourseTarget;
use App\Models\BranchTarget;
use App\Models\City;
use App\Models\ContactCode;
use App\Models\Country;
use App\Models\Course;
use App\Models\Lead;
use App\Models\State;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class BranchController extends Controller
{
    /**
     * @description it returns branches listing view, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $user = auth()->user();
            if (in_array('campus_head', $user->getUserRoleNames())) {
                $data = Branch::where('id', $user->employee->branch_id)->get();
            } else {
                $data = Branch::all();
            }

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.branches.actions', compact('row'));
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('backend.branches.index');
    }

    /**
     * @description it returns branch create view, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function create()
    {
        $countries = Country::where('status_id', 1)->get();
        $statuses = Status::all();
        $contactCodes = ContactCode::where('status_id', 1)->get();
        $courses = Course::where('status_id', 1)->get();
        return view('backend.branches.create', compact('countries', 'statuses', 'contactCodes', 'courses'));
    }

    /**
     * @description function to save new branch record, @Date: 19/05/2022, Author: @Hammad Shoaib Khan
     */
    public function store(Request $request)
    {
        $this->validations($request);
        $branch = Branch::create($request->except('_token', 'course_ids'));
        if ($branch->wasRecentlyCreated === true) {
            $branch->courses()->sync($request->course_ids);
        }
        return redirect()->route('branches.index')->with('success', 'Branch is created successfully.');
    }

    /**
     * @description function to load country all active states, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function countryStates(Request $request)
    {
        if ($request->ajax()) {
            $countryID = $request->country_id;
            $states = State::where(['country_id' => $countryID, 'status_id' => 1])->get();
            return view('backend.branches.partials.states', compact('states', 'countryID'));
        }
    }

    /**
     * @description function to load state all active cities, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function stateCities(Request $request)
    {
        if ($request->ajax()) {
            $cities = City::where(['country_id' => $request->country_id, 'state_id' => $request->state_id, 'status_id' => 1])->get();
            return view('backend.branches.partials.cities', compact('cities'));
        }
    }

    /**
     * @description function to show edit branch view, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function edit($id)
    {
        if ($id != null) {
            $branch = Branch::findOrFail($id);
            $loggedInUser = auth()->user();
            if (!$loggedInUser->hasRole('super_admin')) {
                $userAssignedBranchID = $loggedInUser->employee->branch->id;
                if ($userAssignedBranchID != $branch->id) {
                    return redirect('404');
                }
            }
            if (!empty($branch)) {
                $countries = Country::where('status_id', 1)->get();
                $statuses = Status::all();
                $contactCodes = ContactCode::where('status_id', 1)->get();
                $courses = Course::where('status_id', 1)->get();
                $branchCourses = $branch->courses->pluck('id')->toArray();
                return view('backend.branches.edit', compact('branch', 'countries', 'contactCodes', 'statuses', 'courses', 'branchCourses'));
            }
        }
        return redirect()->route('branches.index');
    }

    /**
     * @description function to update branch record, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function update(Request $request)
    {
        $this->validations($request);
        $branch = Branch::find($request->id);
        $branch->update($request->except('_token', 'id', 'course_ids'));
        if ($branch) {
            $branch->courses()->sync($request->course_ids);
        }
        return back()->with('success', 'Branch updated successfully.');
    }
    /**
     * @description function to update branch record, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function updateTarget(Request $request)
    {
        $branchTarget = BranchTarget::find($request->id);
        BranchTarget::where('branch_id', $branchTarget->branch_id)->update(['is_default' => 0]);
        $branchTarget->update(['is_default' => $request->is_active]);
    }

    /**
     * @description function to delete branch record, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    public function delete(Request $request)
    {
        try {
            $branch = Branch::find($request->id);
            $branch->courses()->detach();
            $branch->leads()->delete();
            $branch->employees()->delete();
            $branch->targets()->delete();
            $branch->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function sessions(Request $request)
    {
        $branchID = $request->branch_id;
        $branchSession = BranchTarget::where(['branch_id'=>$branchID])->latest()->get();
        if($request->has('data_filter') && $request->data_filter == 'generate-lead'){
            $branchSession = BranchTarget::where(['branch_id'=>$branchID,'is_default'=>1])->latest()->get();
        }
        return response()->json($branchSession);
    }

    /**
     * @description function to validate branch form inputs, @Date: 19/05/2022, @Author: Hammad Shoaib Khan
     */
    private function validations($request)
    {
        $request->validate([
            'beams_id' => 'required|integer',
            'name' => 'required|max:255',
            'link' => 'nullable|max:255',
            'country_id' => 'required|integer',
            'state_id' => 'required|integer',
            'city_id' => 'required|integer',
            'contact_code_id' => 'required|integer',
            'contact_number' => 'required|digits_between:8,15',
            'course_ids' => 'required'
        ], [
            'name.required' => 'Name is required!',
            'country_id.required' => 'Country is required!',
            'state_id.required' => 'State is required!',
            'city_id.required' => 'City is required!',
            'contact_code_id.required' => 'Contact code is required!',
            'contact_number.required' => 'Contact number is required!',
            'course_ids.required' => 'Course is required!'
        ]);
    }

    public function loadCourses(Request $request)
    {
        if ($request->ajax()) {
            $branchCourses = collect();
            if ($request->id != null) {
                $branchCourses = Branch::find($request->id)->courses;
            }
            return view('backend.leads.partials.load-courses', compact('branchCourses'));
        }
    }

    public function loadEmployees(Request $request)
    {
        if ($request->ajax()) {
            $branchEmployees = collect();
            if ($request->id != null) {
                $branchEmployees = Branch::find($request->id)->employees->where('status_id', 1)->map(function ($query) {
                    if ($query->user->hasRole(['admission_advisor'])) {
                        return $query;
                    }
                })->filter();
                $autoSelectedEmployeeId = Lead::employeeAutoAssign($request->id);
            }
            return view('backend.leads.partials.load-employees', compact('branchEmployees', 'autoSelectedEmployeeId'));
        }
    }

    public function targets(Request $request)
    {
        $branchTargets = BranchTarget::where('branch_id', $request->id)->latest()->get();
        return Response()->json($branchTargets);
    }

    public function targetStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'branch_id' => 'required',
            'name' => 'required',
            'target' => 'required|numeric',
            'date_range' => 'required'
        ]);

        if ($validator->fails()) {
            return Response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 400);
        }

        $dateRangeExplode = explode('to', $request->date_range);
        if (isset($dateRangeExplode[1])) {
            $from = $dateRangeExplode[0];
            $to = $dateRangeExplode[1];
        } else {
            $from = $to = $dateRangeExplode[0];
        }

        BranchTarget::updateOrCreate([
            'id' => $request->id
        ], ['branch_id' => $request->branch_id, 'name' => $request->name, 'from' => $from, 'to' => $to, 'target' => $request->target]);
        $branchTargets = BranchTarget::where('branch_id', $request->branch_id)->latest()->get();
        return Response()->json($branchTargets);
    }

    public function editTarget(Request $request)
    {
        $branchTarget = BranchTarget::where('id', $request->id)->first();
        return Response()->json($branchTarget);
    }

    public function deleteTarget(Request $request)
    {
        $target = BranchTarget::where('id', $request->id);
        $branchID = $target->first()->branch_id;
        $target->delete();
        $branchTargets = BranchTarget::where('branch_id', $branchID)->latest()->get();
        return Response()->json($branchTargets);
    }

    public function saveCourseTarget(Request $request)
    {
        BranchCourseTarget::updateOrCreate([
            'session_id' => $request->session_id,
            'branch_id' => $request->branch_id,
            'course_id' => $request->course_id
        ], ['target' => $request->target]);
    }

    public function sessionCourses(Request $request)
    {
        $branch = Branch::where('id', $request->branch_id)->first();
        $sessionID = $request->session_id;
        return view('backend.branches.partials.load-session-courses', compact('branch', 'sessionID'));
    }
}
