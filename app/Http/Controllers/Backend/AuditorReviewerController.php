<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\AuditorReviewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class AuditorReviewerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AuditorReviewer::with('user')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    return view('backend.auditor_review.actions', ['row' => $row]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('backend.auditor_review.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.auditor_review.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request->all());

        try {
            $auditor_reviewer = AuditorReviewer::create([
                'user_id' => auth()->user()->id,
                ...$request->except('_token')
            ]);

            return redirect()->route('auditor-review.index')->with('success', 'CRM review created successfully');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', 'Something went wrong! Please try again later :(');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\AuditorReviewer $auditorReviewer
     * @return \Illuminate\Http\Response
     */
    public function show(AuditorReviewer $auditorReviewer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\AuditorReviewer $auditorReviewer
     * @return \Illuminate\Http\Response
     */
    public function edit(AuditorReviewer $auditor_review)
    {
        return view('backend.auditor_review.edit', compact('auditor_review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\AuditorReviewer $auditorReviewer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AuditorReviewer $auditor_review)
    {
        $this->validateRequest($request->all());

        try {
            $auditor_review->update([
                'user_id' => auth()->user()->id,
                ...$request->except(['_token', '_method'])
            ]);

            return redirect()->route('auditor-review.index')->with('success', 'CRM review updated successfully');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', 'Something went wrong! Please try again later :(');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\AuditorReviewer $auditorReviewer
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuditorReviewer $auditor_review)
    {
        try {
            $auditor_review->delete();
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @param $inputs
     * @return void
     */
    private function validateRequest($inputs)
    {
        $rules = [
            'review_date' => 'required|date_format:Y-m-d',
            'lead_count_acc' => 'required|boolean',
            'lead_assignment_acc' => 'required|boolean',
            'review_acc' => 'required|boolean',
            'admission_acc' => 'required|boolean',
            'outbound_call_acc' => 'required|boolean',
            'review_remarks' => 'string|max:1100',
        ];

        Validator::validate($inputs, $rules);
    }
}
