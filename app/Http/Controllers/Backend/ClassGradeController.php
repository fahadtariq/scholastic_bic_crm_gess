<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ClassGrade;
use App\Models\Status;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ClassGradeController extends Controller
{
    /**
     * @description function to show classes listing view, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function index(Request $request)
    {
        $statuses = Status::all();
        if ($request->ajax()) {
            $data = ClassGrade::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.class_grades.actions', ['row' => $row]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('backend.class_grades.index', compact('statuses'));
    }

    /**
     * @description function to store new class record, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function store(Request $request)
    {
        $request->validate([
            'beams_id' => 'required|integer',
            'name' => 'required|max:255|unique:class_grades,name',
            'status_id' => 'required|integer',
        ]);

        ClassGrade::create($request->except('_token'));
        return redirect()->route('class-grades.index')
            ->with('success', 'Class has been created successfully.');
    }

    /**
     * @description function to show edit class record view, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function edit($id)
    {
        $class = ClassGrade::find($id);
        if (!empty($class)) {
            $statuses = Status::get();
            return view('backend.class_grades.index', compact('class', 'statuses'));
        }
        return redirect()->route('class-grades.index');
    }

    /**
     * @description function to update class record, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function update(Request $request)
    {
        $request->validate([
            'beams_id' => 'required|integer',
            'name' => 'required|max:255|unique:class_grades,name,' . $request->id,
            'status_id' => 'required|integer',
        ]);
        $class = ClassGrade::find($request->id);
        $class->update($request->except('_token', 'id'));
        return redirect()->route('class-grades.index')
            ->with('success', 'Class has been updated successfully.');
    }

    /**
     * @description function to delete class record, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function delete(Request $request)
    {
        try {
            $classGrade = ClassGrade::find($request->id);
            $classGrade->students()->delete();
            $classGrade->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
