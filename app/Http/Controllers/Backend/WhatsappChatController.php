<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\WhatsappChat;
use App\Models\WhatsappChatMessage;
use Illuminate\Http\Request;

class WhatsappChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $chats = WhatsappChat::
        with(array('messages' => function($query) {
            $query->orderBy('created_at', 'DESC');
        }))->get();

        return view('backend.leads.whatsapp_chat_index', compact('chats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function chat(Request $request)
    {
        $chatId = $request->input('chat_id');

        $messages = WhatsappChatMessage::where('chat_id', $chatId)
            ->orderBy('created_at', 'ASC')
            ->get();

        //Get to or student number
        /*$messages = WhatsappChatMessage::where('chat_id', $chatId)
            ->orderBy('created_at', 'ASC')
            ->get();*/


        $html = view('backend.leads.whatsapp_chat_history', compact('messages', 'chatId'))->render();

        return $html;
    }

    /**
     * Send Email/SMS to selected compain.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function send(Request $request)
    {
        $businessNumber = env('SENDER_WHATSAPP_NUMBER');
        $leadId = 0;
        $chatId = $request->chat_id;
        //$toNumber = $request->number;
        $message = $request->message;

        //Get send to number from chat
        $chatRecord = WhatsappChat::find($chatId);
        //Get to number that is different from business number
        if($chatRecord->user1_phone_number != $businessNumber){
            $toNumber = $chatRecord->user1_phone_number;
        }else{
            $toNumber = $chatRecord->user2_phone_number;
        }

        $attachmentUrl = isset($request->attachment_url) ? $request->attachment_url : '';
        $attachmentArray = array_filter(explode(',', $attachmentUrl));

        //This is temporary because message was not sending to whatsapp because of newlines & spaces.
        //$messageBody = "Welcome to Homebridge By Beaconhouse, we are thrilled to assist you.\nPlease Share Your Details:\nName: \nEmail: \nSchool: \nPlease take a few moments to fill out the information above. We value your time and appreciate your interest in Homebridge By Beaconhouse. Our Team will get in touch with you shortly.";

        $messageStatus = sendWhatsappMessage($businessNumber, $toNumber, $leadId, $message, $attachmentArray );

        if($messageStatus){
            return Response()->json(array(
                'success' => true,
                'errors' => '',
                'message' => 'Message sent successfully!. Message Id: ' . $messageStatus
            ), 200);
        }else{
            return Response()->json(array(
                'success' => false,
                'errors' => 'Unable to send message. Either number format is wrong or there\'s no whatsapp on this number',
                'message' => 'Unable to send message. Either number format is wrong or there\'s no whatsapp on this number',
            ), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

    }
}
