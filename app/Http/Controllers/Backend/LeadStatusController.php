<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\LeadStatus;
use App\Models\Status;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class LeadStatusController extends Controller
{
    /**
     * @description function to show lead statues listing view, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function index(Request $request)
    {
        $statuses = Status::all();
        if ($request->ajax()) {
            $data = LeadStatus::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.lead_statuses.actions', ['row' => $row]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }

        return view('backend.lead_statuses.index', compact('statuses'));
    }

    /**
     * @description function to store new lead status record, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:lead_statuses,name',
            'status_id' => 'required|integer',
        ]);

        LeadStatus::create($request->except('_token'));

        return redirect()->route('lead-status.index')
            ->with('success', 'Lead Type has been created successfully.');
    }

    /**
     * @description function to show edit lead status record view, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function edit($id)
    {
        $lead_status = LeadStatus::find($id);
        if (!empty($lead_status)) {
            $statuses = Status::get();
            return view('backend.lead_statuses.index', compact('lead_status', 'statuses'));
        }
        return redirect()->route('lead-status.index');
    }

    /**
     * @description function to update lead status record, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:lead_statuses,name,' . $request->id,
            'status_id' => 'required|integer',
        ]);
        $lead_status = LeadStatus::find($request->id);

        $lead_status->update($request->except('_token', 'id'));

        return redirect()->route('lead-status.index')
            ->with('success', 'Lead Status has been updated successfully.');
    }

    /**
     * @description function to delete lead status record, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function delete(Request $request)
    {
        try {
            $leadStatus = LeadStatus::find($request->id);
            $leadStatus->leads()->delete();
            $leadStatus->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
