<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\LeadType;
use App\Models\Status;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class LeadTypeController extends Controller
{
    /**
     * @description function to show lead types listing view, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function index(Request $request)
    {
        $statuses = Status::all();
        if ($request->ajax()) {
            $data = LeadType::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.lead_types.actions', ['row' => $row]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }

        return view('backend.lead_types.index', compact('statuses'));
    }

    /**
     * @description function to store new lead type record, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:lead_types,name',
            'status_id' => 'required|integer',
        ]);

        LeadType::create($request->except('_token'));

        return redirect()->route('lead-type.index')
            ->with('success', 'Lead Type has been created successfully.');
    }

    /**
     * @description function to show edit lead type record view, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function edit($id)
    {
        $lead_type = LeadType::find($id);
        if (!empty($lead_type)) {
            $statuses = Status::get();
            return view('backend.lead_types.index', compact('lead_type', 'statuses'));
        }
        return redirect()->route('lead-type.index');
    }

    /**
     * @description function to update lead type record, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:lead_types,name,' . $request->id,
            'status_id' => 'required|integer',
        ]);
        $class = LeadType::find($request->id);

        $class->update($request->except('_token', 'id'));

        return redirect()->route('lead-type.index')
            ->with('success', 'Lead Type has been updated successfully.');
    }

    /**
     * @description function to delete lead type record, @Date: 24/05/2022, @Author: Jan Muhammad Mirza
     */
    public function delete(Request $request)
    {
        try {
            $leadType = LeadType::find($request->id);
            $leadType->leads()->delete();
            $leadType->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
