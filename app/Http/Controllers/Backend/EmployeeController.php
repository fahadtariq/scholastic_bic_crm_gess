<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Mail\EmployeeRegisteredEmail;
use App\Models\Branch;
use App\Models\ContactCode;
use App\Models\Employee;
use App\Models\Role;
use App\Models\Source;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;
use App\Models\Status;

class EmployeeController extends Controller
{
    /**
     * @description function to show employees listing view, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $user = auth()->user();
            $data = Employee::with(['branch', 'user']);

            if ($request->branch_id && $request->branch_id > 0) {
                $data->where('branch_id', $request->branch_id);
            }

            if ($request->role_id && $request->role_id > 0) {
                $data->whereHas('user', function ($query) use ($request) {
                    $query->whereHas('roles', function ($query) use ($request) {
                        $query->where('id', $request->role_id);
                    });
                });
            }

            if ($user->hasRole('super_admin')) {
                $data->where('employees.user_id', '!=', $user->id);
            } else {
                $data->where('employees.user_id', '!=', $user->id)
                    ->where('branch_id', $user->employee->branch_id);
            }

            if (isset($request->mySearch)) {
                $data->whereHas('user', function ($query) use ($request) {
                    $query->where('email', 'like', '%' . $request->mySearch . '%');
                });
            }

            $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('branch', function ($row) {
                    return $row->branch->name;
                })
                ->addColumn('first_name', function ($row) {
                    return $row->first_name;
                })
                ->addColumn('last_name', function ($row) {
                    return $row->last_name;
                })
                ->addColumn('email', function ($row) {
                    return $row->user->email;
                })
                ->addColumn('role', function ($row) {
                    return implode(',', $row->user->roles()->pluck('display_name')->toArray());
                })
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.employees.actions', compact('row'));
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        $branches = Branch::all();
        $roles = Role::whereNotIn('id', [1, 4])->get();
        return view('backend.employees.index', compact('branches', 'roles'));
    }

    /**
     * @description function to show create employee view, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function create()
    {
        $branches = Branch::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $contactCodes = ContactCode::where('status_id', 1)->get();
        $statuses = Status::all();
        $roles = Role::whereNotIn('name',['super_admin', 'social_media_manager'])->get();
        return view('backend.employees.create', compact('branches', 'contactCodes', 'statuses', 'roles'));
    }

    /**
     * @description function to store new employee record, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function store(Request $request)
    {
        $this->validations($request);
        $usersTableInputs = [
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ];

        $user = User::create($usersTableInputs);

        if ($user->wasRecentlyCreated === true) {
            $user->roles()->sync($request->role_ids);
            $loggedInUser = auth()->user();
            $branchID = (isset($request->branch_id)) ? $request->branch_id : null;
            if (!$loggedInUser->hasRole('super_admin')) {
                $branchID = $loggedInUser->employee->branch_id;
            }
            $employeeInputs = [
                'user_id' => $user->id,
                'branch_id' => $branchID,
                'employee_beams_id' => $request->employee_beams_id,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'contact_code_id' => $request->contact_code_id,
                'contact_number' => $request->contact_number,
                'status_id' => $request->status_id,
                'address' => $request->address,
                'employee_code' => $request->branch_id . rand(100, 999) . $user->id
            ];
            $employee = Employee::create($employeeInputs);
            $password = $request->password;
            Mail::to($user->email)->send(new EmployeeRegisteredEmail($employee, $password));
            return redirect()->route('employees.index')->with('success', 'Employee has been created successfully.');
        }
        return back()->with('error', 'Something went wrong. Please try again!');
    }

    /**
     * @description function to show edit employee record view, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        if (!empty($employee)) {

            $loggedInUser = auth()->user();
            if (!$loggedInUser->hasRole('super_admin')) {
                $branchEmployeesIDs = $loggedInUser->employee->branch->employees->pluck('id')->toArray();
                if (!in_array($id, $branchEmployeesIDs)) {
                    return redirect('404');
                }
            }

            $branches = Branch::where('status_id', 1)->orderBy('name', 'ASC')->get();
            $contactCodes = ContactCode::where('status_id', 1)->get();
            $statuses = Status::all();
            $roles = Role::whereNotIn('name',['super_admin', 'social_media_manager'])->get();
            $employeeRoles = [];
            if (count($employee->user->roles) > 0) {
                foreach ($employee->user->roles as $userRole) {
                    array_push($employeeRoles, $userRole->id);
                }
            }
            return view('backend.employees.edit', compact('employee', 'branches', 'contactCodes', 'statuses', 'roles', 'employeeRoles'));
        }
        return redirect()->route('employees.index');
    }

    /**
     * @description function to update employee record, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function update(Request $request)
    {
        $this->validations($request, $request->user_id);
        $usersTableInputs = [
            'name' => $request->first_name . ' ' . $request->last_name,
        ];
        $user = User::find($request->user_id);
        $userUpdated = $user->update($usersTableInputs);
        if ($userUpdated) {
            $user->roles()->sync($request->role_ids);
            $employeeInputs = [
                'employee_beams_id' => $request->employee_beams_id,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'contact_code_id' => $request->contact_code_id,
                'contact_number' => $request->contact_number,
                'status_id' => $request->status_id,
                'address' => $request->address
            ];
            if (auth()->user()->hasRole('super_admin')) {
                $employeeInputs['branch_id'] = $request->branch_id;
            }
            Employee::find($request->id)->update($employeeInputs);
            return back()->with('success', 'Employee has been updated successfully.');
        }
        return back()->with('error', 'Something went wrong. Please try again!');
    }

    /**
     * @description function to delete employee record, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    public function delete(Request $request)
    {
        try {
            $employee = Employee::find($request->id);
            $employeeUser = $employee->user;
            $employee->delete();
            $employeeUser->roles()->detach();
            $employee->user->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @description function to validate employee input fields, @Date: 23/05/2022, @Author: Hammad Shoaib Khan
     */
    private function validations($request, $update = 0)
    {
        $validationArr = [
            'first_name' => 'required|max:255',
            'contact_code_id' => 'required|integer',
            'contact_number' => 'required|digits_between:8,15',
            'status_id' => 'required'
        ];
        if (!$update) {
            $validationArr['email'] = 'required|email|unique:users';
            $validationArr['password'] = 'required';
        }
        $request->validate($validationArr, [
            'first_name.required' => 'First name is required!',
            'email.required' => 'Email is required!',
            'password.required' => 'Password is required!',
            'contact_code_id.required' => 'Contact code is required!',
            'contact_number.required' => 'Contact number is required!',
            'status_id.required' => 'Select the status!'
        ]);
    }
}
