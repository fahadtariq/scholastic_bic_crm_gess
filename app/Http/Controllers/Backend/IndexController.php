<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\BranchTarget;
use App\Models\Employee;
use App\Models\Lead;
use App\Models\LeadStatus;
use App\Models\PreviousLead;
use App\Models\PromotionalAdmission;
use App\Models\Source;
use App\Models\User;
use App\Models\Course;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\returnValue;

class IndexController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $leads = $branchesCount = $users = $employees = 0;
        if ($user->hasRole('super_admin|social_media_manager|reviewer')) {
            $leads = Lead::count();
            $branchesCount = Branch::count();
            $users = User::where('users.id', '!=', $user->id)->whereDoesntHave('employee')->count();
            $employees = Employee::count();
        } else if ($user->hasRole('campus_head|admin_officer')) {
            $leads = Lead::where('branch_id', $user->employee->branch->id)->count();
            $employees = $user->employee->branch->employees->where('user_id', '!=', $user->id)->count();
        } else if ($user->hasRole('admission_advisor')) {
            $leads = Lead::where('user_id', $user->id)->orWhere('employee_id', $user->employee->id)->count();
        }
        $userEmployee = User::where('id', '=', $user->id)->with('employee')->first();
        $branches = Branch::select('id', 'name')->where('status_id', 1)->get();
        $sources = Source::select('id', 'name')->where('status_id', 1)->get();
        $loggedInUser = auth()->user();
        $sessions = collect();
        if ($loggedInUser->hasRole('campus_head|admission_advisor')) {
            $sessions = BranchTarget::select('id', 'name')->where('branch_id', $loggedInUser->employee->branch->id)->orderBy('id', 'DESC')->get();
        }
        return view('backend.dashboard', compact('leads', 'branchesCount', 'users', 'employees', 'branches', 'userEmployee', 'sessions', 'sources'));
    }

    public function salesFunnel()
    {
        $user = auth()->user();
        $leads = $branchesCount = $users = $employees = 0;
        if ($user->hasRole('super_admin|social_media_manager|reviewer')) {
            $leads = Lead::count();
            $branchesCount = Branch::count();
            $users = User::where('users.id', '!=', $user->id)->whereDoesntHave('employee')->count();
            $employees = Employee::count();
        } else if ($user->hasRole('campus_head|admin_officer')) {
            $leads = Lead::where('branch_id', $user->employee->branch->id)->count();
            $employees = $user->employee->branch->employees->where('user_id', '!=', $user->id)->count();
        } else if ($user->hasRole('admission_advisor')) {
            $leads = Lead::where('user_id', $user->id)->orWhere('employee_id', $user->employee->id)->count();
        }
        $userEmployee = User::where('id', '=', $user->id)->with('employee')->first();
        $branches = Branch::with("promotion")->select('id', 'name')->where('status_id', 1)->get();
        $sources = Source::select('id', 'name')->where('status_id', 1)->get();
        $loggedInUser = auth()->user();
        $sessions = collect();
        if ($loggedInUser->hasRole('campus_head|admission_advisor')) {
            $sessions = BranchTarget::select('id', 'name')->where('branch_id', $loggedInUser->employee->branch->id)->orderBy('id', 'DESC')->get();
        }
        $courses = Course::whereStatusId(1)->select("id", "name", "status_id")->get();
        $totalPromotionAdmissionCount = PromotionalAdmission::sum('no_of_promotional_admissions');
        $session_ids = BranchTarget::where('is_default', 1)->pluck('id');
        $continueCategoryCount = Lead::where(["category" => "continuing", "lead_status_id" => 6])->where('beams_id', "!=", null)->where('beams_paid_date', "!=", null)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count();
        $newCategoryCount = Lead::where(["category" => "new", "lead_status_id" => 6])->where('beams_id', "!=", null)->where('beams_paid_date', "!=", null)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count();
        $totalCategoryCount = Lead::where(["category" => "new", "category" => "continuing", "lead_status_id" => 6])->where('beams_id', "!=", null)->where('beams_paid_date', "!=", null)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count();
        return view('backend.saleFunnel', compact('leads', 'branchesCount', 'users', 'employees', 'branches', 'userEmployee', 'sessions', 'sources', 'totalPromotionAdmissionCount', 'courses', 'continueCategoryCount', 'newCategoryCount', 'totalCategoryCount'));
    }

    public function funnelData()
    {
        // Get funnel data
        $session_ids = BranchTarget::where('is_default', 1)->pluck('id');
        $data['branch_target_sum'] = BranchTarget::where('is_default', 1)->sum('target');
        $data['totalLeads'] = Lead::whereIn('lead_status_id', [2, 3, 1, 5, 6])->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count();
        $data['totalAssignLeads'] = Lead::where('lead_status_id', 2)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count();
        $data['totalContactedLeads'] = Lead::where('lead_status_id', 3)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count(); // processing
        $data['totalInterestedLeads'] = Lead::where('lead_status_id', 3)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count(); // pending
        $data['totalRegisteredLeads'] = Lead::where('lead_status_id', 5)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count();
        // For Registration
        $data['totalDroppedLeads'] = Lead::where('lead_status_id', 4)->whereIn('session_id', $session_ids)->where('beams_registration_date', "!=", null)->where('branch_id', '!=', 5)->count();
        $data['totalWithDrawlLeads'] = Lead::where('lead_status_id', 8)->whereIn('session_id', $session_ids)->where('beams_registration_date', "!=", null)->where('branch_id', '!=', 5)->count();
        //
        // dropped count
        // $data['dropped_lead_count'] = Lead::where('beams_registration_date', "!=", null)->whereIn('session_id', $session_ids)->where('lead_status_id', 4)->count();
        // // withdrawl count
        // $data['withdrawl_lead_count'] = Lead:
        $promotionAdmissionCount = PromotionalAdmission::where('is_add_to_paid_lead', 1)->sum('no_of_promotional_admissions');
        $data['totalPaidLeads'] = Lead::where('lead_status_id', 6)->where('beams_id', "!=", null)->where('beams_paid_date', "!=", null)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5)->count();
        // $data['totalPaidLeads'] = $data['totalPaidLeads'] + $promotionAdmissionCount;
        return response()->json(['data' => $data]);
    }

    public function filterFunnelData(Request $request)
    {
        $session_ids = BranchTarget::where('is_default', 1)->pluck('id');
        $data['branch_target_sum'] = BranchTarget::where('is_default', 1);
        $data['totalLeads'] = Lead::whereIn('lead_status_id', [2, 3, 1, 5, 6])->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5);
        $data['totalAssignLeads'] = Lead::where('lead_status_id', 2)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5);
        $data['totalContactedLeads'] = Lead::where('lead_status_id', 3)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5); // processing
        $data['totalInterestedLeads'] = Lead::where('lead_status_id', 3)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5); // pending
        $data['totalRegisteredLeads'] = Lead::where('lead_status_id', 5)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5);
        //
        $data['totalDroppedLeads'] = Lead::where('lead_status_id', 4)->whereIn('session_id', $session_ids)->where('beams_registration_date', "!=", null)->where('branch_id', '!=', 5);
        $data['totalWithDrawlLeads'] = Lead::where('lead_status_id', 8)->whereIn('session_id', $session_ids)->where('beams_registration_date', "!=", null)->where('branch_id', '!=', 5);
        //
        $data['totalPaidLeads'] = Lead::where('lead_status_id', 6)->where('beams_id', "!=", null)->where('beams_paid_date', "!=", null)->whereIn('session_id', $session_ids)->where('branch_id', '!=', 5);
        $promotionAdmissionCount = PromotionalAdmission::sum('no_of_promotional_admissions');
        if (isset($request->branch_id) && !empty($request->branch_id)) {
            $data['totalLeads']->where("branch_id", $request->branch_id);
            $data['totalAssignLeads']->where("branch_id", $request->branch_id);
            $data['totalContactedLeads']->where("branch_id", $request->branch_id);
            $data['totalInterestedLeads']->where("branch_id", $request->branch_id);
            $data['totalRegisteredLeads']->where("branch_id", $request->branch_id);
            //
            $data['totalDroppedLeads']->where("branch_id", $request->branch_id);
            $data['totalWithDrawlLeads']->where("branch_id", $request->branch_id);
            //
            $data['totalPaidLeads']->where("branch_id", $request->branch_id);
            $data['branch_target_sum']->where("branch_id", $request->branch_id);
            $promotionAdmissionCount = PromotionalAdmission::where(['branch_id' => $request->branch_id])->sum('no_of_promotional_admissions');
        }
        if (isset($request->source_id) && !empty($request->source_id)) {
            $data['totalLeads']->where("source_id", $request->source_id);
            $data['totalAssignLeads']->where("source_id", $request->source_id);
            $data['totalContactedLeads']->where("source_id", $request->source_id);
            $data['totalInterestedLeads']->where("source_id", $request->source_id);
            //
            $data['totalDroppedLeads']->where("source_id", $request->source_id);
            $data['totalWithDrawlLeads']->where("source_id", $request->source_id);
            //
            $data['totalRegisteredLeads']->where("source_id", $request->source_id);
            $data['totalPaidLeads']->where("source_id", $request->source_id);
        }
        if (isset($request->course_id) && !empty($request->course_id)) {
            $data['totalLeads']->where("course_id", $request->course_id);
            $data['totalAssignLeads']->where("course_id", $request->course_id);
            $data['totalContactedLeads']->where("course_id", $request->course_id);
            $data['totalInterestedLeads']->where("course_id", $request->course_id);
            //
            $data['totalRegisteredLeads']->where("course_id", $request->course_id);
            $data['totalDroppedLeads']->where("course_id", $request->course_id);
            //
            $data['totalWithDrawlLeads']->where("course_id", $request->course_id);
            $data['totalPaidLeads']->where("course_id", $request->course_id);
        }
        if (isset($request->category) && !empty($request->category)) {
            $data['totalLeads']->where("category", $request->category);
            $data['totalAssignLeads']->where("category", $request->category);
            $data['totalContactedLeads']->where("category", $request->category);
            $data['totalInterestedLeads']->where("category", $request->category);
            //
            $data['totalRegisteredLeads']->where("category", $request->category);
            $data['totalDroppedLeads']->where("category", $request->category);
            //
            $data['totalWithDrawlLeads']->where("category", $request->category);
            $data['totalPaidLeads']->where("category", $request->category);
        }
        $data['totalLeads'] = $data['totalLeads']->count();
        $data['totalAssignLeads'] = $data['totalAssignLeads']->count();
        $data['totalContactedLeads'] = $data['totalContactedLeads']->count();
        $data['totalInterestedLeads'] = $data['totalInterestedLeads']->count();
        $data['totalRegisteredLeads'] = $data['totalRegisteredLeads']->count();
        //
        $data['totalDroppedLeads'] = $data['totalDroppedLeads']->count();
        $data['totalWithDrawlLeads'] = $data['totalWithDrawlLeads']->count();
        //
        $data['totalPaidLeads'] = $data['totalPaidLeads']->count();
        $data['promotionAdmissionCount'] = $promotionAdmissionCount;
        // $data['totalPaidLeads'] = $data['totalPaidLeads']->count()+$promotionAdmissionCount;
        $data['branch_target_sum'] = $data['branch_target_sum']->sum('target');
        return response()->json(['data' => $data]);
    }

    public function loadContent(Request $request)
    {
        if ($request->has('branch_id') && count($request->branch_id) > 1) {
            $request->session_id = null;
        }
        $user = auth()->user();
        $sources = Source::where('status_id', 1)->select('id', 'name')->get();
        $sourcesLabels = $sourcesCounts = [];
        if (count($sources) > 0) {
            foreach ($sources as $source) {
                $leadCountBySource = Lead::where('source_id', $source->id);
                if ($request->has('session_id') && $request->session_id != null) {
                    $leadCountBySource = $leadCountBySource->where('session_id', $request->session_id);
                }
                if (isset($request->branch_id)) {
                    $leadCountBySource = $leadCountBySource->whereIn('branch_id', $request->branch_id);
                }
                if (!$user->hasRole('super_admin|social_media_manager|reviewer')) {
                    $leadCountBySource = $leadCountBySource->where('branch_id', $user->employee->branch->id);
                    if (!$user->hasRole('campus_head|admin_officer')) {
                        $leadCountBySource->where(function ($query) use ($user) {
                            $query->where('user_id', $user->id)
                                ->orWhere('employee_id', $user->employee->id);
                        });
                    }
                    $leadCountBySource = $leadCountBySource->count();
                } else {
                    $leadCountBySource = $leadCountBySource->count();
                }
                array_push($sourcesLabels, $source->name);
                array_push($sourcesCounts, $leadCountBySource);
            }
        }

        $leadStatuses = LeadStatus::select('id', 'name')->where('status_id', 1)->get();
        $statusLabels = $statusCounts = [];
        if (count($leadStatuses) > 0) {
            foreach ($leadStatuses as $status) {
                $leadCountsByStatus = Lead::where('lead_status_id', $status->id);
                if ($request->has('session_id') && $request->session_id != null) {
                    $leadCountsByStatus = $leadCountsByStatus->where('session_id', $request->session_id);
                }

                if (isset($request->branch_id)) {
                    $leadCountsByStatus = $leadCountsByStatus->whereIn('branch_id', $request->branch_id);
                }

                if (!$user->hasRole('super_admin|social_media_manager|reviewer')) {
                    $leadCountsByStatus = $leadCountsByStatus->where('branch_id', $user->employee->branch->id);
                    if (!$user->hasRole('campus_head|admin_officer')) {
                        $leadCountsByStatus->where(function ($query) use ($user) {
                            $query->where('user_id', $user->id)
                                ->orWhere('employee_id', $user->employee->id);
                        });
                    }
                    $leadCountsByStatus = $leadCountsByStatus->count();
                } else {
                    $leadCountsByStatus = $leadCountsByStatus->count();
                }
                array_push($statusLabels, $status->name);
                array_push($statusCounts, $leadCountsByStatus);
            }
        }

        $branches = Branch::select('id', 'name')->where('status_id', 1);
        if (isset($request->branch_id)) {
            $branches = $branches->whereIn('id', $request->branch_id);
        }

        if ($user->hasRole('campus_head|admin_officer')) {
            $branches = $branches->where('id', $user->employee->branch->id);
        }
        $branches = $branches->get();
        $branchLabels = $branchTargets = $branchTargetAchieved = [];
        if (count($branches) > 0) {
            foreach ($branches as $branch) {
                if ($request->has('session_id') && $request->session_id != null) {
                    $branchLatestTarget = BranchTarget::where('branch_id', $branch->id)->where('id', $request->session_id)->latest()->first();
                } else {
                    $branchLatestTarget = BranchTarget::where('branch_id', $branch->id)->latest()->first();
                }
                if (!empty($branchLatestTarget)) {
                    $leadCounts = Lead::where(['lead_status_id' => 6, 'branch_id' => $branch->id])->where('session_id', $branchLatestTarget->id)->count();
                    array_push($branchLabels, $branch->name);
                    array_push($branchTargets, $branchLatestTarget->target);
                    array_push($branchTargetAchieved, $leadCounts);
                }
            }
        }
        return view('backend.dashboard.partials.content', compact('sourcesLabels', 'sourcesCounts', 'statusLabels', 'statusCounts', 'branchLabels', 'branchTargets', 'branchTargetAchieved'));
    }

    public function getPreviousDataReport(Request $request)
    {
        $date = preg_replace('/\(.*\)/', '', $request->date);
        $date = Carbon::parse($date);
        $data2022_23 = $this->get2022_23data($date, $request);
        $data2023_24 = $this->get2023_24data($date, $request);
        $data['rrr_bpc'] = (($data2023_24['current_year_target_bpc'] * $data2022_23['bpc_22_23_target_achieved']) / 100) - $data2023_24['bpc_23_24'];
        $data['rrr_isb'] = (($data2023_24['current_year_target_isb'] * $data2022_23['isb_22_23_target_achieved']) / 100) - $data2023_24['isb_23_24'];
        $data['rrr_lhr'] = (($data2023_24['current_year_target_lhr'] * $data2022_23['lhr_22_23_target_achieved']) / 100) - $data2023_24['lhr_23_24'];
        $data['rrr_fsb'] = (($data2023_24['current_year_target_fsb'] * $data2022_23['fsb_22_23_target_achieved']) / 100) - $data2023_24['fsb_23_24'];
        $data['rrr_total'] = (642 * round($data2022_23['total_22_23_target_achieved'], 2) / 100) - $data2023_24['total_23_24'];
        // dd($data2023_24);
        $formattedData = [];
        $formattedData[] = [
            $date->format('d-M'),
            $data2022_23['bpc_22_23']->count(),
            round($data2022_23['bpc_22_23_target_achieved'], 2) . "%",
            $data2023_24['bpc_23_24'],
            round($data2023_24['bpc_23_24_target_achieved'], 2) . "%",
            round($data['rrr_bpc']),
            $data2022_23['isb_22_23']->count(),
            round($data2022_23['isb_22_23_target_achieved'], 2) . "%",
            $data2023_24['isb_23_24'],
            round($data2023_24['isb_23_24_target_achieved'], 2) . "%",
            round($data['rrr_isb']),
            $data2022_23['lhr_22_23']->count(),
            round($data2022_23['lhr_22_23_target_achieved'], 2) . "%",
            $data2023_24['lhr_23_24'],
            round($data2023_24['lhr_23_24_target_achieved'], 2) . "%",
            round($data['rrr_lhr']),
            $data2022_23['fsb_22_23']->count(),
            round($data2022_23['fsb_22_23_target_achieved'], 2) . "%",
            $data2023_24['fsb_23_24'],
            round($data2023_24['fsb_23_24_target_achieved'], 2) . "%",
            round($data['rrr_fsb']),
            $data2022_23['total_22_23']->count(),
            round($data2022_23['total_22_23_target_achieved'], 2) . "%",
            $data2023_24['total_23_24'],
            round($data2023_24['total_23_24_target_achieved'], 2) . "%",
            round($data['rrr_total'])
        ];
        return DataTables::of($formattedData)
            ->escapeColumns([]) // Specify the index of the action column that contains raw HTML
            ->make(true);
    }

    public function get2022_23data($date, $request)
    {
        $startDate = Carbon::parse("06-04-2022"); // Date of the 2022 year
        $date = $date->format('d-m-') . '2022';
        $date = Carbon::parse($date);
        if ($request->dataType == "fall-to-fall" || $request->dataType == "add-continue") {
            $data['bpc_22_23'] = PreviousLead::where('branch', "BPC")->whereYear('admission_date', 2022)->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['isb_22_23'] = PreviousLead::where('branch', "BIC Islamabad Campus")->whereYear('admission_date', 2022)->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['lhr_22_23'] = PreviousLead::where('branch', "BIC Lahore Campus")->whereYear('admission_date', 2022)->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['fsb_22_23'] = PreviousLead::where('branch', "BIC Faisalabad Campus")->whereYear('admission_date', 2022)->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['total_22_23'] = PreviousLead::whereIn('branch', ["BIC Faisalabad Campus", "BIC Lahore Campus", "BIC Islamabad Campus", "BPC"])->whereYear('admission_date', 2022)->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);

            $data['bpc_22_23_target_achieved'] = ($data['bpc_22_23']->count() / 20) * 100;
            $data['isb_22_23_target_achieved'] = ($data['isb_22_23']->count() / 200) * 100;
            $data['lhr_22_23_target_achieved'] = ($data['lhr_22_23']->count() / 150) * 100;
            $data['fsb_22_23_target_achieved'] = ($data['fsb_22_23']->count() / 130) * 100;
            $data['total_22_23_target_achieved'] = ($data['total_22_23']->count() / 500) * 100;
        }

        if ($request->dataType == "year-to-year" || $request->dataType == "add-continue") {
            $data['bpc_22_23'] = PreviousLead::where('branch', "BPC")->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['isb_22_23'] = PreviousLead::where('branch', "BIC Islamabad Campus")->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['lhr_22_23'] = PreviousLead::where('branch', "BIC Lahore Campus")->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['fsb_22_23'] = PreviousLead::where('branch', "BIC Faisalabad Campus")->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['total_22_23'] = PreviousLead::whereIn('branch', ["BIC Faisalabad Campus", "BIC Lahore Campus", "BIC Islamabad Campus", "BPC"])->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);

            $data['bpc_22_23_target_achieved'] = ($data['bpc_22_23']->count() / 30) * 100;
            $data['isb_22_23_target_achieved'] = ($data['isb_22_23']->count() / 235) * 100;
            $data['lhr_22_23_target_achieved'] = ($data['lhr_22_23']->count() / 185) * 100;
            $data['fsb_22_23_target_achieved'] = ($data['fsb_22_23']->count() / 160) * 100;
            $data['total_22_23_target_achieved'] = ($data['total_22_23']->count() / 610) * 100;
        }

        if ($request->dataType == "spring-to-spring" || $request->dataType == "add-continue") {
            $data['bpc_22_23'] = PreviousLead::where('branch', "BPC")->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['isb_22_23'] = PreviousLead::where('branch', "BIC Islamabad Campus")->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['lhr_22_23'] = PreviousLead::where('branch', "BIC Lahore Campus")->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['fsb_22_23'] = PreviousLead::where('branch', "BIC Faisalabad Campus")->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);
            $data['total_22_23'] = PreviousLead::whereIn('branch', ["BIC Faisalabad Campus", "BIC Lahore Campus", "BIC Islamabad Campus", "BPC"])->whereBetween('paid_date', [$startDate->format('Y-m-d H:i:s'), $date->format('Y-m-d H:i:s')]);

            $data['bpc_22_23_target_achieved'] = ($data['bpc_22_23']->count() / 30) * 100;
            $data['isb_22_23_target_achieved'] = ($data['isb_22_23']->count() / 235) * 100;
            $data['lhr_22_23_target_achieved'] = ($data['lhr_22_23']->count() / 185) * 100;
            $data['fsb_22_23_target_achieved'] = ($data['fsb_22_23']->count() / 160) * 100;
            $data['total_22_23_target_achieved'] = ($data['total_22_23']->count() / 610) * 100;
        }

        return $data;
    }

    public function get2023_24data($date, $request)
    {
        $targetsBPC = BranchTarget::where(['branch_id' => 2, 'is_default' => 1]); // Potohar
        $targetsIsb = BranchTarget::where(['branch_id' => 1, 'is_default' => 1]); // ISB
        $targetsLhr = BranchTarget::where(['branch_id' => 3, 'is_default' => 1]); // LHR
        $targetsFsb = BranchTarget::where(['branch_id' => 4, 'is_default' => 1]); // FSB

        // $datetime = Carbon::parse($targetsIsb->first()->from); // Set as default for session year
        // $yearFromDatetime = $datetime->year;
        $date2023 = $date->format('d-m-') . '' . '2023';
        $date2024 = $date->format('d-m-') . '' . '2024';
        $date2023 = Carbon::parse($date2023);
        $date2024 = Carbon::parse($date2024);
        $startDate = Carbon::parse("26-01-2023");
        $endDate = Carbon::parse("29-02-2024");

        $data['bpc_23_24'] = DB::table('leads')
            ->where('session_id', 10)
            ->where('lead_status_id', 6)
            ->where('branch_id', 2)
            ->where('beams_id', "!=", null)
            ->where('beams_paid_date', "!=", null)
            ->whereBetween('beams_paid_date', [$startDate->format('Y-m-d H:i:s'), $date2023->format('Y-m-d H:i:s')]);
        $data['isb_23_24'] = DB::table('leads')
            ->where('session_id', 9)
            ->where('lead_status_id', 6)
            ->where('branch_id', 1)
            ->where('beams_id', "!=", null)
            ->where('beams_paid_date', "!=", null)
            ->whereBetween('beams_paid_date', [$startDate->format('Y-m-d H:i:s'), $date2023->format('Y-m-d H:i:s')]);
        $data['lhr_23_24'] = DB::table('leads')
            ->where('session_id', 11)
            ->where('lead_status_id', 6)
            ->where('branch_id', 3)
            ->where('beams_id', "!=", null)
            ->where('beams_paid_date', "!=", null)
            ->whereBetween('beams_paid_date', [$startDate->format('Y-m-d H:i:s'), $date2023->format('Y-m-d H:i:s')]);
        $data['fsb_23_24'] = DB::table('leads')
            ->where('session_id', 12)
            ->where('lead_status_id', 6)
            ->where('branch_id', 4)
            ->where('beams_id', "!=", null)
            ->where('beams_paid_date', "!=", null)
            ->whereBetween('beams_paid_date', [$startDate->format('Y-m-d H:i:s'), $date2023->format('Y-m-d H:i:s')]);
        $bpc_continue_23_24 = 0;
        $isb_continue_23_24 = 0;
        $fsb_continue_23_24 = 0;
        $lhr_continue_23_24 = 0;
        if ($request->addContinueVal == "yes") {
            $bpc_continue_23_24 = PromotionalAdmission::where("branch_id", 2)->sum('no_of_promotional_admissions');
            $isb_continue_23_24 = PromotionalAdmission::where("branch_id", 1)->sum('no_of_promotional_admissions');
            $fsb_continue_23_24 = PromotionalAdmission::where("branch_id", 4)->sum('no_of_promotional_admissions');
            $lhr_continue_23_24 = PromotionalAdmission::where("branch_id", 3)->sum('no_of_promotional_admissions');
        }
        $data['bpc_23_24'] = $data['bpc_23_24']->count() + $bpc_continue_23_24;
        $data['isb_23_24'] = $data['isb_23_24']->count() + $isb_continue_23_24;
        $data['lhr_23_24'] = $data['lhr_23_24']->count() + $lhr_continue_23_24;
        $data['fsb_23_24'] = $data['fsb_23_24']->count() + $fsb_continue_23_24;
        $data['total_23_24'] = $data['bpc_23_24'] + $data['isb_23_24'] + $data['lhr_23_24'] + $data['fsb_23_24'];

        $data['bpc_23_24_target_achieved'] = ($data['bpc_23_24'] / 30) * 100;
        $data['isb_23_24_target_achieved'] = ($data['isb_23_24'] / 266) * 100;
        $data['lhr_23_24_target_achieved'] = ($data['lhr_23_24'] / 170) * 100;
        $data['fsb_23_24_target_achieved'] = ($data['fsb_23_24'] / 176) * 100;
        $data['total_23_24_target_achieved'] = ($data['total_23_24'] / 642) * 100;

        $data['current_year_target_bpc'] = $targetsBPC->first()->target;
        $data['current_year_target_isb'] = $targetsIsb->first()->target;
        $data['current_year_target_lhr'] = $targetsLhr->first()->target;
        $data['current_year_target_fsb'] = $targetsFsb->first()->target;
        $data['current_year_target_total'] = 30 + 170 + 266 + 176;

        return $data;
    }
}
