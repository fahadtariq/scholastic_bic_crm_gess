<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Source;
use App\Models\Status;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SourceController extends Controller
{
    /**
     * @description function to show source listing view, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function index(Request $request)
    {
        $statuses = Status::all();
        if ($request->ajax()) {
            $data = Source::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.sources.actions', compact('row'));
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('backend.sources.index', compact('statuses'));
    }

    /**
     * @description function to store new source record, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function store(Request $request)
    {
        $request->validate(['beams_id' => 'nullable|max:255', 'name' => 'required|max:15', 'status_id' => 'required|integer'], ['name.required' => 'Source name is required!', 'status_id.required' => 'Select the status!']);
        Source::create($request->except('_token'));
        return redirect()->route('sources.index')->with('success', 'Source is created successfully');
    }

    /**
     * @description function to edit source view, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function edit($id)
    {
        $source = Source::find($id);
        if (!empty($source)) {
            $statuses = Status::all();
            return view('backend.sources.index', compact('source', 'statuses'));
        }
        return redirect()->route('sources.index');
    }

    /**
     * @description function to update source record, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function update(Request $request)
    {
        $request->validate(['beams_id' => 'nullable|max:255', 'name' => 'required|max:15', 'status_id' => 'required|integer'], ['name.required' => 'Source name is required!', 'status_id.required' => 'Select the status!']);
        Source::find($request->id)->update(['name' => $request->name, 'abbreviation' => $request->abbreviation, 'beams_id' => $request->beams_id]);
        return redirect()->route('sources.index')->with('success', 'Source is updated successfully');
    }

    /**
     * @description function to delete source record, @Date: 20/05/2022, @Author: Hammad Shoaib Khan
     */
    public function delete(Request $request)
    {
        $source = Source::find($request->id);
        try {
            $source->leads()->delete();
            $source->leadFollowUps()->delete();
            $source->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
