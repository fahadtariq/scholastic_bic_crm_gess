<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Imports\LeadsImport;
use App\Imports\PreviousLeadsImport;
use App\Models\Branch;
use App\Models\BranchTarget;
use App\Models\ClassGrade;
use App\Models\ContactCode;
use App\Models\Course;
use App\Models\Employee;
use App\Models\Lead;
use App\Models\LeadFollowUp;
use App\Models\LeadStatus;
use App\Models\LeadTracking;
use App\Models\LeadType;
use App\Models\Reason;
use App\Models\Source;
use App\Models\Reminder;
use App\Models\Student;
use App\Models\StudentEmergencyContact;
use App\Models\StudentGuardian;
use App\Models\Tag;
use App\Models\LeadProcessEmail;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Calculation\LookupRef\Selection;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Arr;
use App\Exports\LeadsEmailExport;
use App\Exports\LeadExport;
use Carbon\Carbon;
use App\Mail\LeadPEmail;
use App\Mail\LeadWEmail;
use Illuminate\Support\Facades\Mail;

class LeadController extends Controller
{
    public function index(Request $request)
    {
        $loggedInUser = auth()->user();
        $inputs = $request->all();
        $pageLength = 10;
        $leads = Lead::orderBy('id', 'DESC');

        if (isset($inputs['page_length']) && $inputs['page_length'] != '') {
            $pageLength = $inputs['page_length'];
            if ($pageLength == -1) {
                $pageLength = $leads->count();
            }
        }

        if (isset($inputs['lead_id_search']) && $inputs['lead_id_search'] > 0) {
            $leads->where('id', $inputs['lead_id_search']);
        }

        if (isset($inputs['beams_system_id_search']) && $inputs['beams_system_id_search'] > 0) {
            $leads->where('beams_system_id', $inputs['beams_system_id_search']);
        }

        if (isset($inputs['branch_id']) && $inputs['branch_id'] > 0) {
            $leads->where('branch_id', $inputs['branch_id']);
        }

        if (isset($inputs['session_id']) && $inputs['session_id'] > 0) {
            $leads->where('session_id', $inputs['session_id']);
        }

        if (isset($inputs['source_id']) && $inputs['source_id'] > 0) {
            $leads->where('source_id', $inputs['source_id']);
        }

        if (isset($inputs['tag_id']) && $inputs['tag_id'] > 0) {
            $leads->where('tag_id', $inputs['tag_id']);
        }

        if (isset($inputs['lead_type_id']) && $inputs['lead_type_id'] > 0) {
            $leads->where('lead_type_id', $inputs['lead_type_id']);
        }

        if (isset($inputs['lead_status_id']) && $inputs['lead_status_id'] > 0) {
            $leads->where('lead_status_id', $inputs['lead_status_id']);
        }

        if (isset($inputs['employee_id']) && $inputs['employee_id'] > 0) {
            $leads->where('employee_id', $inputs['employee_id']);
        }

        if (isset($inputs['follow_up_employee_id']) && $inputs['follow_up_employee_id'] > 0) {
            $leads->whereHas('followUps', function ($query) use ($inputs) {
                $query->where('user_id', $inputs['follow_up_employee_id']);
            });
        }

        if (isset($inputs['follow_up_by_employee_date']) && $inputs['follow_up_by_employee_date'] > 0) {
            $leads->whereHas('followUps', function ($query) use ($inputs) {
                $query->whereDate('follow_up_date', $inputs['follow_up_by_employee_date']);
            });
        }

        if (isset($inputs['is_referred'])) {
            $leads->where('is_referred', $inputs['is_referred']);
        }

        if (isset($inputs['is_sync'])) {
            $leads->where('is_sync', $inputs['is_sync']);
        }

        if (isset($inputs['is_confirmed'])) {
            $leads->where('is_confirmed', $inputs['is_confirmed']);
        }

        if (isset($inputs['is_important'])) {
            $leads->where('is_important', $inputs['is_important']);
        }

        if (isset($inputs['course_id'])) {
            $leads->whereIn('course_id', $inputs['course_id']);
        }

        if (isset($inputs['category'])) {
            $leads->where('category', $inputs['category']);
        }

        if (isset($inputs['mySearch'])) {
            $leads->whereHas('student', function ($query) use ($inputs) {
                $query->where('first_name', 'like', '%' . $inputs['mySearch'] . '%');
            });
        }

        if (isset($inputs['student_number'])) {
            $leads->whereHas('student', function ($query) use ($inputs) {
                $query->where('contact_number', 'like', '%' . $inputs['student_number'] . '%');
            });
        }

        if (isset($inputs['date_range']) && $inputs['date_range'] != '') {
            $dates = explode(" to ", $inputs['date_range']);
            if (count($dates) > 1) {
                $leads = $leads->whereDate('created_at', '>=', $dates[0])
                    ->whereDate('created_at', '<=', $dates[1]);
            } else {
                $leads = $leads->whereDate('created_at', $dates[0]);
            }
        }

        if ($loggedInUser->hasRole('campus_head|admin_officer')) {
            $leads->where('branch_id', $loggedInUser->employee->branch_id);
        } else if ($loggedInUser->hasRole('admission_advisor')) {
            $leads->where(function($query) use($loggedInUser){
                $query->where('employee_id', $loggedInUser->employee->id)
                    ->orWhere('user_id', $loggedInUser->id);
            });
        }

        $leadCounts = $leads->count();
        $leads = $leads->paginate($pageLength);
        $leads->appends($request->all())->render();

        if ($request->ajax()) {
            $view = view('backend.leads.partial', ['leads' => $leads])->render();
            $pagination = view('layouts.pagination_common', ['pagination' => $leads])->render();
            return response()->json(['view' => $view, 'pagination' => $pagination,'dbCounter' => $leadCounts]);
        }
        $branches = Branch::select('id', 'name')->where('status_id', 1)->where('status_id', 1)->get();
        $sources = Source::select('id', 'name')->where('status_id', 1)->get();
        $tags = Tag::select('id', 'name')->where('status_id', 1)->get();
        $leadTypes = LeadType::select('id', 'name')->where('status_id', 1)->get();
        $leadStatuses = LeadStatus::select('id', 'name')->where('status_id', 1)->get();
        $sessions = collect();
        if ($loggedInUser->hasRole('campus_head|admission_advisor')) {
            $courses = $loggedInUser->employee->branch->courses()->where('status_id', 1)->get();
            $sessions = BranchTarget::select('id', 'name')->where('branch_id', $loggedInUser->employee->branch->id)->orderBy('id', 'DESC')->get();
        } else {
            $courses = Course::select('id', 'name')->where('status_id', 1)->get();
        }

        $employees = collect();
        if ($loggedInUser->hasRole('super_admin|social_media_manager|reviewer')) {
            $employees = Employee::all();
        } else if ($loggedInUser->hasRole(['campus_head', 'admin_officer'])) {
            $employees = $loggedInUser->employee->branch->employees->map(function ($employee) {
                if (!$employee->user->hasRole('campus_head')) {
                    return $employee;
                }
            })->filter();
        }
        else{
            $employees = Employee::where('id', $loggedInUser->employee->id)->get();
        }
        return view('backend.leads.index')->with([
            'leads' => $leads,
            'leadCounts' => $leadCounts,
            'branches' => $branches,
            'sources' => $sources,
            'tags' => $tags,
            'leadTypes' => $leadTypes,
            'leadStatuses' => $leadStatuses,
            'employees' => $employees,
            'courses' => $courses,
            'sessions' => $sessions
        ]);
    }

    public function create()
    {
        $sources = Source::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $tags = Tag::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $branches = Branch::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $leadTypes = LeadType::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $leadStatuses = LeadStatus::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $classes = ClassGrade::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $contactCodes = ContactCode::where('status_id', 1)->get();
        return view('backend.leads.create', compact('sources', 'tags', 'branches', 'leadTypes', 'leadStatuses', 'classes', 'contactCodes'));
    }

    public function generateLead()
    {
        $sources = Source::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $branches = Branch::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $leadTypes = LeadType::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $leadStatuses = LeadStatus::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $classes = ClassGrade::where('status_id', 1)->orderBy('name', 'ASC')->get();
        $contactCodes = ContactCode::where('status_id', 1)->get();
        $tags = Tag::where('status_id', 1)->orderBy('name', 'ASC')->get();
        if(request('social') == "facebook"){
            return view('leadformfacebook', compact('sources', 'branches', 'leadTypes', 'leadStatuses', 'classes', 'contactCodes','tags'));
        } elseif(request('social') == "website"){
            return view('leadformwebsite', compact('sources', 'branches', 'leadTypes', 'leadStatuses', 'classes', 'contactCodes','tags'));
        } else{
            return view('leadform', compact('sources', 'branches', 'leadTypes', 'leadStatuses', 'classes', 'contactCodes','tags'));
        }
    }

    public function generateLeadStore(Request $request)
    {
        // $this->validations($request, 'create');
        $validator = Validator::make($request->all(), [
            'contact_number' => 'unique:students,contact_number', // Replace 'students' with your table name and 'phone' with your column name
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $userID = 1;
        $selectedTag = Tag::where(['name'=>'Walk In','status_id'=>1])->first();
        $selectedLeadType = LeadType::where(['name'=>'0%','status_id'=>1])->first();
        if(isset($selectedTag) && !empty($selectedTag) && isset($selectedLeadType) && !empty($selectedLeadType)){
            $studentInputs = [
                'first_name' => $request->first_name,
                'class_grade_id' => $request->class_grade_id,
                'gender' => $request->gender,
                'contact_code_id' => $request->has('contact_code_id') ? $request->contact_code_id : 1,
                'contact_number' => $request->contact_number,
                'email' => $request->email,
                'city' => $request->city
            ];

            $student = Student::create($studentInputs);

            if (!empty($student)) {
                $branchID = $request->branch_id;

                $leadInformationInputs = [
                    // 'beams_id' => $request->beams_id,
                    'is_confirmed' => $request->has('is_confirmed') ? $request->is_confirmed : 0,
                    'code' => $student->id . rand(100, 999) . $userID,
                    'user_id' => $userID,
                    'student_id' => $student->id,
                    'source_id' => $request->source_id,
                    'tag_id' => $request->has('tag_id') ? $request->tag_id : $selectedTag->id,
                    'lead_type_id' => $request->has('lead_type_id') ? $request->lead_type_id : $selectedLeadType->id,
                    'lead_status_id' => ($request->employee_id == '') ? 3 : 2,
                    'branch_id' => $branchID,
                    'session_id' => $request->session_id,
                    'course_id' => $request->course_id,
                    'employee_id' => ($request->employee_id == '') ? null : $request->employee_id,
                    'is_eligible' => $request->has('is_eligible') ? $request->is_eligible : 0,
                    'is_referred' => $request->has('is_referred') ? $request->is_referred : 0,
                    'is_important' => $request->has('is_important') ? $request->is_important : 0,
                    'advisor_name' => ($request->is_referred == 0) ? Null : $request->advisor_name,
                    'advisor_campus_name' => ($request->is_referred == 0) ? Null : $request->advisor_campus_name,
                    'previous_class' => $request->previous_class,
                    'previous_branch' => $request->previous_branch
                ];

                $lead = Lead::create($leadInformationInputs);
                if($request->form_type == "facebook"){
                    $lead->source_id = 4; // Social Media
                    $lead->tag_id = 3; // Facebook
                    $lead->save();
                }
                if($request->form_type == "website"){
                    $lead->source_id =  6;// Web
                    $lead->tag_id = 4; // Tag Website
                    $lead->save();
                }
                if (!empty($lead)) {
                    // $leadFollowUpsInputs = [
                    //     'user_id' => $userID,
                    //     'lead_id' => $lead->id,
                    //     'source_id' => $request->source_id,
                    //     'follow_up_date' => $request->follow_up_date,
                    //     'follow_up_by' => $request->follow_up_by,
                    //     'follow_up_duration' => ($request->follow_up_duration == NULL) ? 0 : $request->follow_up_duration,
                    //     'follow_up_remarks' => $request->follow_up_remarks
                    // ];

                    // LeadFollowUp::create($leadFollowUpsInputs);

                    LeadTracking::create([
                        'lead_id' => $lead->id,
                        'lead_type_id' => $request->has('lead_type_id') ? $request->lead_type_id : $selectedLeadType->id,
                        'type' => 0
                    ]);

                    LeadTracking::create([
                        'lead_id' => $lead->id,
                        'lead_status_id' => 1, /*Pending*/
                        'type' => 1
                    ]);

                    if ($lead->employee_id != null) {
                        LeadTracking::create([
                            'lead_id' => $lead->id,
                            'lead_status_id' => 2, /*Assigned*/
                            'type' => 1
                        ]);
                    }

                    if (!empty($request->employee_id)) {
                        LeadTracking::create([
                            'lead_id' => $lead->id,
                            'employee_id' => $request->employee_id,
                            'type' => 2
                        ]);
                    }

                    LeadTracking::create([
                        'lead_id' => $lead->id,
                        'branch_id' => $request->branch_id,
                        'type' => 3
                    ]);
                }
                if($request->form_type == 'facebook' || $request->form_type == 'website'){
                    return redirect()->route('generate.lead',['social'=>$request->form_type])->with('success', 'Lead Generated Successfully.');
                }else{
                    return redirect()->route('generate.lead')->with('success', 'Lead Generated Successfully.');
                }
            }
        }else{
            return redirect()->route('generate.lead')->with('error', 'Please select tag or if exist then change name to Walk In');
        }
    }

    public function store(Request $request)
    {
        $this->validations($request, 'create');
        $userID = auth()->user()->id;
        $studentInputs = [
            'first_name' => $request->first_name,
            'class_grade_id' => $request->class_grade_id,
            'gender' => $request->gender,
            'contact_code_id' => $request->contact_code_id,
            'contact_number' => $request->contact_number,
            'email' => $request->email,
            'city' => $request->city
        ];

        $student = Student::create($studentInputs);

        if (!empty($student)) {
            $branchID = $request->branch_id;
            if (!auth()->user()->hasRole('super_admin|social_media_manager')) {
                $branchID = auth()->user()->employee->branch->id;
            }

            /** auto assign employee to lead**/
            $employeeId = ($request->employee_id == '') ? null : $request->employee_id;

            if (empty($employeeId) && !empty($request->branch_id)) {
                $employeeId = Lead::employeeAutoAssign($request->branch_id);
            }

            $leadInformationInputs = [
                // 'beams_id' => $request->beams_id,
                'is_confirmed' => $request->is_confirmed,
                'code' => $student->id . rand(100, 999) . $userID,
                'user_id' => $userID,
                'student_id' => $student->id,
                'source_id' => $request->source_id,
                'tag_id' => $request->tag_id,
                'lead_type_id' => $request->lead_type_id,
                'lead_status_id' => ($request->employee_id == '') ? 1 : 2,
                'branch_id' => $branchID,
                'session_id' => $request->session_id,
                'course_id' => $request->course_id,
                'employee_id' => $employeeId,
                'is_eligible' => $request->is_eligible,
                'is_referred' => $request->is_referred,
                'is_important' => $request->is_important,
                'advisor_name' => ($request->is_referred == 0) ? Null : $request->advisor_name,
                'advisor_campus_name' => ($request->is_referred == 0) ? Null : $request->advisor_campus_name,
                'previous_class' => $request->previous_class,
                'previous_branch' => $request->previous_branch
            ];

            $lead = Lead::create($leadInformationInputs);
            //For outreach lead status change
            if($request->has('out_reach_id') && $request->out_reach_id){
                $lead->lead_status_id = 2; // Assigned
                $lead->save();
            }
            if (!empty($lead)) {
                $leadFollowUpsInputs = [
                    'user_id' => $userID,
                    'lead_id' => $lead->id,
                    'source_id' => $request->source_id,
                    'tag_id' => $request->tag_id,
                    'follow_up_date' => $request->follow_up_date,
                    'follow_up_by' => $request->follow_up_by,
                    'follow_up_duration' => ($request->follow_up_duration == NULL) ? 0 : $request->follow_up_duration,
                    'follow_up_remarks' => $request->follow_up_remarks
                ];

                LeadFollowUp::create($leadFollowUpsInputs);

                LeadTracking::create([
                    'lead_id' => $lead->id,
                    'lead_type_id' => $request->lead_type_id,
                    'type' => 0
                ]);

                LeadTracking::create([
                    'lead_id' => $lead->id,
                    'lead_status_id' => 1, /*Pending*/
                    'type' => 1
                ]);

                if ($lead->employee_id != null) {
                    LeadTracking::create([
                        'lead_id' => $lead->id,
                        'lead_status_id' => 2, /*Assigned*/
                        'type' => 1
                    ]);
                }

                if (!empty($request->employee_id)) {
                    LeadTracking::create([
                        'lead_id' => $lead->id,
                        'employee_id' => $request->employee_id,
                        'type' => 2
                    ]);
                }

                LeadTracking::create([
                    'lead_id' => $lead->id,
                    'branch_id' => $request->branch_id,
                    'type' => 3
                ]);
            }

            return redirect()->route('leads.index')->with('success', 'Lead Generated Successfully.');
        }
    }

    public function edit($id)
    {
        $lead = Lead::find($id);
        if (!empty($lead)) {

            $loggedInUser = auth()->user();
            if (!$loggedInUser->hasRole('super_admin|social_media_manager')) {
                $accessAbleLeadIDs = [];
                array_push( $accessAbleLeadIDs, $loggedInUser->leads->pluck('id')->toArray());
                array_push( $accessAbleLeadIDs, $loggedInUser->employee->leads->pluck('id')->toArray());
                array_push($accessAbleLeadIDs, $loggedInUser->employee->branch->leads->pluck('id')->toArray());

                $accessAbleLeadIDs = array_unique(Arr::flatten($accessAbleLeadIDs));
                /*if lead is not created by, or not user own branch or not assigned to current logged in user redirect to 404*/
                if (!in_array($id, $accessAbleLeadIDs)) {
                    return redirect('404');
                }
            }

            $sources = Source::where('status_id', 1)->orderBy('name', 'ASC')->get();
            $tags = Tag::where('status_id', 1)->orderBy('name', 'ASC')->get();
            $branches = Branch::where('status_id', 1)->orderBy('name', 'ASC')->get();
            $leadTypes = LeadType::where('status_id', 1)->orderBy('name', 'ASC')->get();
            $leadStatuses = LeadStatus::where('status_id', 1)->orderBy('name', 'ASC')->get();
            $employees = Employee::where('status_id', 1)->orderBy('first_name', 'ASC')->get();
            $classes = ClassGrade::where('status_id', 1)->orderBy('name', 'ASC')->get();
            $followUp = $lead->followUps->first();
            $contactCodes = ContactCode::where('status_id', 1)->get();
            return view('backend.leads.edit', compact('lead', 'sources', 'tags', 'branches', 'leadTypes', 'leadStatuses', 'employees', 'classes', 'followUp', 'contactCodes'));
        }
    }

    public function update(Request $request)
    {
        $this->validations($request);
        $leadID = $request->lead_id;
        $studentID = $request->student_id;
        $studentInputs = [
            'first_name' => $request->first_name,
            'class_grade_id' => $request->class_grade_id,
            'gender' => $request->gender,
            'contact_code_id' => $request->contact_code_id,
            'contact_number' => $request->contact_number,
            'email' => $request->email,
            'city' => $request->city
        ];

        $studentUpdated = Student::find($studentID)->update($studentInputs);

        if ($studentUpdated) {

            $branchID = $request->branch_id;
            if (!auth()->user()->hasRole('super_admin|social_media_manager')) {
                $branchID = auth()->user()->employee->branch->id;
            }

            $lead = Lead::find($leadID);
            $oldBranchID = $lead->branch_id;
            $oldEmployeeID = $lead->employee_id;

            $leadStatusID = $lead->lead_status_id;
            $oldEmployeeID = $lead->employee_id;
            if ($request->employee_id != '' && $oldEmployeeID != $request->employee_id && !in_array($lead->lead_status_id, [4, 5, 6])) {
                $leadStatusID = 2;
            }
            $leadInformationInputs = [
                // 'beams_id' => $request->beams_id,
                'is_confirmed' => $request->is_confirmed,
                'student_id' => $studentID,
                'branch_id' => $branchID,
                'session_id' => $request->session_id,
                'tag_id' => $request->tag_id,
                'course_id' => $request->course_id,
                'employee_id' => (empty($request->employee_id)) ? null : $request->employee_id,
                'is_eligible' => $request->is_eligible,
                'is_referred' => $request->is_referred,
                'is_important' => $request->is_important,
                'advisor_name' => ($request->is_referred == 0) ? Null : $request->advisor_name,
                'advisor_campus_name' => ($request->is_referred == 0) ? Null : $request->advisor_campus_name,
                'previous_class' => $request->previous_class,
                'previous_branch' => $request->previous_branch,
                'source_id' => isset($request->source_id) ? $request->source_id : null,
                'lead_status_id' => $leadStatusID,
                // 'category' => $request->category
            ];

            $lead->update($leadInformationInputs);

            $leadTrackingInputs = [
                'lead_id' => $lead->id,
                'lead_type_id' => null,
                'lead_status_id' => null
            ];

            if ($lead->branch_id != $oldBranchID) {
                $leadTrackingInputs['branch_id'] = $branchID;
                $leadTrackingInputs['type'] = 3;
                LeadTracking::create($leadTrackingInputs);
            }
            if ($lead->employee_id != $oldEmployeeID) {
                $leadTrackingInputs['employee_id'] = $branchID;
                $leadTrackingInputs['type'] = 2;
                LeadTracking::create($leadTrackingInputs);
            }

            if ($lead->employee_id != $oldEmployeeID && $lead->employee_id == null) {
                $leadTrackingInputs['lead_status_id'] = 1;
                $leadTrackingInputs['type'] = 1;
                LeadTracking::create($leadTrackingInputs);
            }
            return back()->with('success', 'Lead Updated Successfully.');
        }
        return back()->with('error', 'Lead not Updated.');
    }

    public function delete(Request $request)
    {
        try {
            $lead = Lead::find($request->id);
            if (!empty($lead)) {
                $lead->student()->delete();
                $lead->followUps()->delete();
                $lead->trackings()->delete();
                $lead->delete();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function show(Request $request)
    {
        if ($request->ajax()) {
            $lead = Lead::find($request->id);
            return view('backend.leads.partials.lead-info-modal', compact('lead'));
        }
    }
    public function message(Request $request)
    {
        if ($request->ajax()) {
            $lead = Lead::find($request->id);
            $enableWhatsappChat = false;

            //Add + in the begining if not already exists
            $leadContactNumber = '+'.ltrim($lead->student->contact_number, '+');

            /*//check if mesage is recieved from this lead, to enable chat text
            $checkWhatsappMessage = WhatsppHistory::where('from', $leadContactNumber)
                ->orWhere('to', $leadContactNumber)->where('status', 'Recieved')->first();

            if($checkWhatsappMessage){
                $enableWhatsappChat = true;
            }else{
                $enableWhatsappChat = false;
            }

            //Get chat history
            $subQuery = DB::table('whatsapp_history as t')
                ->select('*')
                ->where(function($query) use($leadContactNumber) {
                    $query->where('from', $leadContactNumber)
                        ->orWhere('to', $leadContactNumber);
                })
                ->orderBy('created_at', 'ASC');

            $chatHistory = DB::table(DB::raw("({$subQuery->toSql()}) as chat"))
                ->mergeBindings($subQuery) // required to include bindings from the subquery
                ->select('*')
                ->selectRaw("(chat.from + chat.to) as dist")
                ->orderBy('created_at', 'ASC')
                ->get();*/

            $messages = getWhatsAppMessages($leadContactNumber);

            if($messages) {
                $enableWhatsappChat = true;
            }

            return view('backend.leads.partials.lead-message-modal', compact('lead','enableWhatsappChat', 'messages'));
        }
    }

    public function messageSend(Request $request)
    {
        //dd($request->all());
        if ($request->ajax()) {

            $businessNumber = env('SENDER_WHATSAPP_NUMBER');
            $toNumber = $request->number;
            $lead = Lead::find($request->lead_id);
            $message = $request->message;

            $attachmentUrl = isset($request->attachment_url) ? $request->attachment_url : '';
            $attachmentArray = array_filter(explode(',', $attachmentUrl));

            //This is temporary because message was not sending to whatsapp because of newlines & spaces.
            //$messageBody = "Welcome to Homebridge By Beaconhouse, we are thrilled to assist you.\nPlease Share Your Details:\nName: \nEmail: \nSchool: \nPlease take a few moments to fill out the information above. We value your time and appreciate your interest in Homebridge By Beaconhouse. Our Team will get in touch with you shortly.";

            $messageStatus = sendWhatsappMessage($businessNumber, $toNumber, $lead->id, $message, $attachmentArray );

            if($messageStatus){
                return Response()->json(array(
                    'success' => true,
                    'errors' => '',
                    'message' => 'Message sent successfully!. Message Id: ' . $messageStatus
                ), 200);
            }else{
                return Response()->json(array(
                    'success' => false,
                    'errors' => 'Unable to send message. Either number format is wrong or there\'s no whatsapp on this number',
                    'message' => 'Unable to send message. Either number format is wrong or there\'s no whatsapp on this number',
                ), 400);
            }
        }
    }
    public function changeInfo(Request $request)
    {
        if ($request->ajax()) {
            $action = $request->action;
            $lead = Lead::find($request->id);
            $compactArr['lead'] = $lead;
            $compactArr['action'] = $action;

            if ($action == 'type') {
                $compactArr['leadTypes'] = LeadType::where('status_id', 1)->get();
            } else if ($action == 'status') {
                $compactArr['leadStatuses'] = collect();
                if(auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('campus_head') || auth()->user()->hasRole('admission_advisor')){
                    if ($lead->lead_status_id == 1) {
                        $compactArr['leadStatuses'] = LeadStatus::whereIn('id', [2, 3, 5, 7])->where('status_id', 1)->get();
                    } else if ($lead->lead_status_id == 2) {
                        $compactArr['leadStatuses'] = LeadStatus::whereIn('id', [3, 5, 7])->where('status_id', 1)->get();
                    } else if ($lead->lead_status_id == 3) {
                        $compactArr['leadStatuses'] = LeadStatus::whereIn('id', [2, 5, 7])->where('status_id', 1)->get();
                    } else if ($lead->lead_status_id == 5) {
                        $compactArr['leadStatuses'] = LeadStatus::orderBy('name', 'DESC')->whereIn('id', [4, 6])->where('status_id', 1)->get();
                    } else if ($lead->lead_status_id == 7) {
                        $compactArr['leadStatuses'] = LeadStatus::orderBy('name', 'DESC')->whereIn('id', [2, 3])->where('status_id', 1)->get();
                    }else if ($lead->lead_status_id == 6) {
                        $compactArr['leadStatuses'] = LeadStatus::orderBy('name', 'DESC')->whereIn('id', [8])->where('status_id', 1)->get();
                    }
                }else{
                    if ($lead->lead_status_id == 1) {
                        $compactArr['leadStatuses'] = LeadStatus::whereIn('id', [2, 3, 7])->where('status_id', 1)->get();
                    } else if ($lead->lead_status_id == 2) {
                        $compactArr['leadStatuses'] = LeadStatus::whereIn('id', [3, 5, 7])->where('status_id', 1)->get();
                    } else if ($lead->lead_status_id == 3) {
                        $compactArr['leadStatuses'] = LeadStatus::whereIn('id', [2, 5, 7])->where('status_id', 1)->get();
                    } else if ($lead->lead_status_id == 5) {
                        $compactArr['leadStatuses'] = LeadStatus::orderBy('name', 'DESC')->whereIn('id', [4])->where('status_id', 1)->get();
                    } else if ($lead->lead_status_id == 7) {
                        $compactArr['leadStatuses'] = LeadStatus::orderBy('name', 'DESC')->whereIn('id', [2, 3])->where('status_id', 1)->get();
                    }
                }
                $compactArr['reasons'] = Reason::where('status_id', 1)->get();
            } else if ($action == 'employee') {
                $compactArr['employees'] = Employee::where('branch_id', $lead->branch_id)->get();
            } else if ($action == 'branch') {
                $compactArr['branches'] = Branch::where('status_id', 1)->get();
            }
            return view('backend.leads.partials.change-info-modal', $compactArr);
        }
    }

    public function changeInfoStore(Request $request)
    {
        $close = 0;
        $lead = Lead::find($request->lead_id);
        if ($request->has('lead_status_id')) {
            $lead->lead_status_id = !empty($request->lead_status_id) ? $request->lead_status_id : $lead->lead_status_id;
            $request->request->add(['type' => 1]);
        } else if ($request->has('lead_type_id')) {
            $lead->lead_type_id = !empty($request->lead_type_id) ? $request->lead_type_id : $lead->lead_type_id;
            $request->request->add(['type' => 0]);
        } else if ($request->has('employee_id')) {
            $lead->employee_id = !empty($request->employee_id) ? $request->employee_id : null;
            $lead->lead_status_id = !empty($request->employee_id) ? 2 : 1;
            $request->request->add(['type' => 2]);
        } else if ($request->has('branch_id')) {
            $lead->branch_id = !empty($request->branch_id) ? $request->branch_id : $lead->branch_id;
            $lead->employee_id = null;
            $request->request->add(['type' => 3]);
            if (auth()->user()->hasRole('campus_head|admin_officer')) {
                $close = 1;
            }
        }
        if (in_array($request->lead_status_id, [4, 7])) {
            $lead->reason_id = $request->reason;
        }
        $lead->save();
        if ($lead->wasChanged()) {
            if ($request->has('employee_id')) {
                LeadTracking::create(['lead_id' => $lead->id, 'lead_status_id' => !empty($request->employee_id) ? 2 : 1, 'type' => 1]);
                if(!empty($request->employee_id)) {
                    LeadTracking::create($request->except('_token'));
                }
            }

            if(!$request->has('employee_id')) {
                LeadTracking::create($request->except('_token'));
            }
        }
        return Response()->json(['close' => $close]);
    }

    public function createFollowUp(Request $request)
    {
        if ($request->ajax()) {
            $lead = Lead::find($request->id);
            $sources = Source::all();
            return view('backend.leads.partials.add-follow-up-modal', compact('lead', 'sources'));
        }
    }

    public function storeFollowUp(Request $request)
    {
        if ($request->ajax()) {
            $validator = Validator::make($request->all(), ['source_id' => 'required', 'follow_up_date' => 'required']);
            if ($validator->fails()) {
                return Response()->json(array(
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray()

                ), 400);
            }
            $inputs = [
                'lead_id' => $request->lead_id,
                'source_id' => $request->source_id,
                'follow_up_date' => $request->follow_up_date,
                'follow_up_by' => auth()->user()->name,
                'follow_up_duration' => ($request->follow_up_duration != null) ? $request->follow_up_duration : 0,
                'follow_up_remarks' => $request->follow_up_remarks,
                'user_id' => auth()->user()->id
            ];
            LeadFollowUp::create($inputs);
        }
    }

    public function loadEmployees()
    {
        $user = auth()->user();
        $employees = Employee::where('status_id', 1)->get();
        if ($user->hasRole('campus_head|admin_officer')) {
            $employees = $user->employee->branch->employees->where('status_id', 1)->map(function ($query) {
                if ($query->user->hasRole(['admission_advisor'])) {
                    return $query;
                }
            })->filter();
        }
        return view('backend.leads.partials.load-employees', ['branchEmployees' => $employees]);
    }

    public function multipleAssignToEmployee(Request $request)
    {
        $employeeID = $request->employee_id;
        $leadIDs = $request->lead_ids;
        if (count($leadIDs) > 0) {
            foreach ($leadIDs as $leadID) {
                $lead = Lead::where('id', $leadID)->first();
                $oldEmployeeID = $lead->employee_id;
                $lead->update(['employee_id' => $employeeID, 'lead_status_id' => 2]);

                if ($oldEmployeeID != $lead->employee_id) {
                    LeadTracking::create([
                        'lead_id' => $leadID,
                        'branch_id' => null,
                        'employee_id' => $employeeID,
                        'lead_type_id' => null,
                        'lead_status_id' => null,
                        'type' => 2
                    ]);
                    LeadTracking::create([
                        'lead_id' => $leadID,
                        'branch_id' => null,
                        'employee_id' => null,
                        'lead_type_id' => null,
                        'lead_status_id' => 2,
                        'type' => 1
                    ]);
                }
            }
        }
    }

    public function import(Request $request)
    {
        $validations = [
            'file' => 'required|file|max:40000|mimes:xlsx,xls'
        ];
        if (auth()->user()->hasRole('super_admin|social_media_manager')) {
            $validations['branch_id'] = "required|integer";
            $validations['session_id'] = "required|integer";
        }
        $validator = Validator::make($request->all(), $validations);

        if($validator->fails()){
            return redirect()->back()->with('error', $validator->getMessageBag()->first());
        }
        Excel::import(new LeadsImport($request), $request->file);
        return back()->with('success', 'Leads Imported Successfully.');
    }

    public function importFall(Request $request)
    {
        $validations = [
            'file' => 'required|file|max:40000|mimes:xlsx,xls'
        ];
        $validator = Validator::make($request->all(), $validations);
        if($validator->fails()){
            return redirect()->back()->with('error', $validator->getMessageBag()->first());
        }
        Excel::import(new PreviousLeadsImport($request), $request->file('file'));
        return back()->with('success', 'Previous Lead Imported Successfully.');
    }

    public function exportEmail()
    {
        $filename = Carbon::now()->format('Y-m-d').'-leads-emails.xlsx';
        return Excel::download(new LeadsEmailExport(), $filename);
    }

    public function exportLeads(Request $request)
    {
        $filename = Carbon::now()->format('Y-m-d').'-leads.xlsx';
        return Excel::download(new LeadExport($request), $filename);
    }

    public function assignByRange(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'min' => 'required|integer',
            'max' => 'required|integer',
            'employee_id' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $employeeID = $request->employee_id;
        $findLeadsByRange = Lead::select('id', 'employee_id')->where('branch_id', auth()->user()->employee->branch->id)->where('id', '>=', $request->min)->where('id', '<=', $request->max)->get();
        if (count($findLeadsByRange) > 0) {
            foreach ($findLeadsByRange as $lead) {
                $oldEmployeeID = $lead->employee_id;
                $lead->update(['employee_id' => $employeeID, 'lead_status_id' => 2]);

                if ($oldEmployeeID != $lead->employee_id) {
                    LeadTracking::create([
                        'lead_id' => $lead->id,
                        'branch_id' => null,
                        'employee_id' => $employeeID,
                        'lead_type_id' => null,
                        'lead_status_id' => null,
                        'type' => 2
                    ]);
                    LeadTracking::create([
                        'lead_id' => $lead->id,
                        'branch_id' => null,
                        'employee_id' => null,
                        'lead_type_id' => null,
                        'lead_status_id' => 2,
                        'type' => 1
                    ]);
                }
            }
        }
    }

    public function reminders(Request $request)
    {
        $leadReminders = Reminder::where('lead_id', $request->id)->where('user_id', auth()->user()->id)->latest()->get();
        return Response()->json($leadReminders);
    }

    public function reminderStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lead_id' => 'required',
            'message' => 'required',
            'date_time' => 'required'
        ]);

        if ($validator->fails()) {
            return Response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 400);
        }

        Reminder::create(['user_id' => auth()->user()->id, 'lead_id' => $request->lead_id, 'message' => $request->message, 'date_time' => $request->date_time]);
        $leadReminders = Reminder::where('lead_id', $request->lead_id)->where('user_id', auth()->user()->id)->latest()->get();
        return Response()->json($leadReminders);
    }

    public function deleteReminder(Request $request)
    {
        $reminder = Reminder::where('id', $request->id)->where('user_id', auth()->user()->id);
        $leadID = $reminder->first()->lead_id;
        $reminder->delete();
        $leadReminders = Reminder::where('lead_id', $leadID)->where('user_id', auth()->user()->id)->latest()->get();
        return Response()->json($leadReminders);
    }

    // Store lead in beams
    public function leadStoreBeams(){
        $count = \Artisan::call('leads:store');
        return redirect()->route('leads.index')->with('success', "$count Leads created in beams.");
    }

    // get lead status from beams
    public function leadGetStatusBeams(){
        $regiserCount = \Artisan::call('getlead:register');
        $paidCount = \Artisan::call('getlead:paid');
        return redirect()->route('leads.index')->with('success', "Updated $regiserCount Register Status and $paidCount Paid Status for Leads");
    }

    public function listDuplicateLeads(Request $request)
    {
        $loggedInUser = auth()->user();
        $inputs = $request->all();
        $pageLength = $request->page_length ?? 10;

        $student_ids = DB::table('students AS st')
            ->join(DB::raw("(SELECT email, contact_number
                    FROM students
                    WHERE email IS NOT NULL AND email != '' AND contact_number IS NOT NULL AND contact_number != ''
                    GROUP BY email, contact_number
                    HAVING COUNT(*) > 1) AS duplicates"), function ($join) {
                $join->on('st.contact_number', '=', 'duplicates.contact_number')
                    ->on('st.email', '=', 'duplicates.email')
                    ->on('st.contact_number', '=', 'duplicates.contact_number');
            })
            ->select('st.id')
            ->pluck('id');

        $leads = Lead::whereIn('student_id', $student_ids)
            ->with('student');

        /*$leads = Lead::select('leads.id', 'leads.beams_system_id', 'leads.branch_id', 'leads.session_id', 'leads.source_id', 'leads.tag_id', 'leads.lead_type_id', 'leads.lead_status_id', 'leads.employee_id', 'leads.user_id', 'leads.is_referred', 'leads.is_sync', 'leads.is_confirmed', 'leads.is_important', 'leads.course_id',
            'leads.created_at', 'students.email', 'students.contact_number')
            ->join('students', 'leads.student_id', '=', 'students.id')
            ->joinSub(function ($query) {
                $query->select('students.email', 'students.contact_number')
                    ->from('students')
                    ->groupBy('email', 'contact_number')
                    ->havingRaw('COUNT(*) > 1');
            }, 'duplicate_pairs', function ($join) {
                $join->on('students.email', '=', 'duplicate_pairs.email')
                    ->on('students.contact_number', '=', 'duplicate_pairs.contact_number');
            })
            ->orderBy('students.email')
            ->orderBy('students.contact_number')
            ->limit($pageLength);*/


        /*$leads = DB::SELECT('select `leads`.*, `students`.`email`, `students`.`contact_number` from `leads` inner join `students` on `leads`.`student_id` = `students`.`id` inner join (select `students`.`email`, `students`.`contact_number` from `students` group by `email`, `contact_number` having COUNT(*) > 1) as `duplicate_pairs` on `students`.`email` = `duplicate_pairs`.`email` and `students`.`contact_number` = `duplicate_pairs`.`contact_number` where `leads`.`deleted_at` is null order by `students`.`email` asc, `students`.`contact_number` asc');*/


        if (isset($inputs['lead_id_search']) && $inputs['lead_id_search'] > 0) {
            $leads->where('id', $inputs['lead_id_search']);
        }

        if (isset($inputs['beams_system_id_search']) && $inputs['beams_system_id_search'] > 0) {
            $leads->where('beams_system_id', $inputs['beams_system_id_search']);
        }

        if (isset($inputs['branch_id']) && $inputs['branch_id'] > 0) {
            $leads->where('branch_id', $inputs['branch_id']);
        }

        if (isset($inputs['session_id']) && $inputs['session_id'] > 0) {
            $leads->where('session_id', $inputs['session_id']);
        }

        if (isset($inputs['source_id']) && $inputs['source_id'] > 0) {
            $leads->where('source_id', $inputs['source_id']);
        }

        if (isset($inputs['tag_id']) && $inputs['tag_id'] > 0) {
            $leads->where('tag_id', $inputs['tag_id']);
        }

        if (isset($inputs['lead_type_id']) && $inputs['lead_type_id'] > 0) {
            $leads->where('lead_type_id', $inputs['lead_type_id']);
        }

        if (isset($inputs['lead_status_id']) && $inputs['lead_status_id'] > 0) {
            $leads->where('lead_status_id', $inputs['lead_status_id']);
        }

        if (isset($inputs['employee_id']) && $inputs['employee_id'] > 0) {
            $leads->where('employee_id', $inputs['employee_id']);
        }

        if (isset($inputs['follow_up_employee_id']) && $inputs['follow_up_employee_id'] > 0) {
            $leads->whereHas('followUps', function ($query) use ($inputs) {
                $query->where('user_id', $inputs['follow_up_employee_id']);
            });
        }

        if (isset($inputs['follow_up_by_employee_date']) && $inputs['follow_up_by_employee_date'] > 0) {
            $leads->whereHas('followUps', function ($query) use ($inputs) {
                $query->whereDate('follow_up_date', $inputs['follow_up_by_employee_date']);
            });
        }

        if (isset($inputs['is_referred'])) {
            $leads->where('is_referred', $inputs['is_referred']);
        }

        if (isset($inputs['is_sync'])) {
            $leads->where('is_sync', $inputs['is_sync']);
        }

        if (isset($inputs['is_confirmed'])) {
            $leads->where('is_confirmed', $inputs['is_confirmed']);
        }

        if (isset($inputs['is_important'])) {
            $leads->where('is_important', $inputs['is_important']);
        }

        if (isset($inputs['course_id'])) {
            $leads->whereIn('course_id', $inputs['course_id']);
        }

        if (isset($inputs['mySearch'])) {
            $leads->whereHas('student', function ($query) use ($inputs) {
                $query->where('first_name', 'like', '%' . $inputs['mySearch'] . '%');
            });
        }

        if (isset($inputs['student_number'])) {
            $leads->whereHas('student', function ($query) use ($inputs) {
                $query->where('contact_number', 'like', '%' . $inputs['student_number'] . '%');
            });
        }

        if (isset($inputs['date_range']) && $inputs['date_range'] != '') {
            $dates = explode(' to ', $inputs['date_range']);
            if (count($dates) > 1) {
                $leads = $leads->whereDate('created_at', '>=', $dates[0])
                    ->whereDate('created_at', '<=', $dates[1]);
            } else {
                $leads = $leads->whereDate('created_at', $dates[0]);
            }
        }

        if ($loggedInUser->hasRole('campus_head|admin_officer')) {
            $leads->where('branch_id', $loggedInUser->employee->branch_id);
        } else if ($loggedInUser->hasRole('admission_advisor')) {
            $leads->where(function ($query) use ($loggedInUser) {
                $query->where('employee_id', $loggedInUser->employee->id)
                    ->orWhere('user_id', $loggedInUser->id);
            });
        }

        $leads = $leads->get()->sortBy(['student.contact_number']);
        $leadCounts = $leads->count();

        $page = $leadCounts > $pageLength ?  request()->query('page', 1) : 1;
        $paginator = new LengthAwarePaginator(
            $leads->forPage($page, $pageLength),
            $leads->count(),
            $pageLength,
            $page,
            ['path' => request()->url()]
        );
        $leads = $paginator->appends(request()->query())->getCollection();

        if ($request->ajax()) {
            $view = view('backend.leads.partial', ['leads' => $leads])->render();
            $pagination = view('layouts.pagination_common', ['pagination' => $paginator])->render();
            return response()->json(['view' => $view, 'pagination' => $pagination, 'dbCounter' => $leadCounts]);
        }
        $branches = Branch::select('id', 'name')->where('status_id', 1)->where('status_id', 1)->get();
        $sources = Source::select('id', 'name')->where('status_id', 1)->get();
        $tags = Tag::select('id', 'name')->where('status_id', 1)->get();
        $leadTypes = LeadType::select('id', 'name')->where('status_id', 1)->get();
        $leadStatuses = LeadStatus::select('id', 'name')->where('status_id', 1)->get();
        $sessions = collect();
        if ($loggedInUser->hasRole('campus_head|admission_advisor')) {
            $courses = $loggedInUser->employee->branch->courses()->where('status_id', 1)->get();
            $sessions = BranchTarget::select('id', 'name')->where('branch_id', $loggedInUser->employee->branch->id)->orderBy('id', 'DESC')->get();
        } else {
            $courses = Course::select('id', 'name')->where('status_id', 1)->get();
        }

        $employees = collect();
        if ($loggedInUser->hasRole('super_admin|social_media_manager|reviewer')) {
            $employees = Employee::all();
        } else if ($loggedInUser->hasRole(['campus_head', 'admin_officer'])) {
            $employees = $loggedInUser->employee->branch->employees->map(function ($employee) {
                if (!$employee->user->hasRole('campus_head')) {
                    return $employee;
                }
            })->filter();
        } else {
            $employees = Employee::where('id', $loggedInUser->employee->id)->get();
        }

        return view('backend.leads.list-duplicate')->with([
            'leads' => $leads,
            'pagination' => $paginator,
            'leadCounts' => $leadCounts,
            'branches' => $branches,
            'sources' => $sources,
            'tags' => $tags,
            'leadTypes' => $leadTypes,
            'leadStatuses' => $leadStatuses,
            'employees' => $employees,
            'courses' => $courses,
            'sessions' => $sessions
        ]);
    }
    // get fall data from beams through api
    public function leadGetFallDataBeams(){
        $count = 0;
        $fallLeads = \Artisan::call('fall:leads');
        if($fallLeads){
            dd($fallLeads);
        }
        return redirect()->route('leads.index')->with('success', "$count Leads saved against fall leads");
    }

    // get visitors lead data from beams through api
    public function leadGetVistorLeadBeams(){
        $count = \Artisan::call('visitors:leads');
        return redirect()->route('leads.index')->with('success', "$count Leads registration and paid date updated");
    }

    private function validations($request, $type = null)
    {
        $validations = [
            'beams_id' => 'nullable|max:255',
            'first_name' => 'required|max:255',
            'class_grade_id' => 'required|integer',
            'gender' => 'required|integer',
            'contact_code_id' => 'required|integer',
            'contact_number' => 'required|digits_between:8,15|unique:students,contact_number,'.$request->student_id,
            'email' => 'required|string|max:255',
            'city' => 'nullable|string:max:255',
            'previous_class' => 'nullable|string:max:255',
            'previous_branch' => 'nullable|string:max:255',
            'course_id' => 'required|integer',
            'is_eligible' => 'required|integer',
            'is_referred' => 'required|integer',
            'is_important' => 'required|integer',
            'is_confirmed' => 'required|integer',
            'employee_id' => 'nullable|integer',
            'session_id' => 'required|integer',
        ];

        if ($type == 'create') {
            if (auth()->user()->hasRole('super_admin|social_media_manager')) {
                $validations['branch_id'] = 'required';
            }

            $validations['lead_type_id'] = 'required';
            $validations['source_id'] = 'required';
            $validations['follow_up_date'] = 'required';
            $validations['follow_up_by'] = 'required';
        }

        $request->validate($validations, [
            'first_name.required' => 'Name is required!',
            'class_grade_id.required' => 'Select the class!',
            'gender.required' => 'Select the gender!',
            'contact_code_id.required' => 'Select the contact code!',
            'contact_number.required' => 'Contact number is required!',
            'email.required' => 'Email is required!',
            'city.required' => 'City is required!',
            'source_id.required' => 'Select the source!',
            'lead_type_id.required' => 'Select the type!',
            'lead_status_id.required' => 'Select the status!',
            'branch_id.required' => 'Select the branch!',
            'course_id.required' => 'Select the course!',
            'employee_id.required' => 'Select the employee!',
            'is_eligible.required' => 'Select the eligibility!',
            'is_referred.required' => 'Select the referred!',
            'follow_up_date.required' => 'Select the follow up date!',
            'follow_up_by.required' => 'Follow up by is required!'
        ]);
    }

    public function showEmailBody(Request $request)
    {
        if ($request->ajax()) {
            $lead = Lead::find($request->id);
            $type = $request->type;
            if($type == "Process"){
                return view('backend.leads.partials.p-emailbody',compact('lead'));
            }else{
                return view('backend.leads.partials.w-emailbody',compact('lead'));
            }
        }
    }

    public function sendPEmailBody(Request $request)
    {
        $data['email'] = $request->receiveremail;
        $data['subject'] = $request->emailsubject;
        $lead = Lead::find($request->lead_id);
        $data['lead'] = $lead;
        if($lead){
            if($lead->process_email_sent == "Yes"){
                return response()->json(['success'=>false,"lead"=>$data['lead'],"message"=>"Process Email Already Sent"]);
            }
            $lead->process_email_sent = "Yes";
            $lead->save();
            // Create Lead Process Email Data
            $leadProcess = LeadProcessEmail::where(["lead_id"=>$lead->id,"type"=>"process"])->first();
            if(empty($leadProcess)){
                LeadProcessEmail::create([
                    "lead_id" => $lead->id,
                    "to" => $data['email'],
                    "subject" => $data['subject'],
                    "type" => "process",
                    "remarks" => $request->message,
                    'attachment_link' => $request->attachment_link
                ]);
            }
        }
        $data['message'] = $request->message;
        $data['attachment_link'] = $request->attachment_link;
        Mail::to([$data['email'],'sumaira.mudassar@bic.edu.pk'])->send(new LeadPEmail($data));
        return response()->json(['success'=>true,"lead"=>$data['lead'],"message"=>"Email Sent Successfully"]);
    }

    public function sendWEmailBody(Request $request)
    {
        $data['email'] = $request->receiveremail;
        $data['subject'] = $request->emailsubject;
        $lead = Lead::find($request->lead_id);
        $data['lead'] = $lead;
        if($lead){
            if($lead->withdraw_email_sent == "Yes"){
                return response()->json(['success'=>false,"lead"=>$data['lead'],"message"=>"Withdrawal Email Already Sent"]);
            }
            $lead->withdraw_email_sent = "Yes";
            $lead->save();
            // Create Lead Process Email Data
            $leadProcess = LeadProcessEmail::where(["lead_id"=>$lead->id,"type"=>"withdraw"])->first();
            if(empty($leadProcess)){
                LeadProcessEmail::create([
                    "lead_id" => $lead->id,
                    "to" => $data['email'],
                    "subject" => $data['subject'],
                    "type" => "withdraw",
                    "remarks" => $request->message,
                    'attachment_link' => $request->attachment_link
                ]);
            }
        }
        $data['message'] = $request->message;
        $data['attachment_link'] = $request->attachment_link;
        Mail::to([$data['email'],'sumaira.mudassar@bic.edu.pk'])->send(new LeadWEmail($data));
        return response()->json(['success'=>true,"lead"=>$data['lead'],"message"=>"Email Sent Successfully"]);
    }
}
