<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ActivityController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = DB::table('audits')->orderBy('id', 'DESC');

            if ($request->event && $request->event > 0) {
                $data->where('event', $request->event);
            }

            if (isset($request->modal_search)) {
                $data->where('auditable_type', 'like', '%' . $request->modal_search . '%');
            }

            if(isset($request->date_range) && $request->date_range != '') {
                $dates = explode(" to ",$request->date_range);
                if(count($dates) > 1){
                    $data = $data->whereDate('created_at', '>=', $dates[0])
                    ->whereDate('created_at', '<=', $dates[1]);
                }
                else{
                    $data = $data->whereDate('created_at', $dates[0]);
                }
            }

            $data->get();
//            dd($data);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('performed_on', function ($row) {
                    return substr($row->auditable_type, 11). ' (' . $row->auditable_id . ')' ;
                })
                ->addColumn('performed_by', function ($row) {
                    $user = User::find($row->user_id);
                    return (!empty($user)) ? $user->name . ' (' . $user->email . ')' : 'System';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.activities.actions', ['row' => $row]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('backend.activities.index');
    }

    public function detail(Request $request){
        $activityLog = DB::table('audits')->where('id', $request->id)->first();
        if(empty($activityLog)){
            return response()->json(['status' => 0]);
        }
        return response()->json(['status' => 1, 'activityLog' => $activityLog]);
    }
}
