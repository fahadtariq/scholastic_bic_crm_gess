<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\SystemModule;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Laratrust\Helper;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmployeeRegisteredEmail;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::with(['roles', 'employee'])
                ->whereDoesntHave('employee')
                ->where('users.id', '!=', 1)
                ->where('users.id', '!=', auth()->user()->id)
                ->get();

            $dataTables =  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    return view('backend.users.actions', compact('row'));
                })
                ->addColumn('role', function ($row) {
                    $roles = implode(',' , $row->roles()->pluck('display_name')->toArray());
                    return $roles;
                });

            if(auth()->user()->hasRole('super_admin')){
                $dataTables->addColumn('is_active', function ($row) {
                    $active = $row->is_active ? 'checked' : 'unchecked';
                    $url = route('user.toggle-activation', ['user' => $row->id ]);
                    return
                        "<div class=' form-switch form-switch-md' dir='ltr'>
                                    <input type='checkbox' class='form-check-input is_active' name='' data-url='{$url}' data-id='{$row->id}'  onclick='' {$active} >
                        </div>";
                });

            }
            return $dataTables
                ->rawColumns(['action', 'roles', 'is_active'])
                ->make(true);
        }
        return view('backend.users.index');
    }

    public function create()
    {
        $roles = Role::whereIn('id', [1, 4, 6])->get();
        return view('backend.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:users',
            'password' => 'required',
            'role_ids' => 'required'
        ]);

        $inputs = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ];

        $user = User::create($inputs);

        if (!empty($user)) {
            $user->roles()->sync($request->role_ids);
            $password = $request->password;
            Mail::to($user->email)->send(new EmployeeRegisteredEmail($user, $password, 2));
        }

        return redirect()->route('users.index')->with('success', 'User created successfully.');
    }

    public function show(Request $request)
    {
        $user = User::find($request->id);
        return view('backend.users.user_profile_modal', compact('user'));
    }

    public function edit($id)
    {
        if ($id != null && $id != 1 && $id != auth()->user()->id) {
            $user = User::find($id);
            if (!empty($user)) {
                $roles = Role::whereIn('id', [1, 4, 6])->get();
                $userRolesCollection = $user->roles;
                $userRoles = [];
                if (count($userRolesCollection) > 0) {
                    foreach ($userRolesCollection as $userRole) {
                        array_push($userRoles, $userRole->id);
                    }
                }
                return view('backend.users.edit', compact('user', 'roles', 'userRoles'));
            }
        }
        return redirect()->route('users.index');
    }

    public function update(Request $request)
    {

        $request->validate([
            'name' => 'required|max:255',
            'role_ids' => 'required',
            'new_password' =>'max:255',
            'confirm_password' => 'same:new_password|max:255'
        ]);

        $inputs = [
            'name' => $request->name
        ];

        if(!empty($request->new_password)){
            $inputs['password'] = Hash::make($request->new_password);
        }

        $user = User::find($request->id);
        $userUpdate = $user->update($inputs);
        if ($userUpdate) {
            $user->roles()->sync($request->role_ids);
        }
        return redirect()->route('users.index')->with('success', 'User updated successfully.');
    }
    // roles permissions assignments
    public function userRolesPermissionList(Request $request)
    {

        $modelsKeys = array_keys(Config::get('laratrust.user_models'));
        $modelKey = $request->get('model') ?? $modelsKeys[0] ?? null;
        //dd(User::with(['roles','permissions'])->get()->toArray());
        if ($request->ajax()) {
            $data = User::with(['roles', 'permissions'])->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '
                        <a class="btn btn-sm btn-success btn-icon waves-effect waves-light" href="' . route("edit-with-role-permissions", ['id' => $row->id]) . '"><i class="mdi mdi-lead-pencil"></i></a>
                    ';

                    return $actionBtn;
                })
                ->addColumn('roles', function ($row) {
                    $count = ($row->roles->count());
                    return $count;
                })
                ->addColumn('permissions', function ($row) {
                    $count = ($row->permissions->count());
                    return $count;
                })
                ->rawColumns(['action', 'roles', 'permissions'])
                ->make(true);
        }

        return view('role_permissions_assignment.index', [
            'models' => $modelsKeys,
            'modelKey' => $modelKey,
        ]);
    }

    public function editUserRolesPermissions(Request $request, $id)
    {

        $user = User::query()
            ->with(['roles:id,name', 'permissions:id,name'])
            ->findOrFail($id);
        $roles = Role::orderBy('name')->get(['id', 'name', 'display_name', 'description'])
            ->map(function ($role) use ($user) {
                $role->assigned = $user->roles
                    ->pluck('id')
                    ->contains($role->id);
                $role->isRemovable = Helper::roleIsRemovable($role);

                return $role;
            });

        //['id', 'name', 'display_name' ,'description']
        /*   $permissions = Permission::with('system_modules')->orderBy('system_module_id')
           ->get()
           ->map(function ($permission) use ($user) {
               $permission->assigned = $user->permissions
                   ->pluck('id')
                   ->contains($permission->id);

               return $permission;
           });

  */
        $system_modules = SystemModule::where('parent_id', '<=>')->with('modules_permission')->orderBy('name')
            ->get();

        foreach ($system_modules as $key => $system_module) {

            $system_module->modules_permission->map(function ($permission) use ($user) {
                $permission->assigned = $user->permissions->pluck('id')->contains($permission->id);
                return $permission;
            });
        }





        //dd($system_modules->toArray());
        $data['roles'] = $roles;
        $data['permissions'] = $system_modules;
        $data['user'] = $user;

        return view('role_permissions_assignment.edit', $data);
    }

    public function updateUserRolesPermissions(Request $request, $id)
    {
        $modelKey = 'users';
        $userModel = Config::get('laratrust.user_models')[$modelKey] ?? null;

        if (!$userModel) {
            //'Model was not specified in the request';
            //return redirect()->back()->with('error','Unfortunately not able to update the role assignment');
        }

        $user = $userModel::findOrFail($id);
        $user->syncRoles($request->get('roles') ?? []);
        $user->syncPermissions($request->get('permissions') ?? []);

        return redirect()->back()->with('success', 'Your details for the user have been successfully updated!');
    }

    public function editProfile()
    {

        $user = auth()->user();
        if (!empty($user)) {
            return view('backend.users.edit-profile', compact('user'));
        }
        return redirect()->url('/');
    }

    public function updateProfile(Request $request)
    {
        $request->validate(['name' => 'required|max:255'], ['name.required' => 'Name is required!']);
        $user = auth()->user();
        $inputs['name'] = $request->name;
        if ($request->password != null) {
            $request->validate(['old_password' => 'required'], ['old_password.required' => 'Old password is required!']);
            if (Hash::check($request->old_password, $user->password)) {
                $inputs['password'] = Hash::make($request->password);
            } else {
                return back()->with('error', 'Old password is incorrect');
            }
        }
        User::find($user->id)->update($inputs);
        return redirect()->route('user.profile.edit')->with('success', 'Profile updated successfully.');
    }

    public function toggleUserActivation(User $user, Request $request)
    {
        try {
            return $user->update(['is_active' => $request->is_active]);
        }catch (QueryException $e){
            print_r($e->errorInfo);
        }
    }

    public function showSalesInsights()
    {
        return view('backend.data_insights.data_insights');
    }

    public function showCallCenterInsights()
    {
        return view('backend.data_insights.call_center_insights');
    }
}
