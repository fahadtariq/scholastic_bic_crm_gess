<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use App\Models\Status;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DataTables;

class CityController extends Controller
{

    //it returns main page and list of states  19/05/2022 by Jan Muhammad
    public function index(Request $request)
    {
        $statuses = Status::get();
        $countries = Country::where('status_id', 1)->get();
        if ($request->ajax()) {
            $data = City::with(['country', 'state'])->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.cities.actions', ['row' => $row]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('backend.cities.index', compact('statuses', 'countries'));
    }

    //it returns main page and store new states  19/05/2022 by Jan Muhammad
    public function store(Request $request)
    {
        $request->validate([
            'beams_id' => 'nullable|max:255',
            'name' => 'required|max:255|unique:cities,name',
            'status_id' => 'required|integer',
            'country_id' => 'required|integer',
            'state_id' => 'required|integer',
        ]);

        City::create($request->all());
        return redirect()->route('city.index')
            ->with('success', 'City created successfully.');
    }


    //it returns edit page  19/05/2022 by Jan Muhammad
    public function edit($id)
    {
        $city = City::find($id);
        $statuses = Status::get();
        $countries = Country::where('status_id', 1)->get();
        $states = State::where('status_id', 1)->get();
        return view('backend.cities.index', ['city' => $city, 'statuses' => $statuses, 'countries' => $countries, 'states' => $states]);
    }

    //it returns main page and update state  19/05/2022 by Jan Muhammad
    public function update(Request $request)
    {
        $city = City::find($request->id);
        $request->validate([
            'beams_id' => 'nullable|max:255',
            'name' => 'required|max:255|unique:cities,name,' . $request->id,
            'status_id' => 'required|integer',
            'country_id' => 'required|integer',
            'state_id' => 'required|integer',
        ]);

        $city->update($request->all());
        return redirect()->route('city.index')
            ->with('success', 'City updated successfully.');
    }

    //it returns main page and delete country  19/05/2022 by Jan Muhammad
    public function destroy(Request $request)
    {
        $city = City::find($request->id);
        try {
            return $city->delete();
        } catch (QueryException $e) {
            print_r($e->errorInfo);
        }
    }
}
