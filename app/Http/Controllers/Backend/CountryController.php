<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Status;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DataTables;

class CountryController extends Controller
{

    //it returns main page and list of countries  19/05/2022 by Jan Muhammad
    public function index(Request $request)
    {
        $statuses = Status::get();
        if ($request->ajax()) {
            $data = Country::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $status = getStatusPropertiesByID($row->status_id);
                    return '<span class="badge ' . $status['badge'] . '">' . $status['name'] . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return view('backend.countries.actions', ['row' => $row]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('backend.countries.index', compact('statuses'));
    }

    //it returns main page and store new country  19/05/2022 by Jan Muhammad
    public function store(Request $request)
    {
        $request->validate([
            'beams_id' => 'nullable|max:255',
            'name' => 'required|max:255|unique:countries,name',
            'status_id' => 'required|integer',
        ]);

        Country::create($request->all());
        return redirect()->route('country.index')
            ->with('success', 'Country created successfully.');
    }


    //it returns edit page  19/05/2022 by Jan Muhammad
    public function edit($id)
    {
        $country = Country::find($id);
        $statuses = Status::get();
        return view('backend.countries.index', ['country' => $country, 'statuses' => $statuses]);
    }

    //it returns main page and update country  19/05/2022 by Jan Muhammad
    public function update(Request $request)
    {
        $country = Country::find($request->id);
        $request->validate([
            'beams_id' => 'nullable|max:255',
            'name' => 'required|max:255|unique:countries,name,' . $request->id,
            'status_id' => 'required|integer',
        ]);

        $country->update($request->all());
        return redirect()->route('country.index')
            ->with('success', 'Country updated successfully.');
    }

    //it returns main page and delete country  19/05/2022 by Jan Muhammad
    public function destroy(Request $request)
    {
        $country = Country::find($request->id);
        try {
            return $country->delete();
        } catch (QueryException $e) {
            print_r($e->errorInfo);
        }
    }
}
