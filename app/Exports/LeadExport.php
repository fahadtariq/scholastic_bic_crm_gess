<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use App\Models\Lead;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LeadExport implements FromCollection,WithHeadings
{
    protected $requestData;

    public function __construct(Request $request)
    {
        $this->requestData = $request->all();
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $loggedInUser = auth()->user();
        $inputs = $this->requestData;
        $leads = Lead::orderBy('id', 'DESC');

        if (isset($inputs['lead_id_search']) && $inputs['lead_id_search'] > 0) {
            $leads->where('id', $inputs['lead_id_search']);
        }

        if (isset($inputs['beams_system_id_search']) && $inputs['beams_system_id_search'] > 0) {
            $leads->where('beams_system_id', $inputs['beams_system_id_search']);
        }

        if (isset($inputs['branch_id']) && $inputs['branch_id'] > 0) {
            $leads->where('branch_id', $inputs['branch_id']);
        }

        if (isset($inputs['session_id']) && $inputs['session_id'] > 0) {
            $leads->where('session_id', $inputs['session_id']);
        }

        if (isset($inputs['source_id']) && $inputs['source_id'] > 0) {
            $leads->where('source_id', $inputs['source_id']);
        }

        if (isset($inputs['tag_id']) && $inputs['tag_id'] > 0) {
            $leads->where('tag_id', $inputs['tag_id']);
        }

        if (isset($inputs['lead_type_id']) && $inputs['lead_type_id'] > 0) {
            $leads->where('lead_type_id', $inputs['lead_type_id']);
        }

        if (isset($inputs['lead_status_id']) && $inputs['lead_status_id'] > 0) {
            $leads->where('lead_status_id', $inputs['lead_status_id']);
        }

        if (isset($inputs['employee_id']) && $inputs['employee_id'] > 0) {
            $leads->where('employee_id', $inputs['employee_id']);
        }

        if (isset($inputs['follow_up_employee_id']) && $inputs['follow_up_employee_id'] > 0) {
            $leads->whereHas('followUps', function ($query) use ($inputs) {
                $query->where('user_id', $inputs['follow_up_employee_id']);
            });
        }

        if (isset($inputs['follow_up_by_employee_date']) && $inputs['follow_up_by_employee_date'] > 0) {
            $leads->whereHas('followUps', function ($query) use ($inputs) {
                $query->whereDate('follow_up_date', $inputs['follow_up_by_employee_date']);
            });
        }

        if (isset($inputs['is_referred'])) {
            $leads->where('is_referred', $inputs['is_referred']);
        }

        if (isset($inputs['is_sync'])) {
            $leads->where('is_sync', $inputs['is_sync']);
        }

        if (isset($inputs['is_confirmed'])) {
            $leads->where('is_confirmed', $inputs['is_confirmed']);
        }

        if (isset($inputs['is_important'])) {
            $leads->where('is_important', $inputs['is_important']);
        }

        if (isset($inputs['course_id'])) {
            $leads->whereIn('course_id', $inputs['course_id']);
        }

        if (isset($inputs['category'])) {
            $leads->where('category', $inputs['category']);
        }

        if (isset($inputs['mySearch'])) {
            $leads->whereHas('student', function ($query) use ($inputs) {
                $query->where('first_name', 'like', '%' . $inputs['mySearch'] . '%');
            });
        }

        if (isset($inputs['student_number'])) {
            $leads->whereHas('student', function ($query) use ($inputs) {
                $query->where('contact_number', 'like', '%' . $inputs['student_number'] . '%');
            });
        }

        if (isset($inputs['date_range']) && $inputs['date_range'] != '') {
            $dates = explode(" to ", $inputs['date_range']);
            if (count($dates) > 1) {
                $leads = $leads->whereDate('created_at', '>=', $dates[0])
                    ->whereDate('created_at', '<=', $dates[1]);
            } else {
                $leads = $leads->whereDate('created_at', $dates[0]);
            }
        }

        if ($loggedInUser->hasRole('campus_head|admin_officer')) {
            $leads->where('branch_id', $loggedInUser->employee->branch_id);
        } else if ($loggedInUser->hasRole('admission_advisor')) {
            $leads->where(function($query) use($loggedInUser){
                $query->where('employee_id', $loggedInUser->employee->id)
                    ->orWhere('user_id', $loggedInUser->id);
            });
        }
        $leads = $leads->get();
        $allLeads = [];
        foreach($leads as $key => $lead){
            $allLeads[] = [
                'ID' => $lead->id,
                'Student Name' => $lead->student->first_name ?? '',
                'Student Number' => $lead->student->contact_number ?? '',
                'Branch Name' => $lead->branch->name ?? '',
                'Code' => $lead->code ?? '',
                'Source' => $lead->source->name ?? '',
                'Tag' => $lead->tag->name ?? '',
                'Type' => $lead->type->name ?? '',
                'Course' => $lead->course->name ?? '',
                // 'Category' => $lead->category ?? '',
                'Status' => $lead->status->name ?? '',
                'Visitor ID' => $lead->beams_id ?? '',
                'Registration ID' => $lead->beams_registration_id ?? '',
                'Student ID' => $lead->beams_system_id ?? '',
                'Assigned To' => isset($lead->employee->first_name) ? $lead->employee->first_name . ' ' . $lead->employee->last_name : '',
                'Session' => $lead->session->name ?? '',
                'Created At' => $lead->created_at
            ];
        }
        return collect($allLeads);
    }
    public function headings(): array
    {
        // Specify the header
        return ['ID','Student Name','Student Number','Branch Name','Code','Source','Tag','Type','Course','Status','Visitor ID','Registration ID','Student ID','Assigned To','Session','Created At'];
    }
}
