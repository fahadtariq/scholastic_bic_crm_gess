<?php

namespace App\Exports;

use App\Models\Lead;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LeadsEmailExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $leads = Lead::with('student')->get();
        $emails = [];
        foreach($leads as $lead){
            // Check if the lead has a related student and if the student has an email
            if ($lead->student && $lead->student->email) {
                $emails[] = ['email' => $lead->student->email];
            }
        }
        return collect($emails);
    }

    public function headings(): array
    {
        // Specify the header for the "Email" column
        return ['Email'];
    }
}
