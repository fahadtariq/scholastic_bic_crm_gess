<?php
use App\Models\WhatsappChat;
use App\Models\WhatsappChatMessage;
use Twilio\Exceptions\RestException;
use Twilio\Rest\Client;

function getStatusPropertiesByID($id) {
    $badge = 'bg-danger';
    $statusName = \App\Models\Status::where('id', $id)->value('name');
    if ($statusName == 'Active') {
        $badge = 'bg-success';
    }
    return [
        'name' => $statusName,
        'badge' => $badge
    ];
}

function dateFormat($date) {
    return date("M d, Y", strtotime($date));
}

function dateTimeFormat($date) {
    return date("M d, Y h:i a", strtotime($date));
}

function getReasonByID(int $reasonID = null) {
    $reason = "";
    if ($reasonID != null) {
        $reason = \App\Models\Reason::where('id', $reasonID)->value('name');
    }
    return $reason;
}

function branchLeadsDroppedStatusReasonCount($branchID = null, int $reasonID = null, $onDate = null, $betweenDates = [], $sessionID = null, $leadStatusID = null) {
    $reasonCounts = 0;
    if ($branchID != null && $reasonID != null && $leadStatusID != null) {
        $leads = \App\Models\Lead::where(['branch_id' => $branchID, 'reason_id' => $reasonID, 'lead_status_id' => $leadStatusID]);
        $branch = \App\Models\Branch::where('id', $branchID)->first();
        if ($betweenDates != null) {
            $leads = $leads->whereDate('created_at', '>=', $betweenDates[0])
                ->whereDate('created_at', '<=', $betweenDates[1]);
        }

        if ($onDate != null ) {
            $leads = $leads->whereDate('created_at', $onDate);
        }
        $leads = $leads->where('session_id', $sessionID);
        $reasonCounts = $leads->count();
    }
    return $reasonCounts;
}
/*function is used to count no of leads those are dropped after successfully registered. method takes leads collection as a PARAMETER*/
function countDroppedLeadsAfterRegistered($leads) {
    $count = 0;
    if (count($leads) > 0) {
        foreach ($leads as $lead) {
            $leadTracking = \App\Models\LeadTracking::select('lead_id', 'lead_status_id', 'type')->where(['lead_id' => $lead->id])->latest();
            $latestLeadTracking = $leadTracking->first();
            /*if lead tracking latest record status is dropped*/
            if ($latestLeadTracking->lead_status_id == 4) {
                $leadTrackingSecondLatestRecord = $leadTracking->skip(1)->take(1)->first();
                if (!empty($leadTrackingSecondLatestRecord) && $leadTrackingSecondLatestRecord->lead_status_id == 5) {
                    $count++;
                }
            }
        }
    }
    return $count;
}

function pageLengthHTML() {
    return '<div class="col-md-2 col-sm-12">
                <div class="form-label-group in-border">
                    <select class="form-select" id="page_length" name="page_length">
                        <option value="" disabled>Records per page</option>
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <label for="page_length" class="form-label">Records per page</label>
                </div>
            </div>';
}

function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}

function potentialAdmission($branchID = null, $sessionID = null, $courseID = null){
    $total = 0;
    $leadCounts = 0;
    $types = \App\Models\LeadType::get();

    foreach ($types as $type) {
        if($type->id != 4){
            $leads = \App\Models\Lead::where('branch_id', $branchID)->where('lead_type_id', $type->id);
            if(!empty($courseID)){
                $leads = $leads->where('course_id', $courseID);
            }
            if(!empty($sessionID)){
                $leads = $leads->where('session_id', $sessionID);
            }
            $leadCounts = $leads->count();

            $probability = ($leadCounts * (int) $type->name) / 100;
            $total += $probability;
        }
    }

    return $total;
}
// Whatsapp integration
function sendWhatsappMessage($fromNumber, $sendToNumber, $leadId, $messageText, $attachmentUrlArray ){
    //$twilioSid = getenv('TWILIO_SID_SANDBOX');
    //$twilioToken = getenv('TWILIO_TOKEN_SANDBOX');

    $twilioSid = getenv('TWILIO_SID');
    $twilioToken = getenv('TWILIO_TOKEN');

    $businessNumber = env('SENDER_WHATSAPP_NUMBER');

    try {
        //To number
        $toNumber = "whatsapp:" . $sendToNumber;
        $fromNumber = "whatsapp:". $businessNumber;
        //Default twilio param array
        $twilioParamArray = array(
            "from" => $fromNumber,
            "body" => $messageText,
        );

        //If there's attachment add to twilio params
        if (isset($attachmentUrlArray) && count($attachmentUrlArray) > 0) {
            $twilioParamArray["mediaUrl"] = $attachmentUrlArray;
        }

        //dd($twilioParamArray);

        $twilio = new Client($twilioSid, $twilioToken);
        $message = $twilio->messages
            ->create(
                $toNumber, // to
                $twilioParamArray
            );

        if (
            $message->status == 'sent' ||
            $message->status == 'queued'
        ) {
            //Send whatsapp message
            saveWhatsappMessage( $businessNumber, $sendToNumber, $leadId, $messageText, 'Sent' );
            return $message->sid;

        } else {
            return false;
        }
    } catch (RestException $e) {
        return false;
    }
}

function saveWhatsappMessage($fromNumber, $sendToNumber, $leadId, $message, $status ){

    //Check if conversion already exists into database
    $chat = WhatsappChat::where(function ($query) use ($fromNumber, $sendToNumber) {
        $query->where('user1_phone_number', $fromNumber)
            ->where('user2_phone_number', $sendToNumber);
    })
        ->orWhere(function ($query) use ($fromNumber, $sendToNumber) {
            $query->where('user1_phone_number', $sendToNumber)
                ->where('user2_phone_number', $fromNumber);
        })->first();

    if($chat) {

        if($leadId > 0) {
            //Update lead id
            $chat->lead_id = $leadId;
            $chat->save();
        }
        //Create message record
        $whatsappMessage = WhatsappChatMessage::create(
            [
                'chat_id' => $chat->id,
                'text' => $message,
                'sender_phone_number' => $fromNumber,
                'status' => $status
            ]
        );
    }else{
        //Create new conversation
        $whatsappChatRecord = WhatsappChat::create(
            [
                'lead_id' => $leadId,
                'user1_phone_number' => $fromNumber,
                'user2_phone_number' => $sendToNumber
            ]
        );
        $whatsappMessage = WhatsappChatMessage::create(
            [
                'chat_id' => $whatsappChatRecord->id,
                'text' => $message,
                'sender_phone_number' => $fromNumber,
                'status' => $status
            ]
        );
    }

    return $whatsappMessage;
}
function getWhatsAppMessages($phoneNumber){

    $businessNumber = env('SENDER_WHATSAPP_NUMBER');

    $messages = NULL;

    //Check if chat/conversation exits
    $chat = WhatsappChat::where(function ($query) use ($businessNumber, $phoneNumber) {
        $query->where('user1_phone_number', $businessNumber)
            ->where('user2_phone_number', $phoneNumber);
        })
        ->orWhere(function ($query) use ($businessNumber, $phoneNumber) {
            $query->where('user1_phone_number', $phoneNumber)
                ->where('user2_phone_number', $businessNumber);
        })
        ->first();

    if($chat) {
        $messages = WhatsappChatMessage::where('chat_id', $chat->id)
            ->orderBy('created_at')
            ->get();
    }

    return $messages;
}
/**
 * @description function to get campaign email templates id and titles
 * @return string[]
 */
function emailTemplates() {
    return [
        1 => 'Basic',
        // 2 => 'UAE',
        // 3 => 'Akhuwat'
    ];
}

function outReachSources() {
    return [
        'BSS A Levels'
    ];
}
