<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmployeeRegisteredEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $employee;
    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($employee, $password, $type = 1)
    {
        $this->employee = $employee;
        $this->password = $password;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.employee-registered')
            ->subject("Employee Registered and System Credentials")
            ->with([
                'employee' => $this->employee,
                'password' => $this->password,
                'type' => $this->type
            ]);
    }
}
