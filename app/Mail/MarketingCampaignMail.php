<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MarketingCampaignMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = "emails.campaigns.basic";
        if ($this->data->email_template_id == 2) {
            $view = "emails.campaigns.uae";
        } else if ($this->data->email_template_id == 3) {
            $view = "emails.campaigns.akhuwat";
        }
        return $this->subject($this->data['title'])
            ->view($view)->with('content', $this->data['email_content']);
    }
}
