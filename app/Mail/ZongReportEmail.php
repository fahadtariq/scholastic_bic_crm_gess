<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ZongReportEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $outgoings;
    protected $incomings;
    protected $branchId;
    protected $branches;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($outgoings,$incomings,$branchId,$branches)
    {
        $this->outgoings = $outgoings;
        $this->incomings = $incomings;
        $this->branchId = $branchId;
        $this->branches = $branches;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        date_default_timezone_set("Asia/Karachi");
        $currentDate = date('M d, Y');
        return $this->markdown('emails.zong-report')
            ->subject("Zong Portal Report ($currentDate)")
            ->with([
                'outgoings' => $this->outgoings,
                'incomings' => $this->incomings,
                'branchId' => $this->branchId,
                'branches' => $this->branches,
                'currentDate' => $currentDate
            ]);
    }
}
