<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReminderEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $lead;
    protected $reminder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$lead,$reminder)
    {
        $this->user = $user;
        $this->lead = $lead;
        $this->reminder = $reminder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.reminder')
            ->subject("Lead Reminder")
            ->with([
                'user' => $this->user,
                'lead' => $this->lead,
                'reminder' => $this->reminder
            ]);
    }
}
