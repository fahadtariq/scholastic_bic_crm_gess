<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('reminder:execute')->everyMinute();
        $schedule->command('zong:report')->timezone('Asia/Karachi')->dailyAt('23:55');
        $schedule->command('zongportal:report')->timezone('Asia/Karachi')->dailyAt('23:55');
        // $schedule->command('leads:store')->timezone('Asia/Karachi')->dailyAt('11:00');
        // $schedule->command('getlead:register')->timezone('Asia/Karachi')->dailyAt('11:05');
        // $schedule->command('getlead:paid')->timezone('Asia/Karachi')->dailyAt('11:10');
        // $schedule->command('leads:store')->timezone('Asia/Karachi')->dailyAt('15:00');
        // $schedule->command('getlead:register')->timezone('Asia/Karachi')->dailyAt('15:05');
        // $schedule->command('getlead:paid')->timezone('Asia/Karachi')->dailyAt('15:10');
        $schedule->command('leads:store')->timezone('Asia/Karachi')->everyFifteenMinutes();
        $schedule->command('getlead:register')->timezone('Asia/Karachi')->everyThirtyMinutes();
        $schedule->command('getlead:paid')->timezone('Asia/Karachi')->everyThirtyMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
