<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Lead;
use App\Models\LeadTracking;
use Carbon\Carbon;

use function PHPUnit\Framework\isNull;

class GetPaidLead extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getlead:paid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command return lead which is paid';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = env('BEAMS_API_URL') . "/beaconhouse/index.php/bic/get_bic_paid_lead";
        $count = 0;
        $leadIdString = '';
        $leads = Lead::select('id', 'beams_id', 'beams_registration_id')
                    ->where('lead_status_id', 5)
                    ->where('is_sync', 1)
                    ->where('beams_id', '!=', null)
                    ->where('beams_id', '!=', '')
                    ->where('beams_registration_id', '!=', '')
                    ->where('beams_registration_id', '!=', null)
                    ->get();
        foreach ($leads as $lead) {
            try {
                $response = Http::withHeaders([
                    'api_key' => env('BEAMS_API_KEY'),
                    'api_secret' => env('BEAMS_API_SECRET')
                ])->get($url, ['registration_id' => $lead->beams_registration_id]);
                if ($response->getStatusCode() === 200) {
                    $response = json_decode($response->getBody()->getContents());
                    if ($response->success) {
                        $system_id = null;
                        $beams_paid_date = null;
                        if($response->result->system_id){
                            $system_id = $response->result->system_id->system_id;
                            $paidDateTime = isset($response->result->system_id->paid_date) && !empty($response->result->system_id->paid_date) ? Carbon::createFromFormat('d-M-y', $response->result->system_id->paid_date)->startOfDay()->format('Y-m-d H:i:s') : null;
                            $beams_paid_date = $paidDateTime;
                        }
                        $lead->update([
                            'beams_paid_date' => $beams_paid_date,
                            'beams_system_id' => $system_id,
                            'lead_status_id' => 6,
                        ]);
                        LeadTracking::create([
                            'lead_id' => $lead->id,
                            'lead_status_id' => 6, /*Paid*/
                            'type' => 1
                        ]);
                        $count++;
                        $leadIdString .= $lead->id . ',';
                    }
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return $count;
    }
}
