<?php

namespace App\Console\Commands;

use App\Models\Lead;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

use function PHPUnit\Framework\isNull;

class GetVisitorLeadBEAMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visitors:leads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purpose of this command is to get visitors leads from beams system. by calling beams api feeding all the data.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = 0;
        $url = env('BEAMS_API_URL') . "/beaconhouse/index.php/bic/get_bic_visitors_lead";
        $visitor_ids = Lead::where("beams_id",'!=', null)->where('beams_id', '!=', '')->where('lead_status_id',6)->pluck('beams_id')->toArray();
        if (count($visitor_ids) > 0) {
            try {
                $response = Http::withHeaders([
                    'api_key' => env('BEAMS_API_KEY'),
                    'api_secret' => env('BEAMS_API_SECRET')
                ])->get($url, ['visitor_id' => implode(',',$visitor_ids)]);
                if ($response->getStatusCode() === 200) {
                    $response = json_decode($response->getBody()->getContents());
                    if ($response->success) {
                        $beamsLeads = $response->result->leads;
                        foreach ($beamsLeads as $key => $lead) {
                            if(isset($lead) && !empty($lead) && !empty($lead->ENTERED_ON) && !empty($lead->PAID_DATE)){
                                $dateTime = Carbon::createFromFormat('d-M-y', $lead->ENTERED_ON)->startOfDay();
                                $paidDateTime = Carbon::createFromFormat('d-M-y', $lead->PAID_DATE)->startOfDay();
                                $visitor_lead = Lead::where("beams_id",$lead->VISITOR_ID)->first();
                                if($visitor_lead){
                                    $visitor_lead->beams_registration_date = $dateTime->format('Y-m-d H:i:s');
                                    $visitor_lead->beams_paid_date = $paidDateTime->format('Y-m-d H:i:s');
                                    // $visitor_lead->sync_date = Carbon::now()->format('Y-m-d H:i:s');
                                    $visitor_lead->save();
                                    $count++;
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return $count;
    }
}
