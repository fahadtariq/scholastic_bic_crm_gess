<?php

namespace App\Console\Commands;

use App\Models\Lead;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class GetLeadsFromBEAMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leads:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purpose of this command is to get synced leads updated status from beams system. by calling beams api feeding with requested lead beam ids';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = env('BEAMS_API_URL') . "/beaconhouse/index.php/bic/get_bic_leads";
        $leadsBeamIDs = Lead::whereNotIn('lead_status_id', [4, 6, 7])->where('is_sync', 1)->where('beams_id', '!=', null)->where('beams_id', '!=', '')->pluck('beams_id')->toArray();
        if (count($leadsBeamIDs) > 0) {
            try {
                $response = Http::withHeaders([
                    'api_key' => env('BEAMS_API_KEY'),
                    'api_secret' => env('BEAMS_API_SECRET')
                ])->get($url, ['lead_ids' => implode(',', $leadsBeamIDs)]);
                if ($response->getStatusCode() === 200) {
                    $response = json_decode($response->getBody()->getContents());
                    if ($response->success) {
                        $beamsLeads = $response->result->leads;
                        dd($beamsLeads);
                    }
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return 0;
    }
}
