<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Reminder;
use App\Models\User;
use App\Models\Lead;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReminderEmail;

class ReminderExecute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:execute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminders Execution';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        date_default_timezone_set("Asia/Karachi");
        $currentDate = date('Y-m-d H:i:00');
        $reminders = Reminder::where('is_executed', 0)->where('date_time', '<=',  $currentDate)->get();
        foreach ($reminders as $reminder) {
            $userInfo = User::select('id','name','email')->where('id', $reminder->user_id)->first();
            $leadInfo = Lead::where('id', $reminder->lead_id)->first();
            if(!empty($userInfo)){
                try{
                    Mail::to($userInfo->email)->send(new ReminderEmail($userInfo, $leadInfo, $reminder));
                }
                catch (e){

                }
            }
            $reminder->update(['is_executed' => 1]);
        }
        return 0;
    }
}
