<?php

namespace App\Console\Commands;

use Carbon\CarbonPeriod;
use App\Models\ZongPortal;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class StoreZongPortalData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zongportal:report { daterange?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Zong Portal Data acording to date range';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = 0;
        date_default_timezone_set("Asia/Karachi");
        if (!is_null($this->argument('daterange'))) {
            $dates = explode(" to ", $this->argument('daterange'));
            if (count($dates) > 1 && $dates[0] < $dates[1]) {
                $currentDate = $dates[0];
                $endDate = $dates[1];
                $period = CarbonPeriod::create($dates[0], $dates[1]);
            } else {
                $currentDate = $dates[0];
                $endDate = $dates[0];
                $period = CarbonPeriod::create($dates[0], $dates[0]);
            }
        } else {
            $currentDate = date('Y-m-d');
            $endDate = date('Y-m-d');
            $period = CarbonPeriod::create($currentDate, $endDate);

        }
        foreach ($period as $date) {
            $outgoings = $incomings = [];
            $apiToken = 'brEejfgLPu7x1HQuwQkgEEW7CevyEggRpwUMr4nkeupRwZR6KKhCsfkRJ7m9';
            $outgoingCallUrl = 'https://zong-cap.com.pk:93/api/customer/outgoing-calls';
            $incomingCallUrl = 'https://zong-cap.com.pk:93/api/customer/incoming-calls';
            $paramArr = array(
                'api_token' => $apiToken,
                'date_from' => $date->format('Y-m-d'),
                'date_to' => $date->format('Y-m-d'),
            );

            $outgoingResponse = Http::post($outgoingCallUrl, $paramArr);
            if ($outgoingResponse->successful()) {
                $outgoingRecords = $outgoingResponse->object();
                if (isset($outgoingRecords->Total)) {
                    $outgoings = collect($outgoingRecords->calls);
                    foreach ($outgoings as $call) {
                        $checData = ZongPortal::where('on_date', $call->datetime)->first();
                        if (is_null($checData)) {
                            $callData = [
                                'phone' => $call->phone ? substr($call->phone, 0) : '',
                                'client_ext' => is_null($call->ext) ? 'IVR' : ($call->ext == '' ? 'Queue' : $call->ext),
                                'on_date' => $call->datetime ? $call->datetime : '',
                                'duration' => $call->duration ? $call->duration : 0,
                                'status' => isset($call->status) ? $call->status : '',
                                'type' => 0,
                                'recording' => isset($call->recording) ? $call->recording : '',
                            ];
                            ZongPortal::create($callData);
                        }
                        $count++;
                    }
                }
            }

            $incomingResponse = Http::post($incomingCallUrl, $paramArr);
            if ($incomingResponse->successful()) {
                $incomingRecords = $incomingResponse->object();
                if (isset($incomingRecords->Total)) {
                    $incomings = collect($incomingRecords->calls);
                    foreach ($incomings as $call) {
                        $checData = ZongPortal::where('on_date', $call->datetime)->first();
                        if (is_null($checData)) {
                            $callData = [
                                'phone' => $call->phone ? substr($call->phone, 0) : '',
                                'client_ext' => is_null($call->ext) ? 'IVR' : ($call->ext == '' ? 'Queue' : $call->ext),
                                'on_date' => $call->datetime ? $call->datetime : '',
                                'duration' => $call->duration ? $call->duration : 0,
                                'status' => isset($call->status) ? $call->status : '',
                                'type' => 1,
                                'recording' => isset($call->recording) ? $call->recording : '',
                            ];
                            ZongPortal::create($callData);
                            $count++;
                        }
                    }
                }
            }
        }
        return $count;
    }
}
