<?php

namespace App\Console\Commands;

use App\Models\Lead;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class StoreLeadsOnBEAMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leads:store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purpose of this command is to store all the confirmed and not synced leads from bic to beams system';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = env('BEAMS_API_URL') . "/beaconhouse/index.php/bic/save_bic_leads";
        $leads = Lead::where(['is_confirmed' => 1, 'is_sync' => 0])->get();
        $count = 0;
        $leadIdString = '';
        if (count($leads) > 0) {
            foreach ($leads as $lead) {
                $params = [
                    'branch_id' => isset($lead->branch->beams_id) ? $lead->branch->beams_id : NULL,
                    'class_id' => isset($lead->student->classGrade) ? $lead->student->classGrade->beams_id : NULL,
                    'vis_date' => date('d-M-y', strtotime($lead->created_at)),
                    'vis_category' => "1",
                    'vis_name' => isset($lead->student->first_name) ? $lead->student->first_name : NULL,
                    'child_name' => NULL,
                    'cnic_number' => NULL,
                    'mobile_number' => isset($lead->student->contact_number) ? $lead->student->contact_number : NULL,
                    'address' => NULL,
                    'purpose_detail' => NULL,
                    'previous_school' => $lead->previous_branch,
                    'vis_email' => isset($lead->student->email) ? $lead->student->email : NULL,
                    'time_in' => NULL,
                    'time_out' => NULL,
                    'entered_by' => 999950,
                    'entered_on' => date('d-M-y'),
                    'close_date' => NULL,
                    'closed_by' => NULL,
                    'updt_userid' => 999950,
                    'updt_time' => date('d-M-y'),
                    'school_id' => NULL,
                    'inquiry_type' => isset($lead->source) ? $lead->source->name : NULL,
                    'abss_id' => NULL,
                    'close_reason_id' => NULL,
                    'bss_parent' => NULL,
                    'longitude' => NULL,
                    'latitude' => NULL,
                    'on_line' => NULL,
                    'abss_other' => NULL,
                    'online_class_id' => NULL
                ];
                try {
                    $response = Http::withHeaders([
                        'api_key' => env('BEAMS_API_KEY'),
                        'api_secret' => env('BEAMS_API_SECRET')
                    ])->post($url, $params);

                    if ($response->getStatusCode() === 200) {
                        $response = json_decode($response->getBody()->getContents());
                        if ($response->success) {
                            $beamsLeadID = $response->result->visitor_id;
                            $lead->update(['beams_id' => $beamsLeadID, 'is_sync' => 1, 'sync_date' => Carbon::now()->format('Y-m-d H:i:s')]);
                            $count++;
                            $leadIdString .= $lead->id . ',';
                        }
                    }
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }

        return $count;
    }
}
