<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\Branch;
use App\Models\Employee;
use App\Mail\ZongReportEmail;
use Illuminate\Support\Facades\Http;

class ZongReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zong:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Zong Report';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        date_default_timezone_set("Asia/Karachi");
        $currentDate = date('Y-m-d');
        $outgoings = $incomings = [];
        $apiToken = 'brEejfgLPu7x1HQuwQkgEEW7CevyEggRpwUMr4nkeupRwZR6KKhCsfkRJ7m9';
        $outgoingCallUrl = 'https://zong-cap.com.pk:93/api/customer/outgoing-calls';
        $incomingCallUrl = 'https://zong-cap.com.pk:93/api/customer/incoming-calls';
        $paramArr = array(
            'api_token' => $apiToken,
            'date_from' => $currentDate,
            'date_to' => $currentDate,
        );
        
        $outgoingResponse = Http::post($outgoingCallUrl, $paramArr);
        if($outgoingResponse->successful()){
            $outgoingRecords = $outgoingResponse->object();
            if(isset($outgoingRecords->Total)){
                $outgoings = collect($outgoingRecords->calls);
            }
        }

        $incomingResponse = Http::post($incomingCallUrl, $paramArr);
        if($incomingResponse->successful()){
            $incomingRecords = $incomingResponse->object();
            if(isset($incomingRecords->Total)){
                $incomings = collect($incomingRecords->calls);
            }
        }

        $branchId = 0; // For Admin
        $branches = Branch::select('id','name')->get();
        Mail::to(['usman.ismail@beaconhousetechnology.com','arslan.munir@bic.edu.pk','raanasarmad@beaconhouse.net'])->send(new ZongReportEmail($outgoings, $incomings, $branchId, $branches));
        foreach ($branches as $branch) {
            $employees = $branch->employees;
            $userEmails = $employees->map(function ($employee) {
                if ($employee->user->hasRole('campus_head')) {
                    return $employee->user->email;
                }
            })->filter();
            if(count($userEmails) > 0){
                Mail::to($userEmails->toArray())->send(new ZongReportEmail($outgoings, $incomings, $branch->id, $branches)); 
            }
        }
        return 0;
    }
}
