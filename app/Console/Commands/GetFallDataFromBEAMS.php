<?php

namespace App\Console\Commands;

use App\Models\Lead;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class GetFallDataFromBEAMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fall:leads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purpose of this command is to get fall 2022 sessions leads from beams system. by calling beams api feeding all the data.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = env('BEAMS_API_URL') . "/beaconhouse/index.php/bic/get_bic_fall_2022_leads";
        try {
            $response = Http::withHeaders([
                'api_key' => env('BEAMS_API_KEY'),
                'api_secret' => env('BEAMS_API_SECRET')
            ])->get($url);
            if ($response->getStatusCode() === 200) {
                $response = json_decode($response->getBody()->getContents());
                if ($response->success) {
                    $beamsLeads = $response->result->leads;
                    return $beamsLeads;
                }
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 0;
    }
}
