<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Lead;
use App\Models\LeadTracking;
use Carbon\Carbon;

use function PHPUnit\Framework\isNull;

class GetRegisterLead extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getlead:register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command return leads which is register';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = env('BEAMS_API_URL') . "/beaconhouse/index.php/bic/get_bic_register_lead";
        $count = 0;
        $leadIdString = '';
        $leads = Lead::select('id', 'beams_id')
                    ->whereIn('lead_status_id', [1,2,3])
                    ->where('is_sync', 1)
                    ->where('beams_id', '!=', null)
                    ->where('beams_id', '!=', '')
                    ->get();
        foreach ($leads as $lead) {
            try {
                $response = Http::withHeaders([
                    'api_key' => env('BEAMS_API_KEY'),
                    'api_secret' => env('BEAMS_API_SECRET')
                ])->get($url, ['lead_id' => $lead->beams_id]);
                if ($response->getStatusCode() === 200) {
                    $response = json_decode($response->getBody()->getContents());
                    if ($response->success) {
                        $registration_id = null;
                        $registration_date = null;
                        if($response->result->registration_id){
                            $registration_id = $response->result->registration_id->registration_id;
                            $registerDateTime = isset($response->result->registration_id->registration_date) && !empty($response->result->registration_id->registration_date) ? Carbon::createFromFormat('d-M-y', $response->result->registration_id->registration_date)->startOfDay()->format('Y-m-d H:i:s') : null;
                            $registration_date = $registerDateTime;
                        }
                        $lead->update([
                            'beams_registration_date' => $registration_date,
                            'beams_registration_id' => $registration_id,
                            'lead_status_id' => 5,
                        ]);
                        LeadTracking::create([
                            'lead_id' => $lead->id,
                            'lead_status_id' => 5, /*Registered*/
                            'type' => 1
                        ]);
                        $count++;
                        $leadIdString .= $lead->id . ',';
                    }
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return $count;
    }
}
