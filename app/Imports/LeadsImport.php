<?php

namespace App\Imports;

use App\Models\Branch;
use App\Models\BranchTarget;
use App\Models\Lead;
use App\Models\Source;
use App\Models\Course;
use App\Models\LeadTracking;
use App\Models\LeadFollowUp;
use App\Models\Student;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;

class LeadsImport implements ToCollection, WithHeadingRow
{
    protected $request;

    public function __construct($request)
    {
        $this->request = $request->all();
    }
    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        $Validator = Validator::make($rows->toArray(), [
            '*.std_name' => 'required|max:255',
            '*.sms_number' => 'required|digits_between:7,15',
            '*.class' => 'nullable|max:255',
            '*.official_email' => 'nullable|email|max:255',
            '*.source' => 'nullable|max:255',
            '*.br_name' => 'nullable|max:255',
            '*.remarks' => 'nullable|string|max:10000',
            '*.is_referred' => 'required|digits_between:0,1',
            '*.is_important' => 'required|digits_between:0,1',
            '*.course' => 'nullable|max:255',
        ])->validate();

        foreach ($rows as $row) {
            $student = Student::create([
                'first_name' => isset($row['std_name']) ? $row['std_name'] : '',
                'class_grade_id' => null,
                'gender' => 0,
                'contact_code_id' => 1,
                'contact_number' => isset($row['sms_number']) ? $row['sms_number'] : '',
                'email' => isset($row['official_email']) ? $row['official_email'] : ''
            ]);

            $userID = auth()->user()->id;
            // $branchID = (isset($row['br_name'])) ? Branch::where('name', $row['br_name'])->value('id') : 0;

            $branchID = 0;
            if(auth()->user()->hasRole('super_admin|social_media_manager')){
                 $branchID = $this->request['branch_id'];
            } else{
                $branchID = auth()->user()->employee->branch_id;
            }

            $source = Source::where('name', isset($row['source']) ? $row['source'] : null)->first();
            $course = Course::where('name', isset($row['course']) ? $row['course'] : null)->first();

            $branchTarget = BranchTarget::where('branch_id', $branchID)->latest()->first();

            /** auto assign employee to lead**/
            $employeeId = null;

            if (empty($employeeId) && !empty($branchID)) {
                $employeeId = Lead::employeeAutoAssign($branchID);
            }


            $lead = Lead::create([
                'code' => $student->id . rand(100, 999) . $userID,
                'user_id' => $userID,
                'student_id' => $student->id,
                'source_id' => isset($source) ? $source->id : null,
                'lead_type_id' => 5,
                'lead_status_id' => 1,
                'branch_id' => empty($branchID) ? null : $branchID,
                // 'session_id' => !empty($branchTarget) ? $branchTarget->id : null,
                'session_id' => isset($this->request['session_id']) ? $this->request['session_id'] : null,
                'course_id' => isset($course) ? $course->id : null,
                'employee_id' => $employeeId,
                'is_eligible' => 0,
                'is_referred' => $row['is_referred'],
                'is_important' => $row['is_important'],
                'previous_class' => isset($row['class']) ? $row['class'] : '',
                'previous_branch' => isset($row['br_name']) ? $row['br_name'] : ''
            ]);

            if(isset($row['remarks'])){
                LeadFollowUp::create([
                    'user_id' => auth()->user()->id,
                    'lead_id' => $lead->id,
                    'source_id' => isset($source) ? $source->id : null,
                    'follow_up_date' => date('Y-m-d H:i:s'),
                    'follow_up_by' => auth()->user()->name,
                    'follow_up_remarks' => $row['remarks']
                ]);
            }

            if (!empty($lead->branch_id) && $lead->branch_id != 0) {
                LeadTracking::create([
                    'lead_id' => $lead->id,
                    'branch_id' => $branchID,
                    'type' => 3
                ]);
            }

            LeadTracking::create([
                'lead_id' => $lead->id,
                'lead_status_id' => 1,
                'type' => 1
            ]);

            LeadTracking::create([
                'lead_id' => $lead->id,
                'lead_type_id' => 2,
                'type' => 0
            ]);
        }
    }
}
