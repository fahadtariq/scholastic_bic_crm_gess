<?php

namespace App\Imports;

use App\Models\OutreachData;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OutReachImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = [];
        foreach ($rows as $row) {
            // Process each row and collect data for bulk insert
            $data[] = [
                'city_id' => isset($row['city_id']) ? $row['city_id'] : 1,
                'source' => 'BSS A Levels',
                'city_name' => isset($row['city_name']) ? $row['city_name'] : null,
                'student_id' => isset($row['student_id']) ? $row['student_id'] : null,
                'student_name' => isset($row['student']) ? $row['student'] : null,
                'school' => isset($row['school']) ? $row['school'] : '',
                'class' => isset($row['class']) ? $row['class'] : null,
                'section' => isset($row['section']) ? $row['section'] : null,
                'contact_number' => isset($row['sms_number']) ? str_replace('-', '', $row['sms_number']) : null,
                'address' => isset($row['street_address']) ? $row['street_address'] : null,
                'email' => isset($row['communication_email']) ? $row['communication_email'] : null,
                'year' => isset($row['year']) ? (int) $row['year'] : null,
                'session' => isset($row['session']) ? $row['session'] : null,
                'received_date' => isset($row['received_date']) ? date('Y-m-d', strtotime($row['received_date'])) : null,
                'remarks' => isset($row['remarks']) ? $row['remarks'] : null,
                'status' => 2, // Mild
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }
        OutreachData::insert($data);
    }
}
