<?php

namespace App\Imports;

use App\Models\PreviousLead;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class PreviousLeadsImport implements ToCollection, WithHeadingRow, WithMultipleSheets
{
    protected $request;

    public function __construct($request)
    {
        $this->request = $request->all();
    }

    public function sheets(): array
    {
        return [
            '2' => $this, // Using $this means the collection() method of this class will be used and 2 is index of sheet
        ];
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collections)
    {
        $data = [];
        
        foreach ($collections as $row) {
            // Process each row and collect data for bulk insert
            $admissionDate = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['admission_date']);
            $paidDate = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['paid_date']);
            $data[] = [
                'region' => $row['region'], // Replace with your column names
                'school_id' => $row['school_id'],
                'branch' => $row['school'],
                'student_id' => $row['student_id'],
                'student_name' => $row['name'],
                'student_gender' => $row['gender'],
                'student_class' => $row['class'],
                'student_section' => $row['section'],
                'student_father_cnic' => $row['father_cnic'],
                'student_mother_cnic' => $row['mother_cnic'],
                'admission_fee' => $row['admission_fee'],
                'total' => $row['total'],
                'period' => $row['period'],
                'admission_date' => $admissionDate,
                'paid_date' => $paidDate,
                'total' => $row['total'],
                'registration_id' => $row['registration_id'],
            ];
        }
        // Bulk insert the collected data
        PreviousLead::insert($data);
    }
}
