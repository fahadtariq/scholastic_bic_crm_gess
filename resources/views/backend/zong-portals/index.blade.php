@extends('layouts.master')

@push('header_scripts')
@endpush

@section('content')
    @include('backend.components.flash_message')

    <div class="row">
        <div class="col-lg-12">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Calls <span id="dbCounter">(Count: {{ $callCounts }})</span></h4>
                @permission('add-branch')
                    <div class="flex-shrink-0">
                        <a href="{{ route('zong-portal.today-data') }}" class="btn btn-success btn-sm">
                            Get Today Data
                        </a>
                    </div>
                @endpermission
            </div>
            <div class="card">
                <div class="card-body">
                    <form action="javascript:;" id="filter-form">
                        <div class="row">
                            {!! pageLengthHTML() !!}
                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="form-select" name="call_type" id="call_type" onchange="changeType()">
                                        <option value="" selected>Please Select</option>
                                        <option value="0">Outgoing Calls</option>
                                        <option value="1">Incoming Calls</option>
                                    </select>
                                    <label for="call_type" class="form-label">Call Type</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="form-select custom-select2" name="agent_no[]" id="agent_no" multiple>
                                        @if (auth()->user()->hasRole('super_admin') ||
                                                (auth()->user()->hasRole('campus_head') &&
                                                    auth()->user()->employee->branch_id == 1))
                                            <optgroup label="BIC Islamabad">
                                                <option value="03178222171">03178222171</option>
                                                <option value="03178222172">03178222172</option>
                                            </optgroup>
                                        @endif
                                        @if (auth()->user()->hasRole('super_admin') ||
                                                (auth()->user()->hasRole('campus_head') &&
                                                    auth()->user()->employee->branch_id == 2))
                                            <optgroup label="BIC Potohar">
                                                <option value="03178222177">03178222177</option>
                                            </optgroup>
                                        @endif
                                        @if (auth()->user()->hasRole('super_admin') ||
                                                (auth()->user()->hasRole('campus_head') &&
                                                    auth()->user()->employee->branch_id == 3))
                                            <optgroup label="BIC Lahore">
                                                <option value="03178222173">03178222173</option>
                                                <option value="03178222174">03178222174</option>
                                            </optgroup>
                                        @endif
                                        @if (auth()->user()->hasRole('super_admin') ||
                                                (auth()->user()->hasRole('campus_head') &&
                                                    auth()->user()->employee->branch_id == 4))
                                            <optgroup label="BIC Faisalabad">
                                                <option value="03178222175">03178222175</option>
                                                <option value="03178222176">03178222176</option>
                                            </optgroup>
                                        @endif
                                        @if (auth()->user()->hasRole('super_admin'))
                                            <optgroup label="Other">
                                                <option value="03178222178">03178222178</option>
                                                <option value="03178222179">03178222179</option>
                                                <option value="03178222180">03178222180</option>
                                            </optgroup>
                                        @endif
                                    </select>
                                    <label for="agent_no" class="form-label">Extensions</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input type="text" class="form-control" name="client_no" id="client_no">
                                    <label for="client_no" class="form-label">Client No.</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="form-select" name="call_status" id="call_status">
                                        <option value="" selected>Please Select</option>
                                        <option value="Answered" class="incoming" style="display: none">Answered</option>
                                        <option value="Missed" class="incoming" style="display: none">Missed</option>
                                        <option value="Agent Hang Up" class="outgoing">Agent Hang Up</option>
                                        <option value="Busy" class="outgoing">Network Error</option>
                                        <option value="User Busy" class="outgoing">User Busy</option>
                                        <option value="User Not Attended" class="outgoing">User Not Attended</option>
                                        <option value="Connected" class="outgoing">Connected</option>
                                    </select>
                                    <label for="call_status" class="form-label">Status</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input type="number" min="0" class="form-control" name="duration" id="duration"
                                        value="0">
                                    <label for="duration" class="form-label">Duration (Min)</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="form-select" name="duration_filter" id="duration_filter">
                                        <option value="0" selected>>=</option>
                                        <option value="1">=</option>
                                        <option value="2">< </option>
                                    </select>
                                    <label for="duration_filter" class="form-label">Duration Filter</label>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input class="form-control filter" name="date_range" id="date_range"
                                        value="{{ date('Y-m-d') }} to {{ date('Y-m-d') }}">
                                    <label for="date_range" class="form-label">Date</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <button onclick="filterDataTable('{{ route('zong-portal.index') }}')" type="button"
                                    class="filter-lead-table-btn btn btn-primary btn-label rounded-pill"><i
                                        class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                    Filter</button>
                            </div>
                        </div>
                    </form>
                    <div id="custom-datatable-wrapper">
                        @include('backend.zong-portals.partial')
                    </div>
                    <!--Pagination-->
                    <div id="custom-pagination-wrapper">
                        <div class="row mt-3">
                            {{ $calls->links('layouts.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
    <script src="{{ asset('theme/dist/default/assets/js/pages/flatpickr.min.js') }}"></script>
    <script src="{{ asset('backend/modules/zong-portal.js') }}"></script>
@endpush
