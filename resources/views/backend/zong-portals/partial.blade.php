<table id="zong-data-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
    <thead>
        <tr>
            <th>Type</th>
            <th>Agent</th>
            <th>Clients</th>
            <th>Date / Time</th>
            <th>Duration (HH:MM:SS)</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach($calls as $call)
            <tr>
                <th>
                    {{ $call->type == 0 ? 'Outgoing' : 'Incoming' }}
                </th>
                <th>
                    {{ $call->client_ext }}
                </th>
                <th>
                    {{ $call->type == 0 ? substr($call->phone, 2) : substr($call->phone, 0) }}
                </th>

                <th>{{ dateTimeFormat($call->on_date) }}</th>
                <th>{{ gmdate("H:i:s", $call->duration)}}</th>
                <th>
                    @if($call->status == 'Connected' || $call->status == 'Answered')
                        <audio controls>
                            <source src="{{str_replace(".gsm", ".mp3", $call->recording)}}">
                            Your browser does not support the audio element.
                        </audio>
                    @else
                        <span style="color:red">{{ ($call->status == 'Busy') ? 'Network Error' : $call->status }}</span>
                    @endif
                </th>
            </tr>
        @endforeach
    </tbody>
</table>
