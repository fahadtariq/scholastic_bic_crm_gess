<div style="overflow-x: scroll">
    <table id="walk-in-sources-report-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%;">
        <tr><th colspan="100%">Sources</th></tr>

        <tr class="text-center">
            <th rowspan="3" style="background: #405189;color: white;">Branch</th>
            @foreach($sources as $source)
                <th colspan="{{count($leadStatuses)+3+count($leadTypes)}}" style="background-color: #{{random_color()}};color:white !important;">{{$source->name}}</th>
            @endforeach
        </tr>

        <tr class="text-center">
            @foreach($sources as $source)
                <th colspan="{{count($leadStatuses)+3}}" style="background: #6a94c3;color:white !important;">Data</th>
                <th colspan="{{count($leadTypes)}}" style="background: #9cb2cb;color:white !important;">Joining Probability</th>
            @endforeach
        </tr>

        <tr class="text-center">
            @foreach($sources as $source)
                @foreach($leadStatuses as $leadStatus)
                    <th style="background: #6a94c3;color:white !important;">
                        {{$leadStatus->name}}
                    </th>
                @endforeach
                <th style="background: #6a94c3;color:white !important;">
                    Registered%
                </th>
                <th style="background: #6a94c3;color:white !important;">
                    Paid%
                </th>
                <th style="background: #6a94c3;color:white !important;">
                    Total
                </th>

                @foreach($leadTypes->reverse() as $leadType)
                    @if($leadType->id != 4)
                        <th style="background: #9cb2cb;color:white !important;">
                            {{$leadType->name}}
                        </th>
                    @endif
                @endforeach
                <th style="background: #9cb2cb;color:white !important;">
                    Weight
                </th>
            @endforeach
        </tr>

        @php
            $totalLeadsCountsByStatus = $totalLeadsCountsByTypes = $sourcesVerticalTotalCounts = $registeredLeadsCountsBySources = $paidLeadsCountsBySources = $joiningProbabilityHorizontalLeadCounts = [];
        @endphp

        @foreach($branches as $branch)
            @php
                $branchTargetId = 0;
                if (isset($branch->session_id_bss_matric)) {
                    $branchTargetObject = $branch->targets()->where('id', $branch->session_id_bss_matric)->first();

                } else {
                    if (!is_null($branch->targets()->where('is_default', 1)->first())) {
                        $branchTargetObject = $branch->targets()->where('is_default', 1)->first();
                    } else {
                        $branchTargetObject = $branch->targets()->latest()->first();
                    }
                }
                if(isset($branchTargetObject)){
                    $branchTargetId = $branchTargetObject->id;
                }
            @endphp
            <tr class="text-center">
                <td>
                    {{$branch->name}}
                    <div class="form-label-group in-border">
                        <select class="session_filter session_filter_bss_matric form-select"
                            data-table-wrapper="walk-in-report-table-wrapper" name="session_id" placeholder="Session"
                            data-route="{{ route('reports.walk-in-sources') }}" style="width: 200px !important">
                            <option value="">Select Session</option>
                            @foreach ($branch->targets as $branchTarget)
                                <option
                                    @if (isset($branch->session_id_bss_matric))
                                    {{ $branchTarget->id == $branch->session_id_bss_matric ? 'selected' : '' }}
                                    @else
                                    {{ $branchTarget->is_default == 1 ? 'selected' : '' }} @endif
                                    value="{{ $branch->id . ',' . $branchTarget->id }}">{{ $branchTarget->name }}
                                </option>
                            @endforeach
                        </select>
                        <label for="session_id" class="form-label">Session</label>
                    </div>
                </td>
                @foreach($sources as $source)
                    @php
                        $totalLeadsCountsByBranchSource = $leadsCountsBySourceStatus = [];
                    @endphp
                    @foreach($leadStatuses as $leadStatus)
                        @php
                            $leadCounts = 0;
                            $sourceStatusLeads = $leadStatus->leads()->where('source_id', $source->id)->where('branch_id', $branch->id)->where('session_id', $branchTargetId);
                            if (isset($betweenDates) && !empty($betweenDates)) {
                                $sourceStatusLeads = $sourceStatusLeads->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                            }
                            if (isset($onDate) && !empty($onDate)) {
                                $sourceStatusLeads = $sourceStatusLeads->whereDate('created_at', $onDate);
                            }
                            $leadCounts = $sourceStatusLeads->count();

                            $leadsCountsBySourceStatus[$source->name][$leadStatus->name] = $leadCounts;
                            $totalLeadsCountsByStatus[$source->name][$leadStatus->name] = (isset($totalLeadsCountsByStatus[$source->name][$leadStatus->name])) ? $totalLeadsCountsByStatus[$source->name][$leadStatus->name] + $leadCounts : $leadCounts;
                            $totalLeadsCountsByBranchSource[$source->name] = (isset($totalLeadsCountsByBranchSource[$source->name])) ? $totalLeadsCountsByBranchSource[$source->name] + $leadCounts : $leadCounts;
                        @endphp
                        <td>
                            {{ $leadCounts }}
                        </td>
                    @endforeach

                    <td>
                        @foreach($leadStatuses as $leadStatus)
                            @if($leadStatus->id == 5)
                                @php
                                    $registeredPercentage = 0;
                                    if(isset($leadsCountsBySourceStatus[$source->name][$leadStatus->name]) && $totalLeadsCountsByBranchSource[$source->name] > 0){
                                        $registeredPercentage = number_format((($leadsCountsBySourceStatus[$source->name][$leadStatus->name]/$totalLeadsCountsByBranchSource[$source->name])*100), 1, '.');
                                    }
                                @endphp
                            @endif
                        @endforeach
                        {{ $registeredPercentage }}%
                    </td>

                    <td>
                        @foreach($leadStatuses as $leadStatus)
                            @if($leadStatus->id == 6)
                                @php
                                    $paidPercentage = 0;
                                    if(isset($leadsCountsBySourceStatus[$source->name][$leadStatus->name]) && $totalLeadsCountsByBranchSource[$source->name] > 0){
                                        $paidPercentage = number_format((($leadsCountsBySourceStatus[$source->name][$leadStatus->name]/$totalLeadsCountsByBranchSource[$source->name])*100), 1, '.');
                                    }
                                @endphp
                            @endif
                        @endforeach
                        {{ $paidPercentage }}%
                    </td>

                    <th>
                        @php
                            $sourcesVerticalTotalCounts[$source->name] = isset($sourcesVerticalTotalCounts[$source->name]) ? $sourcesVerticalTotalCounts[$source->name] + $totalLeadsCountsByBranchSource[$source->name] : $totalLeadsCountsByBranchSource[$source->name]
                        @endphp
                        {{ isset($totalLeadsCountsByBranchSource[$source->name]) ? $totalLeadsCountsByBranchSource[$source->name] : 0 }}
                    </th>

                    @foreach($leadTypes->reverse() as $leadType)
                        @if($leadType->id != 4)
                            @php
                                $probabilityLeadCounts = 0;
                                $sourceTypeLeads = $leadType->leads()->where('source_id', $source->id)->where('branch_id', $branch->id)->where('session_id', $branchTargetId);
                                if (isset($betweenDates) && !empty($betweenDates)) {
                                    $sourceTypeLeads = $sourceTypeLeads->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                                }
                                if (isset($onDate) && !empty($onDate)) {
                                    $sourceTypeLeads = $sourceTypeLeads->whereDate('created_at', $onDate);
                                }
                                $probabilityLeadCounts = $sourceTypeLeads->count();
                                $totalLeadsCountsByTypes[$source->name][$leadType->name] = isset($totalLeadsCountsByTypes[$source->name][$leadType->name]) ? $totalLeadsCountsByTypes[$source->name][$leadType->name] + $probabilityLeadCounts : $probabilityLeadCounts;
                            @endphp
                            <td>
                                @php
                                    $joiningProbability = ($probabilityLeadCounts * (int) $leadType->name) / 100;
                                    $joiningProbabilityHorizontalLeadCounts[$branch->name][$source->name] = isset($joiningProbabilityHorizontalLeadCounts[$branch->name][$source->name]) ? $joiningProbabilityHorizontalLeadCounts[$branch->name][$source->name] + $joiningProbability : $joiningProbability;
                                @endphp
                                {{ $joiningProbability }}
                            </td>
                        @endif
                    @endforeach
                    <td>
                        {{ isset($joiningProbabilityHorizontalLeadCounts[$branch->name][$source->name]) ? $joiningProbabilityHorizontalLeadCounts[$branch->name][$source->name] : 0 }}
                    </td>
                @endforeach
            </tr>
        @endforeach

        <tr class="text-center">
            <th>
                Total
            </th>
            @foreach($sources as $source)
                @foreach($leadStatuses as $leadStatus)
                    <th>
                        {{ isset($totalLeadsCountsByStatus[$source->name][$leadStatus->name]) ? $totalLeadsCountsByStatus[$source->name][$leadStatus->name] : 0 }}
                    </th>

                @endforeach
                <th>
                    @if(isset($registeredLeadsCountsBySources[$source->name]) && $sourcesVerticalTotalCounts[$source->name] > 0)
                        {{ floor(($registeredLeadsCountsBySources[$source->name]/$sourcesVerticalTotalCounts[$source->name]) * 100) }}%
                    @else
                        0%
                    @endif
                </th>
                <th>
                    @if(isset($paidLeadsCountsBySources[$source->name]) && $sourcesVerticalTotalCounts[$source->name] > 0)
                        {{ floor(($paidLeadsCountsBySources[$source->name]/$sourcesVerticalTotalCounts[$source->name]) * 100) }}%
                    @else
                        0%
                    @endif
                </th>
                <th>
                    {{ isset($sourcesVerticalTotalCounts[$source->name]) ? $sourcesVerticalTotalCounts[$source->name] : 0 }}
                </th>

                @php
                    $joiningProbabilityVerticalLeadCounts = 0;
                @endphp

                @foreach($leadTypes->reverse() as $leadType)
                    @if($leadType->id != 4)
                        @php
                            $joiningProbabilityFooterTotal = isset($totalLeadsCountsByTypes[$source->name][$leadType->name]) ? ($totalLeadsCountsByTypes[$source->name][$leadType->name] * (int) $leadType->name) / 100 : 0;
                            $joiningProbabilityVerticalLeadCounts += $joiningProbabilityFooterTotal;
                        @endphp
                        <th>
                            {{ $joiningProbabilityFooterTotal }}
                        </th>
                    @endif
                @endforeach
                <th>
                    {{ $joiningProbabilityVerticalLeadCounts }}
                </th>
            @endforeach
        </tr>
    </table>
</div>
<br>

<div style="overflow-x: scroll">
    <table id="walk-in-data-report-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
        <thead>
            <tr>
                <th colspan="100%" style="text-align: center; background-color: #{{random_color()}};color:white !important;">Data</th>
            </tr>
            <tr style="background-color: #{{random_color()}};color:white !important;">
                <th>Branch</th>
                <th>Target</th>
                @forelse($leadStatuses as $status)
                    @if(in_array($status->id, [5, 6]))
                        <th>{{ $status->name }}</th>
                    @endif
                @empty
                @endforelse
                @forelse($leadStatuses as $status)
                    @if(in_array($status->id, [5, 6]))
                        <th>{{ $status->name }}%</th>
                    @endif
                @empty
                @endforelse
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @php
                $overAllStatusCount = []; $overallTotalLeadsCount = $branchTargetTotalCounts = 0;
            @endphp
            @foreach($branches as $branch)
                @php
                    $branchTargetId = $branchTarget = $branchTotalLeads = 0;
                    if (isset($branch->session_id_target_data)) {
                    $branchTargetObject = $branch->targets()->where('id', $branch->session_id_target_data)->first();
                    } else {
                        if (!is_null($branch->targets()->where('is_default', 1)->first())) {
                            $branchTargetObject = $branch->targets()->where('is_default', 1)->first();
                        } else {
                            $branchTargetObject = $branch->targets()->latest()->first();
                        }
                    }
                    if(isset($branchTargetObject)){
                        $branchTargetId = $branchTargetObject->id;
                        $branchTarget = $branchTargetObject->target;
                    }
                    $branchTargetTotalCounts += $branchTarget;
                    $branchTotalLeads = $branch->leads()->where('session_id', $branchTargetId)->count();
                    $overallTotalLeadsCount += $branchTotalLeads;
                    $rowStatusCount = [];
                @endphp
                <tr>
                    <td>
                        {{ $branch->name }}
                        <div class="form-label-group in-border">
                            <select class="session_filter session_filter_target_data form-select"
                                data-table-wrapper="walk-in-report-table-wrapper" name="session_id" placeholder="Session"
                                data-route="{{ route('reports.walk-in-sources') }}" style="width: 200px !important" >
                                <option value="">Select Session</option>
                                @foreach ($branch->targets as $target)
                                    <option
                                        @if (isset($branch->session_id_target_data))
                                        {{ $target->id == $branch->session_id_target_data ? 'selected' : '' }}
                                        @else
                                        {{ $target->is_default == 1 ? 'selected' : '' }} @endif
                                        value="{{ $branch->id . ',' . $target->id }}">{{ $target->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="session_id" class="form-label">Session</label>
                        </div>
                    </td>
                    <td>
                        {{ $branchTarget }}
                    </td>
                    @foreach($leadStatuses as $status)
                        @if(in_array($status->id, [5, 6]))
                            @php
                                $statusLeads = $status->leads()->where('branch_id', $branch->id)->where('session_id', $branchTargetId);
                                if (isset($betweenDates) && !empty($betweenDates)) {
                                    $statusLeads = $statusLeads->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                                }
                                if (isset($onDate) && !empty($onDate)) {
                                    $statusLeads = $statusLeads->whereDate('created_at', $onDate);
                                }
                                $statusCount = $statusLeads->count();

                                $rowStatusCount[$status->name] = $statusCount;
                                $overAllStatusCount[$status->name] = (array_key_exists($status->name, $overAllStatusCount)) ? $overAllStatusCount[$status->name] + $statusCount : $statusCount;
                            @endphp
                            <td>{{ $statusCount }}</td>
                        @endif
                    @endforeach

                    @foreach($leadStatuses as $status)
                        @if(in_array($status->id, [5, 6]))
                            @php
                                if(isset($rowStatusCount[$status->name]) && $branchTotalLeads > 0){
                                    $statusPercentageCount = ($rowStatusCount[$status->name]/$branchTotalLeads)*100;
                                }
                                else{
                                    $statusPercentageCount = 0;
                                }
                            @endphp
                            <td>
                                {{ number_format($statusPercentageCount, 1, '.') }}%
                            </td>
                        @endif
                    @endforeach
                    <td>
                        {{$branchTotalLeads}}
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                <th>{{ $branchTargetTotalCounts }}</th>
                @forelse($leadStatuses as $status)
                    @if(in_array($status->id, [5, 6]))
                        <td>{{ isset($overAllStatusCount[$status->name]) ? $overAllStatusCount[$status->name] : 0 }}</td>
                    @endif
                @empty
                @endforelse
                @forelse($leadStatuses as $status)
                    @if(in_array($status->id, [5, 6]))
                        <td>
                            @if($overallTotalLeadsCount > 0)
                                @if(isset($overAllStatusCount[$status->name]))
                                    {{ number_format((($overAllStatusCount[$status->name]/$overallTotalLeadsCount) * 100) , 1, '.') }}%
                                @else
                                    0%
                                @endif
                            @else
                                0%
                            @endif
                        </td>
                    @endif
                @empty
                @endforelse
                <th>{{ $overallTotalLeadsCount }}</th>
            </tr>
        </tfoot>
    </table>
</div>
<br>

<div style="overflow-x: scroll">
    <table id="joining-report-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
        <thead>
            <tr>
                <th colspan="100%" style="text-align: center; background-color: #{{random_color()}};color:white !important;">Joining Probability</th>
            </tr>
            <tr style="background-color: #{{random_color()}};color:white !important;">
                <th>Branch</th>
                <th>No. of Students</th>
                <th>Probability</th>
                <th>Weighted Total</th>
            </tr>
        </thead>
        <tbody>
            @forelse($branches as $branch)
                @php
                    $branchTargetId = $totalNoOfStudents = $weightedTotal = $overallWeightedTotal = 0;
                    if (isset($branch->session_id_joining_probablity)) {
                    $branchTargetObject = $branch->targets()->where('id', $branch->session_id_joining_probablity)->first();
                    } else {
                        if (!is_null($branch->targets()->where('is_default', 1)->first())) {
                            $branchTargetObject = $branch->targets()->where('is_default', 1)->first();
                        } else {
                            $branchTargetObject = $branch->targets()->latest()->first();
                        }
                    }

                    if(isset($branchTargetObject)){
                        $branchTargetId = $branchTargetObject->id;
                    }
                @endphp
                    <tr>
                        <td rowspan="5">
                            {{ $branch->name }}
                            <div class="form-label-group in-border">
                                <select class="session_filter session_filter_joining_probablity form-select"
                                    data-table-wrapper="walk-in-report-table-wrapper" name="session_id" placeholder="Session"
                                    data-route="{{ route('reports.walk-in-sources') }}">
                                    <option value="">Select Session</option>
                                    @foreach ($branch->targets as $target)
                                        <option
                                            @if (isset($branch->session_id_joining_probablity))
                                            {{ $target->id == $branch->session_id_joining_probablity ? 'selected' : '' }}
                                            @else
                                            {{ $target->is_default == 1 ? 'selected' : '' }} @endif
                                            value="{{ $branch->id . ',' . $target->id }}">{{ $target->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <label for="session_id" class="form-label">Session</label>
                            </div>
                        </td>
                    </tr>
                    @foreach($leadTypes->reverse() as $type)
                        @if($type->id != 4)
                            @php
                                $typeLeads = $type->leads()->where('branch_id', $branch->id)->where('session_id', $branchTargetId);
                                if (isset($betweenDates) && !empty($betweenDates)) {
                                    $typeLeads = $typeLeads->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                                }
                                if (isset($onDate) && !empty($onDate)) {
                                    $typeLeads = $typeLeads->whereDate('created_at', $onDate);
                                }
                                $typesCount = $typeLeads->count();
                                $totalNoOfStudents += $typesCount;
                                $weightedTotal = ($typesCount * (int) $type->name) / 100;
                                $overallWeightedTotal += $weightedTotal;
                            @endphp
                            <tr>
                                <td>{{ $typesCount }}</td>
                                <td>{{ $type->name }}</td>
                                <td>
                                    {{ $weightedTotal }}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <th>Total</th>
                        <th>{{ $totalNoOfStudents }}</th>
                        <th>--</th>
                        <th>{{ $overallWeightedTotal }}</th>
                    </tr>
            @endforeach
        </tbody>
    </table>
</div>


