@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')
    <div class="row" id="page-body">
        <div class="col-lg-12">
            <div>
                <h5>Beaconhouse International College</h5>
                <h6>Sources Report</h6>
                <div id="date-changer">January {{ date('Y')}}<br>
                    As of {{ dateFormat(date('Y-m-d')) }}
                </div>
            </div><br>
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Walk-In Report</h4>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <input class="form-control source_date_range" data-table-wrapper="walk-in-report-table-wrapper" name="date_range" id="date_range" data-route="{{ route('reports.walk-in-sources') }}">
                                <label for="date_range" class="form-label">Date</label>
                            </div>
                        </div>

                        @permission('add-branch')
                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="source_branch_filter branch_filter form-select custom-select2" data-table-wrapper="walk-in-report-table-wrapper" id="branch_ids" data-route="{{ route('reports.walk-in-sources') }}" data-session-route="{{ route('branch.sessions') }}" name="branch_ids[]" placeholder="Branch" multiple>
                                        @forelse($branches as $branch)
                                            <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="branch_ids" class="form-label">Branch</label>
                                </div>
                            </div>
                        @endpermission

                        {{-- @if(auth()->user()->hasRole('super_admin'))
                            <div class="col-md-2 col-sm-12 session_select">
                            </div>
                        @elseif(auth()->user()->hasRole('campus_head'))
                            <div class="col-md-2 col-sm-12 session_select">
                                <div class="form-label-group in-border">
                                    <select class="session_filter form-select" data-table-wrapper="walk-in-report-table-wrapper" name="session_id" placeholder="Session">
                                    <option value="">Select Session</option>
                                        @foreach (auth()->user()->employee->branch->targets->reverse() as $branchTarget)
                                            <option value="{{$branchTarget->id}}">{{$branchTarget->name}}</option>
                                        @endforeach
                                    </select>
                                    <label for="session_id" class="form-label">Session</label>
                                </div>
                            </div>
                        @endif --}}


                    </div>

                    <div id="walk-in-report-table-wrapper">
                        @include('backend.reports.walk-in-sources.partial')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('header_scripts')
@endpush

@push('footer_scripts')
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script src="{{ asset('backend/modules/reports.js') }}"></script>
@endpush
