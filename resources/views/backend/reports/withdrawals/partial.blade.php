<div class="card" style="overflow-x:scroll">
    <div class="card-header align-items-center d-flex">
        <h4 class="card-title mb-0 flex-grow-1">Dropped Report</h4>
    </div>
    <div class="card-body">
        <table id="withdrawals-report-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
    <thead>
    <tr>
        <th style="background-color: #{{random_color()}};color:white !important;">Branch</th>
        @forelse($reasons as $reason)
            <th style="background-color: #{{random_color()}};color:white !important;">{{ $reason->name }}</th>
        @empty
        @endforelse
        <th style="background-color: #{{random_color()}};color:white !important;">Total Withdrawal</th>
        <th style="background-color: #{{random_color()}};color:white !important;">Avg Student Strength</th>
        <th style="background-color: #{{random_color()}};color:white !important;">Withdrawal%</th>
    </tr>
    </thead>
    <tbody>
    @php $totalWithdrawals = $totalWithdrawalsCounts = $totalStudents = $totalStudentsCounts = $reasonCounts = 0; $totalReasonCounts = [] ; @endphp
    @forelse($branches as $branch)
        @php
            $branchTargetId = $totalWithdrawals = $totalStudents = 0;
            if (isset($branch->session_id_dropped)) {
                $branchTargetObject = $branch->targets()->where('id', $branch->session_id_dropped)->first();

            } else {
                if (!is_null($branch->targets()->where('is_default', 1)->first())) {
                    $branchTargetObject = $branch->targets()->where('is_default', 1)->first();

                } else {
                    $branchTargetObject = $branch->targets()->latest()->first();

                }
            }

            if(isset($branchTargetObject)){
                $branchTargetId = $branchTargetObject->id;
            }

            $totalWithdrawals = $branch->leads();
            $totalStudents = $branch->leads();
            if (isset($betweenDates) && !empty($betweenDates)) {
                $totalWithdrawals = $totalWithdrawals->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                $totalStudents = $totalStudents->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
            }
            if (isset($onDate) && !empty($onDate)) {
                $totalWithdrawals = $totalWithdrawals->whereDate('created_at', $onDate);
                $totalStudents = $totalStudents->whereDate('created_at', $onDate);
            }
            $totalWithdrawals = $totalWithdrawals->where('lead_status_id', 4)->where('session_id', $branchTargetId)->count();
            $totalStudents = $totalStudents->where('session_id', $branchTargetId)->count();
        @endphp
        <tr>
            <td>{{ $branch->name ?? '' }}
                <div class="form-label-group in-border">
                    <select  class="session_filter session_filter_dropped form-select"
                        data-table-wrapper="withdrawals-report-table-wrapper" name="session_id" placeholder="Session"
                        data-route="{{ route('reports.withdrawals') }}" style="width: 200px !important">
                        <option value="">Select Session</option>
                        @foreach ($branch->targets as $branchTarget)
                            <option
                                @if (isset($branch->session_id_dropped))
                                {{ $branchTarget->id == $branch->session_id_dropped ? 'selected' : '' }}
                                @else
                                {{ $branchTarget->is_default == 1 ? 'selected' : '' }} @endif
                                value="{{ $branch->id . ',' . $branchTarget->id }}">{{ $branchTarget->name }}
                            </option>
                        @endforeach
                    </select>
                    <label for="session_id" class="form-label">Session</label>
                </div>
            </td>
            @forelse($reasons as $reason)
                @php
                    $reasonCounts = 0;
                    if (!empty($branchTargetObject)) {
                        $reasonCounts = branchLeadsDroppedStatusReasonCount($branch->id, $reason->id, isset($onDate) ? $onDate : null, isset($betweenDates) ? $betweenDates: null, $branchTargetId, 4);
                    }
                    $totalReasonCounts[$reason->id][$branch->id] = $reasonCounts;
                @endphp
                <td>{{ $reasonCounts }}</td>
            @empty
            @endforelse
            <td>
                @php $totalWithdrawalsCounts = $totalWithdrawalsCounts + $totalWithdrawals;  @endphp
                {{ $totalWithdrawals }}
            </td>
            <td>
                @php $totalStudentsCounts = $totalStudentsCounts + $totalStudents; @endphp
                {{ $totalStudents }}
            </td>
            <td>
                @if($totalStudents > 0)
                    {{ number_format((($totalWithdrawals/$totalStudents) * 100), 1 , '.') }}%
                @else
                    0%
                @endif
            </td>
        </tr>
    @empty
    @endforelse
    </tbody>
    <tfoot>
    <tr>
        <th>Total</th>
        @forelse($reasons as $reason)
            @if(isset($totalReasonCounts[$reason->id]))
                <th>{{ array_sum($totalReasonCounts[$reason->id]) }}</th>
            @else
                <th>0</th>
            @endif
        @empty
        @endforelse
        <th>{{ $totalWithdrawalsCounts ?? '' }}</th>
        <th>{{ $totalStudentsCounts ?? '' }}</th>
        <th>
            @if($totalStudentsCounts > 0)
                {{ number_format((($totalWithdrawalsCounts/$totalStudentsCounts) * 100), 1, '.') }}%
            @else
                0%
            @endif
        </th>
    </tr>
    </tfoot>
    </table>
    </div>
</div>
<div class="card" style="overflow-x:scroll">
    <div class="card-header align-items-center d-flex">
        <h4 class="card-title mb-0 flex-grow-1">Not Interested Report</h4>
    </div>
    <div class="card-body">
        <table id="withdrawals-report-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
            <thead>
            <tr>
                <th style="background-color: #{{random_color()}};color:white !important;">Branch</th>
                @forelse($reasons as $reason)
                    <th style="background-color: #{{random_color()}};color:white !important;">{{ $reason->name }}</th>
                @empty
                @endforelse
                <th style="background-color: #{{random_color()}};color:white !important;">Total Withdrawal</th>
                <th style="background-color: #{{random_color()}};color:white !important;">Avg Student Strength</th>
                <th style="background-color: #{{random_color()}};color:white !important;">Withdrawal%</th>
            </tr>
            </thead>
            <tbody>
            @php $totalWithdrawals = $totalWithdrawalsCounts = $totalStudents = $totalStudentsCounts = $reasonCounts = 0; $totalReasonCounts = [] ; @endphp
            @forelse($branches as $branch)
                @php
                    $branchTargetId = $totalWithdrawals = $totalStudents = 0;
                    if (isset($branch->session_id_not_interested)) {
                        $branchTargetObject = $branch->targets()->where('id', $branch->session_id_not_interested)->first();

                    } else {
                        if (!is_null($branch->targets()->where('is_default', 1)->first())) {
                            $branchTargetObject = $branch->targets()->where('is_default', 1)->first();

                        } else {
                            $branchTargetObject = $branch->targets()->latest()->first();

                        }
                    }
                    if(isset($branchTargetObject)){
                        $branchTargetId = $branchTargetObject->id;
                    }

                    $totalWithdrawals = $branch->leads();
                    $totalStudents = $branch->leads();
                    if (isset($betweenDates) && !empty($betweenDates)) {
                        $totalWithdrawals = $totalWithdrawals->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                        $totalStudents = $totalStudents->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                    }
                    if (isset($onDate) && !empty($onDate)) {
                        $totalWithdrawals = $totalWithdrawals->whereDate('created_at', $onDate);
                        $totalStudents = $totalStudents->whereDate('created_at', $onDate);
                    }
                    $totalWithdrawals = $totalWithdrawals->where('lead_status_id', 7)->where('session_id', $branchTargetId)->count();
                    $totalStudents = $totalStudents->where('session_id', $branchTargetId)->count();
                @endphp
                <tr>
                    <td>{{ $branch->name ?? '' }}
                        <div class="form-label-group in-border">
                            <select class="session_filter session_filter_not_interested form-select"
                                data-table-wrapper="withdrawals-report-table-wrapper" name="session_id" placeholder="Session"
                                data-route="{{ route('reports.withdrawals') }}" style="width: 200px !important">
                                <option value="">Select Session</option>
                                @foreach ($branch->targets as $branchTarget)
                                    <option
                                        @if (isset($branch->session_id_not_interested))
                                        {{ $branchTarget->id == $branch->session_id_not_interested ? 'selected' : '' }}
                                        @else
                                        {{ $branchTarget->is_default == 1 ? 'selected' : '' }} @endif
                                        value="{{ $branch->id . ',' . $branchTarget->id }}">{{ $branchTarget->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="session_id" class="form-label">Session</label>
                        </div>
                    </td>
                    @forelse($reasons as $reason)
                        @php
                            $reasonCounts = 0;
                            if (!empty($branchTargetObject)) {
                                $reasonCounts = branchLeadsDroppedStatusReasonCount($branch->id, $reason->id, isset($onDate) ? $onDate : null, isset($betweenDates) ? $betweenDates: null, $branchTargetId, 7);
                            }
                            $totalReasonCounts[$reason->id][$branch->id] = $reasonCounts;
                        @endphp
                        <td>{{ $reasonCounts }}</td>
                    @empty
                    @endforelse
                    <td>
                        @php $totalWithdrawalsCounts = $totalWithdrawalsCounts + $totalWithdrawals;  @endphp
                        {{ $totalWithdrawals }}
                    </td>
                    <td>
                        @php $totalStudentsCounts = $totalStudentsCounts + $totalStudents; @endphp
                        {{ $totalStudents }}
                    </td>
                    <td>
                        @if($totalStudents > 0)
                            {{ number_format((($totalWithdrawals/$totalStudents) * 100), 1 , '.') }}%
                        @else
                            0%
                        @endif
                    </td>
                </tr>
            @empty
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <th>Total</th>
                @forelse($reasons as $reason)
                    @if(isset($totalReasonCounts[$reason->id]))
                        <th>{{ array_sum($totalReasonCounts[$reason->id]) }}</th>
                    @else
                        <th>0</th>
                    @endif
                @empty
                @endforelse
                <th>{{ $totalWithdrawalsCounts ?? '' }}</th>
                <th>{{ $totalStudentsCounts ?? '' }}</th>
                <th>
                    @if($totalStudentsCounts > 0)
                        {{ number_format((($totalWithdrawalsCounts/$totalStudentsCounts) * 100), 1, '.') }}%
                    @else
                        0%
                    @endif
                </th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
<div class="card" style="overflow-x:scroll">
    <div class="card-header align-items-center d-flex">
        <h4 class="card-title mb-0 flex-grow-1">Withdrawals Report</h4>
    </div>
    <div class="card-body">
        <table id="withdrawals-report-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
            <thead>
            <tr>
                <th style="background-color: #{{random_color()}};color:white !important;">Branch</th>
                @forelse($reasons as $reason)
                    <th style="background-color: #{{random_color()}};color:white !important;">{{ $reason->name }}</th>
                @empty
                @endforelse
                <th style="background-color: #{{random_color()}};color:white !important;">Total Withdrawal</th>
                <th style="background-color: #{{random_color()}};color:white !important;">Avg Student Strength</th>
                <th style="background-color: #{{random_color()}};color:white !important;">Withdrawal%</th>
            </tr>
            </thead>
            <tbody>
            @php $totalWithdrawals = $totalWithdrawalsCounts = $totalStudents = $totalStudentsCounts = $reasonCounts = 0; $totalReasonCounts = [] ; @endphp
            @forelse($branches as $branch)
                @php
                    $branchTargetId = $totalWithdrawals = $totalStudents = 0;
                    if (isset($branch->session_id_withdrawal)) {
                        $branchTargetObject = $branch->targets()->where('id', $branch->session_id_withdrawal)->first();

                    } else {
                        if (!is_null($branch->targets()->where('is_default', 1)->first())) {
                            $branchTargetObject = $branch->targets()->where('is_default', 1)->first();

                        } else {
                            $branchTargetObject = $branch->targets()->latest()->first();

                        }
                    }
                    if(isset($branchTargetObject)){
                        $branchTargetId = $branchTargetObject->id;
                    }

                    $totalWithdrawals = $branch->leads();
                    $totalStudents = $branch->leads();
                    if (isset($betweenDates) && !empty($betweenDates)) {
                        $totalWithdrawals = $totalWithdrawals->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                        $totalStudents = $totalStudents->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                    }
                    if (isset($onDate) && !empty($onDate)) {
                        $totalWithdrawals = $totalWithdrawals->whereDate('created_at', $onDate);
                        $totalStudents = $totalStudents->whereDate('created_at', $onDate);
                    }
                    $totalWithdrawals = $totalWithdrawals->where('lead_status_id', 8)->where('session_id', $branchTargetId)->count();
                    $totalStudents = $totalStudents->where('session_id', $branchTargetId)->count();
                @endphp
                <tr>
                    <td>{{ $branch->name ?? '' }}
                        <div class="form-label-group in-border">
                            <select class="session_filter session_filter_withdrawal form-select"
                                data-table-wrapper="withdrawals-report-table-wrapper" name="session_id" placeholder="Session"
                                data-route="{{ route('reports.withdrawals') }}" style="width: 200px !important">
                                <option value="">Select Session</option>
                                @foreach ($branch->targets as $branchTarget)
                                    <option
                                        @if (isset($branch->session_id_withdrawal))
                                        {{ $branchTarget->id == $branch->session_id_withdrawal ? 'selected' : '' }}
                                        @else
                                        {{ $branchTarget->is_default == 1 ? 'selected' : '' }} @endif
                                        value="{{ $branch->id . ',' . $branchTarget->id }}">{{ $branchTarget->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="session_id" class="form-label">Session</label>
                        </div>
                    </td>
                    @forelse($reasons as $reason)
                        @php
                            $reasonCounts = 0;
                            if (!empty($branchTargetObject)) {
                                $reasonCounts = branchLeadsDroppedStatusReasonCount($branch->id, $reason->id, isset($onDate) ? $onDate : null, isset($betweenDates) ? $betweenDates: null, $branchTargetId, 8);
                            }
                            $totalReasonCounts[$reason->id][$branch->id] = $reasonCounts;
                        @endphp
                        <td>{{ $reasonCounts }}</td>
                    @empty
                    @endforelse
                    <td>
                        @php $totalWithdrawalsCounts = $totalWithdrawalsCounts + $totalWithdrawals;  @endphp
                        {{ $totalWithdrawals }}
                    </td>
                    <td>
                        @php $totalStudentsCounts = $totalStudentsCounts + $totalStudents; @endphp
                        {{ $totalStudents }}
                    </td>
                    <td>
                        @if($totalStudents > 0)
                            {{ number_format((($totalWithdrawals/$totalStudents) * 100), 1 , '.') }}%
                        @else
                            0%
                        @endif
                    </td>
                </tr>
            @empty
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <th>Total</th>
                @forelse($reasons as $reason)
                    @if(isset($totalReasonCounts[$reason->id]))
                        <th>{{ array_sum($totalReasonCounts[$reason->id]) }}</th>
                    @else
                        <th>0</th>
                    @endif
                @empty
                @endforelse
                <th>{{ $totalWithdrawalsCounts ?? '' }}</th>
                <th>{{ $totalStudentsCounts ?? '' }}</th>
                <th>
                    @if($totalStudentsCounts > 0)
                        {{ number_format((($totalWithdrawalsCounts/$totalStudentsCounts) * 100), 1, '.') }}%
                    @else
                        0%
                    @endif
                </th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
