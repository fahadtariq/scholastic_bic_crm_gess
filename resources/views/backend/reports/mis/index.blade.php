@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')
    <div class="row" id="page-body">
        <div class="col-lg-12">
            <div>
                <h5>Beaconhouse International College</h5>
                <h6>MIS Report</h6>
                <div id="date-changer">January {{ date('Y')}}<br>
                    As of {{ dateFormat(date('Y-m-d')) }}
                </div>
            </div><br>

            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">MIS Report</h4>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <input class="form-control date_range" data-table-wrapper="mis-report-table-wrapper" name="date_range" id="date_range" data-route="{{ route('reports.mis') }}">
                                <label for="date_range" class="form-label">Date</label>
                            </div>
                        </div>

                        @permission('add-branch')
                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="source_branch_filter branch_filter form-select custom-select2" data-table-wrapper="mis-report-table-wrapper" id="branch_ids" data-route="{{ route('reports.mis') }}" data-session-route="{{ route('branch.sessions') }}" name="branch_ids[]" placeholder="Branch" multiple>
                                    @forelse($branches as $branch)
                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <label for="branch_ids" class="form-label">Branch</label>
                            </div>
                        </div>
                        @endpermission

                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="course_filter form-select custom-select2" data-table-wrapper="mis-report-table-wrapper" id="course_ids" data-route="{{ route('reports.mis') }}" name="course_ids[]" placeholder="Course" multiple>
                                    @forelse($courses as $course)
                                        <option value="{{ $course->id }}">{{ $course->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <label for="course_ids" class="form-label">Courses</label>
                            </div>
                        </div>

                        <div class="col-md-2 col-sm-12 justify-content-end">
                            <a href="javascript:;" id="add_continue" class="btn btn-primary btn-label rounded-pill add_continue">
                                <i class="ri-add-box-line label-icon align-middle rounded-pill fs-16 me-2"></i>Add Continuing Data
                            </a>
                            <input type="hidden" value="{{$totalPromotionAdmissionCount}}" id="total_promotion_admission" name="total_promotion_admission" />
                        </div>
                        {{-- @if(auth()->user()->hasRole('super_admin'))
                            <div class="col-md-2 col-sm-12 session_select">
                            </div>
                        @elseif(auth()->user()->hasRole('campus_head'))
                            <div class="col-md-2 col-sm-12 session_select">
                                <div class="form-label-group in-border">
                                    <select class="session_filter form-select" data-table-wrapper="mis-report-table-wrapper" name="session_id" placeholder="Session">
                                        <option value="">Select Session</option>
                                        @foreach (auth()->user()->employee->branch->targets->reverse() as $branchTarget)
                                            <option value="{{$branchTarget->id}}">{{$branchTarget->name}}</option>
                                        @endforeach
                                    </select>
                                    <label for="session_id" class="form-label">Session</label>
                                </div>
                            </div>
                        @endif --}}
                    </div>

                    <div id="mis-report-table-wrapper">
                        @include('backend.reports.mis.partial')
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('header_scripts')
    <style>
        .table-fixed-width{
            table-layout: fixed;
        }
        .w-220-px{
            width: 220px !important
        }
        .w-200-px{
            width: 200px !important
        }
        .mis-report-table-div table th, .mis-report-table-div table td,
        .mis-report-table-branch-wise table th, .mis-report-table-branch-wise table td
        {
            min-width: 104px;
            max-width: 220px;
        }
    </style>
@endpush

@push('footer_scripts')
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script src="{{ asset('backend/modules/reports.js') }}"></script>
@endpush
