<div  class="table-responsive mis-report-table-branch-wise">
    <table id="mis-report-table-branch-wise" class="table table-bordered table-striped align-middle text-center mb-0">
        <thead>
        <tr >
            <th rowspan="2" class="w-220-px" style="background: #405189;color: white;">Branch</th>
        </tr>
        <tr style="background-color: #{{random_color()}};color:white !important;">
            <th>Target</th>
            <th>Register</th>
            <th>Dropped</th>
            <th>Paid</th>
            <th>Not Interested</th>
            {{-- <th>Unpaid</th> --}}
            <th>Potential Admissions</th>
            <th>Total Expected Admissions</th>
            <th>Total Expected Admission / Target %age</th>
            <th>Register / Target %age</th>
            <th>Dropped / Register %age</th>
            <th>Paid / Register %age</th>
            <th>Paid / Targets %age</th>
            <th>Potential Admissions / Targets %age</th>
        </tr>
        </thead>
        <tbody>
        @php
            $totalAddContinue = 0;
            if (isset($addContinueVal) && !empty($addContinueVal) && $addContinueVal == 'yes') {
                if (isset($branch_ids)) {
                    $totalAddContinue = \App\Models\PromotionalAdmission::whereIn("branch_id",$branch_ids)->sum('no_of_promotional_admissions');
                }else {
                    $totalAddContinue = $totalPromotionAdmissionCount;
                }
            } 
        @endphp
        @php $totalTargetCounts = $totalRegisteredCounts = $totalDroppedCounts = $totalPaidCounts = $totalNotInterestedCounts = $totalUnpaidCounts = $totalPotentialCounts = $totalPipelineCounts = 0; @endphp
        @foreach($branches as $branch)
            <tr>
                <td>
                    {{ $branch->name }}
                    <div class="form-label-group in-border w-200-px">
                        <select  class="session_filter session_filter_business_part2 form-select"
                                 data-table-wrapper="mis-report-table-wrapper" name="session_id" placeholder="Session"
                                 data-route="{{ route('reports.mis') }}" style="">
                            <option value="">Select Session</option>
                            @foreach ($branch->targets as $branchTarget)
                                <option
                                    @if (isset($branch->session_id_business_part2))
                                        {{ $branchTarget->id == $branch->session_id_business_part2 ? 'selected' : '' }}
                                    @else
                                        {{ $branchTarget->is_default == 1 ? 'selected' : '' }} @endif
                                    value="{{ $branch->id . ',' . $branchTarget->id }}">{{ $branchTarget->name }}
                                </option>
                            @endforeach
                        </select>
                        <label for="session_id" class="form-label">Session</label>
                    </div>
                </td>
                @php
                    $branchTargetId = 0;
                    if (isset($branch->session_id_business_part2)) {
                        $branchTargetObject = $branch->targets()->where('id', $branch->session_id_business_part2)->first();

                    } else {
                        if (!is_null($branch->targets()->where('is_default', 1)->first())) {
                            $branchTargetObject = $branch->targets()->where('is_default', 1)->first();

                        } else {
                            $branchTargetObject = $branch->targets()->latest()->first();

                        }
                    }

                    if(isset($branchTargetObject)){
                        $branchTargetId = $branchTargetObject->id;
                        $branchTarget = $branchTargetObject->target;
                    }

                    $totalTargetCounts = isset($totalTargetCounts) ? $totalTargetCounts + $branchTarget : $branchTarget;
                    $branchLeads = \App\Models\Lead::where(['branch_id' => $branch->id, 'session_id' => $branchTargetId]);
                    $continueAdm = 0;
                    if (isset($addContinueVal) && !empty($addContinueVal) && $addContinueVal == 'yes') {
                        $continueAdm = \App\Models\PromotionalAdmission::where("branch_id",$branch->id)->sum('no_of_promotional_admissions');
                    }
                    if (isset($betweenDates) && !empty($betweenDates)) {
                        $branchLeads = $branchLeads->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                    }
                    if (isset($onDate) && !empty($onDate)) {
                        $branchLeads = $branchLeads->whereDate('created_at', $onDate);
                    }
                    $branchLeads = $branchLeads->get();
                    $droppedLeadsByCourse = $registeredLeadsByCourse = $paidLeadsByCourse = $notInterestedLeadsByCourse = 0;
                    foreach ($branchLeads as $branchLead) {
                        if($branchLead->lead_status_id == 4){
                            $droppedLeadsByCourse++;
                        }
                        elseif($branchLead->lead_status_id == 5){
                            $registeredLeadsByCourse++;
                        }
                        elseif($branchLead->lead_status_id == 6 && $branchLead->beams_id != null && $branchLead->beams_paid_date != null){
                            $paidLeadsByCourse++;
                        }
                        elseif($branchLead->lead_status_id == 7){
                            $notInterestedLeadsByCourse++;
                        }
                    }

                    $totalDroppedCounts = isset($totalDroppedCounts) ?  $totalDroppedCounts + $droppedLeadsByCourse : $droppedLeadsByCourse;
                    $totalRegisteredCounts = isset($totalRegisteredCounts) ? $totalRegisteredCounts + $registeredLeadsByCourse : $registeredLeadsByCourse;
                    $totalPaidCounts = isset($totalPaidCounts) ? $totalPaidCounts + $paidLeadsByCourse : $paidLeadsByCourse;
                    $totalNotInterestedCounts = isset($totalNotInterestedCounts) ? $totalNotInterestedCounts + $notInterestedLeadsByCourse : $notInterestedLeadsByCourse;

                    $potentialCountsByCourse = potentialAdmission($branch->id, $branchTargetId);
                    $totalPotentialCounts = isset($totalPotentialCounts) ? $totalPotentialCounts + $potentialCountsByCourse : $potentialCountsByCourse;

                    $unPaidCountsByCourse = $registeredLeadsByCourse - $droppedLeadsByCourse - $paidLeadsByCourse;
                    $totalUnpaidCounts = isset($totalUnpaidCounts) ? $totalUnpaidCounts + $unPaidCountsByCourse : $unPaidCountsByCourse;

                    $pipelineCounts = $registeredLeadsByCourse + $paidLeadsByCourse + $potentialCountsByCourse;
                    $totalPipelineCounts += $pipelineCounts;
                @endphp

                <td>{{ $branchTarget }}</td>
                <td>{{ $registeredLeadsByCourse+$paidLeadsByCourse }}</td>
                <td>{{ $droppedLeadsByCourse }}</td>
                <td>{{ $paidLeadsByCourse+$continueAdm }}</td>
                <td>{{ $notInterestedLeadsByCourse }}</td>
                {{-- <td>{{ $unPaidCountsByCourse }}</td> --}}
                <td>{{ $potentialCountsByCourse }}</td>
                <td>{{ $pipelineCounts }}</td>
                <td>
                    @if($branchTarget > 0)
                        {{ number_format((($pipelineCounts/$branchTarget) * 100), 1, '.') }}%
                    @else
                        0.0%
                    @endif
                </td>
                <td>
                    @if($branchTarget > 0)
                        {{ number_format((($registeredLeadsByCourse/$branchTarget) * 100), 1, '.') }}%
                    @else
                        0.0%
                    @endif
                </td>
                <td>
                    @if($registeredLeadsByCourse > 0)
                        {{ number_format((($droppedLeadsByCourse/$registeredLeadsByCourse) * 100), 1, '.') }}%
                    @else
                        0.0%
                    @endif
                </td>
                <td>
                    @if($registeredLeadsByCourse > 0)
                        {{ number_format((($paidLeadsByCourse/$registeredLeadsByCourse) * 100), 1, '.') }}%
                    @else
                        0.0%
                    @endif
                </td>
                <td>
                    @if($branchTarget > 0)
                        {{ number_format((($paidLeadsByCourse/$branchTarget) * 100), 1, '.') }}%
                    @else
                        0.0%
                    @endif
                </td>
                <td>
                    @if($branchTarget > 0)
                        {{ number_format((($potentialCountsByCourse/$branchTarget) * 100), 1, '.') }}%
                    @else
                        0.0%
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>Total</th>
            <th>{{ isset($totalTargetCounts) ? $totalTargetCounts : 0 }}</th>
            <th>{{ isset($totalRegisteredCounts) ? $totalRegisteredCounts + $totalPaidCounts : 0 }}</th>
            <th>{{ isset($totalDroppedCounts) ? $totalDroppedCounts : 0 }}</th>
            <th>{{ isset($totalPaidCounts) ? $totalPaidCounts+$totalAddContinue : 0 }}</th>
            <th>{{ isset($totalNotInterestedCounts) ? $totalNotInterestedCounts : 0 }}</th>
            {{-- <th>{{ isset($totalUnpaidCounts) ? $totalUnpaidCounts : 0 }}</th> --}}
            <th>{{ isset($totalPotentialCounts) ? $totalPotentialCounts : 0 }}</th>
            <th>{{ $totalPipelineCounts }}</th>
            <th>
                @if(isset(($totalPipelineCounts)) && isset(($totalTargetCounts)) && $totalTargetCounts > 0)
                    {{ number_format((($totalPipelineCounts/$totalTargetCounts) * 100), 1, '.') }}%
                @else
                    0.0%
                @endif
            </th>
            <th>
                @if(isset(($totalRegisteredCounts)) && isset(($totalTargetCounts)) && $totalTargetCounts > 0)
                    {{ number_format((($totalRegisteredCounts/$totalTargetCounts) * 100), 1, '.') }}%
                @else
                    0.0%
                @endif
            </th>
            <th>
                @if(isset(($totalDroppedCounts)) && isset(($totalRegisteredCounts)) && $totalRegisteredCounts > 0)
                    {{ number_format((($totalDroppedCounts/$totalRegisteredCounts) * 100), 1, '.') }}%
                @else
                    0.0%
                @endif
            </th>
            <th>
                @if(isset(($totalPaidCounts)) && isset(($totalRegisteredCounts)) && $totalRegisteredCounts > 0)
                    {{ number_format((($totalPaidCounts/$totalRegisteredCounts) * 100), 1, '.') }}%
                @else
                    0.0%
                @endif
            </th>
            <th>
                @if(isset(($totalPaidCounts)) && isset(($totalTargetCounts)) && $totalTargetCounts > 0)
                    {{ number_format((($totalPaidCounts/$totalTargetCounts) * 100), 1, '.') }}%
                @else
                    0.0%
                @endif
            </th>
            <th>
                @if(isset(($totalPotentialCounts)) && isset(($totalTargetCounts)) && $totalTargetCounts > 0)
                    {{ number_format((($totalPotentialCounts/$totalTargetCounts) * 100), 1, '.') }}%
                @else
                    0.0%
                @endif
            </th>
        </tr>
        </tfoot>
    </table>
</div>
<hr class="my-4">

<div class="mis-report-table-div table-responsive">
    <table id="mis-report-table" class="table table-bordered table-striped align-middle text-center">
        <thead>
            @php $courseColors = []; @endphp
            @if(isset($selectedCourseIDs) && !empty($selectedCourseIDs))
                @php $courses = $courses->whereIn('id', $selectedCourseIDs); @endphp
            @else
                @php $courses = $courses; @endphp
            @endif
            <tr>
                <th rowspan="2" class="w-220-px"  style="background: #405189;color: white;">Branch</th>
                @foreach($courses as $course)
                    @php $courseColors[$course->name] = random_color(); @endphp
                    <th colspan="11" style="background-color: #{{$courseColors[$course->name]}};color:white !important;">{{$course->name}}</th>
                @endforeach
            </tr>
            <tr>
                @foreach($courses as $course)
                    <th class="w-220-px"  style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Target</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Register</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Dropped</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Paid</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Not Interested</th>
                    {{-- <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Unpaid</th> --}}
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Potential Admissions</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Register / Target %age</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Dropped / Register %age</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Paid / Register %age</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Paid / Targets %age</th>
                    <th style="background-color: #{{$courseColors[$course->name]}};color:white !important;">Potential Admissions / Targets %age</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @php $totalTargetCounts = $totalRegisteredCounts = $totalDroppedCounts = $totalPaidCounts = $totalUnpaidCounts = $totalNotInterestedCounts = $totalPotentialCounts = []; @endphp
            @foreach($branches as $branch)
                <tr>
                    <td>
                        {{ $branch->name }}
                        <div class="form-label-group in-border">
                            <select class="session_filter session_filter_business form-select"
                                data-table-wrapper="mis-report-table-wrapper" name="session_id" placeholder="Session"
                                data-route="{{ route('reports.mis') }}" style="width: 200px !important">
                                <option value="">Select Session</option>
                                @foreach ($branch->targets as $branchTarget)
                                    <option
                                        @if (isset($branch->session_id_business))
                                        {{ $branchTarget->id == $branch->session_id_business ? 'selected' : '' }}
                                        @else
                                        {{ $branchTarget->is_default == 1 ? 'selected' : '' }} @endif
                                        value="{{ $branch->id . ',' . $branchTarget->id }}">{{ $branchTarget->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="session_id" class="form-label">Session</label>
                        </div>
                    </td>
                    @foreach($courses as $course)
                        @php
                            $branchTargetId = $courseTarget = 0;
                            if (isset($branch->session_id_business)) {
                                $branchTargetObject = $branch->targets()->where('id', $branch->session_id_business)->first();

                            } else {
                                if (!is_null($branch->targets()->where('is_default', 1)->first())) {
                                    $branchTargetObject = $branch->targets()->where('is_default', 1)->first();
                                } else {
                                    $branchTargetObject = $branch->targets()->latest()->first();
                                }
                            }

                            if(isset($branchTargetObject)){
                                $branchTargetId = $branchTargetObject->id;
                                $courseTargetObject = \App\Models\BranchCourseTarget::where(['branch_id' => $branch->id, 'course_id' => $course->id, 'session_id' => $branchTargetId])->first();
                                if(isset($courseTargetObject) && !empty($courseTargetObject)){
                                    $courseTarget = $courseTargetObject->target;
                                }
                            }

                            $totalTargetCounts[$course->name] = isset($totalTargetCounts[$course->name]) ? $totalTargetCounts[$course->name] + $courseTarget : $courseTarget;
                            $courseLeads = \App\Models\Lead::where('course_id', $course->id)->where(['branch_id' => $branch->id, 'session_id' => $branchTargetId]);
                            if (isset($betweenDates) && !empty($betweenDates)) {
                                $courseLeads = $courseLeads->whereDate('created_at', '>=', $betweenDates[0])->whereDate('created_at', '<=', $betweenDates[1]);
                            }
                            if (isset($onDate) && !empty($onDate)) {
                                $courseLeads = $courseLeads->whereDate('created_at', $onDate);
                            }
                            $courseLeads = $courseLeads->get();
                            $droppedLeadsByCourse = $registeredLeadsByCourse = $paidLeadsByCourse = $notInterestedLeadsByCourse = 0;
                            foreach ($courseLeads as $courseLead) {
                                if($courseLead->lead_status_id == 4){
                                    $droppedLeadsByCourse++;
                                }
                                elseif($courseLead->lead_status_id == 5){
                                    $registeredLeadsByCourse++;
                                }
                                elseif($courseLead->lead_status_id == 6){
                                    $paidLeadsByCourse++;
                                }
                                elseif($courseLead->lead_status_id == 7){
                                    $notInterestedLeadsByCourse++;
                                }
                            }

                            $totalDroppedCounts[$course->name] = isset($totalDroppedCounts[$course->name]) ?  $totalDroppedCounts[$course->name] + $droppedLeadsByCourse : $droppedLeadsByCourse;
                            $totalRegisteredCounts[$course->name] = isset($totalRegisteredCounts[$course->name]) ? $totalRegisteredCounts[$course->name] + $registeredLeadsByCourse : $registeredLeadsByCourse;
                            $totalPaidCounts[$course->name] = isset($totalPaidCounts[$course->name]) ? $totalPaidCounts[$course->name] + $paidLeadsByCourse : $paidLeadsByCourse;
                            $totalNotInterestedCounts[$course->name] = isset($totalNotInterestedCounts[$course->name]) ? $totalNotInterestedCounts[$course->name] + $notInterestedLeadsByCourse : $notInterestedLeadsByCourse;

                            $potentialCountsByCourse = potentialAdmission($branch->id, $branchTargetId, $course->id);
                            $totalPotentialCounts[$course->name] = isset($totalPotentialCounts[$course->name]) ? $totalPotentialCounts[$course->name] + $potentialCountsByCourse : $potentialCountsByCourse;

                            $unPaidCountsByCourse = $registeredLeadsByCourse - $droppedLeadsByCourse - $paidLeadsByCourse;
                            $totalUnpaidCounts[$course->name] = isset($totalUnpaidCounts[$course->name]) ? $totalUnpaidCounts[$course->name] + $unPaidCountsByCourse : $unPaidCountsByCourse;
                        @endphp

                        <td>{{ $courseTarget }}</td>
                        <td>{{ $registeredLeadsByCourse }}</td>
                        <td>{{ $droppedLeadsByCourse }}</td>
                        <td>{{ $paidLeadsByCourse }}</td>
                        <td>{{ $notInterestedLeadsByCourse }}</td>
                        {{-- <td>{{ $unPaidCountsByCourse }}</td> --}}
                        <td>{{ $potentialCountsByCourse }}</td>
                        <td>
                            @if($courseTarget > 0)
                                {{ number_format((($registeredLeadsByCourse/$courseTarget) * 100), 1, '.') }}%
                            @else
                                0.0%
                            @endif
                        </td>
                        <td>
                            @if($registeredLeadsByCourse > 0)
                            {{ number_format((($droppedLeadsByCourse/$registeredLeadsByCourse) * 100), 1, '.') }}%
                            @else
                            0.0%
                            @endif
                        </td>
                        <td>
                            @if($registeredLeadsByCourse > 0)
                                {{ number_format((($paidLeadsByCourse/$registeredLeadsByCourse) * 100), 1, '.') }}%
                            @else
                                0.0%
                            @endif
                        </td>
                        <td>
                            @if($courseTarget > 0)
                                {{ number_format((($paidLeadsByCourse/$courseTarget) * 100), 1, '.') }}%
                            @else
                                0.0%
                            @endif
                        </td>
                        <td>
                            @if($courseTarget > 0)
                                {{ number_format((($potentialCountsByCourse/$courseTarget) * 100), 1, '.') }}%
                            @else
                                0.0%
                            @endif
                        </td>
                    @endforeach
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                @foreach($courses as $course)
                    <th>{{ isset($totalTargetCounts[$course->name]) ? $totalTargetCounts[$course->name] : 0 }}</th>
                    <th>{{ isset($totalRegisteredCounts[$course->name]) ? $totalRegisteredCounts[$course->name] : 0 }}</th>
                    <th>{{ isset($totalDroppedCounts[$course->name]) ? $totalDroppedCounts[$course->name] : 0 }}</th>
                    <th>{{ isset($totalPaidCounts[$course->name]) ? $totalPaidCounts[$course->name] : 0 }}</th>
                    <th>{{ isset($totalNotInterestedCounts[$course->name]) ? $totalNotInterestedCounts[$course->name] : 0 }}</th>
                    {{-- <th>{{ isset($totalUnpaidCounts[$course->name]) ? $totalUnpaidCounts[$course->name] : 0 }}</th> --}}
                    <th>{{ isset($totalPotentialCounts[$course->name]) ? $totalPotentialCounts[$course->name] : 0 }}</th>
                    <th>
                        @if(isset(($totalRegisteredCounts[$course->name])) && isset(($totalTargetCounts[$course->name])) && $totalTargetCounts[$course->name] > 0)
                            {{ number_format((($totalRegisteredCounts[$course->name]/$totalTargetCounts[$course->name]) * 100), 1, '.') }}%
                        @else
                            0.0%
                        @endif
                    </th>
                    <th>
                        @if(isset(($totalDroppedCounts[$course->name])) && isset(($totalRegisteredCounts[$course->name])) && $totalRegisteredCounts[$course->name] > 0)
                            {{ number_format((($totalDroppedCounts[$course->name]/$totalRegisteredCounts[$course->name]) * 100), 1, '.') }}%
                        @else
                            0.0%
                        @endif
                    </th>
                    <th>
                        @if(isset(($totalPaidCounts[$course->name])) && isset(($totalRegisteredCounts[$course->name])) && $totalRegisteredCounts[$course->name] > 0)
                            {{ number_format((($totalPaidCounts[$course->name]/$totalRegisteredCounts[$course->name]) * 100), 1, '.') }}%
                        @else
                            0.0%
                        @endif
                    </th>
                    <th>
                        @if(isset(($totalPaidCounts[$course->name])) && isset(($totalTargetCounts[$course->name])) && $totalTargetCounts[$course->name] > 0)
                            {{ number_format((($totalPaidCounts[$course->name]/$totalTargetCounts[$course->name]) * 100), 1, '.') }}%
                        @else
                            0.0%
                        @endif
                    </th>
                    <th>
                        @if(isset(($totalPotentialCounts[$course->name])) && isset(($totalTargetCounts[$course->name])) && $totalTargetCounts[$course->name] > 0)
                            {{ number_format((($totalPotentialCounts[$course->name]/$totalTargetCounts[$course->name]) * 100), 1, '.') }}%
                        @else
                            0.0%
                        @endif
                    </th>
                @endforeach

            </tr>
        </tfoot>
    </table>
</div>
<hr>
