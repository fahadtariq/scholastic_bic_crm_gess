<div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Create New Contact Code</h4>
            <!-- <div class="flex-shrink-0">
                <div class="form-check form-switch form-switch-right form-switch-md">
                    <label for="FormVaidationCustom" class="form-label text-muted">Show Code</label>
                    <input class="form-check-input code-switcher" type="checkbox" id="FormVaidationCustom">
                </div>
            </div> -->
        </div><!-- end card header -->

        <div class="card-body">
            <div class="live-preview">
                <form class="row g-3 needs-validation" novalidate action="{{ route('contact-code.store') }}"
                    method="post">
                    @csrf
                    <div class="col-md-6">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('code')) is-invalid @endif"
                                id="code" name="code" placeholder="Please enter Code" value="{{ old('code') }}"
                                required>
                            <label for="code" class="form-label">Code</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('code'))
                                    {{ $errors->first('code') }}
                                @else
                                    Code is required!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="country_id" required>
                                <option value="" disabled selected>Country options</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}"
                                        @if (old('country_id') == $country->id) {{ 'selected' }} @endif>
                                        {{ $country->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="countryID" class="form-label">Country list</label>
                            <div class="invalid-tooltip">Select the Country!</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="status_id" required>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status->id }}"
                                        @if (old('status_id') == $status->id) {{ 'selected' }} @endif>
                                        {{ $status->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="statusID" class="form-label">Status</label>
                            <div class="invalid-tooltip">Select the status!</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control" name="icon" id="icon"
                                placeholder="Please enter icon" value="{{ old('icon') }}">
                            <label for="icon" class="form-label">Icon</label>
                            <div class="invalid-tooltip">Icon is required!</div>
                        </div>
                    </div>
                    <div class="col-12 text-end">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                        <button type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
