<div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Edit Contact Code</h4>
            <div class="flex-shrink-0">
                <!-- <a href="{{ route('contact-code.index') }}" class="btn btn-success btn-label btn-sm">
                    <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Add New Country
                </a> -->
                {{-- @permission('add-country') --}}
                <a href="{{ route('contact-code.index') }}" class="btn btn-sm btn-soft-success">
                    <i class="ri-add-circle-line align-middle me-1"></i> Add New Contact Code
                </a>
                {{-- @endpermission --}}
            </div>
        </div><!-- end card header -->

        <div class="card-body">
            <div class="live-preview">
                <form class="row g-3 needs-validation" novalidate action="{{ route('contact-code.update') }}"
                    method="post">
                    @csrf
                    <div class="col-md-6">
                        <div class="form-label-group in-border">
                            <input name="id" value="{{ $contact_code->id }}" hidden>
                            <input type="text" class="form-control @if ($errors->has('code')) is-invalid @endif"
                                id="code" name="code" placeholder="Please enter code"
                                value="{{ $contact_code->code }}" required>
                            <label for="code" class="form-label">Code</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('code'))
                                    {{ $errors->first('code') }}
                                @else
                                    Code is required!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="country_id" required>
                                <option value="" disabled selected>Country options</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}"
                                        @if ($contact_code->country_id == $country->id) {{ 'selected' }} @endif>
                                        {{ $contact_code->name }}</option>
                                @endforeach
                            </select>
                            <label for="countryID" class="form-label">Country list</label>
                            <div class="invalid-tooltip">Select the Country!</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="status_id" required>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status->id }}"
                                        @if ($contact_code->status_id == $status->id) {{ 'selected' }} @endif>
                                        {{ $status->name }}</option>
                                @endforeach
                            </select>
                            <label for="statusID" class="form-label">Status</label>
                            <div class="invalid-tooltip">Select the status!</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control" name="icon" id="icon"
                                placeholder="Please enter icon" value="{{ $contact_code->icon }}">
                            <label for="icon" class="form-label">Icon</label>
                            <div class="invalid-tooltip">Icon is required!</div>
                        </div>
                    </div>
                    <div class="col-12 text-end">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                        <a href="{{ route('contact-code.index') }}"
                            class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
