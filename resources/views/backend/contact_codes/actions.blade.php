@permission('edit-contact-code')
<a href="{{ route('contact-code.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
<i class="mdi mdi-lead-pencil"></i>
</a>
@endpermission

@permission('delete-contact-code')
<a href="{{ route('contact-code.destroy') }}" data-rowid="{{ $row->id }}" data-table="contact-code-data-table"
class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
<i class="ri-delete-bin-5-line"></i>
</a>
@endpermission

@if (!auth()->user()->hasPermission('edit-contact-code') &&
    !auth()->user()->hasPermission('delete-contact-code'))
    <span>N/A</span>
@endif
