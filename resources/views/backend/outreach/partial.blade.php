<table id="outreach-data-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
    <thead>
    <tr>
        <th>Student ID</th>
        <th>Student Name</th>
        <th>Email</th>
        <th>School</th>
        <th>Contact Number</th>
        <th>Year</th>
        <th>Source</th>
        <th>Received Date</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($outReachData as $outreach)
        <tr>
            <td>{{ $outreach->student_id }}</td>
            <td>{{ $outreach->student_name }}</td>
            <td>{{ $outreach->email }}</td>
            <td>{{ $outreach->school }}</td>
            <td>{{ $outreach->contact_number }}</td>
            <td>{{ $outreach->year }}</td>
            <td>{{ $outreach->source }}</td>
            <td>{{ $outreach->received_date }}</td>
            <td>

                @permission('generate-outreach-lead')
                    <a href="{{ route('lead.create', ['name' => $outreach->student_name, 'contact_number' => $outreach->contact_number, 'email' => $outreach->email, 'city' => $outreach->city_name, 'out_reach_id' => $outreach->id, 'source' => $outreach->source]) }}" class="btn btn-lg btn-primary btn-icon waves-effect waves-light blink_me" title="Generate Lead">
                        <i class="mdi mdi-upload"></i>
                    </a>
                @endpermission

                @permission('edit-outreach-data')
                    <a href="{{ route('outreach.edit', $outreach->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
                        <i class="mdi mdi-lead-pencil"></i>
                    </a>
                @endpermission

                @permission('delete-outreach-data')
                <a href="{{ route('outreach.delete') }}" data-rowid="{{ $outreach->id }}"
                   class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-outreach">
                    <i class="ri-delete-bin-5-line"></i>
                </a>
                @endpermission

                @if (!auth()->user()->hasPermission('edit-outreach-data') &&
                    !auth()->user()->hasPermission('delete-outreach-data') &&
                    !auth()->user()->hasPermission('generate-outreach-lead'))
                    <span>N/A</span>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
