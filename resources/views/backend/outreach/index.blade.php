@extends('layouts.master')

@push('header_scripts')
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.core.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.snow.css') }}" rel="stylesheet" type="text/css"/>

    <style>
        .border-follow-up {
            padding: 3px;
            border: solid 1px #d3caca;
        }

        .lead-card {
            min-height: 200px;
            overflow: auto;
            max-height: 200px;
        }

        .lead-card-error-msg {
            overflow: auto;
            max-height: 200px;
        }

        ul.timeline {
            list-style-type: none;
            position: relative;
        }

        ul.timeline:before {
            content: ' ';
            background: #d4d9df;
            display: inline-block;
            position: absolute;
            left: 29px;
            width: 2px;
            height: 100%;
            z-index: 400;
        }

        ul.timeline > li {
            margin: 20px 0;
            padding-left: 20px;
        }

        ul.timeline > li:before {
            content: ' ';
            background: white;
            display: inline-block;
            position: absolute;
            border-radius: 50%;
            border: 3px solid #22c0e8;
            left: 20px;
            width: 20px;
            height: 20px;
            z-index: 400;
        }

        .blink_me {
            animation: blinker 1s linear infinite;
        }

        .button-container {
            display: flex;
            align-items: center;
        }

        .button-container > * {
            margin-right: 10px; /* Adjust the spacing between buttons */
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }

        /*Blinking Styles Starts*/
        @-webkit-keyframes invalid {
            from {
                background-color: rgb(255, 153, 102);
            }
            to {
                background-color: inherit;
            }
        }

        @-moz-keyframes invalid {
            from {
                background-color: rgb(255, 153, 102);
            }
            to {
                background-color: inherit;
            }
        }

        @-o-keyframes invalid {
            from {
                background-color: rgb(255, 153, 102);
            }
            to {
                background-color: inherit;
            }
        }

        @keyframes invalid {
            from {
                background-color: rgb(255, 153, 102);
            }
            to {
                background-color: inherit;
            }
        }

        .blink-invalid {
            -webkit-animation: invalid 2s infinite;
        / Safari 4 + / -moz-animation: invalid 2 s infinite;
        / Fx 5 + / -o-animation: invalid 2 s infinite;
        / Opera 12 + / animation: invalid 2 s infinite;
        / IE 10 + /
        }

        /*Blinking Styles Ends*/
    </style>
@endpush

@section('content')
    @include('backend.components.flash_message')

    <div class="row">
        <div class="col-lg-12">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Out Reach Data <span id="dbCounter">(Count: {{$outReachDataCounts}})</span>
                </h4>
                <div class="flex-shrink-0 button-container">
                    @permission('import-outreach-data')
                    <form id="uploadForm" action="{{ route('import.outreach') }}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file" id="fileInput" style="display: none;">
                        <button type="button"
                                class="btn btn-success btn-label btn-sm import-leads-btn import-outreach-btn">
                            <i class="ri-upload-cloud-line label-icon align-middle fs-16 me-2"></i> Import
                        </button>
                    </form>
                    @endpermission

                    @permission('add-outreach-data')
                    <a href="{{ route('outreach.create') }}" class="btn btn-success btn-label btn-sm">
                        <i class="ri-add-fill label-icon align-middle fs-16 me-2"></i> Add New
                    </a>
                    @endpermission
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <form action="javascript:;" id="filter-form">
                        <div class="row">
                            {!! pageLengthHTML() !!}

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input id="student_id" type="text" placeholder="Search.." class="form-control"
                                           name="student_id">
                                    <label for="student_id" class="form-label">Student ID</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input id="student_name" type="text" placeholder="Student Number"
                                           class="form-control" name="student_name">
                                    <label for="student_name" class="form-label">Student Name</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="form-control" name="source">
                                        <option value="">Select Source</option>
                                        @foreach(outReachSources() as $source)
                                            <option value="{{ $source }}">{{ $source }}</option>
                                        @endforeach
                                    </select>
                                    <label for="student_name" class="form-label">Source</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input id="year" type="text" placeholder="Student Number"
                                           class="form-control" name="year">
                                    <label for="year" class="form-label">Year</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input id="date" type="text" placeholder="Student Number"
                                           class="form-control" autocomplete="false" name="date_range">
                                    <label for="date" class="form-label">Received Date</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <button onclick="filterDataTable('{{route("outreach.index")}}')" type="button"
                                        class="filter-outreach-table-btn btn btn-primary btn-label rounded-pill"><i
                                        class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                    Filter
                                </button>
                            </div>
                        </div>
                    </form>
                    <div id="custom-datatable-wrapper">
                        @include('backend.outreach.partial')
                    </div>

                    <!--Pagination-->
                    <div id="custom-pagination-wrapper">
                        <div class="row mt-3">
                            {{ $outReachData->links('layouts.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input id="ajaxRoute" value="{{ route('leads.index') }}" hidden/>

    {{--    @include('backend.leads.partials.import')--}}
@endsection

@push('footer_scripts')
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script src="{{ asset('backend/modules/outreach.js') }}"></script>
    @if (count($errors) > 0)
        <script>
            $(document).ready(function () {
                $('#importLeadsModal').modal('show');
            });
        </script>
    @endif
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            const button = document.querySelector(".import-outreach-btn");
            const fileInput = document.querySelector("#fileInput");
            const form = document.querySelector("#uploadForm");

            button.addEventListener("click", function (event) {
                event.preventDefault();
                fileInput.click();
            });

            fileInput.addEventListener("change", function () {
                form.submit();
            });
        });
    </script>
@endpush
