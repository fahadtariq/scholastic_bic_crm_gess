@extends('layouts.master')
@push('header_scripts')
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.snow.css') }}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    @include('backend.components.flash_message')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Update Out Reach Data</h4>
                </div>

                <div class="card-body">
                    <form class="row g-3 needs-validation" action="{{ route('outreach.update', $outReachData->id) }}" method="POST" enctype="multipart/form-data" novalidate>
                        @csrf

                        <input type="hidden" name="id" value="{{ $outReachData->id }}">

                        <h6><u>Student&nbsp;Information</u></h6>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('student_id')) is-invalid @endif" id="student_id" name="student_id" placeholder="First Name" value="{{ $outReachData->student_id }}"  required>
                                <label for="student_id" class="form-label">ID</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('student_id'))
                                        {{ $errors->first('student_id') }}
                                    @else
                                        Student id is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('student_name')) is-invalid @endif" id="student_name" name="student_name" placeholder="Name" value="{{ $outReachData->student_name }}"  required>
                                <label for="student_name" class="form-label">Name</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('student_name'))
                                        {{ $errors->first('student_name') }}
                                    @else
                                        Name is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="email" class="form-control @if($errors->has('email')) is-invalid @endif" id="email" name="email" placeholder="Email" value="{{ $outReachData->email }}" required>
                                <label for="email" class="form-label">Email</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('email'))
                                        {{ $errors->first('email') }}
                                    @else
                                        Email is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="number" class="form-control @if($errors->has('contact_number')) is-invalid @endif" id="contact_number" name="contact_number" placeholder="Contact Number" value="{{ $outReachData->contact_number }}" required>
                                <label for="contact_number" class="form-label">Contact&nbsp;Number</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('contact_number'))
                                        {{ $errors->first('contact_number') }}
                                    @else
                                        Contact number is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('school')) is-invalid @endif" id="school" name="school" placeholder="School" value="{{ $outReachData->school }}"  required>
                                <label for="school" class="form-label">School</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('school'))
                                        {{ $errors->first('school') }}
                                    @else
                                        School is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('class')) is-invalid @endif" id="class" name="class" placeholder="Class" value="{{ $outReachData->class }}"  required>
                                <label for="class" class="form-label">Class</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('class'))
                                        {{ $errors->first('class') }}
                                    @else
                                        Class is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('section')) is-invalid @endif" id="section" name="section" placeholder="First Name" value="{{ $outReachData->section }}"  required>
                                <label for="section" class="form-label">Section</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('section'))
                                        {{ $errors->first('section') }}
                                    @else
                                        Section is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('session_info')) is-invalid @endif" id="session_info" name="session_info" placeholder="Session" value="{{ $outReachData->session }}"  required>
                                <label for="session_info" class="form-label">Session</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('session_info'))
                                        {{ $errors->first('session_info') }}
                                    @else
                                        Session info is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{ $outReachData->city_name }}">
                                <label for="city" class="form-label">City</label>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-label-group in-border">
                                <textarea type="text" class="form-control" id="address" name="address" placeholder="Address">{{ $outReachData->address }}</textarea>
                                <label for="address" class="form-label">Address</label>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('source')) is-invalid @endif" id="source" name="source" aria-label="Source select" required>
                                    <option value="">Select Source</option>
                                    @foreach(outReachSources() as $source)
                                        <option value="{{ $source }}" {{ ($outReachData->source == $source) ? 'selected' : '' }}>{{ $source }}</option>
                                    @endforeach
                                </select>
                                <label for="source" class="form-label">Source</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('source'))
                                        {{ $errors->first('source') }}
                                    @else
                                        Select the source!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('year')) is-invalid @endif" id="year" name="year" placeholder="Year" value="{{ $outReachData->year }}" required>
                                <label for="year" class="form-label">Year</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('year'))
                                        {{ $errors->first('year') }}
                                    @else
                                        Year is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('received_date')) is-invalid @endif" id="received_date" name="received_date" placeholder="Received" value="{{ $outReachData->received_date }}" required>
                                <label for="received_date" class="form-label">Received Date</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('received_date'))
                                        {{ $errors->first('received_date') }}
                                    @else
                                        Select the received date!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <h6><u>Remarks</u></h6>
                            <div class="form-label-group in-border">
                                <div id="snow-editor" style="height: 300px;">{!! $outReachData->remarks !!}</div>
                                <input type="hidden" class="form-control" id="remarks" name="remarks" placeholder="Remarks">
                            </div>
                        </div>

                        <div class="col-12 text-end">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <a href="{{ route('outreach.index') }}" type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
    <script src="{{ asset('theme/dist/default/assets/libs/quill/quill.min.js') }}"></script>
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script src="{{ asset('backend/modules/outreach.js') }}"></script>
@endpush
