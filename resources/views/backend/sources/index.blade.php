@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')

    <div class="row">
        @if (isset($source))
            @permission('edit-source')
            @include('backend.sources.edit')
            @endpermission
        @else
             @permission('add-source')
                @include('backend.sources.create')
             @endpermission
        @endif

        <div class="col-lg-12">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Sources</h4>
            </div>
            <div class="card">
                <div class="card-body">
                    <table id="sources-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Abbreviation</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Abbreviation</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ route('sources.index') }}" id="source_list_url">
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('backend/modules/sources.js') }}"></script>
@endpush
