@permission('edit-source')
<a href="{{ route('source.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
<i class="mdi mdi-lead-pencil"></i>
</a>
@endpermission

@permission('delete-source')
<a href="{{ route('source.delete') }}" data-rowid="{{ $row->id }}" data-table="sources-table"
class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
<i class="ri-delete-bin-5-line"></i>
</a>
@endpermission

 @if (!auth()->user()->hasPermission('edit-source') &&
    !auth()->user()->hasPermission('delete-source'))
     <span>N/A</span>
 @endif
