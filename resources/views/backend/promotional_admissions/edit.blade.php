<div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Update Continue Admission</h4>
        </div>

        <div class="card-body">
            <div class="live-preview">
                <form class="row g-3 promotional_admission_form needs-validation" novalidate action="{{ route('promotional-admissions.update', $promotionalAdmission->id ) }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="branch_id" required>
                                <option value="" disabled selected>Branch options</option>
                                @foreach($branches as $branch)
                                    <option value="{{ $branch->id }}" {{ $branch->id == $promotionalAdmission->branch_id ? 'selected' : '' }}>{{ $branch->name }}</option>
                                @endforeach
                            </select>
                            <label for="branch_id" class="form-label">Branch list</label>
                            <div class="invalid-tooltip">Select the Branch!</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('no_of_promotional_admissions')) is-invalid @endif"
                                   id="no_of_promotional_admissions" name="no_of_promotional_admissions" placeholder="Please enter Promotional Admission" value="{{ $promotionalAdmission->no_of_promotional_admissions }}"
                                   required>
                            <label for="no_of_promotional_admissions" class="form-label">Num of Continue Admissions</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('no_of_promotional_admissions'))
                                    {{ $errors->first('no_of_promotional_admissions') }}
                                @else
                                    Num of Continue Admissions is required!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('admission_date')) is-invalid @endif" name="admission_date" id="admission_date"
                                   placeholder="Entry Date" readonly>
                            <label for="admission_date" class="form-label">Entry Date</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('admission_date'))
                                    {{ $errors->first('admission_date') }}
                                @else
                                    Num of Continue Admissions is required!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-end">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                        <button type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
