@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')
    <div class="row">
        @if (isset($promotionalAdmission))
            @permission('')
            @endpermission
            @include('backend.promotional_admissions.edit');
        @else
            @permission('')
            @endpermission
            {{-- @include('backend.promotional_admissions.create') --}}
        @endif

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Continue Admissions</h4>
                </div><!-- end card header -->
                <div class="card-body">
                    <table id="promotional-admissions_table" class="table table-bordered table-striped align-middle table-nowrap mb-0"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Branch</th>
                            <th>Num of Continue Admissions</th>
                            <th>Date</th>
                            {{-- <th>Added to Paid Lead</th> --}}
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Branch</th>
                            <th>Num of Continue Admissions</th>
                            <th>Date</th>
                            {{-- <th>Added to Paid Lead</th> --}}
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input id="ajaxRoute" value="{{ route('promotional-admissions.index') }}" hidden />
    @isset($promotionalAdmission)
        <input type="hidden" id="omit_promotional_admission" name="omit_promotional_admission" value="{{ $promotionalAdmission->id  }}">
    @endisset
@endsection

@push('footer_scripts')
{{--    <script type="text/javascript" src="{{ asset('backend/modules/class_grades.js') }}"></script>--}}
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/modules/promotional_admissions.js') }}"></script>
@endpush
