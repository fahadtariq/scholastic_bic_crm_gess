@extends('layouts.master')

@push('header_scripts')
    <style>
        .sales-insigts {
            width: 100%;
            height: 191rem;
            max-height: 100%;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                {{--<div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Sales Insights</h4>
                    <div class="flex-shrink-0">
                        <a href="{{ url()->previous() }}" class="btn btn-success btn-label btn-sm">
                            <i class="ri-arrow-left-fill label-icon align-middle fs-16 me-2"></i> Back
                        </a>
                    </div>
                </div>--}}

                <iframe width="1220" height="1520" src="https://lookerstudio.google.com/embed/reporting/4ee311b5-3d94-4b6a-9498-f72a97e3dc2b/page/q9qYD" frameborder="0" style="border:0" allowfullscreen></iframe>
                {{--<div class="card-body">
                </div>--}}
            </div>
        </div>

@endsection


@push('header_scripts')


@endpush
