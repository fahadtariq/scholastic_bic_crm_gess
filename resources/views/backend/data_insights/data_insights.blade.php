@extends('layouts.master')

@push('header_scripts')
    <style>
        .sales-insigts {
            width: 100%;
            height: 191rem;
            max-height: 100%;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                {{--<div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Sales Insights</h4>
                    <div class="flex-shrink-0">
                        <a href="{{ url()->previous() }}" class="btn btn-success btn-label btn-sm">
                            <i class="ri-arrow-left-fill label-icon align-middle fs-16 me-2"></i> Back
                        </a>
                    </div>
                </div>--}}

                <iframe width="1220" height="1520" src="https://lookerstudio.google.com/embed/reporting/10e2c2e3-80e0-4ac9-bb6f-c07e0f52ee84/page/p_k5r49o2t7c" frameborder="0" style="border:0" allowfullscreen></iframe>
                {{--<div class="card-body">
                </div>--}}
            </div>
        </div>

@endsection


@push('header_scripts')


@endpush
