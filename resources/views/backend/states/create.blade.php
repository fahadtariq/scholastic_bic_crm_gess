<div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Create New State</h4>
        </div>

        <div class="card-body">
            <div class="live-preview">
                <form class="row g-3 needs-validation" novalidate action="{{ route('state.store') }}" method="post">
                    @csrf
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('beams_id')) is-invalid @endif" name="beams_id" id="beams_id"
                                   placeholder="BEAMS ID" value="{{ old('beams_id') }}">
                            <label for="beams_id" class="form-label">BEAMS ID</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('beams_id'))
                                    {{ $errors->first('beams_id') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('beams_id')) is-invalid @endif" name="beams_id" id="beams_id"
                                   placeholder="BEAMS ID" value="{{ old('beams_id') }}">
                            <label for="beams_id" class="form-label">BEAMS ID</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('beams_id'))
                                    {{ $errors->first('beams_id') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif"
                                id="stateName" name="name" placeholder="Please enter state name"
                                value="{{ old('name') }}" required>
                            <label for="stateName" class="form-label">State name</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('name'))
                                    {{ $errors->first('name') }}
                                @else
                                    State name is required!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control" name="abbreviation" id="stateNameAbbr"
                                placeholder="Please enter abbreviation" value="{{ old('abbreviation') }}">
                            <label for="stateNameAbbr" class="form-label">Abbreviation</label>
                            <div class="invalid-tooltip">State name abbreviation is required!</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="country_id" required>
                                <option value="" disabled selected>Country options</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}"
                                        @if (old('country_id') == $country->id) {{ 'selected' }} @endif>
                                        {{ $country->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="countryID" class="form-label">Country list</label>
                            <div class="invalid-tooltip">Select the Country!</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="status_id" required>

                                @foreach ($statuses as $status)
                                    <option value="{{ $status->id }}"
                                        @if (old('status_id') == $status->id) {{ 'selected' }} @endif>
                                        {{ $status->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="statusID" class="form-label">Status</label>
                            <div class="invalid-tooltip">Select the status!</div>
                        </div>
                    </div>

                    <div class="col-12 text-end">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                        <button type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
