<div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Edit State</h4>
            <div class="flex-shrink-0">
                 @permission('add-state')
                    <a href="{{ route('state.index') }}" class="btn btn-sm btn-soft-success">
                        <i class="ri-add-circle-line align-middle me-1"></i> Add New State
                    </a>
                 @endpermission
            </div>
        </div><!-- end card header -->

        <div class="card-body">
            <div class="live-preview">
                <form class="row g-3 needs-validation" novalidate action="{{ route('state.update') }}" method="post">
                    @csrf
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('beams_id')) is-invalid @endif" name="beams_id" id="beams_id"
                                   placeholder="BEAMS ID" value="{{ $state->beams_id }}">
                            <label for="beams_id" class="form-label">BEAMS ID</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('beams_id'))
                                    {{ $errors->first('beams_id') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input name="id" value="{{ $state->id }}" hidden>
                            <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif"
                                id="stateName" name="name" placeholder="Please enter state name"
                                value="{{ $state->name }}" required>
                            <label for="stateName" class="form-label">State name</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('name'))
                                    {{ $errors->first('name') }}
                                @else
                                    State name is required!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control" name="abbreviation" id="stateNameAbbr"
                                placeholder="Please enter abbreviation" value="{{ $state->abbreviation }}">
                            <label for="stateNameAbbr" class="form-label">Abbreviation</label>
                            <div class="invalid-tooltip">Abbreviation is required!</div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="country_id" required>
                                <option value="" disabled selected>Country options</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}"
                                        @if ($state->country_id == $country->id) {{ 'selected' }} @endif>
                                        {{ $country->name }}</option>
                                @endforeach
                            </select>
                            <label for="countryID" class="form-label">Country list</label>
                            <div class="invalid-tooltip">Select the Country!</div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="status_id" required>

                                @foreach ($statuses as $status)
                                    <option value="{{ $status->id }}"
                                        @if ($country->status_id == $status->id) {{ 'selected' }} @endif>
                                        {{ $status->name }}</option>
                                @endforeach
                            </select>
                            <label for="statusID" class="form-label">Status</label>
                            <div class="invalid-tooltip">Select the status!</div>
                        </div>
                    </div>

                    <div class="col-12 text-end">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                        <a href="{{ route('state.index') }}"
                            class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
