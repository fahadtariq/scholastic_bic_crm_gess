@extends('layouts.master')

@push('header_scripts')
    <link rel="stylesheet" href="{{ asset('theme/dist/default/assets/libs/dragula/dragula.min.css') }}" />
    <style>
        .table-triangle {
            position: absolute;
            left: 0;
            bottom: 0;
            margin-left: 11px;
        }

    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col">
            <div class="h-100">
                <div class="row">
                    {{-- @permission('show-leads-widget')
            <div class="col-xl-3 col-md-6">
               <div class="card card-animate">
                  <div class="card-body">
                     <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                           <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Leads</p>
                        </div>
                     </div>
                     <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                           <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value"
                                 data-target="{{ $leads ?? '' }}">0</span></h4>
                           <a href="{{ route('leads.index') }}" class="text-decoration-underline">View leads</a>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                           <span class="avatar-title bg-soft-success rounded fs-3">
                              <i class="ri-customer-service-2-line text-success"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endpermission

            @permission('show-branches-widget')
            <div class="col-xl-3 col-md-6">
               <div class="card card-animate">
                  <div class="card-body">
                     <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                           <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Branches</p>
                        </div>
                     </div>
                     <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                           <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value"
                                 data-target="{{ $branchesCount ?? '' }}">0</span></h4>
                           <a href="{{ route('branches.index') }}" class="text-decoration-underline">View branches</a>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                           <span class="avatar-title bg-soft-info rounded fs-3">
                              <i class="bx bx-shopping-bag text-info"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endpermission

            @permission('show-users-widget')
            <div class="col-xl-3 col-md-6">
               <div class="card card-animate">
                  <div class="card-body">
                     <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                           <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Users</p>
                        </div>
                     </div>
                     <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                           <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value"
                                 data-target="{{ $users ?? '' }}">0</span></h4>
                           <a href="{{ route('users.index') }}" class="text-decoration-underline">View users</a>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                           <span class="avatar-title bg-soft-warning rounded fs-3">
                              <i class="bx bx-user-circle text-warning"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endpermission

            @permission('show-employees-widget')
            <div class="col-xl-3 col-md-6">
               <div class="card card-animate">
                  <div class="card-body">
                     <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                           <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Employees</p>
                        </div>
                     </div>
                     <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                           <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value"
                                 data-target="{{ $employees ?? '' }}">0</span></h4>
                           <a href="{{ route('employees.index') }}" class="text-decoration-underline">View employees</a>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                           <span class="avatar-title bg-soft-primary rounded fs-3">
                              <i class="bx bx-wallet text-primary"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endpermission --}}
                    @if (!auth()->user()->hasRole('super_admin|social_media_manager|reviewer'))
                        <div class="row">
                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select disabled class="filter form-select branch_filter" name="branch_id"
                                        placeholder="Branch">
                                        <option value="">Please select</option>
                                        @forelse($branches as $branch)
                                            <option value="{{ $branch->id }}"
                                                {{ isset($userEmployee->employee->branch_id) &&
                                                !empty($userEmployee->employee->branch_id) &&
                                                $userEmployee->employee->branch_id == $branch->id
                                                    ? 'selected'
                                                    : '' }}>
                                                {{ $branch->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="branch_id" class="form-label">Branch</label>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select source_select" id="source_id" name="source_id"
                                        placeholder="Source">
                                        <option value="">Please select</option>
                                        @forelse($sources as $source)
                                            <option value="{{ $source->id }}">{{ $source->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="source_id" class="form-label">Source</label>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select course_select" id="course_id" name="course_id"
                                        placeholder="Course">
                                        <option value="">Please select</option>
                                        @forelse($courses as $course)
                                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="course_id" class="form-label">Course</label>
                                </div>
                            </div>
                            {{-- <div class="col-md-2 col-sm-12">
                  <div class="form-label-group in-border">
                     <select class="filter form-select category_select" id="category_id" name="category_id"
                        placeholder="Category">
                        <option value="">Please select</option>
                        <option value="continuing">Continuing</option>
                        <option value="new">New</option>
                     </select>
                     <label for="category_id" class="form-label">Category</label>
                  </div>
               </div> --}}
                            <div class="col-md-4 col-sm-12">
                                <button onclick="filterFunnelData('{{ route('funnel.data.filter') }}',0)" type="button"
                                    class="filter-lead-table-btn btn btn-primary btn-label rounded-pill"><i
                                        class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                    Filter</button>
                            </div>
                            <hr />
                            <div id="chartdiv" style="width: 100%; height: 600px; font-size: 11px;"></div>
                            {{-- For Islamabad: Branch : 1 dashboard --}}
                            {{-- @if ($userEmployee->employee->branch_id == 1)
               <div class="col-xl-12 col-md-12">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">

                           <div class="col-md-12 col-sm-12">
                              <div class="form-label-group in-border session_select">
                                 <iframe width="1200" height="1300"
                                    src="https://lookerstudio.google.com/embed/reporting/07ff8e44-e54b-46f3-adf4-6eec6a0f766e/page/p_57zqcb5t7c"
                                    frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
               @endif --}}

                            {{-- For Lahore: Branch : 3 dashboard --}}
                            {{-- @if ($userEmployee->employee->branch_id == 3)
               <div class="col-xl-12 col-md-12">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">

                           <div class="col-md-12 col-sm-12">
                              <div class="form-label-group in-border session_select">
                                 <iframe width="1200" height="1300"
                                    src="https://lookerstudio.google.com/embed/reporting/84ef6477-4154-4020-8230-cb63cc8f53ac/page/p_57zqcb5t7c"
                                    frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
               @endif --}}

                            {{-- For Potohar : Branch : 2 dashboard --}}
                            {{-- @if ($userEmployee->employee->branch_id == 2)
               <div class="col-xl-12 col-md-12">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">

                           <div class="col-md-12 col-sm-12">
                              <div class="form-label-group in-border session_select">
                                 <iframe width="1200" height="1300"
                                    src="https://lookerstudio.google.com/embed/reporting/3352cacc-a327-4966-83d4-0b2dc87e0668/page/p_57zqcb5t7c"
                                    frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
               @endif --}}

                            {{-- For Faisalabad : Branch : 4 dashboard --}}
                            {{-- @if ($userEmployee->employee->branch_id == 4)
               <div class="col-xl-12 col-md-12">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">

                           <div class="col-md-12 col-sm-12">
                              <div class="form-label-group in-border session_select">
                                 <iframe width="1200" height="1300"
                                    src="https://lookerstudio.google.com/embed/reporting/0088b828-7813-4ffa-a19e-1f3361463df7/page/p_57zqcb5t7c"
                                    frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>
               </div>
               @endif --}}
                    @endif

                    @if (auth()->user()->hasRole('super_admin|social_media_manager|reviewer'))
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="filter form-select branch_filter" name="branch_id"
                                                    placeholder="Branch">
                                                    <option value="">Please select</option>
                                                    @forelse($branches as $branch)
                                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                                <label for="branch_id" class="form-label">Branch</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="filter form-select source_select" id="source_id"
                                                    name="source_id" placeholder="Source">
                                                    <option value="">Please select</option>
                                                    @forelse($sources as $source)
                                                        <option value="{{ $source->id }}">{{ $source->name }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                                <label for="source_id" class="form-label">Source</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="filter form-select course_select" id="course_id"
                                                    name="course_id" placeholder="Course">
                                                    <option value="">Please select</option>
                                                    @forelse($courses as $course)
                                                        <option value="{{ $course->id }}">{{ $course->name }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                                <label for="course_id" class="form-label">Course</label>
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-2 col-sm-12">
                              <div class="form-label-group in-border">
                                 <select class="filter form-select category_select" id="category_id" name="category_id"
                                    placeholder="Category">
                                    <option value="">Please select</option>
                                    <option value="continuing">Continuing</option>
                                    <option value="new">New</option>
                                 </select>
                                 <label for="category_id" class="form-label">Category</label>
                              </div>
                           </div> --}}
                                        <div class="col-md-4 col-sm-12">
                                            <button onclick="filterFunnelData('{{ route('funnel.data.filter') }}',0)"
                                                type="button"
                                                class="filter-lead-table-btn btn btn-primary btn-label rounded-pill"><i
                                                    class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                                Filter</button>

                                            {{-- <button type="button"
                                       class="btn btn-primary btn-label rounded-pill"><i
                                       class="ri-add-box-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                   Get Promotional Data</button> --}}
                                        </div>
                                        <div class="col-md-2 col-sm-12 d-flex pb-2 align-items-center justify-content-end">
                                            <a href="javascript:;"
                                                onclick="filterFunnelData('{{ route('funnel.data.filter') }}',1)"
                                                id="add_promotion" data-table="promotional-admissions_table"
                                                class="btn btn-primary btn-label rounded-pill">
                                                <i
                                                    class="ri-add-box-line label-icon align-middle rounded-pill fs-16 me-2"></i>Add
                                                Continuing Data
                                            </a>
                                            <input type="hidden" value="{{ $totalPromotionAdmissionCount }}"
                                                id="total_promotion_admission" name="total_promotion_admission" />
                                            {{-- <a href="/"  data-table="promotional-admissions_table"
                                   class="ms-3">
                                    <i class="ri-question-line label-icon align-middle rounded-pill fs-24 me-2"></i>
                                </a> --}}
                                        </div>
                                        <hr />
                                        <div class="chartdiv-cover position-relative">

                                            <div id="chartdiv" style="width: 100%; height: 600px; font-size: 12px;"></div>
                                            <div
                                                class="my-3 d-inline-block w-auto text-dark p-3 rounded-2 mt-0 table-triangle">
                                                <h6 style="color: black;">Registered to paid Conversion (incl dropped)</h6>

                                                <table id="promotional-admissions_table"
                                                    class="table table-bordered table-striped align-middle table-nowrap mb-0"
                                                    style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Stage</th>
                                                            <th>Leads</th>
                                                            <th>Conversion %</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Registered</td>
                                                            <td id="registeredlead">NO DATA FOUND </td>
                                                            <td id="registeredleadspercentage">NO DATA FOUND </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Paid</td>
                                                            <td id="paidlead">NO DATA FOUND</td>
                                                            <td id="paidleadpercentage">NO DATA FOUND</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <!-- Blade View -->


                                        <div style="display: flex; align-items: center;">

                                            <h5 style=" font-size: 12px; color: black; margin-left: 22px; " class="card-title">Num. of leads
                                                dropped after Registration: <span id="droppedleads">No Dropped Leads found></span>
                                            </h5>
                                        </div>


                                        <div style="display: flex; align-items: center;">
                                            <h5 style=" font-size: 12px; color: black; margin-left: 22px;" class="card-title">Num. of
                                                Withdrawals after Admission: <span id="withdrawlleads">No
                                                    Withdrawals Leads found></span></h5>
                                        </div>



                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="my-3 d-inline-block w-auto text-dark p-3 rounded-2 mt-0">
                                                        <table id="promotional-admissions_table"
                                                            class="table table-bordered table-striped align-middle table-nowrap mb-0"
                                                            style="width:100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Branch</th>
                                                                    <th>Num of Continuing Admission</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse ($branches as $key => $branch)
                                                                    <tr>
                                                                        <td>{{ $key + 1 }}</td>
                                                                        <td>{{ $branch->name }}</td>
                                                                        <td>{{ $branch->promotion->no_of_promotional_admissions ?? 0 }}
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                    <tr>No data found</tr>
                                                                @endforelse
                                                                <tr>
                                                                    <td></td>
                                                                    <td><b>Total</b></td>
                                                                    <td><b>{{ $totalPromotionAdmissionCount }}</b></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div style="background-color: #FFF9DF; width: 100%; padding: 15px;"
                                                        class="my-3 d-inline-block text-dark p-3 rounded-2 mt-3">
                                                        <b style="font-size: 14px; color:#D8A800;">NOTE</b>
                                                        <p style="font-size: 12px" class="m-0">* On the side label it's
                                                            shows lead conversion % on each stage.</p>
                                                        <p style="font-size: 12px" class="m-0">* On the tooltip it's
                                                            shows lead conversion % on each stage against target.</p>
                                                    </div>
                                                </div>

                                                {{-- <div class="col-md-4">
                                                    <div
                                                        class="my-3 d-inline-block w-auto text-dark p-3 rounded-2 mt-0">
                                                        <h6 style="color: black;">Registered to paid Conversion (inc
                                                            dropped)</h6>

                                                        <table id="promotional-admissions_table"
                                                            class="table table-bordered table-striped align-middle table-nowrap mb-0"
                                                            style="width:100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Stage</th>
                                                                    <th>Leads</th>
                                                                    <th>Conversion %</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>Registered</td>
                                                                    <td id="registeredlead">NO DATA FOUND </td>
                                                                    <td id="registeredleadspercentage">NO DATA FOUND </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>Paid</td>
                                                                    <td id="paidlead">NO DATA FOUND</td>
                                                                    <td id="paidleadpercentage">NO DATA FOUND</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>


                                        {{-- <div class="container-fluid">
                              <div class="row text-center" style="overflow:hidden;">
                                 <div class="col-sm-3" style="float: none !important;display: inline-block;">
                                    <label class="text-left">Angle:</label>
                                    <input class="chart-input" data-property="angle" type="range" min="0" max="60"
                                       value="40" step="1" />
                                 </div>

                                 <div class="col-sm-3" style="float: none !important;display: inline-block;">
                                    <label class="text-left">Depth:</label>
                                    <input class="chart-input" data-property="depth3D" type="range" min="1" max="120"
                                       value="100" step="1" />
                                 </div>
                              </div>
                           </div> --}}
                                        {{-- <div class="col-md-4 col-sm-12">
                              <div class="form-label-group in-border">
                                 <select class="form-select branch_filter"
                                    data-session-route="{{ route('branch.sessions') }}" id="branch_id"
                                    name="branch_id[]" aria-label="Branch select" multiple>
                                    @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                    @endforeach
                                 </select>
                                 <label for="branch_id" class="form-label">Branch</label>
                              </div>
                           </div> --}}

                                        {{-- <div class="col-md-12 col-sm-12">
                              <div class="form-label-group in-border session_select">
                                 <iframe width="1200" height="1300"
                                    src="https://lookerstudio.google.com/embed/reporting/bbd9e819-4ccf-49c7-a2e1-0f2b9f2e07b3/page/p_57zqcb5t7c"
                                    frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>
                           </div> --}}
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <a href="javascript:void(0)" class="btn btn-success btn-label btn-sm"
                                                onclick="getRRRReport('fall-to-fall')">
                                                <i
                                                    class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                                Fall to Fall Comparison
                                            </a>
                                            <a href="javascript:void(0)" class="btn btn-warning btn-label btn-sm"
                                                onclick="getRRRReport('year-to-year')">
                                                <i
                                                    class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                                Year to Year Comparison
                                            </a>
                                            <a href="javascript:void(0)" class="btn btn-primary btn-label btn-sm"
                                                onclick="getRRRReport('spring-to-spring')">
                                                <i
                                                    class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                                Spring to Spring Comparison
                                            </a>
                                        </div>
                                        <div class="col-md-3 text-right">
                                            <a href="javascript:void(0)" id="add_continue_val"
                                                class="btn btn-primary btn-label btn-sm"
                                                onclick="getRRRReport('add-continue')">
                                                <i
                                                    class="ri-add-box-line label-icon align-middle rounded-pill fs-16 me-2"></i>
                                                Add Continuing Data
                                            </a>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="card-header align-items-center d-flex">
                                                <h4 class="card-title mb-0 flex-grow-1" id="required-run-rate-text">
                                                    Required Run Rate Report </h4>
                                                <div class="col-md-3 col-sm-12 follow_up_date_wrapper">
                                                    <div class="form-label-group in-border">
                                                        <input class="form-control filter"
                                                            name="follow_up_by_employee_date"
                                                            id="follow_up_by_employee_date">
                                                        <label for="follow_up_by_employee_date" class="form-label">Select
                                                            Date</label>
                                                    </div>
                                                </div>
                                            </div><!-- end card header -->
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="class_grades_table"
                                                        class="table table-bordered table-striped align-middle table-nowrap mb-0 text-center"
                                                        style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th colspan="5">BIC Potohar Campus</th>
                                                                <th colspan="5">BIC Islamabad Campus
                                                                </th>
                                                                <th colspan="5">BIC Lahore Campus</th>
                                                                <th colspan="5">BIC Faisalabad Campus
                                                                </th>
                                                                <th colspan="5">Total</th>
                                                            </tr>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th colspan="2">2022-23</th>
                                                                <th colspan="3">2023-24</th>
                                                                <th colspan="2">2022-23</th>
                                                                <th colspan="3">2023-24</th>
                                                                <th colspan="2">2022-23</th>
                                                                <th colspan="3">2023-24</th>
                                                                <th colspan="2">2022-23</th>
                                                                <th colspan="3">2023-24</th>
                                                                <th colspan="2">2022-23</th>
                                                                <th colspan="3">2023-24</th>
                                                            </tr>
                                                            <tr>
                                                                <th></th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>RRR</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>RRR</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>RRR</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>RRR</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>No.s</th>
                                                                <th>Target Achieved</th>
                                                                <th>RRR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

    {{-- @include('backend.dashboard.partials.content') --}}

    <div id="load-content"></div>

    <input type="hidden" value="{{ route('dashboard.content.load') }}" id="dashboard_content_url">

@endsection

@push('footer_scripts')
    <script src="{{ asset('theme/dist/default/assets/libs/dragula/dragula.min.js') }}"></script>
    <script src="{{ asset('theme/dist/default/assets/libs/dom-autoscroller/dom-autoscroller.min.js') }}"></script>
    <script src="{{ asset('theme/dist/default/assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('theme/dist/default/assets/js/pages/flatpickr.min.js') }}"></script>
    <script src="{{ asset('backend/modules/leads.js') }}"></script>
    {{-- <script src="{{ asset('backend/modules/dashboard.js') }}"></script> --}}
    <script>
        var funnelColor = ["#2E5495", "#8DA9DB", "#BCD6EF", "#D9E2F3", "#dcebd5", "#C3E0B0"];
        var funnelDesc = [];
        var promotionalTotal = 0;
        funnelDesc['totalLeadsDescription'] =
            "All data sets that we have provided our campuses for outreach including BSS data, Engineering Board, etc. Any incoming calls or Walk-ins will automatically be added, if they aren't already part of our data.";
        funnelDesc['totalAssignLeadsDescription'] =
            "All leads that have been assigned to an admission advisor to call for attempted conversion. Ideally, total leads should be assigned. Any incoming calls or Walk-in will automatically be added here";
        funnelDesc['totalContactedLeadsDescription'] =
            "Students who have been contacted and have had a conversation with our admission advisors. If there is no response, they would NOT be considered contacted. Any Walk-ins in response to inbound/outbound calls will be added here";
        funnelDesc['totalInterestedLeadsDescription'] =
            "Students who have been contacted and shown a potential interest will be included here. Any Walk-ins in response to inbound/outbound calls will be added here Walk-Ins in response to outreach (via QR) will automatically be added here.";
        funnelDesc['totalRegisteredLeadsDescription'] = "Students who have registered will reflect here.";
        funnelDesc['totalPaidLeadsDescription'] = "Students who have paid will reflect here.";
        $(document).ready(function() {
            // Fetch data using AJAX
            if ($(".branch_filter").val()) {
                $('.filter-lead-table-btn').trigger('click');
            } else {
                getFunnelData();
            }
        });

        function filterFunnelData(url, check) {
            var branch = $(".branch_filter").val();
            var sendRequest = true;
            // if(branch){
            //    sendRequest = true;
            // }else{
            //    alert('Please Select Branch');
            // }
            if (sendRequest) {
                $.ajax({
                    url: url,
                    data: {
                        branch_id: $(".branch_filter").val(),
                        source_id: $(".source_select").val(),
                        course_id: $(".course_select").val(),
                        // category: $(".category_select").val()
                    },
                    type: 'POST',
                    dataType: 'json',
                    headers: {
                        "X-CSRF-TOKEN": "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        var data = response.data;
                        // For add or remove promotional admission count
                        if (check == 1) {
                            if (promotionalTotal == $("#total_promotion_admission").val()) {
                                $("#add_promotion").removeClass("btn-danger");
                                $("#add_promotion").addClass("btn-primary");
                                $("#add_promotion").html(
                                    '<i class="ri-add-box-line label-icon align-middle rounded-pill fs-16 me-2"></i>Add Continuing Data'
                                );
                                promotionalTotal = 0;
                            } else {
                                $("#add_promotion").removeClass("btn-primary");
                                $("#add_promotion").addClass("btn-danger");
                                $("#add_promotion").html(
                                    '<i class="ri-add-box-line label-icon align-middle rounded-pill fs-16 me-2"></i>Sub Continuing Data'
                                );
                                promotionalTotal = data.promotionAdmissionCount;
                                $("#total_promotion_admission").val(promotionalTotal)
                            }
                        }
                        // Calculate rounded percentages
                        var totalLeads = data.totalLeads + data.totalDroppedLeads + data.totalWithDrawlLeads;

                        var totalPaidLeads = data.totalPaidLeads;
                        var totalRegisteredLeads = data.totalRegisteredLeads + totalPaidLeads;
                        var totalInterestedLeads = data.totalInterestedLeads + totalRegisteredLeads + data
                            .totalDroppedLeads + data.totalWithDrawlLeads;
                        var totalContactedLeads = data.totalInterestedLeads + data.totalAssignLeads + data
                            .totalRegisteredLeads + data.totalPaidLeads + data.totalDroppedLeads + data
                            .totalWithDrawlLeads;
                        var totalAssignLeads = data.totalAssignLeads + totalContactedLeads;
                        totalPaidLeads += parseInt(promotionalTotal);
                        var assignedLeadsPercentage = (totalAssignLeads / totalContactedLeads) * 100;
                        var contactedLeadsPercentage = 100;
                        var interestedLeadsPercentage = (totalInterestedLeads / totalContactedLeads) * 100;
                        var registeredLeadsPercentage = (totalRegisteredLeads / totalContactedLeads) * 100;
                        var paidLeadsPercentage = (totalPaidLeads / totalContactedLeads) * 100;


                        var totalTargetSum = data.branch_target_sum;
                        var baloonTotalPercentage = (totalLeads / totalTargetSum) * 100;
                        var baloonContactedPercentage = (totalContactedLeads / totalTargetSum) * 100;
                        var baloonAdvancePercentage = (totalInterestedLeads / totalTargetSum) * 100;
                        var baloonRegisteredPercentage = (totalRegisteredLeads / totalTargetSum) * 100;
                        var baloonPaidPercentage = (totalPaidLeads / totalTargetSum) * 100;
                        // Registered Leads Withdrawl + Dropped
                        var totalRegisteredWithdrawl = totalRegisteredLeads + data.totalDroppedLeads + data
                            .totalWithDrawlLeads;
                        var totalRegisteredWithdrawlpercentage = 100;
                        var PaidPercentage = (totalPaidLeads / totalRegisteredWithdrawl) * 100;
                        $('#paidlead').text(totalPaidLeads);
                        $('#registeredlead').text(totalRegisteredWithdrawl);

                        $('#paidleadpercentage').text(PaidPercentage.toFixed(2));
                        $('#registeredleadspercentage').text(totalRegisteredWithdrawlpercentage.toFixed(2));


                        var droppedleads = data.totalDroppedLeads;
                        $('#droppedleads').text(droppedleads);

                        var withdrawlleads = data.totalWithDrawlLeads
                        $('#withdrawlleads').text(withdrawlleads);





                        // console.log(totalPaidLeads,totalRegisteredLeads,totalInterestedLeads,totalContactedLeads,totalAssignLeads);
                        var funnelData = [
                            // { "title": "Total Leads", "value": totalLeads, "percentage": 100, "newdescription": funnelDesc.totalLeadsDescription },
                            {
                                "target": "Total Target",
                                "targetValue": totalTargetSum,
                                "title": "Total Leads",
                                "value": totalLeads,
                                "percentage": "",
                                "baloonPercentage": baloonTotalPercentage.toFixed(2),
                                "newdescription": funnelDesc.totalAssignLeadsDescription
                            },
                            {
                                "target": "Total Target",
                                "targetValue": totalTargetSum,
                                "title": "Contacted Leads",
                                "value": totalContactedLeads,
                                "percentage": `(${contactedLeadsPercentage.toFixed(2)})%`,
                                "baloonPercentage": baloonTotalPercentage.toFixed(2),
                                "newdescription": funnelDesc.totalContactedLeadsDescription
                            },
                            {
                                "target": "Total Target",
                                "targetValue": totalTargetSum,
                                "title": "Advanced Leads",
                                "value": totalInterestedLeads,
                                "percentage": `(${interestedLeadsPercentage.toFixed(2)})%`,
                                "baloonPercentage": baloonAdvancePercentage.toFixed(2),
                                "newdescription": funnelDesc.totalContactedLeadsDescription
                            },
                            {
                                "target": "Total Target",
                                "targetValue": totalTargetSum,
                                "title": "Walk-in Leads",
                                "value": totalInterestedLeads,
                                "percentage": `(${interestedLeadsPercentage.toFixed(2)})%`,
                                "baloonPercentage": baloonAdvancePercentage.toFixed(2),
                                "newdescription": funnelDesc.totalInterestedLeadsDescription
                            },
                            {
                                "target": "Total Target",
                                "targetValue": totalTargetSum,
                                "title": "Registered Leads",
                                "value": totalRegisteredLeads,
                                "percentage": `(${registeredLeadsPercentage.toFixed(2)})%`,
                                "baloonPercentage": baloonRegisteredPercentage.toFixed(2),
                                "newdescription": funnelDesc.totalRegisteredLeadsDescription
                            },
                            {
                                "target": "Total Target",
                                "targetValue": totalTargetSum,
                                "title": "Paid Leads",
                                "value": totalPaidLeads,
                                "percentage": `(${paidLeadsPercentage.toFixed(2)})%`,
                                "baloonPercentage": baloonPaidPercentage.toFixed(2),
                                "newdescription": funnelDesc.totalPaidLeadsDescription
                            }
                        ]
                        var chart = AmCharts.makeChart("chartdiv", {
                            "fontSize": 16,
                            "fontWeight": 'bold',
                            "type": "funnel",
                            "theme": "light",
                            "dataProvider": funnelData,
                            "labelText": "[[title]] - [[value]] [[percentage]]",
                            "balloon": {
                                "fixedPosition": true
                            },
                            "valueField": "value",
                            "titleField": "title",
                            "marginRight": 240,
                            "marginLeft": 50,
                            "startX": -500,
                            "depth3D": 100,
                            "angle": 40,
                            "outlineAlpha": 1,
                            "outlineColor": "#FFFFFF",
                            "outlineThickness": 2,
                            "labelPosition": "right",
                            "balloonText": "[[target]] - [[targetValue]] Achieved ([[baloonPercentage]])%",
                            "export": {
                                "enabled": true
                            },
                            "colors": funnelColor
                        });
                    }
                });
            }
        }

        function getFunnelData() {
            $.ajax({
                url: '/get-funnel-data', // Update with the correct URL
                type: 'POST',
                dataType: 'json',
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function(response) {
                    var data = response.data;
                    // Calculate rounded percentages
                    var totalLeads = data.totalLeads + data.totalDroppedLeads + data.totalWithDrawlLeads;

                    var totalPaidLeads = data.totalPaidLeads;
                    var totalRegisteredLeads = data.totalRegisteredLeads + totalPaidLeads;
                    var totalInterestedLeads = data.totalInterestedLeads + totalRegisteredLeads + data
                        .totalDroppedLeads + data.totalWithDrawlLeads;
                    var totalContactedLeads = data.totalInterestedLeads + data.totalAssignLeads + data
                        .totalRegisteredLeads + data.totalPaidLeads + data.totalDroppedLeads + data
                        .totalWithDrawlLeads;
                    var totalAssignLeads = data.totalAssignLeads + totalContactedLeads;

                    var assignedLeadsPercentage = (totalAssignLeads / totalContactedLeads) * 100;
                    var contactedLeadsPercentage = 100;
                    var interestedLeadsPercentage = (totalInterestedLeads / totalContactedLeads) * 100;
                    var registeredLeadsPercentage = (totalRegisteredLeads / totalContactedLeads) * 100;
                    var paidLeadsPercentage = (totalPaidLeads / totalContactedLeads) * 100;

                    var totalTargetSum = data.branch_target_sum;
                    var baloonTotalPercentage = (totalLeads / totalTargetSum) * 100;
                    var baloonContactedPercentage = (totalContactedLeads / totalTargetSum) * 100;
                    var baloonAdvancePercentage = (totalInterestedLeads / totalTargetSum) * 100;
                    var baloonRegisteredPercentage = (totalRegisteredLeads / totalTargetSum) * 100;
                    var baloonPaidPercentage = (totalPaidLeads / totalTargetSum) * 100;
                    totalPaidLeads += parseInt(promotionalTotal);
                    // Registered Leads Withdrawl + Dropped


                    var totalRegisteredWithdrawl = totalRegisteredLeads + data.totalDroppedLeads + data
                        .totalWithDrawlLeads;
                    var totalRegisteredWithdrawlpercentage = 100;
                    var PaidPercentage = (totalPaidLeads / totalRegisteredWithdrawl) * 100;
                    $('#paidlead').text(totalPaidLeads);
                    $('#registeredlead').text(totalRegisteredWithdrawl);

                    var droppedleads = data.totalDroppedLeads;
                    $('#droppedleads').text(droppedleads);

                    var withdrawlleads = data.totalWithDrawlLeads
                    $('#withdrawlleads').text(withdrawlleads);







                    $('#paidleadpercentage').text(PaidPercentage.toFixed(2));
                    $('#registeredleadspercentage').text(totalRegisteredWithdrawlpercentage.toFixed(2));
                    // console.log(totalPaidLeads,totalRegisteredLeads,totalInterestedLeads,totalContactedLeads,totalAssignLeads);
                    var funnelData = [
                        // { "title": "Total Leads", "value": totalLeads, "percentage": 100, "newdescription": funnelDesc.totalLeadsDescription },
                        {
                            "target": "Total Target",
                            "targetValue": totalTargetSum,
                            "title": "Total Leads",
                            "value": totalLeads,
                            "percentage": "",
                            "baloonPercentage": baloonTotalPercentage.toFixed(2),
                            "newdescription": funnelDesc.totalAssignLeadsDescription
                        },
                        {
                            "target": "Total Target",
                            "targetValue": totalTargetSum,
                            "title": "Contacted Leads",
                            "value": totalContactedLeads,
                            "percentage": `(${contactedLeadsPercentage.toFixed(2)})%`,
                            "baloonPercentage": baloonTotalPercentage.toFixed(2),
                            "newdescription": funnelDesc.totalContactedLeadsDescription
                        },
                        {
                            "target": "Total Target",
                            "targetValue": totalTargetSum,
                            "title": "Advanced Leads",
                            "value": totalInterestedLeads,
                            "percentage": `(${interestedLeadsPercentage.toFixed(2)})%`,
                            "baloonPercentage": baloonAdvancePercentage.toFixed(2),
                            "newdescription": funnelDesc.totalContactedLeadsDescription
                        },
                        {
                            "target": "Total Target",
                            "targetValue": totalTargetSum,
                            "title": "Walk-in Leads",
                            "value": totalInterestedLeads,
                            "percentage": `(${interestedLeadsPercentage.toFixed(2)})%`,
                            "baloonPercentage": baloonAdvancePercentage.toFixed(2),
                            "newdescription": funnelDesc.totalInterestedLeadsDescription
                        },
                        {
                            "target": "Total Target",
                            "targetValue": totalTargetSum,
                            "title": "Registered Leads",
                            "value": totalRegisteredLeads,
                            "percentage": `(${registeredLeadsPercentage.toFixed(2)})%`,
                            "baloonPercentage": baloonRegisteredPercentage.toFixed(2),
                            "newdescription": funnelDesc.totalRegisteredLeadsDescription
                        },
                        {
                            "target": "Total Target",
                            "targetValue": totalTargetSum,
                            "title": "Paid Leads",
                            "value": totalPaidLeads,
                            "percentage": `(${paidLeadsPercentage.toFixed(2)})%`,
                            "baloonPercentage": baloonPaidPercentage.toFixed(2),
                            "newdescription": funnelDesc.totalPaidLeadsDescription
                        }
                    ]
                    var chart = AmCharts.makeChart("chartdiv", {
                        "fontSize": 16,
                        "fontWeight": 'bold',
                        "type": "funnel",
                        "theme": "light",
                        "dataProvider": funnelData,
                        "labelText": "[[title]] - [[value]] [[percentage]]",
                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "value",
                        "titleField": "title",
                        "marginRight": 240,
                        "marginLeft": 50,
                        "startX": -500,
                        "depth3D": 100,
                        "angle": 40,
                        "outlineAlpha": 1,
                        "outlineColor": "#FFFFFF",
                        "outlineThickness": 2,
                        "labelPosition": "right",
                        "balloonText": "[[target]] - [[targetValue]] Achieved ([[baloonPercentage]])%", // Adjusted balloonText
                        "export": {
                            "enabled": true
                        },
                        "colors": funnelColor
                    });
                },
                error: function(xhr, status, error) {
                    console.error('Error fetching data:', error);
                }
            });
        }
    </script>
    <script>
        var dataType = "fall-to-fall";
        var datatable;
        var selectedDate;
        var addContinueVal = 'no';
        $(document).ready(function() {
            var flatpickrInstance = $('#follow_up_by_employee_date').flatpickr({
                dateFormat: 'd-F',
                altFormat: 'd-F', // Displayed in the input field
                altInput: true, // Show the formatted date in the input field
                defaultDate: 'today',
                onChange: function(selectedDates, dateStr, instance) {
                    // Reload the DataTable when date range is selected
                    datatable.ajax.reload();
                }
            });
            selectedDate = flatpickrInstance.selectedDates[0];
            datatable = $("#class_grades_table").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: false,
                lengthChange: false,
                lengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, 'All'],
                ],
                scrollX: true,
                language: {
                    search: "",
                    searchPlaceholder: "Search...",
                },
                ajax: {
                    url: "/get-previous-data-report",
                    data: function(d) {
                        d.addContinueVal = addContinueVal;
                        d.date = flatpickrInstance.selectedDates[0];
                        d.dataType = dataType;
                    }
                }
            });
        });

        function getRRRReport(newDataType) {
            dataType = newDataType;
            // var removedash = dataType.replace(/-/g, ' ').toUpperCase();
            // var text = `Required Run Rate Report (${removedash} Comparison)`;
            // $("#required-run-rate-text").text(text);
            if (dataType == "spring-to-spring") {
                $('#class_grades_table tbody').empty(); // Clear existing rows
                $('#class_grades_table tbody').append('<tr><td colspan="12">Coming Soon</td></tr>');
            }
            if (dataType == 'year-to-year' || dataType == 'fall-to-fall') {
                datatable.ajax.reload();
            }
            if (dataType == 'add-continue') {
                if (addContinueVal == 'no') {
                    addContinueVal = 'yes';
                    $("#add_continue_val").removeClass("btn-primary");
                    $("#add_continue_val").addClass("btn-danger");
                    $("#add_continue_val").html(
                        '<i class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i> Remove Continuing Data'
                    );
                } else {
                    addContinueVal = 'no';
                    $("#add_continue_val").removeClass("btn-danger");
                    $("#add_continue_val").addClass("btn-primary");
                    $("#add_continue_val").html(
                        '<i class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i> Add Continuing Data'
                    );
                }
                datatable.ajax.reload();
            }
        }
    </script>
@endpush
