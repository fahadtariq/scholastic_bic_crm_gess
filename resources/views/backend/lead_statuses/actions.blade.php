 @permission('edit-lead-status')
<a href="{{ route('lead-status.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
<i class="mdi mdi-lead-pencil"></i>
</a>
 @endpermission

 @permission('delete-lead-status')
<a href="{{ route('lead-status.delete') }}" data-rowid="{{ $row->id }}" data-table="lead_status_table"
class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
<i class="ri-delete-bin-5-line"></i>
</a>
 @endpermission

@if (!auth()->user()->hasPermission('edit-lead-status') &&
!auth()->user()->hasPermission('delete-lead-status'))
<span>N/A</span>
@endif
