<div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Update Tag</h4>
        </div>

        <div class="card-body">
            <div class="live-preview">
                <form class="row g-3 needs-validation" novalidate action="{{ route('tags.update') }}" method="post">
                    @csrf
                    {{-- <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('beams_id')) is-invalid @endif" name="beams_id" id="beams_id"
                                   placeholder="BEAMS ID" value="{{ $tag->beams_id }}">
                            <label for="beams_id" class="form-label">BEAMS ID</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('beams_id'))
                                    {{ $errors->first('beams_id') }}
                                @endif
                            </div>
                        </div>
                    </div> --}}

                    <input type="hidden" value="{{ $tag->id }}" name="id">
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif"
                                   id="name" name="name" placeholder="Please enter tag name"
                                   value="{{ $tag->name ?? '' }}" required>
                            <label for="name" class="form-label">Tag name</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('name'))
                                    {{ $errors->first('name') }}
                                @else
                                    Tag name is required!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control" name="abbreviation" id="abbreviation"
                                   placeholder="Please enter abbreviation" value="{{ $tag->abbreviation ?? '' }}">
                            <label for="abbreviation" class="form-label">Abbreviation</label>
                            <div class="invalid-tooltip">Tag name abbreviation is required!</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3 @if ($errors->has('status_id')) is-invalid @endif" name="status_id" required>

                                @foreach ($statuses as $status)
                                    <option {{ ($status->id == $tag->status_id) ? 'selected' : '' }} value="{{ $status->id }}"
                                    @if (old('status_id') == $status->id) {{ 'selected' }} @endif>
                                        {{ $status->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="statusID" class="form-label">Status</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('status_id'))
                                    {{ $errors->first('status_id') }}
                                @else
                                    Select the status!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-end">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                        <button type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
