@permission('edit-tag')
<a href="{{ route('tags.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
<i class="mdi mdi-lead-pencil"></i>
</a>
@endpermission

@permission('delete-tag')
<a href="{{ route('tags.delete') }}" data-rowid="{{ $row->id }}" data-table="tags-table"
class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
<i class="ri-delete-bin-5-line"></i>
</a>
@endpermission

 @if (!auth()->user()->hasPermission('edit-tag') &&
    !auth()->user()->hasPermission('delete-tag'))
     <span>N/A</span>
 @endif
