@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')

    <div class="row">
        @if (isset($tag))
            @permission('edit-tag')
            @include('backend.tags.edit')
            @endpermission
        @else
             @permission('add-tag')
                @include('backend.tags.create')
             @endpermission
        @endif

        <div class="col-lg-12">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Tags</h4>
            </div>
            <div class="card">
                <div class="card-body">
                    <table id="tags-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Abbreviation</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Abbreviation</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ route('tags.index') }}" id="tag_list_url">
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('backend/modules/tags.js') }}"></script>
@endpush
