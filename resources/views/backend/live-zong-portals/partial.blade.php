<table id="zong-data-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
    <thead>
        <tr>
            <th>Agent</th>
            <th>Clients</th>
            <th>Date / Time</th>
            <th>Duration (HH:MM:SS)</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($calls as $call)
            <tr>
                <th>
                    @if (is_null($call->ext))
                        IVR
                    @elseif($call->ext == '')
                        Queue
                    @else
                        {{ $call->ext }}
                    @endif
                </th>
                <th>
                    @if ($callType == 1)
                        {{ $call->phone ?? '' }}
                    @else
                        {{ substr($call->phone, 2) }}
                    @endif
                </th>
                <th>{{ dateTimeFormat($call->datetime) }}</th>
                <th>{{ gmdate('H:i:s', $call->duration) }}</th>
                <th>
                    @if ($call->status == 'Connected' || $call->status == 'Answered')
                        <audio controls>
                            <source src="{{ str_replace('.gsm', '.mp3', $call->recording) }}">
                            Your browser does not support the audio element.
                        </audio>
                    @else
                        <span style="color:red">{{ $call->status == 'Busy' ? 'Network Error' : $call->status }}</span>
                    @endif
                </th>
            </tr>
        @endforeach
    </tbody>
</table>
