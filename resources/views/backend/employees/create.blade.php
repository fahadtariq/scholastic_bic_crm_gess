@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Add Employee</h4>
                </div>

                <div class="card-body">
                    <form class="row g-3 needs-validation" action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data" novalidate>
                        @csrf

                        @if(auth()->user()->hasRole('super_admin'))
                            <div class="col-md-4 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="roles-select2 form-select @if($errors->has('branch_id')) is-invalid @endif" id="branch_id" name="branch_id" aria-label="Branch select" required>
                                         @foreach ($branches as $branch)
                                            <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                        @endforeach
                                    </select>
                                    <label for="branch_id" class="form-label">Branch</label>
                                    <div class="invalid-tooltip">
                                        @if($errors->has('branch_id'))
                                            {{ $errors->first('branch_id') }}
                                        @else
                                            Select the branch!
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control" id="employee_beams_id" name="employee_beams_id" placeholder="Employee Beams ID" value="{{ old('employee_beams_id') }}" >
                                <label for="employee_beams_id" class="form-label">Employee Beams ID</label>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('first_name')) is-invalid @endif" id="first_name" name="first_name" placeholder="First Name" value="{{ old('first_name') }}"  required>
                                <label for="first_name" class="form-label">First Name</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('first_name'))
                                        {{ $errors->first('first_name') }}
                                    @else
                                        First name is required!
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
                                <label for="last_name" class="form-label">Last Name</label>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="email" class="form-control @if($errors->has('email')) is-invalid @endif" id="EmpEmail" name="email" placeholder="email@domain.com" value="{{ old('email') }}" required>
                                <label for="EmpEmail" class="form-label">Email</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('email'))
                                        {{ $errors->first('email') }}
                                    @else
                                        Email is required!
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="password" class="form-control @if($errors->has('password')) is-invalid @endif" id="password" name="password" placeholder="Password" value="{{ old('password') }}" required>
                                <label for="password" class="form-label">Password</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('password'))
                                        {{ $errors->first('password') }}
                                    @else
                                        Password is required!
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('contact_code_id')) is-invalid @endif" id="contact_code_id" name="contact_code_id" aria-label="Contact code select" required>
                                    <option value="">Please select a contact code</option>
                                    @foreach ($contactCodes as $contactCode)
                                        <option value="{{ $contactCode->id }}">{{ $contactCode->code }}</option>
                                    @endforeach
                                </select>
                                <label for="contact_code_id" class="form-label">Contact Code</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('contact_code_id'))
                                        {{ $errors->first('contact_code_id') }}
                                    @else
                                        Contact code is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="number" class="form-control @if($errors->has('contact_number')) is-invalid @endif" id="contact_number" name="contact_number" placeholder="Contact Number" required>
                                <label for="contact_number" class="form-label">Contact Number</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('contact_number'))
                                        {{ $errors->first('contact_number') }}
                                    @else
                                        Contact number is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="roles-select2 form-select @if($errors->has('role_ids')) is-invalid @endif" id="role_ids" name="role_ids[]" aria-label="Role select" required>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                                    @endforeach
                                </select>
                                <label for="role_ids" class="form-label">Roles</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('role_ids'))
                                        {{ $errors->first('role_ids') }}
                                    @else
                                        Role is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-label-group in-border">
                                <textarea class="form-control" name="address"></textarea>
                                <label for="address" class="form-label">Address</label>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select" id="status_id" name="status_id" aria-label="Status select" required>
                                    @forelse($statuses as $status)
                                        <option value="{{ $status->id }}">{{ $status->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <label for="status_id" class="form-label">Status</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('status_id'))
                                        {{ $errors->first('status_id') }}
                                    @else
                                        Status is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-12 text-end">
                            <button class="btn btn-primary" type="submit">Submit form</button>
                            <a href="{{ route('employees.index') }}" type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
    <script>
        $(document).ready(function () {
            $('.roles-select2').select2();
        })
    </script>
@endpush
