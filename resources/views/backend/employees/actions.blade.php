@permission('edit-employee')
<a href="{{ route('employee.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
<i class="mdi mdi-lead-pencil"></i>
</a>
@endpermission

@permission('delete-employee')
<a href="{{ route('employee.delete') }}" data-rowid="{{ $row->id }}" data-table="employees-table"
class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
<i class="ri-delete-bin-5-line"></i>
</a>
@endpermission

 @if (!auth()->user()->hasPermission('edit-employee') &&
    !auth()->user()->hasPermission('delete-employee'))
     <span>N/A</span>
 @endif
