@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')

    <div class="row">
        <div class="col-lg-12">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Employees</h4>

                @permission('add-employee')
                    <div class="flex-shrink-0">
                        <a href="{{ route('employee.create') }}" class="btn btn-success btn-label btn-sm">
                            <i class="ri-add-fill label-icon align-middle fs-16 me-2"></i> Add New
                        </a>
                    </div>
                @endpermission

            </div>
            <div class="card">
                <div class="card-body">

                    <div class="row">

                        @permission('add-branch')
                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="filter form-select" id="branch_id" name="branch_id" placeholder="Branch">
                                    <option value="">Please select</option>
                                    @forelse($branches as $branch)
                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <label for="branch_id" class="form-label">Branch</label>
                            </div>
                        </div>
                        @endpermission

                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="filter form-select" id="role_id" name="role_id" placeholder="Role">
                                    <option value="">Please select</option>
                                    @forelse($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <label for="branch_id" class="form-label">Role</label>
                            </div>
                        </div>


                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <input id="mySearch" type="text" placeholder="Search.." class="form-control" name="mySearch">
                                <label for="mySearch" class="form-label">Email</label>
                            </div>
                        </div>
                    </div>

                    <table id="employees-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
                        <thead>
                        <tr>
                            <th>Branch</th>
                            <th>First&nbspName</th>
                            <th>Last&nbspName</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Branch</th>
                            <th>First&nbspName</th>
                            <th>Last&nbspName</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ route('employees.index') }}" id="employees_list_url">
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('backend/modules/employees.js') }}"></script>
@endpush
