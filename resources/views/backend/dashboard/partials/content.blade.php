<div class="row">
    @permission('show-lead-by-source')
        <div class="col-md-6 col-sm-12">
            <div class="card card-height-100">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Leads by Source</h4>
                </div>

                <div class="card-body">
                    <div id="lead-by-source-chart" data-colors='["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]' class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    @endpermission
    <div class="col-md-6 col-sm-12">
        <div class="card card-height-100">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Leads by Status</h4>
            </div>

            <div class="card-body">
                <div id="lead-by-status-chart" data-colors='["--vz-primary", "--vz-success", "--vz-warning", "--vz-danger", "--vz-info"]' class="apex-charts" dir="ltr"></div>
            </div>
        </div>
    </div>

    @if(auth()->user()->hasRole('super_admin|campus_head|admin_officer|social_media_manager'))
        <div class="col-md-12 col-sm-12">
            <div class="card card-height-100">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Branch Lead Targets</h4>
                </div>

                <div class="card-body">
                    <div id="branch-target-chart" data-colors='["--vz-primary", "--vz-success"]' class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    @endif
</div>

<script>

    function getChartColorsArray(e) { if (null !== document.getElementById(e)) { var e = document.getElementById(e).getAttribute("data-colors"); return (e = JSON.parse(e)).map(function(e) { var t = e.replace(" ", ""); if (-1 === t.indexOf(",")) { var o = getComputedStyle(document.documentElement).getPropertyValue(t); return o || t }
        e = e.split(","); return 2 != e.length ? t : "rgba(" + getComputedStyle(document.documentElement).getPropertyValue(e[0]) + "," + e[1] + ")" }) } }

        /*lead by source donut chart*/
    var chartDonutBasicColors=getChartColorsArray("lead-by-source-chart"),
        options={
            series: {!! json_encode($sourcesCounts) !!},
            labels: {!! json_encode($sourcesLabels) !!},
            chart:{height:333,type:"donut"},
            legend:{position:"bottom"},
            stroke:{show:!1},
            dataLabels:{
                dropShadow:{enabled:!1}
            },
            colors:chartDonutBasicColors
        };
    (chart=new ApexCharts(document.querySelector("#lead-by-source-chart"),options)).render();

    /*Leads by status pie chart*/
    var chartDonutBasicColors=getChartColorsArray("lead-by-status-chart"),
        options={
            series: {!! json_encode($statusCounts) !!},
            labels: {!! json_encode($statusLabels) !!},
            chart:{height:333,type:"pie"},
            legend:{position:"bottom"},
            stroke:{show:!1},
            dataLabels:{
                dropShadow:{enabled:!1}
            },
            colors:chartDonutBasicColors
        };
    (chart=new ApexCharts(document.querySelector("#lead-by-status-chart"),options)).render();

    /*Branches lead targets bar chart*/
    var options = {
        series: [{
            name: "Target",
            data: {!! json_encode($branchTargets) !!}
        }, {
            name: "Achieved",
            data: {!! json_encode($branchTargetAchieved) !!}
        }],
        chart: {
            type: 'bar',
            height: 430,
            toolbar: {
                show: false,
            },
        },
        plotOptions: {
            bar: {
                horizontal: false,
                dataLabels: {
                    position: 'top',
                },
            }
        },
        dataLabels: {
            enabled: true,
            offsetX: -6,
            style: {
                fontSize: '12px',
                colors: ['#fff']
            }
        },
        stroke: {
            show: true,
            width: 1,
            colors: ['#fff']
        },
        tooltip: {
            shared: true,
            intersect: false
        },
        xaxis: {
            categories: {!! json_encode($branchLabels) !!},
        },
    };

    var chart = new ApexCharts(document.querySelector("#branch-target-chart"), options);
    chart.render();
</script>
