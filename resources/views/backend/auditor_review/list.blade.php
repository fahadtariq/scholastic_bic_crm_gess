@extends('layouts.master')

@section('title', 'Scholastic | CRM | Auditor Review')

@section('content')

    @include('backend.components.flash_message')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h4 class="card-title mb-0 flex-grow-1">CRM Review</h4>
                    @permission('add-review')
                    @endpermission
                    <div class="flex-shrink-0">
                        <a href="{{ route('auditor-review.create') }}" class="btn btn-success btn-label btn-sm">
                            <i class="ri-add-fill label-icon align-middle fs-16 me-2"></i> Add New
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered  nowrap" id="auditor-review-table" style="width: 100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Review Date</th>
                            <th>Lead Count Accuracy</th>
                            <th>Lead Assignment Accuracy</th>
                            <th>Review Accuracy</th>
                            <th>Admission Accuracy </th>
                            <th>Outbound Call Accuracy</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Review Date</th>
                            <th>Lead Count Accuracy</th>
                            <th>Lead Assignment Accuracy</th>
                            <th>Review Accuracy</th>
                            <th>Admission Accuracy </th>
                            <th>Outbound Call Accuracy</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{ route('auditor-review.index') }}" id="auditor-review_list_url">
    </div>
@endsection

@push('footer_scripts')
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/modules/auditor-review.js') }}"></script>
@endpush
