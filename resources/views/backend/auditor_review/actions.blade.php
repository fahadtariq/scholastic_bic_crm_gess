@permission('')
@endpermission
<a href="{{ route('auditor-review.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
    <i class="mdi mdi-lead-pencil"></i>
</a>

@permission('')
@endpermission
<a href="{{ route('auditor-review.destroy', $row->id) }}" data-rowid="{{ $row->id }}" data-type="DELETE" data-table="auditor-review-table"
   class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
    <i class="ri-delete-bin-5-line"></i>
</a>

