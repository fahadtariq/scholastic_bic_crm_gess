@extends('layouts.master')

@section('title', 'Scholastic | CRM | Auditor Review')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Update CRM Review</h4>
                </div>

                <div class="card-body">
                    <form class="row g-3 needs-validation" onsubmit="disableSubmitBtn(this)" action="{{ route('auditor-review.update', $auditor_review->id) }}" method="POST" novalidate>
                        @csrf
                        @method('PUT')
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('review_date')) is-invalid @endif" id="review_date" name="review_date" placeholder="Review Date" data-old-value="{{ $auditor_review->review_date }}" required>
                                <label for="review_date" class="form-label">Review&nbsp;Date</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('review_date'))
                                        {{ $errors->first('review_date') }}
                                    @else
                                        Select the follow up date!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('lead_count_acc')) is-invalid @endif" id="lead_count_acc" name="lead_count_acc" aria-label="State select" required>
                                    <option value="">Please select Lead Count Accuracy</option>
                                    <option value="1" {{ $auditor_review->lead_count_acc == 1 ? 'selected' : '' }}>Yes</option>
                                    <option value="0" {{ $auditor_review->lead_count_acc == 0 ? 'selected' : '' }}>No</option>
                                </select>
                                <label for="lead_count_acc" class="form-label">Lead Count Accuracy</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('lead_count_acc'))
                                        {{ $errors->first('lead_count_acc') }}
                                    @else
                                        Lead Count Accuracy is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('lead_assignment_acc')) is-invalid @endif" id="lead_assignment_acc" name="lead_assignment_acc" aria-label="State select" required>
                                    <option value="">Please select Lead Assignment Accuracy</option>
                                    <option value="1" {{ $auditor_review->lead_assignment_acc == 1 ? 'selected' : '' }}>Yes</option>
                                    <option value="0" {{ $auditor_review->lead_assignment_acc == 0 ? 'selected' : '' }}>No</option>
                                </select>
                                <label for="lead_assignment_acc" class="form-label">Lead Assignment Accuracy</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('lead_assignment_acc'))
                                        {{ $errors->first('lead_assignment_acc') }}
                                    @else
                                        Lead Assignment Accuracy is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('review_acc')) is-invalid @endif" id="review_acc" name="review_acc" aria-label="State select" required>
                                    <option value="">Please select Review Accuracy</option>
                                    <option value="1" {{ $auditor_review->review_acc == 1 ? 'selected' : '' }}>Yes</option>
                                    <option value="0" {{ $auditor_review->review_acc == 0 ? 'selected' : '' }}>No</option>
                                </select>
                                <label for="review_acc" class="form-label">Review Accuracy</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('review_acc'))
                                        {{ $errors->first('review_acc') }}
                                    @else
                                        Review Accuracy is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('admission_acc')) is-invalid @endif" id="admission_acc" name="admission_acc" aria-label="State select" required>
                                    <option value="">Please select Admission Accuracy</option>
                                    <option value="1" {{ $auditor_review->admission_acc == 1 ? 'selected' : '' }}>Yes</option>
                                    <option value="0" {{ $auditor_review->admission_acc == 0 ? 'selected' : '' }}>No</option>
                                </select>
                                <label for="admission_acc" class="form-label">Admission Accuracy</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('admission_acc'))
                                        {{ $errors->first('admission_acc') }}
                                    @else
                                        Admission Accuracy is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('outbound_call_acc')) is-invalid @endif" id="outbound_call_acc" name="outbound_call_acc" aria-label="State select" required>
                                    <option value="">Please select Outbound Call Accuracy</option>
                                    <option value="1" {{ $auditor_review->outbound_call_acc == 1 ? 'selected' : '' }}>Yes</option>
                                    <option value="0" {{ $auditor_review->outbound_call_acc == 0 ? 'selected' : '' }}>No</option>
                                </select>
                                <label for="outbound_call_acc" class="form-label">Outbound Call Accuracy</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('outbound_call_acc'))
                                        {{ $errors->first('outbound_call_acc') }}
                                    @else
                                        Outbound Call Accuracy is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-label-group in-border">
                                <textarea class="form-control  @if($errors->has('review_remarks')) is-invalid @endif"  name="review_remarks">{{ $auditor_review->review_remarks }}</textarea>
                                <label for="review_remarks" class="form-label">Remarks</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('review_remarks'))
                                        {{ $errors->first('review_remarks') }}
                                    @else
                                        {{--Remarks is required!--}}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-12 text-end">
                            <button class="btn btn-primary" type="submit">Submit form</button>
                            <a href="{{ route('auditor-review.index') }}" type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/modules/auditor-review.js') }}"></script>
@endpush
