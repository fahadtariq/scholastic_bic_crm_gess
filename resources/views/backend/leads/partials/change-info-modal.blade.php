<div id="changeInfoModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zoomInModalLabel">CHANGE {{ strtoupper($action) }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-0">
                <input type="hidden" value="{{ $lead->id ?? '' }}" id="lead_id">
                <div class="row">
                   <div class="col-xxl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="row g-3 needs-validation change-info-form" action="javascript:;" data-route="{{ route('lead.info.change-store') }}" method="POST" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <input type="hidden" name="lead_id" value="{{ $lead->id  }}">
                                    @if($action == 'type')
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('lead_type_id')) is-invalid @endif" id="lead_type_id" name="lead_type_id" aria-label="Lead Type select" required>
                                                    @foreach ($leadTypes as $leadType)
                                                        <option value="{{ $leadType->id }}" {{ ($leadType->id == $lead->lead_type_id) ? 'selected' : '' }}>{{ $leadType->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="lead_type_id" class="form-label">Type</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('lead_type_id'))
                                                        {{ $errors->first('lead_type_id') }}
                                                    @else
                                                        Select the type!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($action == 'status')
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="change-status form-select @if($errors->has('lead_status_id')) is-invalid @endif" id="lead_status_id" name="lead_status_id" aria-label="Lead Status select" required>
                                                    @foreach ($leadStatuses as $leadStatus)
                                                        <option value="{{ $leadStatus->id }}" {{ ($leadStatus->id == $lead->lead_status_id) ? 'selected' : '' }}>{{ $leadStatus->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="lead_status_id" class="form-label">Status</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('lead_status_id'))
                                                        {{ $errors->first('lead_status_id') }}
                                                    @else
                                                        Select the status!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 reason-dropdown-area" style="display:none">
                                            <div class="form-label-group in-border">
                                                <select class="reason-dropdown form-select @if($errors->has('reason')) is-invalid @endif" id="reason" name="reason" aria-label="Reason select" disabled>
                                                    @foreach ($reasons as $reason)
                                                        <option value="{{ $reason->id }}" >{{ $reason->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="reason" class="form-label">Reason</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('reason'))
                                                        {{ $errors->first('reason') }}
                                                    @else
                                                        Select the reason!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                    @elseif($action == 'employee')
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="employees form-select @if($errors->has('employee_id')) is-invalid @endif" id="employee_id" name="employee_id" aria-label="Employee select" required>
                                                    <option value="">Select Employee</option>
                                                    @foreach ($employees as $employee)
                                                        @if($employee->user->hasRole(['admission_advisor']))
                                                            <option value="{{ $employee->id }}" {{ ($employee->id == $lead->employee_id) ? 'selected' : '' }}>{{ $employee->first_name }} {{ $employee->last_name }} -- {{ implode(',', $employee->user->roles()->pluck('display_name')->toArray()) }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <label for="employee_id" class="form-label">Employee</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('employee_id'))
                                                        {{ $errors->first('employee_id') }}
                                                    @else
                                                        Select the employee!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($action == 'branch')
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('branch_id')) is-invalid @endif" name="branch_id" aria-label="Branch select" required>
                                                    @foreach ($branches as $branch)
                                                        <option value="{{ $branch->id }}" {{ ($branch->id == $lead->branch_id) ? 'selected' : '' }}>{{ $branch->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="branch_id" class="form-label">Branch</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('branch_id'))
                                                        {{ $errors->first('branch_id') }}
                                                    @else
                                                        Select the branch!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-12 col-sm-12 remarks-box">
                                        <div class="form-label-group in-border">
                                            <textarea type="text" class="remarks-textarea form-control" id="reason" name="reason" placeholder="Remarks"></textarea>
                                            <label for="reason" class="form-label">Remarks</label>
                                        </div>
                                    </div>
                                    <div class="col-12 text-end">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
