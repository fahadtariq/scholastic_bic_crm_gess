<div id="importLeadsModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zoomInModalLabel">Import Leads</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-0">
                @if (count($errors) > 0)
                    <div class="row error-row lead-card-error-msg">
                        <div class="col-12 col-md-offset-1">
                            <div class="alert alert-danger alert-dismissible">
                                @foreach($errors->all() as $error)
                                    {{ $error }} <br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xxl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="row g-3 needs-validation import-leads-form" onsubmit="disableSubmitBtn(this)" action="{{route('lead.import')}}" method="POST" name="leadImportForm" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    @if(auth()->user()->hasRole('super_admin|social_media_manager'))
                                        <div class="col-md-12 col-sm-12 p-0">
                                            <div class="form-label-group in-border">
                                                <select class="form-select branch_import_filter @if($errors->has('branch_id')) is-invalid @endif" name="branch_id" data-session-route="{{ route('branch.sessions') }}" aria-label="Branch select" required>
                                                    <option value="">Select Branch</option>
                                                    @foreach ($branches as $branch)
                                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="branch_id" class="form-label">Branch</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('branch_id'))
                                                        {{ $errors->first('branch_id') }}
                                                    @else
                                                        Select the branch!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(auth()->user()->hasRole('campus_head|admission_advisor'))
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="filter form-select session_import_select form-control" name="session_id" placeholder="Session" required>
                                                    <option value="">Select Session</option>
                                                    @foreach($sessions as $session)
                                                        <option value="{{ $session->id }}">{{ $session->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="session_id" class="form-label">Session</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('session_id'))
                                                        {{ $errors->first('session_id') }}
                                                    @else
                                                        Select the session!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="filter form-select session_import_select" name="session_id" placeholder="Session" required>
                                                    <option value="">Select Session</option>
                                                </select>
                                                <label for="session_id" class="form-label">Session</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('session_id'))
                                                        {{ $errors->first('session_id') }}
                                                    @else
                                                        Select the session!
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                    <input class="form-control" type="file" class="form-control" name="file" id="file"  required>
                                    <div class="col-12 text-end">
                                        <button class="btn btn-primary" type="submit">Upload</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
