<style>
    ul.timeline {
        margin-left: -22px;
        list-style-type: none;
        position: relative;
    }

    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }

    ul.timeline>li {
        margin: 20px 0;
        padding-left: 20px;
    }

    ul.timeline>li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }
</style>

<div class="modal-content">
    <div class="modal-header pt-3">
        {{-- <h5 class="modal-title" id="zoomInModalLabel">Lead Information</h5> --}}
        <button type="button" class="btn-close z-5" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body pt-0">
        <form class="row g-3 needs-validation lead-message-form" action="javascript:void(0);"
            data-route="{{ route('lead.message.send') }}" method="POST" enctype="multipart/form-data" novalidate>
            @csrf
            <div class="row">
                {{-- Student Information --}}
                <div class="col-xxl-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h6 class="card-title mb-0">Student Information</h6>
                        </div>
                        <div class="card-body student-card">
                            <div class="lead-send-error-message-container"></div>
                            <div class="row">
                                <input type="hidden" name="lead_id" value="{{ $lead->id }}">
                                <div class="col-md-6 mb-1">
                                    <label for="name">Name:</label>
                                    <input readonly type="text" id="name" name="name" class="form-control disabled"
                                        placeholder="Student Name" value="{{ $lead->student->first_name ?? '' }}">
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label for="number">Number:</label>
                                    <input type="text" id="number" name="number" class="form-control disabled"
                                        placeholder="Student Number" value="{{ "+92".$lead->student->contact_number ?? '' }}">
                                </div>
                                <div class="col-md-12 mb-1">
                                    <label for="message">Message:</label>
                                    <textarea id="message" @if($enableWhatsappChat == false) readonly @endif name="message" class="form-control" rows="8"
                                        placeholder="Type your message here">
Welcome to Beaconhouse Internation College, we are thrilled to assist you.
Please Share Your Details:
Name:
Email:
School:
Please take a few moments to fill out the information above. We value your time and appreciate your interest in Scholastic CRM. Our Team will get in touch with you shortly.</textarea>
                                </div>
                                <div class="col-md-12 mb-1">
                                    <label for="name">Attachment URL:</label>
                                    <input type="text" id="attachment_url" name="attachment_url" class="form-control"
                                        placeholder="Attachment URL" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
        </form>
    </div>

    <style>
        .whatsapp-chat-container .chat-list {
            padding: 0;
            font-size: .8rem;
        }

        .whatsapp-chat-container .chat-list li {
            margin-bottom: 10px;
            overflow: auto;
            color: #ffffff;
        }

        .whatsapp-chat-container .chat-list .chat-img {
            float: left;
            width: 48px;
        }

        .whatsapp-chat-container .chat-list .chat-img img {
            -webkit-border-radius: 50px;
            -moz-border-radius: 50px;
            border-radius: 50px;
            width: 100%;
        }

        .whatsapp-chat-container .chat-list .chat-message {
            -webkit-border-radius: 50px;
            -moz-border-radius: 50px;
            border-radius: 50px;
            background: #5a99ee;
            display: inline-block;
            padding: 10px 20px;
            position: relative;
        }

        .whatsapp-chat-container .chat-list .out .chat-message h5 {
            text-align: right;
        }
        .whatsapp-chat-container .chat-list .in .chat-message h5 {
            text-align: left;
        }

        .whatsapp-chat-container .chat-list .chat-message h5 {
            margin: 0 0 5px 0;
            font-weight: 600;
            line-height: 100%;
            font-size: .9rem;
        }

        .whatsapp-chat-container .chat-list .chat-message p {
            line-height: 18px;
            margin: 0;
            padding: 0;
        }

        .whatsapp-chat-container .chat-list .chat-body {
            margin-left: 20px;
            float: left;
            width: 70%;
        }

        .whatsapp-chat-container .chat-list .in .chat-message:before {
            left: -12px;
            border-bottom: 20px solid transparent;
            border-right: 20px solid #5a99ee;
        }

        .whatsapp-chat-container .chat-list .out .chat-img {
            float: right;
        }

        .whatsapp-chat-container .chat-list .out .chat-body {
            float: right;
            margin-right: 20px;
            /*text-align: right;*/
        }

        .whatsapp-chat-container .chat-list .out .chat-message {
            background: #fc6d4c;
        }

        .whatsapp-chat-container .chat-list .out .chat-message:before {
            right: -12px;
            border-bottom: 20px solid transparent;
            border-left: 20px solid #fc6d4c;
        }

        .whatsapp-chat-container .card .card-header:first-child {
            -webkit-border-radius: 0.3rem 0.3rem 0 0;
            -moz-border-radius: 0.3rem 0.3rem 0 0;
            border-radius: 0.3rem 0.3rem 0 0;
        }
        .whatsapp-chat-container .card .card-header {
            background: #17202b;
            border: 0;
            font-size: 1rem;
            padding: .65rem 1rem;
            position: relative;
            font-weight: 600;
            color: #ffffff;
        }

        .whatsapp-chat-container .content{
            margin-top:40px;
        }
    </style>

    <div class="container content whatsapp-chat-container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">Chat</div>
                    <div class="card-body height3">
                        <ul class="chat-list">
                            @if( !isset($messages) )
                                <span>No chat started yet.</span>
                            @else
                                @foreach($messages as $chat)
                                <li class="@if($chat->status == 'Sent') out @elseif($chat->status == 'Received') in @endif">
                                    <div class="chat-img">
                                        @if($chat->status == 'Sent')
                                            <img alt="Avatar" src="{{asset('assets/images/bic-logo.jpg')}}">
                                        @else
                                            <img alt="Avatar" src="{{asset('assets/images/user-avatar.png')}}">
                                        @endif
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-message">
                                            @if($chat->status == 'Sent')
                                                <h5>Beaconhouse Internation College</h5>
                                            @else
                                                <h5>Student</h5>
                                            @endif
                                            <p>{!! nl2br($chat->text) !!}</p>
                                        </div>
                                    </div>
                                </li>

                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
