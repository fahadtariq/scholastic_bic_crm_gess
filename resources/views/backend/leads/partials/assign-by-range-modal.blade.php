<div id="assignByRangeLeadsModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zoomInModalLabel">Leads Assign by Range</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-0">
                <div class="row">
                    <div class="col-12 col-md-offset-1">
                        <div class="alert alert-danger alert-dismissible append-errors" style="display: none">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xxl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="row g-3 needs-validation assign-by-range-leads-form" onsubmit="disableSubmitBtn(this)" action="javascript:;"  data-route="{{route('lead.assign-by-range')}}" method="POST" name="assignByRangeLeadsForm" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input id="min" type="number" min="1" placeholder="Min" class="form-control" name="min" >
                                            <label for="min" class="form-label">Min</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input id="max" type="number" placeholder="Max" class="form-control" name="max" disabled required>
                                            <label for="max" class="form-label">Max</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 p-0">
                                        <div class="form-label-group in-border">
                                            <select class="form-select @if($errors->has('employee_id')) is-invalid @endif" name="employee_id" aria-label="Employee select" required>
                                                <option value="">Select Employee</option>
                                                @foreach ($employees as $employee)
                                                    @if($employee->user->hasRole('admission_advisor'))
                                                      <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }} -- {{ implode(',', $employee->user->roles()->pluck('display_name')->toArray()) }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <label for="employee_id" class="form-label">Employees</label>
                                            <div class="invalid-tooltip">
                                                @if($errors->has('employee_id'))
                                                    {{ $errors->first('employee_id') }}
                                                @else
                                                    Select the employee!
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-end">
                                        <button class="btn btn-primary" type="submit">Assign</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
