<style>
    ul.timeline {
        margin-left: -22px;
        list-style-type: none;
        position: relative;
    }
    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }
    ul.timeline > li {
        margin: 20px 0;
        padding-left: 20px;
    }
    ul.timeline > li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }
</style>

<div class="modal-content">
    <div class="modal-header pt-3">
        {{-- <h5 class="modal-title" id="zoomInModalLabel">Lead Information</h5> --}}
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
        <form class="row g-3 needs-validation lead-p-email-form" action="javascript:void(0);"
            data-route="{{ route('lead.p.sendemail') }}" method="POST" enctype="multipart/form-data" novalidate>
            @csrf
            <div class="row">
                {{-- Student Information --}}
                <div class="col-xxl-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h6 class="card-title mb-0">Admission Email</h6>
                        </div>
                        <div class="card-body student-card">
                            <div class="lead-send-error-message-container"></div>
                            <div class="row">
                                <input type="hidden" name="lead_id" value="{{ $lead->id }}">
                                <div class="col-md-6 mb-1">
                                    <label for="receiver-email">To:</label>
                                    <input type="text" id="receiver-email" name="receiveremail" class="form-control disabled"
                                        placeholder="Receiver Email" value="muzzamal.irfan@bic.edu.pk">
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label for="number">Subject:</label>
                                    <input type="text" id="email-subject" readonly name="emailsubject" class="form-control disabled"
                                        placeholder="Email Subject" value="Process Admission">
                                </div>
                                <div class="col-md-12 mb-1">
                                    <label for="message">Message:</label>
                                    <textarea id="message" readonly name="message" class="form-control" rows="8"
                                        placeholder="Type your message here">
Please process admission of below student
Name: {{$lead->student->first_name}}
Phone: {{$lead->student->contact_number}}
Registration ID: {{$lead->beams_registration_id}}
Visitor ID: {{$lead->beams_id}}
Campus: {{$lead->branch->name}}
Session: {{$lead->session->name}}
                                    </textarea>
                                </div>
                                <div class="col-md-12 mb-1">
                                    <label for="remarks">Remarks:</label>
                                    <textarea id="remarks" name="message" class="form-control" rows="3"
                                        placeholder="Type your remarks here"></textarea>
                                </div>
                                <div class="col-md-12 mb-1">
                                    <label for="number">Attachment Link (Google Drive):</label>
                                    <input type="text" id="attachment_link" name="attachment_link" class="form-control"
                                        placeholder="Attachment Link" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        if ($("#snow-editor").length) {
            var quill_snow;
            quill_snow = new Quill("#snow-editor", {
                modules: {
                    toolbar: [
                        [{ header: [1, 2, 3, 4, 5, 6, false] }],
                        ["bold", "italic", "underline", "strike"],
                        ["code-block"],
                        ["link"],
                        [{ script: "sub" }, { script: "super" }],
                        [{ list: "ordered" }, { list: "bullet" }],
                        ["clean"],
                    ],
                },
                theme: "snow",
            });
            quill_snow.on("text-change", function(delta, oldDelta, source) {
                $("#follow_up_remarks").val(quill_snow.root.innerHTML);
            });
        }
    });
</script>