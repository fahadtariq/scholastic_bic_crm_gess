<option value="">Select Employee</option>
@foreach ($branchEmployees as $employee)
    <option value="{{ $employee->id }}" {{ $autoSelectedEmployeeId == $employee->id ? 'selected' : '' }}>{{ $employee->first_name . ' ' . $employee->last_name }} -- {{ implode(',', $employee->user->roles()->pluck('display_name')->toArray()) }}</option>
@endforeach
