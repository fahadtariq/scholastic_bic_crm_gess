<div id="addFollowUpModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zoomInModalLabel">ADD FOLLOW UP</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-0">
                <input type="hidden" value="{{ $lead->id ?? '' }}" id="lead_id">
                <div class="row">
                    <div class="col-xxl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="row g-3 needs-validation store-lead-follow-up-form" action="javascript:;" data-route="{{ route('lead.follow-up.store') }}" method="POST" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <input type="hidden" name="lead_id" value="{{ $lead->id  }}">

                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <select class="source_id form-select @if($errors->has('source_id')) is-invalid @endif" id="source_id" name="source_id" aria-label="Source select" required>
                                                <option value="">Select Source</option>
                                                @foreach ($sources as $source)
                                                    <option value="{{ $source->id }}">{{ $source->name }}</option>
                                                @endforeach
                                            </select>
                                            <label for="source_id" class="form-label">Source</label>
                                            <div class="invalid-tooltip">
                                                @if($errors->has('source_id'))
                                                    {{ $errors->first('source_id') }}
                                                @else
                                                    Select the source!
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input type="text" class="follow_up_date form-control @if($errors->has('follow_up_date')) is-invalid @endif" id="follow_up_date" name="follow_up_date" placeholder="Follow Up Date" value="{{ old('follow_up_date') }}" required>
                                            <label for="follow_up_date" class="form-label">Follow&nbsp;Up&nbsp;Date</label>
                                            <div class="invalid-tooltip">
                                                @if($errors->has('follow_up_date'))
                                                    {{ $errors->first('follow_up_date') }}
                                                @else
                                                    Select the follow up date!
                                                @endif
                                            </div>
                                        </div>
                                    </div>

{{--                                    <div class="col-md-6 col-sm-12">--}}
{{--                                        <div class="form-label-group in-border">--}}
{{--                                            <input type="text" class="form-control @if($errors->has('follow_up_by')) is-invalid @endif" id="follow_up_by" name="follow_up_by" placeholder="Follow up by" value="{{ old('follow_up_by') }}" required>--}}
{{--                                            <label for="follow_up_by" class="form-label">Follow&nbsp;Up&nbsp;By</label>--}}
{{--                                            <div class="invalid-tooltip">--}}
{{--                                                @if($errors->has('follow_up_by'))--}}
{{--                                                    {{ $errors->first('follow_up_by') }}--}}
{{--                                                @else--}}
{{--                                                    Follow up by is required!--}}
{{--                                                @endif--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input type="number" min="0" class="form-control" id="follow_up_duration" name="follow_up_duration" placeholder="Follow up Duration" value="{{ old('follow_up_duration') }}">
                                            <label for="follow_up_duration" class="form-label">Follow&nbsp;Up&nbsp;Duration&nbsp;in&nbsp;Minutes</label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <textarea class="form-control" id="follow_up_remarks" name="follow_up_remarks" placeholder="Follow Up Remarks"></textarea>
                                            <label for="follow_up_remarks" class="form-label">Remarks</label>
                                        </div>
                                    </div>
                                    <div class="col-12 text-end">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#follow_up_date').flatpickr({
            enableTime: true,
            time_24hr: true
        });
    })
</script>
