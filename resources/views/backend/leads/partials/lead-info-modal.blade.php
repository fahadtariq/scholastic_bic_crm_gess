<style>
    ul.timeline {
        margin-left: -22px;
        list-style-type: none;
        position: relative;
    }
    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }
    ul.timeline > li {
        margin: 20px 0;
        padding-left: 20px;
    }
    ul.timeline > li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }
</style>

<div class="modal-content">
    <div class="modal-header pt-3">
        {{-- <h5 class="modal-title" id="zoomInModalLabel">Lead Information</h5> --}}
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body pt-0">
        <input type="hidden" value="{{ $lead->id ?? '' }}" id="lead_id">
        <input type="hidden" value="{{ route('lead.show') }}" id="lead_info_url">
        <div class="row">
            {{-- Lead Information --}}
            <div class="col-xxl-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h6 class="card-title mb-0">Lead Information ({{$lead->code}})</h6>
                    </div>
                    <div class="card-body lead-card">
                        <div class="row">
                            <div class="col-md-6 mb-1">
                                <strong>Source:</strong> {{ $lead->source->name ?? '' }}
                            </div>
                            <div class="col-md-6 mb-1">
                                <strong>Branch:</strong> {{ $lead->branch->name ?? '' }}
                                @if(auth()->user()->hasRole(['super_admin', 'social_media_manager', 'campus_head', 'admin_officer']))
                                    @if(!in_array($lead->lead_status_id, [4, 5, 6]))
                                        <a href="javascript:;" data-action="branch" data-route="{{ route('lead.info.change') }}" class="btn btn-sm change-info"><i class="mdi mdi-lead-pencil" style="font-size: 13px"></i></a>
                                    @endif
                                @endif
                            </div>
                            <div class="col-md-6 mb-1">
                                <strong>Employee:</strong> {{ $lead->employee->first_name ?? '' }} {{ $lead->employee->last_name ?? '' }}
                                @if(!in_array($lead->lead_status_id, [4, 5, 6]))
                                    @if(auth()->user()->hasRole(['super_admin', 'social_media_manager', 'campus_head', 'admin_officer']))
                                        <a href="javascript:;" data-action="employee" data-route="{{ route('lead.info.change') }}" class="btn btn-sm change-info"><i class="mdi mdi-lead-pencil" style="font-size: 13px"></i></a>
                                    @endif
                                @endif
                            </div>
                            <div class="col-md-6 mb-1">
                                <strong>Type:</strong> {{ $lead->type->name ?? '' }}
                                @if(auth()->user()->hasPermission('lead-info-change'))
                                <a href="javascript:;" data-action="type" data-route="{{ route('lead.info.change') }}" class="btn btn-sm change-info"><i class="mdi mdi-lead-pencil" style="font-size: 13px"></i></a>
                                @endif
                            </div>
                            <div class="col-md-6 mb-1">
                                <strong>Status:</strong> {{ $lead->status->name ?? '' }}
                                @if(!in_array($lead->lead_status_id, [8]) && auth()->user()->hasPermission('lead-info-change'))
                                    <a href="javascript:;" data-action="status" data-route="{{ route('lead.info.change') }}" class="btn btn-sm change-info"><i class="mdi mdi-lead-pencil" style="font-size: 13px"></i></a>
                                @endif
                            </div>
                            <div class="col-md-6 mb-1">
                                <strongF>Eligibility:</strongF>
                                @if($lead->is_eligible == 0)
                                    No
                                @elseif($lead->is_eligible == 1)
                                    Yes
                                @else
                                    Not Sure
                                @endif
                            </div>
                            <div class="col-md-6 mb-1">
                                <strong>BSS Referral:</strong>
                                @if($lead->is_referred == 1)
                                    Yes
                                @else
                                    No
                                @endif
                            </div>
                            {{-- @if($lead->is_referred == 1)
                                <div class="col-md-6 mb-1">
                                    <strong>Advisor Name:</strong> {{ $lead->advisor_name ?? '' }}
                                </div>
                                <div class="col-md-6 mb-1">
                                    <strong>Campus Name:</strong> {{ $lead->advisor_campus_name ?? '' }}
                                </div>
                            @endif --}}
                            <div class="col-md-6 mb-1">
                                <strong>Course:</strong> {{ $lead->course->name ?? '' }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Lead Follow ups --}}
            <div class="col-xxl-6 col-lg-6">
                <div class="card">
                    <div class="card-header align-items-center d-flex">
                        <h6 class="card-title mb-0 flex-grow-1">Lead Follow Ups</h6>
                            <div class="flex-shrink-0">
                                @if(auth()->user()->hasPermission('add-lead-follow-up'))
                                <a href="javascript:;" data-route="{{ route('lead.follow-up.create') }}" class="add-follow-up btn btn-success btn-label btn-sm">
                                    <i class="ri-add-fill label-icon align-middle fs-16 me-2"></i> Add New
                                </a>
                                @endif
                            </div>
                    </div>
                    <div class="card-body lead-card">
                        @forelse ($lead->followUps->reverse() as $followUp)
                            <div class="row border-follow-up mb-2">
                                <div class="col-md-6 mb-1">
                                    <strong>Source:</strong> {{ $followUp->source->name ?? '' }}
                                </div>
                                <div class="col-md-6 mb-1">
                                    <strong>Date & Time:</strong> {{ $followUp->follow_up_date ?? '' }}
                                </div>
                                @if($followUp->follow_up_by)
                                    <div class="col-md-6 mb-1">
                                        <strong>Follow Up By:</strong> {{ $followUp->follow_up_by ?? '' }}
                                    </div>
                                @endif
                                @if($followUp->follow_up_duration > 0)
                                    <div class="col-md-6 mb-1">
                                        <strong>Follow Up Duration:</strong> {{ $followUp->follow_up_duration ?? '' }} min
                                    </div>
                                @endif
                                @if($followUp->follow_up_remarks)
                                    <div class="col-md-12 mb-1">
                                        <strong>Follow Up Remarks:</strong><br>
                                        {!! $followUp->follow_up_remarks !!}
                                    </div>
                                @endif
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            {{-- Lead Trackings --}}
            <div class="col-xxl-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h6 class="card-title mb-0">Lead Trackings</h6>
                    </div>
                    <div class="card-body lead-card">
                        <div class="row">
                            <div class="col-3">
                                <h5>Type</h5>
                                <ul class="timeline">
                                    @forelse ($lead->typeTrack as $typeTrack)
                                        <li>
                                            <a href="javascript:void(0)">{{$typeTrack->leadType->name ?? ''}}</a> <br>
                                            <a href="javascript:void(0)" class="float-right">{{$typeTrack->created_at}}</a>
                                            @if($typeTrack->reason) <p>{{$typeTrack->reason}}</p> @endif
                                        </li>
                                    @empty
                                    @endforelse
                                </ul>
                            </div>
                            <div class="col-3">
                                <h5>Status</h5>
                                <ul class="timeline">
                                    @forelse ($lead->statusTrack as $statusTrack)
                                        <li>
                                            <a href="javascript:void(0)">{{$statusTrack->leadStatus->name ?? ''}}</a> <br>
                                            <a href="javascript:void(0)" class="float-right">{{$statusTrack->created_at}}</a>
                                            @if($statusTrack->reason)
                                                <p>
                                                    @if(is_numeric($statusTrack->reason))
                                                        {{ getReasonByID($statusTrack->reason) }}
                                                    @else
                                                        {{$statusTrack->reason}}
                                                    @endif
                                                </p>
                                            @endif
                                        </li>
                                    @empty
                                    @endforelse
                                </ul>
                            </div>
                            <div class="col-3">
                                <h5>Employee</h5>
                                <ul class="timeline">
                                    @forelse ($lead->employeeTrack as $employeeTrack)
                                        <li>
                                            <a href="javascript:void(0)">{{$employeeTrack->leadEmployee->first_name ?? ''}}</a> <br>
                                            <a href="javascript:void(0)" class="float-right">{{$employeeTrack->created_at}}</a>
                                            @if($employeeTrack->reason) <p>{{$employeeTrack->reason}}</p> @endif
                                        </li>
                                    @empty
                                    @endforelse
                                </ul>
                            </div>
                            <div class="col-3">
                                <h5>Branch</h5>
                                <ul class="timeline">
                                    @forelse ($lead->branchTrack as $branchTrack)
                                        <li>
                                            <a href="javascript:void(0)">{{$branchTrack->leadBranch->name ?? ''}}</a> <br>
                                            <a href="javascript:void(0)" class="float-right">{{$branchTrack->created_at}}</a>
                                            @if($branchTrack->reason) <p>{{$branchTrack->reason}}</p> @endif
                                        </li>
                                    @empty
                                    @endforelse
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Student Information --}}
            <div class="col-xxl-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h6 class="card-title mb-0">Student Information</h6>
                    </div>
                    <div class="card-body lead-card">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <strong>Name:</strong> {{ $lead->student->first_name ?? '' }}
                            </div>
                            <div class="col-md-6 mb-2">
                                <strong>Class:</strong> {{ $lead->student->classGrade->name ?? '' }}
                            </div>
                            <div class="col-md-6 mb-2">
                                <strong>Gender:</strong>
                                @if($lead->student->gender == 1)
                                    Male
                                @elseif($lead->student->gender == 2)
                                    Female
                                @else
                                    Other
                                @endif

                            </div>
                            <div class="col-md-6 mb-2">
                                <strong>Contact Number:</strong> {{ $lead->student->contactCode->code ?? ''}} {{ $lead->student->contact_number ?? '' }}
                            </div>
                            <div class="col-md-6 mb-2">
                                <strong>Email:</strong> {{ $lead->student->email ?? '' }}
                            </div>
                            @if(!empty($lead->student->city))
                                <div class="col-md-6 mb-2">
                                    <strong>City:</strong> {{ $lead->student->city ?? '' }}
                                </div>
                            @endif
                            @if(!empty($lead->previous_class))
                                <div class="col-md-6 mb-2">
                                    <strong>Previous Class:</strong> {{ $lead->previous_class ?? '' }}
                                </div>
                            @endif
                            @if(!empty($lead->previous_branch))
                                <div class="col-md-6 mb-2">
                                    <strong>Previous Branch:</strong> {{ $lead->previous_branch ?? '' }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
