<div id="leadRemindersModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zoomInModalLabel">Reminders</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-0">
                <div class="row">
                    <div class="col-xxl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body" style="max-height: 450px;overflow: auto;">
                                <form class="row g-3 needs-validation lead-reminder-form" data-delete-reminder-route="{{ route('lead.reminder.delete') }}" action="javascript:;" data-route="{{ route('lead.reminder.store') }}" method="POST" enctype="multipart/form-data" novalidate>
                                    <input type="hidden" id="lead_id_form" name="lead_id" value="">
                                    @csrf
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input type="text" class="message form-control @if($errors->has('message')) is-invalid @endif" id="message" name="message" placeholder="Message">
                                            <label for="message" class="form-label">Message</label>
                                            <div class="invalid-tooltip">
                                                @if($errors->has('message'))
                                                    {{ $errors->first('message') }}
                                                @else
                                                    Message is required!
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input type="text" class="date_time form-control @if($errors->has('date_time')) is-invalid @endif" id="date_time" name="date_time" placeholder="Date Time">
                                            <label for="date_time" class="form-label">Date Time</label>
                                            <div class="invalid-tooltip">
                                                @if($errors->has('date_time'))
                                                    {{ $errors->first('date_time') }}
                                                @else
                                                    Select the date time!
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-end">
                                        <button class="btn btn-primary" type="submit">Add</button>
                                    </div>
                                </form>
                                <hr>
                                <table class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Message</th>
                                        <th>Date Time</th>
                                        <th>Is Executed?</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="lead_reminders_listing">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
