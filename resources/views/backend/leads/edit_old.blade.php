@extends('layouts.master')

@push('header_scripts')
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.snow.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    @include('backend.components.flash_message')
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Update Lead</h4>
                </div>
                <div class="card-body form-steps">
                    <form action="{{ route('lead.update') }}" class="needs-validation" method="POST" enctype="multipart/form-data" novalidate>
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $lead->id }}" name="lead_id">
                        <input type="hidden" value="{{ $lead->student->id }}" name="student_id">
                        <div class="step-arrow-nav mb-4">
                            <ul class="nav nav-pills custom-nav nav-justified" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="student-info-tab" data-bs-toggle="pill" data-bs-target="#student-info" type="button" role="tab" aria-controls="student-info" aria-selected="true">Student&nbsp;Information</button>
                                </li>
{{--                                <li class="nav-item" role="presentation">--}}
{{--                                    <button class="nav-link" id="father-info-tab" data-bs-toggle="pill" data-bs-target="#father-info" type="button" role="tab" aria-controls="father-info" aria-selected="false">Father’s / Guardian’s Information</button>--}}
{{--                                </li>--}}
{{--                                <li class="nav-item" role="presentation">--}}
{{--                                    <button class="nav-link" id="mother-info-tab" data-bs-toggle="pill" data-bs-target="#mother-info" type="button" role="tab" aria-controls="mother-info" aria-selected="false">Mother Information</button>--}}
{{--                                </li>--}}
{{--                                <li class="nav-item" role="presentation">--}}
{{--                                    <button class="nav-link" id="previous-schooling-info-tab" data-bs-toggle="pill" data-bs-target="#previous-schooling-info" type="button" role="tab" aria-controls="previous-schooling-info" aria-selected="false">Previous schooling details</button>--}}
{{--                                </li>--}}
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="lead-info-tab" data-bs-toggle="pill" data-bs-target="#lead-info" type="button" role="tab" aria-controls="lead-info" aria-selected="false">Lead Information</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="lead-follow-ups-info-tab" data-bs-toggle="pill" data-bs-target="#lead-follow-ups-info" type="button" role="tab" aria-controls="lead-follow-ups-info" aria-selected="false">Lead Follow Ups</button>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane  show active" id="student-info" role="tabpanel" aria-labelledby="student-info-tab">
                                <div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <input type="text" class="form-control @if($errors->has('first_name')) is-invalid @endif" id="first_name" name="first_name" placeholder="First Name" value="{{ $lead->student->first_name }}"  required>
                                                <label for="first_name" class="form-label">Name</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('first_name'))
                                                        {{ $errors->first('first_name') }}
                                                    @else
                                                        Name is required!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" value="{{ $lead->student->middle_name }}">--}}
{{--                                                <label for="middle_name" class="form-label">Middle Name</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ $lead->student->last_name }}">--}}
{{--                                                <label for="last_name" class="form-label">Last Name</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('class_grade_id')) is-invalid @endif" id="class_grade_id" name="class_grade_id" aria-label="Class select" required>
                                                    <option value="">Select Class</option>
                                                    @forelse($classes as $class)
                                                        <option value="{{ $class->id }}" {{ ($class->id == $lead->student->class_grade_id) ? 'selected' : '' }}>{{ $class->name }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                                <label for="class_grade_id" class="form-label">Admission&nbsp;in&nbsp;Class</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('class_grade_id'))
                                                        {{ $errors->first('class_grade_id') }}
                                                    @else
                                                        Select the class!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('gender')) is-invalid @endif" id="gender" name="gender" aria-label="Gender select" required>

                                                    <option value="">Select gender</option>
                                                    <option value="1" {{ ($lead->student->gender == 1) ? 'selected' : '' }}>Male</option>
                                                    <option value="2" {{ ($lead->student->gender == 2) ? 'selected' : '' }}>Female</option>
                                                    <option value="3" {{ ($lead->student->gender == 3) ? 'selected' : '' }}>Other</option>

                                                </select>
                                                <label for="gender" class="form-label">Gender</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('gender'))
                                                        {{ $errors->first('gender') }}
                                                    @else
                                                        Select the gender!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="Date of Birth" value="{{ $lead->student->date_of_birth }}">--}}
{{--                                                <label for="date_of_birth" class="form-label">Date&nbsp;of&nbsp;Birth</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="place_of_birth" name="place_of_birth" placeholder="Place of Birth" value="{{ $lead->student->place_of_birth }}">--}}
{{--                                                <label for="place_of_birth" class="form-label">Place&nbsp;of&nbsp;Birth</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality" value="{{ $lead->student->nationality }}">--}}
{{--                                                <label for="nationality" class="form-label">Nationality</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="number" class="form-control" id="smart_card" name="smart_card" placeholder="Smart Card" value="{{ $lead->student->smart_card }}">--}}
{{--                                                <label for="smart_card" class="form-label">Smart&nbsp;Card</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="number" class="form-control" id="passport" name="passport" placeholder="Passport" value="{{ $lead->student->passport }}">--}}
{{--                                                <label for="passport" class="form-label">Passport</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="address" name="address" placeholder="Mailing Address" value="{{ $lead->student->address }}">--}}
{{--                                                <label for="address" class="form-label">Mailing&nbsp;Address</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('contact_code_id')) is-invalid @endif" id="contact_code_id" name="contact_code_id" aria-label="Contact code select" required>
                                                    <option value="">Please select a contact code</option>
                                                    @foreach ($contactCodes as $contactCode)
                                                        <option value="{{ $contactCode->id }}" {{ ($contactCode->id == $lead->student->contact_code_id) ? 'selected' : '' }}>{{ $contactCode->code }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="contact_code_id" class="form-label">Contact Code</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('contact_code_id'))
                                                        {{ $errors->first('contact_code_id') }}
                                                    @else
                                                        Contact code is required!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <input type="number" class="form-control @if($errors->has('contact_number')) is-invalid @endif" id="contact_number" name="contact_number" placeholder="Contact Number" value="{{ $lead->student->contact_number }}" required>
                                                <label for="contact_number" class="form-label">Contact&nbsp;Number</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('contact_number'))
                                                        {{ $errors->first('contact_number') }}
                                                    @else
                                                        Contact number is required!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <input type="email" class="form-control @if($errors->has('email')) is-invalid @endif" id="email" name="email" placeholder="Email" value="{{ $lead->student->email }}" required>
                                                <label for="email" class="form-label">Email</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('email'))
                                                        {{ $errors->first('email') }}
                                                    @else
                                                        Email is required!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{ $lead->student->city }}">
                                                <label for="city" class="form-label">City</label>
                                            </div>
                                        </div>

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="illness_allergies" name="illness_allergies" placeholder="Illness Allergies" value="{{ $lead->student->illness_allergies }}" required>--}}
{{--                                                <label for="illness_allergies" class="form-label">Illness&nbsp;Allergies</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="physical_impairment" name="physical_impairment" placeholder="Physical Impairment" value="{{ $lead->student->physical_impairment }}" required>--}}
{{--                                                <label for="physical_impairment" class="form-label">Physical&nbsp;Impairment</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-8 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="file" class="form-control" id="profile-img-file-input" name="profile_image" placeholder="Profile Image">--}}
{{--                                                <label for="profile_image" class="form-label">Profile&nbsp;Image</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <input type="hidden" class="form-control" id="profile-img-file-input">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 offset-8">
                                        <button style="float: right;" type="button" class="btn btn-success btn-label right ms-auto nexttab" data-nexttab="lead-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next</button>
                                    </div>
                                </div>
                            </div>

{{--                            <div class="tab-pane fade" id="father-info" role="tabpanel" aria-labelledby="father-info-tab">--}}
{{--                                <div>--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control @if($errors->has('f_name')) is-invalid @endif" id="f_name" name="f_name" placeholder="Name" value="{{ $studentFatherInfo->name ?? '' }}"  required>--}}
{{--                                                <label for="f_name" class="form-label">Name</label>--}}
{{--                                                <div class="invalid-tooltip">--}}
{{--                                                    @if($errors->has('f_name'))--}}
{{--                                                        {{ $errors->first('f_name') }}--}}
{{--                                                    @else--}}
{{--                                                        Name is required!--}}
{{--                                                    @endif--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="number" class="form-control" id="f_smart_card" name="f_smart_card" placeholder="Smart Card" value="{{ $studentFatherInfo->smart_card ?? '' }}">--}}
{{--                                                <label for="f_smart_card" class="form-label">Smart&nbsp;Card</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="number" class="form-control" id="f_employee_beams_id" name="f_employee_beams_id" placeholder="Beams ID" value="{{ $studentFatherInfo->employee_beams_id ?? '' }}">--}}
{{--                                                <label for="f_employee_beams_id" class="form-label">Employee ID # (Beaconhouse only)</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="f_occupation" name="f_occupation" placeholder="Occupation" value="{{ $studentFatherInfo->occupation ?? '' }}">--}}
{{--                                                <label for="f_occupation" class="form-label">Occupation</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="f_designation" name="f_designation" placeholder="Designation" value="{{ $studentFatherInfo->designation ?? '' }}">--}}
{{--                                                <label for="f_designation" class="form-label">Designation</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="f_organisation" name="f_organisation" placeholder="Organisation" value="{{ $studentFatherInfo->organisation ?? '' }}">--}}
{{--                                                <label for="f_organisation" class="form-label">Organisation</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="email" class="form-control" id="f_email" name="f_email" placeholder="Email" value="{{ $studentFatherInfo->email ?? '' }}">--}}
{{--                                                <label for="f_email" class="form-label">Email</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="f_address" name="f_address" placeholder="Address" value="{{ $studentFatherInfo->address ?? '' }}">--}}
{{--                                                <label for="f_address" class="form-label">Address</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="f_city" name="f_city" placeholder="City" value="{{ $studentFatherInfo->city ?? '' }}">--}}
{{--                                                <label for="f_city" class="form-label">City</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control @if($errors->has('f_contact_number')) is-invalid @endif" id="f_contact_number" name="f_contact_number" placeholder="Contact Number" value="{{ $studentFatherInfo->contact_number ?? '' }}"  required>--}}
{{--                                                <label for="f_contact_number" class="form-label">Contact&nbsp;Number</label>--}}
{{--                                                <div class="invalid-tooltip">--}}
{{--                                                    @if($errors->has('f_contact_number'))--}}
{{--                                                        {{ $errors->first('f_contact_number') }}--}}
{{--                                                    @else--}}
{{--                                                        Contact number is required!--}}
{{--                                                    @endif--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-4 offset-8">--}}
{{--                                        <div style="float: right">--}}
{{--                                            <button type="button" class="btn btn-light btn-label previestab" data-previous="student-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Previous</button>--}}
{{--                                            <button type="button" class="btn btn-success btn-label right ms-auto nexttab" data-nexttab="mother-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next</button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="tab-pane fade" id="mother-info" role="tabpanel">--}}
{{--                                <div>--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control @if($errors->has('m_name')) is-invalid @endif" id="m_name" name="m_name" placeholder="Name" value="{{ $studentMotherInfo->name ?? '' }}"  required>--}}
{{--                                                <label for="m_name" class="form-label">Name</label>--}}
{{--                                                <div class="invalid-tooltip">--}}
{{--                                                    @if($errors->has('m_name'))--}}
{{--                                                        {{ $errors->first('m_name') }}--}}
{{--                                                    @else--}}
{{--                                                        Name is required!--}}
{{--                                                    @endif--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="number" class="form-control" id="m_smart_card" name="m_smart_card" placeholder="Smart Card" value="{{ $studentMotherInfo->smart_card ?? '' }}">--}}
{{--                                                <label for="m_smart_card" class="form-label">Smart&nbsp;Card</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="number" class="form-control" id="m_employee_beams_id" name="m_employee_beams_id" placeholder="Beams ID" value="{{ $studentMotherInfo->employee_beams_id ?? '' }}">--}}
{{--                                                <label for="m_employee_beams_id" class="form-label">Employee ID # (Beaconhouse only)</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="m_occupation" name="m_occupation" placeholder="Occupation" value="{{ $studentMotherInfo->occupation ?? '' }}">--}}
{{--                                                <label for="m_occupation" class="form-label">Occupation</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="m_designation" name="m_designation" placeholder="Designation" value="{{ $studentMotherInfo->designation ?? '' }}">--}}
{{--                                                <label for="m_designation" class="form-label">Designation</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="m_organisation" name="m_organisation" placeholder="Organisation" value="{{ $studentMotherInfo->organisation ?? '' }}">--}}
{{--                                                <label for="m_organisation" class="form-label">Organisation</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="email" class="form-control" id="m_email" name="m_email" placeholder="Email" value="{{ $studentMotherInfo->email ?? '' }}">--}}
{{--                                                <label for="m_email" class="form-label">Email</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="m_address" name="m_address" placeholder="Address" value="{{ $studentMotherInfo->address ?? '' }}">--}}
{{--                                                <label for="m_address" class="form-label">Address</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="m_city" name="m_city" placeholder="City" value="{{ $studentMotherInfo->city ?? '' }}">--}}
{{--                                                <label for="m_city" class="form-label">City</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control @if($errors->has('m_contact_number')) is-invalid @endif" id="m_contact_number" name="m_contact_number" placeholder="Contact Number" value="{{ $studentMotherInfo->contact_number ?? '' }}"  required>--}}
{{--                                                <label for="m_contact_number" class="form-label">Contact&nbsp;Number</label>--}}
{{--                                                <div class="invalid-tooltip">--}}
{{--                                                    @if($errors->has('m_contact_number'))--}}
{{--                                                        {{ $errors->first('m_contact_number') }}--}}
{{--                                                    @else--}}
{{--                                                        Contact number is required!--}}
{{--                                                    @endif--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <h3>Emergency Contact Person</h3><br/><br/>--}}
{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control @if($errors->has('e_name')) is-invalid @endif" id="e_name" name="e_name" placeholder="Name" value="{{ $lead->student->emergencyContact->name ?? '' }}"  required>--}}
{{--                                                <label for="e_name" class="form-label">Name</label>--}}
{{--                                                <div class="invalid-tooltip">--}}
{{--                                                    @if($errors->has('e_name'))--}}
{{--                                                        {{ $errors->first('e_name') }}--}}
{{--                                                    @else--}}
{{--                                                        Name is required!--}}
{{--                                                    @endif--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control @if($errors->has('e_contact_number')) is-invalid @endif" id="e_contact_number" name="e_contact_number" placeholder="Emergency Contact Number" value="{{$lead->student->emergencyContact->contact_number ?? '' }}"  required>--}}
{{--                                                <label for="e_contact_number" class="form-label">Contact&nbsp;Number</label>--}}
{{--                                                <div class="invalid-tooltip">--}}
{{--                                                    @if($errors->has('e_contact_number'))--}}
{{--                                                        {{ $errors->first('e_contact_number') }}--}}
{{--                                                    @else--}}
{{--                                                        Contact number is required!--}}
{{--                                                    @endif--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-md-4 col-sm-12">--}}
{{--                                            <div class="form-label-group in-border">--}}
{{--                                                <input type="text" class="form-control" id="relation" name="relation" placeholder="Relation" value="{{ $lead->student->emergencyContact->relation ?? '' }}">--}}
{{--                                                <label for="relation" class="form-label">Relation</label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}

{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-4 offset-8">--}}
{{--                                        <div style="float: right">--}}
{{--                                            <button type="button" class="btn btn-light btn-label previestab" data-previous="father-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Previous</button>--}}
{{--                                            <button type="button" class="btn btn-success btn-label right ms-auto nexttab" data-nexttab="previous-schooling-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next</button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="tab-pane fade" id="previous-schooling-info" role="tabpanel">--}}
{{--                                <div>--}}
{{--                                    <p>In progress ...</p>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-4 offset-8">--}}
{{--                                        <div style="float: right">--}}
{{--                                            <button type="button" class="btn btn-light btn-label previestab" data-previous="mother-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Previous</button>--}}
{{--                                            <button type="button" class="btn btn-success btn-label right ms-auto nexttab" data-nexttab="lead-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next</button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="tab-pane fade" id="lead-info" role="tabpanel">
                                <div>
                                    <div class="row">

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('source_id')) is-invalid @endif" id="source_id" name="source_id" aria-label="Source select" required>
                                                    <option value="">Select Source</option>
                                                    @foreach ($sources as $source)
                                                        <option value="{{ $source->id }}" {{ ($source->id == $lead->source_id) ? 'selected' : '' }}>{{ $source->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="source_id" class="form-label">Source</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('source_id'))
                                                        {{ $errors->first('source_id') }}
                                                    @else
                                                        Select the source!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('lead_type_id')) is-invalid @endif" id="lead_type_id" name="lead_type_id" aria-label="Lead Type select" required>
                                                    <option value="">Select Type</option>
                                                    @foreach ($leadTypes as $leadType)
                                                        <option value="{{ $leadType->id }}" {{ ($leadType->id == $lead->lead_type_id) ? 'selected' : '' }}>{{ $leadType->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="lead_type_id" class="form-label">Type</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('lead_type_id'))
                                                        {{ $errors->first('lead_type_id') }}
                                                    @else
                                                        Select the type!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('lead_status_id')) is-invalid @endif" id="lead_status_id" name="lead_status_id" aria-label="Lead Status select" required>
                                                    <option value="">Select Status</option>
                                                    @foreach ($leadStatuses as $leadStatus)
                                                        <option value="{{ $leadStatus->id }}" {{ ($leadStatus->id == $lead->lead_status_id) ? 'selected' : '' }}>{{ $leadStatus->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="lead_status_id" class="form-label">Status</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('lead_status_id'))
                                                        {{ $errors->first('lead_status_id') }}
                                                    @else
                                                        Select the status!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select data-route-courses="{{ route('branch.load-courses') }}" data-route-employees="{{ route('branch.load-employees') }}" class="form-select @if($errors->has('branch_id')) is-invalid @endif" id="branch_id" name="branch_id" aria-label="Branch select" required>
                                                    <option value="">Select Branch</option>
                                                    @foreach ($branches as $branch)
                                                        <option value="{{ $branch->id }}" {{ ($branch->id == $lead->branch_id) ? 'selected' : '' }}>{{ $branch->name }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="branch_id" class="form-label">Branch</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('branch_id'))
                                                        {{ $errors->first('branch_id') }}
                                                    @else
                                                        Select the branch!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="courses form-select @if($errors->has('course_id')) is-invalid @endif" id="course_id" name="course_id" aria-label="Course select" required>
                                                    <option value="">Select Course</option>
                                                    @forelse($lead->branch->courses as $course)
                                                        <option value="{{ $course->id }}" {{ ($course->id == $lead->course_id) ? 'selected' : '' }}>{{ $course->name }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                                <label for="course_id" class="form-label">Course</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('course_id'))
                                                        {{ $errors->first('course_id') }}
                                                    @else
                                                        Select the course!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="employees form-select @if($errors->has('employee_id')) is-invalid @endif" id="employee_id" name="employee_id" aria-label="Employee select" required>
                                                    <option value="">Select Employee</option>
                                                    @forelse($lead->branch->employees as $employee)
                                                        <option value="{{ $employee->id }}" {{ ($employee->id == $lead->employee_id) ? 'selected' : '' }}>{{ $employee->first_name . ' ' . $employee->last_name }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                                <label for="employee_id" class="form-label">Employee</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('employee_id'))
                                                        {{ $errors->first('employee_id') }}
                                                    @else
                                                        Select the employee!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="form-select @if($errors->has('is_eligible')) is-invalid @endif" id="is_eligible" name="is_eligible" aria-label="Eligibility select" required>
                                                    <option value="">Select Eligibility</option>
                                                    <option value="0" {{ ($lead->is_eligible == 0) ? 'selected' : '' }}>No</option>
                                                    <option value="1" {{ ($lead->is_eligible == 1) ? 'selected' : '' }}>Yes</option>
                                                    <option value="2" {{ ($lead->is_eligible == 2) ? 'selected' : '' }}>Not Sure</option>

                                                </select>
                                                <label for="is_eligible" class="form-label">Eligibility</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('is_eligible'))
                                                        {{ $errors->first('is_eligible') }}
                                                    @else
                                                        Select the eligibility!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <select class="is_referred form-select @if($errors->has('is_referred')) is-invalid @endif" id="is_referred" name="is_referred" aria-label="Referred select" required>
                                                    <option value="">Select Option</option>
                                                    <option value="0" {{ ($lead->is_referred == 0) ? 'selected' : '' }}>No</option>
                                                    <option value="1" {{ ($lead->is_referred == 1) ? 'selected' : '' }}>Yes</option>

                                                </select>
                                                <label for="is_referred" class="form-label">BSS Referred</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('is_referred'))
                                                        {{ $errors->first('is_referred') }}
                                                    @else
                                                        Select the referred!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div @if($lead->is_referred == 0) style="display: none" @endif class="col-md-4 col-sm-12 referral-input">
                                            <div class="form-label-group in-border">
                                                <input type="text" class="form-control" id="advisor_name" name="advisor_name" placeholder="Advisor Name" value="{{ $lead->advisor_name ?? '' }}">
                                                <label for="advisor_name" class="form-label">Advisor&nbsp;Name</label>
                                            </div>
                                        </div>

                                        <div @if($lead->is_referred == 0) style="display: none" @endif class="col-md-4 col-sm-12 referral-input">
                                            <div class="form-label-group in-border">
                                                <input type="text" class="form-control" id="advisor_campus_name" name="advisor_campus_name" placeholder="Advisor Campus Name" value="{{ $lead->advisor_campus_name ?? '' }}">
                                                <label for="advisor_campus_name" class="form-label">Advisor&nbsp;Campus&nbsp;Name</label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 offset-8">
                                        <div style="float: right">
                                            <button type="button" class="btn btn-light btn-label previestab" data-previous="student-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Previous</button>
                                            <button type="button" class="btn btn-success btn-label right ms-auto nexttab" data-nexttab="lead-follow-ups-info-tab"><i class="ri-arrow-right-line label-icon align-middle fs-16 ms-2"></i>Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="lead-follow-ups-info" role="tabpanel">
                                <div>
                                    <div class="row">

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <input disabled type="text" class="form-control" id="follow_up_date" placeholder="Follow Up Date" value="{{ $followUp->follow_up_date ?? '' }}">
                                                <label for="follow_up_date" class="form-label">Follow&nbsp;Up&nbsp;Date</label>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <input type="text" class="form-control @if($errors->has('follow_up_by')) is-invalid @endif" id="follow_up_by" name="follow_up_by" placeholder="Follow up by" value="{{ $followUp->follow_up_by ?? '' }}">
                                                <label for="follow_up_by" class="form-label">Follow&nbsp;Up&nbsp;By</label>
                                                <div class="invalid-tooltip">
                                                    @if($errors->has('follow_up_by'))
                                                        {{ $errors->first('follow_up_by') }}
                                                    @else
                                                        Follow up by is required!
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <input type="number" min="0" class="form-control" id="follow_up_duration" name="follow_up_duration" placeholder="Follow up Duration" value="{{ $followUp->follow_up_duration ?? '' }}">
                                                <label for="follow_up_duration" class="form-label">Follow&nbsp;Up&nbsp;Duration&nbsp;in&nbsp;Minutes</label>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-label-group in-border">
                                                <input type="hidden" class="form-control" id="follow_up_remarks" value="{{ $followUp->follow_up_remarks ?? '' }}" name="follow_up_remarks" placeholder="Follow Up Remarks">
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <div id="snow-editor" style="height: 300px;">{!! $followUp->follow_up_remarks ?? '' !!}</div>
                                        </div>

                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-4 offset-8">
                                        <div style="float: right">
                                            <button type="button" class="btn btn-light btn-label previestab" data-previous="lead-info-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i> Previous</button>
                                            <button type="submit" class="btn btn-success btn-label right ms-auto"><i class="ri-tick label-icon align-middle fs-16 ms-2"></i>Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>

            </div>

        </div>

    </div>
@endsection

@push('footer_scripts')
    <script src="{{ asset('theme/dist/default/assets/libs/quill/quill.min.js') }}"></script>
    <!-- form wizard init -->
    <script src="{{asset('theme/dist/default/assets/js/pages/form-wizard.init.js')}}"></script>
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script src="{{ asset('backend/modules/leads.js') }}"></script>
@endpush
