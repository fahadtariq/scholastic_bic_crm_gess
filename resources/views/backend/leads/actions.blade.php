@permission('edit-lead')
<a href="javascript:;" data-id="{{ $row->id }}" data-route="{{ route('lead.show') }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light show-lead">
<i class="mdi mdi-information-variant"></i>
</a>
@endpermission
@permission('edit-lead')
<a href="{{ route('lead.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
<i class="mdi mdi-lead-pencil"></i>
</a>
@endpermission

@permission('delete-lead')
<a href="{{ route('lead.delete') }}" data-rowid="{{ $row->id }}" data-table="leads-data-table"
class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
<i class="ri-delete-bin-5-line"></i>
</a>
@endpermission

@if (!auth()->user()->hasPermission('edit-lead') &&
    !auth()->user()->hasPermission('delete-lead'))
    <span>N/A</span>
@endif
