@extends('layouts.master')

@push('header_scripts')
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.snow.css') }}" rel="stylesheet" type="text/css" />

    <style>
        .border-follow-up{
            padding: 3px;
            border: solid 1px #d3caca;
        }

        .lead-card{
            min-height: 200px;
            overflow: auto;
            max-height: 200px;
        }

        .lead-card-error-msg{
            overflow: auto;
            max-height: 200px;
        }

        ul.timeline {
            list-style-type: none;
            position: relative;
        }
        ul.timeline:before {
            content: ' ';
            background: #d4d9df;
            display: inline-block;
            position: absolute;
            left: 29px;
            width: 2px;
            height: 100%;
            z-index: 400;
        }
        ul.timeline > li {
            margin: 20px 0;
            padding-left: 20px;
        }
        ul.timeline > li:before {
            content: ' ';
            background: white;
            display: inline-block;
            position: absolute;
            border-radius: 50%;
            border: 3px solid #22c0e8;
            left: 20px;
            width: 20px;
            height: 20px;
            z-index: 400;
        }

        /*Blinking Styles Starts*/
        @-webkit-keyframes invalid {
            from { background-color: rgb(255,153,102); }
            to { background-color: inherit; }
        }
        @-moz-keyframes invalid {
            from { background-color: rgb(255,153,102); }
            to { background-color: inherit; }
        }
        @-o-keyframes invalid {
            from { background-color: rgb(255,153,102); }
            to { background-color: inherit; }
        }
        @keyframes invalid {
            from { background-color: rgb(255,153,102); }
            to { background-color: inherit; }
        }
        .blink-invalid {
            -webkit-animation: invalid 2s infinite; / Safari 4+ /
        -moz-animation:    invalid 2s infinite; / Fx 5+ /
        -o-animation:      invalid 2s infinite; / Opera 12+ /
        animation:         invalid 2s infinite; / IE 10+ /
        }
        /*Blinking Styles Ends*/
    </style>
@endpush

@section('content')
    @include('backend.components.flash_message')

    <div class="row">
        <div class="col-lg-12">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Leads <span id="dbCounter">(Count: {{$leadCounts}})</span></h4>
                 @permission('add-lead')
                    <div class="flex-shrink-0">
                        @if(auth()->user()->hasRole('super_admin'))
                            <a href="{{ route('lead.store.beams') }}" class="btn btn-success btn-label btn-sm">
                                <i class=" ri-chat-upload-line label-icon align-middle fs-16 me-2"></i> Store in BEAMS
                            </a>
                            <a href="{{ route('lead.get.status.beams') }}" class="btn btn-success btn-label btn-sm">
                                <i class=" ri-chat-download-line label-icon align-middle fs-16 me-2"></i> Get Status from BEAMS
                            </a>
                            <a href="{{ route('lead.get.visitors.leads.beams') }}" class="btn btn-success btn-label btn-sm">
                                <i class=" ri-chat-download-line label-icon align-middle fs-16 me-2"></i> Get Visitors Data
                            </a>
                        @endif
                        @if(auth()->user()->hasRole('campus_head|admin_officer'))
                            <a href="javascript:;" class="btn btn-warning btn-label btn-sm assign-by-range-btn">
                                <i class="ri-arrow-left-right-line label-icon align-middle fs-16 me-2"></i> Assign by Range
                            </a>
                        @endif
                        @if(auth()->user()->hasRole('campus_head|super_admin|social_media_manager|admin_officer'))
                            <a href="javascript:;" class="btn btn-success btn-label btn-sm import-leads-btn">
                                <i class="ri-upload-cloud-line label-icon align-middle fs-16 me-2"></i> Import
                            </a>                        
                        @endif
                        <a href="{{ route('lead.create') }}" class="btn btn-success btn-label btn-sm">
                            <i class="ri-add-fill label-icon align-middle fs-16 me-2"></i> Add New
                        </a>
                        @if(auth()->user()->hasRole('campus_head|super_admin|social_media_manager|admin_officer'))
                            <a href="{{route('lead.export.email')}}" class="btn btn-primary btn-label btn-sm export-leads-btn">
                                <i class="ri-upload-cloud-line label-icon align-middle fs-16 me-2"></i> Export Emails
                            </a>                        
                        @endif
                        @if(auth()->user()->hasRole('campus_head|super_admin|social_media_manager|admin_officer'))
                            <a href="javascript:;" class="btn btn-primary btn-label btn-sm export-excel-lead-btn">
                                <i class="ri-upload-cloud-line label-icon align-middle fs-16 me-2"></i> Export Excel
                            </a>                        
                        @endif
                        @if(auth()->user()->hasRole('campus_head|super_admin|social_media_manager|admin_officer'))
                        <form id="uploadForm" action="{{ route('leads.fall.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" id="fileInput" style="display: none;">
                            <button type="button" class="btn btn-success btn-label btn-sm import-fall-leads-btn mt-1">
                                <i class="ri-upload-cloud-line label-icon align-middle fs-16 me-2"></i> Import Previous Leads
                            </button>
                        </form>   
                        @endif
                    </div>
                 @endpermission
            </div>
            <div class="card">
                <div class="card-body">
                    <form action="javascript:;" id="filter-form" >
                        <div class="row">
                            {!! pageLengthHTML() !!}
                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input id="lead_id_search" type="text" placeholder="Search By ID.." class="form-control" name="lead_id_search">
                                    <label for="lead_id_search" class="form-label">Lead ID #</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input id="beams_system_id_search" type="text" placeholder="Search By Beams System ID.." class="form-control" name="beams_system_id_search">
                                    <label for="beams_system_id_search" class="form-label">Beams System ID #</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input id="mySearch" type="text" placeholder="Search.." class="form-control" name="mySearch">
                                    <label for="mySearch" class="form-label">Student Name</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input id="student_number" type="text" placeholder="Student Number" class="form-control" name="student_number">
                                    <label for="student_number" class="form-label">Student Number</label>
                                </div>
                            </div>

                            @if(auth()->user()->hasRole('super_admin|social_media_manager'))
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-label-group in-border">
                                        <select class="filter form-select branch_filter" data-session-route="{{ route('branch.sessions') }}" data-filter="admin-lead" name="branch_id" placeholder="Branch">
                                            <option value="">Please select</option>
                                            @forelse($branches as $branch)
                                                <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        <label for="branch_id" class="form-label">Branch</label>
                                    </div>
                                </div>
                            @endif

                            @if(auth()->user()->hasRole('campus_head|admission_advisor'))
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-label-group in-border">
                                        <select class="filter form-select session_select" name="session_id" placeholder="Session">
                                            <option value="">Select Session</option>
                                            @foreach($sessions as $session)
                                                <option value="{{ $session->id }}">{{ $session->name }}</option>
                                            @endforeach
                                        </select>
                                        <label for="session_id" class="form-label">Session</label>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-label-group in-border">
                                        <select class="filter form-select session_select" name="session_id" placeholder="Session">
                                            <option value="">Select Session</option>
                                        </select>
                                        <label for="session_id" class="form-label">Session</label>
                                    </div>
                                </div>
                            @endif

                            @if(auth()->user()->hasRole('super_admin|social_media_manager|campus_head|admission_advisor'))
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-label-group in-border">
                                        <select class="courses filter form-select" name="course_id[]" placeholder="Course" multiple>
                                            @forelse($courses as $course)
                                                <option value="{{ $course->id }}">{{ $course->name }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        <label for="branch_id" class="form-label">Course</label>
                                    </div>
                                </div>
                            @endif

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="source_id" name="source_id" placeholder="Source">
                                        <option value="">Please select</option>
                                        @forelse($sources as $source)
                                            <option value="{{ $source->id }}">{{ $source->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="source_id" class="form-label">Source</label>
                                </div>
                            </div>
                            
                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="tag_id" name="tag_id" placeholder="Tag">
                                        <option value="">Please select</option>
                                        @forelse($tags as $tag)
                                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="tag_id" class="form-label">Tag</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="is_referred" name="is_referred" placeholder="Referral">
                                        <option value="">Please select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <label for="is_referred" class="form-label">BSS Referral</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="is_important" name="is_important" placeholder="Referral">
                                        <option value="">Please select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <label for="is_important" class="form-label">Important</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="is_sync" name="is_sync" placeholder="Synced">
                                        <option value="">Please select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <label for="is_sync" class="form-label">Synced to Beams</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="is_confirmed" name="is_confirmed" placeholder="Confirmed">
                                        <option value="">Please select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <label for="is_confirmed" class="form-label">Confirmed</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="lead_type_id" name="lead_type_id" placeholder="Type">
                                        <option value="">Please select</option>
                                        @forelse($leadTypes as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="lead_type_id" class="form-label">Type</label>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="lead_status_id" name="lead_status_id" placeholder="Status">
                                        <option value="">Please select</option>
                                        @forelse($leadStatuses as $status)
                                            <option value="{{ $status->id }}">{{ $status->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="lead_status_id" class="form-label">Status</label>
                                </div>
                            </div>

                            @if(!auth()->user()->hasRole('admission_advisor'))
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-label-group in-border">
                                        <select class="filter form-select" id="employee_id" name="employee_id" placeholder="Employee">
                                            <option value="">Please select</option>
                                            @forelse($employees as $employee)
                                                <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }} -- {{ implode(',', $employee->user->roles()->pluck('display_name')->toArray()) }}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                        <label for="employee_id" class="form-label">Employee Assigned</label>
                                    </div>
                                </div>
                            @endif

                            <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="follow_up_employee_id" name="follow_up_employee_id" placeholder="Employee">
                                        <option value="">Please select</option>
                                        @forelse($employees as $employee)
                                            <option value="{{ $employee->user->id }}">{{ $employee->first_name }} {{ $employee->last_name }} -- {{ implode(',', $employee->user->roles()->pluck('display_name')->toArray()) }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                    <label for="follow_up_employee_id" class="form-label">Followed by Employee</label>
                                </div>
                            </div>

                            {{-- <div class="col-md-2 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select class="filter form-select" id="category" name="category" placeholder="Category">
                                        <option value="">Please select</option>
                                        <option value="continuing">Continuing</option>
                                        <option value="new">New</option>
                                    </select>
                                    <label for="category" class="form-label">Category</label>
                                </div>
                            </div> --}}
                            <div class="col-md-3 col-sm-12 follow_up_date_wrapper" style="display: none">
                                <div class="form-label-group in-border">
                                    <input class="form-control filter" name="follow_up_by_employee_date" id="follow_up_by_employee_date">
                                    <label for="follow_up_by_employee_date" class="form-label">Followed By Employee Date</label>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12">
                                <div class="form-label-group in-border">
                                    <input class="form-control filter" name="date_range" id="date_range">
                                    <label for="date_range" class="form-label">Date</label>
                                </div>
                            </div>

                            @if(auth()->user()->hasRole('campus_head|admin_officer'))
                                <div class="col-md-4 col-sm-12 employees_assigned_wrapper" style="display: none">
                                    <div class="form-label-group in-border">
                                        <select class="form-select loaded-employees" id="assigned_employee_id" name="assigned_employee_id" placeholder="Employee">

                                        </select>
                                        <label for="assigned_employee_id" class="form-label">Lead Assign to Employee</label>
                                    </div>
                                </div>
                            @endif

                            <div class="col-md-2 col-sm-12">
                                <button onclick="filterDataTable('{{route("leads.index")}}')" type="button" class="filter-lead-table-btn btn btn-primary btn-label rounded-pill"><i class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i> Filter</button>
                            </div>
                        </div>
                    </form>
                    <div id="custom-datatable-wrapper">
                        @include('backend.leads.partial')
                    </div>

                    <!--Pagination-->
                    <div id="custom-pagination-wrapper">
                        <div class="row mt-3">
                            {{ $leads->links('layouts.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="leadInfoModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" id="lead-modal-content">
        </div>
    </div>
    <div id="leadMessageModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" id="lead-msg-modal-content">
        </div>
    </div>
    <div class="change-info-modal-area"></div>
    <div class="add-follow-up-modal-area"></div>

    <input id="ajaxRoute" value="{{ route('leads.index') }}" hidden />

    @include('backend.leads.partials.import')
    @include('backend.leads.partials.assign-by-range-modal')
    @include('backend.leads.partials.reminders-modal')
@endsection

@push('footer_scripts')
    <script src="{{ asset('theme/dist/default/assets/libs/quill/quill.min.js') }}"></script>
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script src="{{ asset('backend/modules/leads.js') }}"></script>
    @if (count($errors) > 0)
        <script>
            $(document).ready(function() {
                $('#importLeadsModal').modal('show');
            });
        </script>           
    @endif
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const button = document.querySelector(".import-fall-leads-btn");
            const fileInput = document.querySelector("#fileInput");
            const form = document.querySelector("#uploadForm");

            button.addEventListener("click", function(event) {
                event.preventDefault();
                fileInput.click();
            });

            fileInput.addEventListener("change", function() {
                form.submit();
            });
        });
    </script> 
    <script>
        $(document).ready(function() {
            $('.export-excel-lead-btn').click(function(){
                var data = $('#filter-form').serialize();
                window.location.href = "export/leads?type=export&"+data;
            });
        });
    </script>
@endpush
