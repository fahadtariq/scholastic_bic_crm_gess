<table id="leads-data-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
    <thead>
        <tr>
            @if(auth()->user()->hasRole('campus_head|admin_officer'))
                <th>
                    <div class="form-check mb-2">
                        <input class="form-check-input lead-check-parent" data-assign-lead-route="{{ route('lead.multiple-assign') }}" data-route="{{ route('load.employees') }}" type="checkbox" id="formCheck1">
                    </div>
                </th>
            @else
                <th></th>
            @endif
            <th>Action</th>
            <th>#</th>
            <th>Student Name</th>
            <th>Student Number</th>
            <th>Branch Name</th>
            <th>Code</th>
            <th>Source</th>
            <th>Tag</th>
            <th>Type</th>
            <th>Course</th>
            {{-- <th>Category</th> --}}
            <th>Status</th>
            <th>Visitor ID</th>
            <th>Registration ID</th>
            <th>Student ID</th>
            <th>Assigned To</th>
                <th>Session</th>
                <th>Created At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($leads as $lead)
            <tr class="{{ ($lead->is_important) ? 'blink-invalid' : '' }}">
                <td>
                    @if(!in_array($lead->lead_status_id, [4, 5, 6]) && auth()->user()->hasRole('campus_head|admin_officer'))
                        <div class="form-check mb-2"><input class="form-check-input lead-check-child" type="checkbox" data-lead-id="{{ $lead->id  }}"></div>
                    @endif
                </td>
                <td>
                    @permission('show-leads')
                    <a href="javascript:;" data-id="{{ $lead->id }}" data-route="{{ route('lead.message') }}"
                        class="btn btn-sm btn-icon waves-effect waves-light show-lead-msg-modal" style="background-color: white">
                        {{-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="green" d="M12.04 2c-5.46 0-9.91 4.45-9.91 9.91c0 1.75.46 3.45 1.32 4.95L2.05 22l5.25-1.38c1.45.79 3.08 1.21 4.74 1.21c5.46 0 9.91-4.45 9.91-9.91c0-2.65-1.03-5.14-2.9-7.01A9.816 9.816 0 0 0 12.04 2m.01 1.67c2.2 0 4.26.86 5.82 2.42a8.225 8.225 0 0 1 2.41 5.83c0 4.54-3.7 8.23-8.24 8.23c-1.48 0-2.93-.39-4.19-1.15l-.3-.17l-3.12.82l.83-3.04l-.2-.32a8.188 8.188 0 0 1-1.26-4.38c.01-4.54 3.7-8.24 8.25-8.24M8.53 7.33c-.16 0-.43.06-.66.31c-.22.25-.87.86-.87 2.07c0 1.22.89 2.39 1 2.56c.14.17 1.76 2.67 4.25 3.73c.59.27 1.05.42 1.41.53c.59.19 1.13.16 1.56.1c.48-.07 1.46-.6 1.67-1.18c.21-.58.21-1.07.15-1.18c-.07-.1-.23-.16-.48-.27c-.25-.14-1.47-.74-1.69-.82c-.23-.08-.37-.12-.56.12c-.16.25-.64.81-.78.97c-.15.17-.29.19-.53.07c-.26-.13-1.06-.39-2-1.23c-.74-.66-1.23-1.47-1.38-1.72c-.12-.24-.01-.39.11-.5c.11-.11.27-.29.37-.44c.13-.14.17-.25.25-.41c.08-.17.04-.31-.02-.43c-.06-.11-.56-1.35-.77-1.84c-.2-.48-.4-.42-.56-.43c-.14 0-.3-.01-.47-.01Z"/></svg> --}}
                        <img src="{{ asset('assets/images/whatsapp.png') }}" height="25px" width="25px"/>
                    </a>
                    <a href="javascript:;" data-id="{{ $lead->id }}" data-route="{{ route('lead.show') }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light show-lead">
                        <i class="mdi mdi-information-variant"></i>
                    </a>
                    <a href="javascript:;" data-type="Process" data-id="{{ $lead->id }}" data-route="{{ route('lead.showemailbody') }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light show-lead-email">
                        P
                    </a>
                    <a href="javascript:;" data-type="W" data-id="{{ $lead->id }}" data-route="{{ route('lead.showemailbody') }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light show-lead-email">
                        W
                    </a>
                    @endpermission
                    @permission('edit-lead')
                    <a href="{{ route('lead.edit', $lead->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
                        <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @endpermission

                    @permission('delete-lead')
                    <a href="{{ route('lead.delete') }}" data-rowid="{{ $lead->id }}"
                       class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-lead">
                        <i class="ri-delete-bin-5-line"></i>
                    </a>
                    @endpermission

                    @permission('edit-lead')
                    <a href="javascript:;" data-delete-reminder-route="{{ route('lead.reminder.delete') }}" data-route="{{ route('lead.reminders') }}" data-lead-id="{{ $lead->id }}" data-toggle="tooltip" title="Remiders" type="button" class="btn btn-sm btn-warning btn-icon waves-effect waves-light lead_reminders-btn">
                        <i class="ri-time-line"></i>
                    </a>
                    @endpermission
                    {{-- <a href="https://wa.me/{{ isset($lead->student->contact_number) && !empty($lead->student->contact_number) ? "92".$lead->student->contact_number : '923106301418' }}" target="_blank" class="btn btn-sm btn-icon waves-effect waves-light">
                        <img src="{{ asset('assets/images/whatsapp.png') }}" height="25px" width="25px"/>
                    </a>                                       --}}
                    @if (!auth()->user()->hasPermission('edit-lead') &&
                        !auth()->user()->hasPermission('delete-lead'))
                        <span>N/A</span>
                    @endif
                </td>
                <td>{{ $lead->id }}</td>
                <td>{{ $lead->student->first_name ?? '' }}</td>
                <td>{{ $lead->student->contact_number ?? '' }}</td>
                <td>{{ $lead->branch->name ?? '' }}</td>
                <td>{{ $lead->code ?? '' }}</td>
                <td>{{ $lead->source->name ?? '' }}</td>
                <td>{{ $lead->tag->name ?? '' }}</td>
                <td>{{ $lead->type->name ?? '' }}</td>
                <td>{{ $lead->course->name ?? '' }}</td>
                {{-- <td>{{ Str::ucfirst($lead->category) ?? '' }}</td> --}}
                <td>{{ $lead->status->name ?? '' }}</td>
                <td>{{ $lead->beams_id ?? '' }}</td>
                <td>{{ $lead->beams_registration_id ?? '' }}</td>
                <td>{{ $lead->beams_system_id ?? '' }}</td>
                <td>{{ isset($lead->employee->first_name) ? $lead->employee->first_name . ' ' . $lead->employee->last_name : '' }}</td>
                <td>{{ $lead->session->name ?? '' }}</td>
                <td>{{ $lead->created_at  }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
