<div class="msg_history">
    @foreach($messages as $message)
    <div class="@if($message->status == 'Sent') outgoing_msg @elseif($message->status == 'Received') incoming_msg @endif">
        @if($message->status == 'Received')<div class="incoming_msg_img"> <img src="{{asset('assets/images/user-profile.png')}}" alt="Student"> </div>@endif
        <div class="@if($message->status == 'Sent') sent_msg @elseif($message->status == 'Received') received_msg @endif">
            <div class="@if($message->status == 'Received') received_withd_msg @endif">
                <p>{{$message->text}}</p>
                <span class="time_date"> {{$message->created_at->format('h:i A \| F j')}}</span></div>
        </div>
    </div>
    <!--<div class="outgoing_msg">
        <div class="sent_msg">
            <p>Test which is a new approach to have all
                solutions</p>
            <span class="time_date"> 11:01 AM    |    June 9</span> </div>
    </div>-->
    @endforeach
</div>
<div class="type_msg">
    <div class="input_msg_write">
        <form class="whatsapp-full-chat-form" action="javascript:void(0);" data-route="{{ route('whatsapp.chat.send') }}" method="POST" enctype="multipart/form-data" novalidate>
            @csrf
            <input type="text" name="message" class="write_msg" placeholder="Type a message" />
            <input type="hidden" id="chat_id" name="chat_id" value="{{$chatId}}" />
            <button class="msg_send_btn btn_whatsapp_chat_send" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
        </form>
    </div>
</div>
