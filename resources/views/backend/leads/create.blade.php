@extends('layouts.master')
@push('header_scripts')
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.snow.css') }}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    @include('backend.components.flash_message')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Add Lead</h4>
                </div>

                <div class="card-body">
                    <form class="row g-3 needs-validation" action="{{ route('lead.store') }}" method="POST" enctype="multipart/form-data" novalidate>
                        @csrf
                        {{-- <div class="col-md-4">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if ($errors->has('beams_id')) is-invalid @endif" name="beams_id" id="beams_id"
                                       placeholder="BEAMS ID" value="{{ old('beams_id') }}">
                                <label for="beams_id" class="form-label">BEAMS ID</label>
                                <div class="invalid-tooltip">
                                    @if ($errors->has('beams_id'))
                                        {{ $errors->first('beams_id') }}
                                    @endif
                                </div>
                            </div>
                        </div> --}}

                        <h6><u>Student&nbsp;Information</u></h6>
                        <input type="hidden" name="out_reach_id" value="{{isset($_GET['out_reach_id']) ? $_GET['out_reach_id'] :null}}"/>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('first_name')) is-invalid @endif" id="first_name" name="first_name" placeholder="First Name" value="{{ isset($_GET['name']) ? $_GET['name'] : old('first_name') }}"  required>
                                <label for="first_name" class="form-label">Name</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('first_name'))
                                        {{ $errors->first('first_name') }}
                                    @else
                                        Name is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('class_grade_id')) is-invalid @endif" id="class_grade_id" name="class_grade_id" aria-label="Class select" required>
                                    <option value="">Select Class</option>
                                    @forelse($classes as $class)
                                        <option value="{{ $class->id }}" {{ ($class->id == old('class_grade_id')) ? 'selected' : '' }}>{{ $class->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <label for="class_grade_id" class="form-label">Admission&nbsp;in&nbsp;Class</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('class_grade_id'))
                                        {{ $errors->first('class_grade_id') }}
                                    @else
                                        Select the class!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('gender')) is-invalid @endif" id="gender" name="gender" aria-label="Gender select" required>

                                    <option value="">Select gender</option>
                                    <option value="1" {{ (old('gender') == 1) ? 'selected' : '' }}>Male</option>
                                    <option value="2" {{ (old('gender') == 2) ? 'selected' : '' }}>Female</option>
                                    <option value="3" {{ (old('gender') == 3) ? 'selected' : '' }}>Other</option>

                                </select>
                                <label for="gender" class="form-label">Gender</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('gender'))
                                        {{ $errors->first('gender') }}
                                    @else
                                        Select the gender!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('contact_code_id')) is-invalid @endif" id="contact_code_id" name="contact_code_id" aria-label="Contact code select" required>
                                    @foreach ($contactCodes as $contactCode)
                                        <option value="{{ $contactCode->id }}" {{ ($contactCode->id == old('contact_code_id')) ? 'selected' : '' }}>{{ $contactCode->code }}</option>
                                    @endforeach
                                </select>
                                <label for="contact_code_id" class="form-label">Contact Code</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('contact_code_id'))
                                        {{ $errors->first('contact_code_id') }}
                                    @else
                                        Contact code is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="number" class="form-control @if($errors->has('contact_number')) is-invalid @endif" id="contact_number" name="contact_number" placeholder="Contact Number" value="{{ isset($_GET['contact_number']) ? $_GET['contact_number'] : old('contact_number') }}" required>
                                <label for="contact_number" class="form-label">Contact&nbsp;Number</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('contact_number'))
                                        {{ $errors->first('contact_number') }}
                                    @else
                                        Contact number is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="email" class="form-control @if($errors->has('email')) is-invalid @endif" id="email" name="email" placeholder="Email" value="{{ isset($_GET['email']) ? $_GET['email'] : old('email') }}" required>
                                <label for="email" class="form-label">Email</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('email'))
                                        {{ $errors->first('email') }}
                                    @else
                                        Email is required!
                                    @endif
                                </div>

                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{ isset($_GET['city']) ? $_GET['city'] : old('city') }}">
                                <label for="city" class="form-label">City</label>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control" id="previous_class" name="previous_class" placeholder="Previous Class" value="{{ old('previous_class') }}">
                                <label for="previous_class" class="form-label">Previous Class</label>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control" id="previous_branch" name="previous_branch" placeholder="Previous Branch" value="{{ old('previous_branch') }}">
                                <label for="previous_branch" class="form-label">Previous Branch</label>
                            </div>
                        </div>

                        <h6><u>Lead&nbsp;Information</u></h6>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('source_id')) is-invalid @endif" id="source_id" name="source_id" aria-label="Source select" required>
                                    <option value="">Select Source</option>
                                    @foreach ($sources as $source)
                                        <option value="{{ $source->id }}" {{ (isset($_GET['source']) && $_GET['source'] == $source->name) ? 'selected' : '' }}>{{ $source->name }}</option>
                                    @endforeach
                                </select>
                                <label for="source_id" class="form-label">Source</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('source_id'))
                                        {{ $errors->first('source_id') }}
                                    @else
                                        Select the source!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('tag_id')) is-invalid @endif" id="tag_id" name="tag_id" aria-label="Tag select" required>
                                    <option value="">Select Tag</option>
                                    @foreach ($tags as $tag)
                                        <option value="{{ $tag->id }}" {{ ($tag->id == old('tag_id')) ? 'selected' : '' }}>{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                                <label for="tag_id" class="form-label">Tag</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('tag_id'))
                                        {{ $errors->first('tag_id') }}
                                    @else
                                        Select the tag!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('lead_type_id')) is-invalid @endif" id="lead_type_id" name="lead_type_id" aria-label="Lead Type select" required>
                                    <option value="">Select Type</option>
                                    @foreach ($leadTypes as $leadType)
                                        <option value="{{ $leadType->id }}" {{ ($leadType->id == old('lead_type_id')) ? 'selected' : '' }}>{{ $leadType->name }}</option>
                                    @endforeach
                                </select>
                                <label for="lead_type_id" class="form-label">Type</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('lead_type_id'))
                                        {{ $errors->first('lead_type_id') }}
                                    @else
                                        Select the type!
                                    @endif
                                </div>
                            </div>
                        </div>

{{--                        <div class="col-md-4 col-sm-12">--}}
{{--                            <div class="form-label-group in-border">--}}
{{--                                <select class="form-select @if($errors->has('lead_status_id')) is-invalid @endif" id="lead_status_id" name="lead_status_id" aria-label="Lead Status select" required>--}}
{{--                                    <option value="">Select Status</option>--}}
{{--                                    @foreach ($leadStatuses as $leadStatus)--}}
{{--                                        <option value="{{ $leadStatus->id }}">{{ $leadStatus->name }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                                <label for="lead_status_id" class="form-label">Status</label>--}}
{{--                                <div class="invalid-tooltip">--}}
{{--                                    @if($errors->has('lead_status_id'))--}}
{{--                                        {{ $errors->first('lead_status_id') }}--}}
{{--                                    @else--}}
{{--                                        Select the status!--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        @if(auth()->user()->hasRole('super_admin|social_media_manager'))
                            <div class="col-md-4 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select data-route-courses="{{ route('branch.load-courses') }}" data-route-employees="{{ route('branch.load-employees') }}" data-route-sessions="{{ route('branch.sessions') }}" class="form-select @if($errors->has('branch_id')) is-invalid @endif" data-filter="generate-lead" id="branch_id" name="branch_id" aria-label="Branch select" required>
                                        <option value="">Select Branch</option>
                                        @foreach ($branches as $branch)
                                            <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                        @endforeach
                                    </select>
                                    <label for="branch_id" class="form-label">Branch</label>
                                    <div class="invalid-tooltip">
                                        @if($errors->has('branch_id'))
                                            {{ $errors->first('branch_id') }}
                                        @else
                                            Select the branch!
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="branch_id" value="{{ auth()->user()->employee->branch->id }}">
                        @endif

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="session_select form-select @if($errors->has('session_id')) is-invalid @endif" id="session_id" name="session_id" aria-label="Session select" required>
                                    <option value="">Select Session</option>

                                    @if(auth()->user()->employee()->exists())
                                        @forelse(auth()->user()->employee->branch->targets->reverse() as $session)
                                            <option value="{{ $session->id }}">{{ $session->name }}</option>
                                        @empty
                                        @endforelse
                                    @endif
                                </select>
                                <label for="session_id" class="form-label">Session</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('session_id'))
                                        {{ $errors->first('session_id') }}
                                    @else
                                        Select the session!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="courses form-select @if($errors->has('course_id')) is-invalid @endif" id="course_id" name="course_id" aria-label="Course select" required>
                                    <option value="">Select Course</option>

                                    @if(auth()->user()->employee()->exists())
                                        @forelse(auth()->user()->employee->branch->courses as $course)
                                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                                        @empty
                                        @endforelse
                                    @endif
                                </select>
                                <label for="course_id" class="form-label">Course</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('course_id'))
                                        {{ $errors->first('course_id') }}
                                    @else
                                        Select the course!
                                    @endif
                                </div>
                            </div>
                        </div>

                        @if(!auth()->user()->hasRole(['admission_advisor']))
                            <div class="col-md-4 col-sm-12">
                                <div class="form-label-group in-border">
                                    <select onchange="appendEmployeeName()" class="employees form-select @if($errors->has('employee_id')) is-invalid @endif" id="employee_id" name="employee_id" aria-label="Employee select">
                                        <option value="">Select Employee</option>

                                        @if(auth()->user()->employee()->exists())
                                            @forelse(auth()->user()->employee->branch->employees->where('user_id', '!=', auth()->user()->id)->where('status_id', 1) as $employee)
                                                @if($employee->user->hasRole(['admission_advisor']))
                                                    <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }} -- {{ implode(',', $employee->user->roles()->pluck('display_name')->toArray()) }}</option>
                                                @endif
                                            @empty
                                            @endforelse
                                        @endif
                                    </select>
                                    <label for="employee_id" class="form-label">Employee</label>
                                    <div class="invalid-tooltip">
                                        @if($errors->has('employee_id'))
                                            {{ $errors->first('employee_id') }}
                                        @else
                                            Select the employee!
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('is_eligible')) is-invalid @endif" id="is_eligible" name="is_eligible" aria-label="Eligibility select" required>
                                    <option value="">Select Eligibility</option>
                                    <option value="0" {{ (old('is_eligible') == 0) ? 'selected' : '' }}>No</option>
                                    <option value="1" {{ (old('is_eligible') == 1) ? 'selected' : '' }}>Yes</option>
                                    <option value="2" {{ (old('is_eligible') == 2) ? 'selected' : '' }}>Not Sure</option>

                                </select>
                                <label for="is_eligible" class="form-label">Eligibility</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('is_eligible'))
                                        {{ $errors->first('is_eligible') }}
                                    @else
                                        Select the eligibility!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('is_important')) is-invalid @endif" id="is_important" name="is_important" aria-label="Important select" required>
                                    <option value="">Select Important</option>
                                    <option value="0" {{ (old('is_important') == 0) ? 'selected' : '' }}>No</option>
                                    <option value="1" {{ (old('is_important') == 1) ? 'selected' : '' }}>Yes</option>

                                </select>
                                <label for="is_important" class="form-label">Important</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('is_important'))
                                        {{ $errors->first('is_important') }}
                                    @else
                                        Select the important!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="is_referred form-select @if($errors->has('is_referred')) is-invalid @endif" id="is_referred" name="is_referred" aria-label="Referred select" required>
                                    <option value="">Select Option</option>
                                    <option value="0" {{ (old('is_referred') == 0) ? 'selected' : '' }}>No</option>
                                    <option value="1" {{ (old('is_referred') == 1) ? 'selected' : '' }}>Yes</option>

                                </select>
                                <label for="is_referred" class="form-label">BSS Referral</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('is_referred'))
                                        {{ $errors->first('is_referred') }}
                                    @else
                                        Select the referred!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div @if(old('is_referred') == 1) style="display: block" @else style="display: none" @endif  class="col-md-4 col-sm-12 referral-input">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control" id="advisor_name" name="advisor_name" placeholder="Advisor Name" value="{{ old('advisor_name') }}">
                                <label for="advisor_name" class="form-label">Advisor&nbsp;Name</label>
                            </div>
                        </div>

                        <div @if(old('is_referred') == 1) style="display: block" @else style="display: none" @endif  class="col-md-4 col-sm-12 referral-input">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control" id="advisor_campus_name" name="advisor_campus_name" placeholder="Advisor Campus Name" value="{{ old('advisor_campus_name') }}">
                                <label for="advisor_campus_name" class="form-label">Advisor&nbsp;Campus&nbsp;Name</label>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('is_confirmed')) is-invalid @endif" id="is_confirmed" name="is_confirmed" aria-label="Referred select" required>
                                    <option value="">Select Option</option>
                                    <option value="0" {{ (old('is_confirmed') == 0) ? 'selected' : '' }}>No</option>
                                    <option value="1" {{ (old('is_confirmed') == 1) ? 'selected' : '' }}>Yes</option>

                                </select>
                                <label for="is_confirmed" class="form-label">Is Confirmed?</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('is_confirmed'))
                                        {{ $errors->first('is_confirmed') }}
                                    @else
                                        Select the option!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <h6><u>Lead&nbsp;Follow&nbsp;Ups</u></h6>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('follow_up_date')) is-invalid @endif" id="follow_up_date" name="follow_up_date" placeholder="Follow Up Date" value="{{ old('follow_up_date') }}" required>
                                <label for="follow_up_date" class="form-label">Follow&nbsp;Up&nbsp;Date</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('follow_up_date'))
                                        {{ $errors->first('follow_up_date') }}
                                    @else
                                        Select the follow up date!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('follow_up_by')) is-invalid @endif" id="follow_up_by" name="follow_up_by" placeholder="Follow up by" value="{{ old('follow_up_by') }}" required>
                                <label for="follow_up_by" class="form-label">Follow&nbsp;Up&nbsp;By</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('follow_up_by'))
                                        {{ $errors->first('follow_up_by') }}
                                    @else
                                        Follow up by is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="number" min="0" class="form-control" id="follow_up_duration" name="follow_up_duration" placeholder="Follow up Duration" value="{{ old('follow_up_duration') }}">
                                <label for="follow_up_duration" class="form-label">Follow&nbsp;Up&nbsp;Duration&nbsp;in&nbsp;Minutes</label>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-label-group in-border">
                                <div id="snow-editor" style="height: 300px;">{!! old('follow_up_remarks') !!}</div>
                                <input type="hidden" class="form-control" id="follow_up_remarks" name="follow_up_remarks" placeholder="Follow Up Remarks">
                            </div>
                        </div>

                        <div class="col-12 text-end">
                            <button class="btn btn-primary" type="submit">Submit form</button>
                            <a href="{{ route('leads.index') }}" type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="create_lead_page">
@endsection

@push('footer_scripts')
    <script src="{{ asset('theme/dist/default/assets/libs/quill/quill.min.js') }}"></script>
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script src="{{ asset('backend/modules/leads.js') }}"></script>
@endpush
