@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Activity Logs</h4>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <input id="modal_search" type="text" placeholder="Search.." class="form-control" name="modal_search">
                                <label for="modal_search" class="form-label">Enter Model</label>
                            </div>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="filter form-select" id="event" name="event" placeholder="Event">
                                    <option value="">Please select</option>
                                    <option value="created">Created</option>
                                    <option value="updated">Updated</option>
                                    <option value="deleted">Deleted</option>
                                </select>
                                <label for="event" class="form-label">Event</label>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="form-label-group in-border">
                                <input class="form-control filter" name="date_range" id="date_range">
                                <label for="date_range" class="form-label">Date</label>
                            </div>
                        </div>
                    </div>
                    <table id="activities-data-table" class="table table-bordered table-striped align-middle table-nowrap mb-0"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Performed&nbsp;On</th>
                            <th>Performed&nbsp;By</th>
                            <th>Performed&nbsp;Action</th>
                            <th>Performed&nbsp;Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Performed&nbsp;On</th>
                            <th>Performed&nbsp;By</th>
                            <th>Performed&nbsp;Action</th>
                            <th>Performed&nbsp;Date</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input id="ajaxRoute" value="{{ route('activities.index') }}" hidden />


    <div id="activityModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="zoomInModalLabel">Acitivity Log Properties Detail (ID: <span id="id-span-activity-log"></span>)</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body pb-0 mb-3" id="activity-log-body">

                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/modules/activities.js') }}"></script>
@endpush
