@extends('layouts.master')
@section('content')
@push('header_scripts')
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('theme/dist/default/assets/libs/quill/quill.snow.css') }}" rel="stylesheet" type="text/css" />
@endpush
@include('backend.components.flash_message')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Create New Campaign</h4>
            </div>

            <div class="card-body">
                <div class="live-preview">
                    <form class="row g-3 needs-validation" novalidate action="{{ route('campaign.store') }}" id="campaign-form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-6">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if ($errors->has('title')) is-invalid @endif"
                                    id="title" name="title" placeholder="Campaign Title" value="{{ old('title') }}"
                                    required>
                                <label for="title" class="form-label">Campaign Title</label>
                                <div class="invalid-tooltip">
                                    @if ($errors->has('title'))
                                        {{ $errors->first('title') }}
                                    @else
                                        Campaign title is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-label-group in-border">
                                <select class="form-select mb-3 @if($errors->has('email_template_id')) is-invalid @endif" name="email_template_id" required>
                                    <option value="">Select Template</option>
                                    @foreach (emailTemplates() as $templateKey => $emailTemplate)
                                        <option value="{{ $templateKey }}" {{ old('email_template_id') == $templateKey ? 'selected' : '' }}>{{ $emailTemplate }}</option>
                                    @endforeach
                                </select>
                                <label for="email_template_id" class="form-label">Template</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('email_template_id'))
                                        {{ $errors->first('email_template_id') }}
                                    @else
                                        Select the email template!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-label-group in-border">
                                <div id="snow-editor" onkeydown="updateEditorInput(this)" style="height: 300px;">{!! old('email_content') !!}</div>
                                <input type="text" class="form-control d-none @if($errors->has('email_content')) is-invalid @endif" id="email_content" name="email_content" required>
                                <label for="email_content" class="form-label">Email Content</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('email_content'))
                                        {{ $errors->first('email_content') }}
                                    @else
                                        Enter the email content!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-label-group in-border">
                                <input class="form-control @if($errors->has('email_list')) is-invalid @endif" type="file" class="form-control" name="email_list" id="email_list" required>
                                <label for="email_list" class="form-label">Email List</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('email_list'))
                                        {{ $errors->first('email_list') }}
                                    @else
                                        Upload an email list!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-label-group in-border">
                                <select class="form-select mb-3 @if($errors->has('is_scheduled')) is-invalid @endif" onchange="isSchedule(this)" id="is_scheduled" name="is_scheduled" required>
                                    <option value="0" {{ old('is_scheduled') == 0 ? 'selected' : '' }}>No</option>
                                    <option value="1" {{ old('is_scheduled') == 1 ? 'selected' : '' }}>Yes</option>
                                </select>
                                <label for="is_scheduled" class="form-label">Is Scheduled</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('is_scheduled'))
                                        {{ $errors->first('is_scheduled') }}
                                    @else
                                        Select the option!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12" id="schedule_date_wrapper" style="{{ old('is_scheduled') == 1 ? 'display: block' : 'display: none' }}">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('scheduled_date_time')) is-invalid @endif" id="scheduled_date_time" name="scheduled_date_time" placeholder="Schedule At" value="{{ old('scheduled_date_time') }}" >
                                <label for="scheduled_date_time" class="form-label">Schedule at</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('scheduled_date_time'))
                                        {{ $errors->first('scheduled_date_time') }}
                                    @else
                                        Select the scheduled date & time!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-12 text-end">
                            <button class="btn btn-primary" type="submit">Start New Campaign</button>
                            <a href="{{ route('campaign.index') }}" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection

@push('footer_scripts')
    <script src="{{ asset('theme/dist/default/assets/libs/quill/quill.min.js') }}"></script>
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/modules/campaign/create.js') }}"></script>
@endpush
