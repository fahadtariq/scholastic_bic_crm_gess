@extends('layouts.master')
@section('content')
@include('backend.components.flash_message')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Campaigns</h4>
                    <div class="flex-shrink-0">
                        <a href="{{ route('campaign.create') }}" class="btn btn-success btn-label btn-sm">
                            <i class="ri-add-fill label-icon align-middle fs-16 me-2"></i> Start New Campaign
                        </a>
                    </div>
                </div><!-- end card header -->
                <div class="card-body">
                    <table id="campaign-data-table"
                           class="table table-bordered table-striped align-middle table-nowrap mb-0"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Email Content</th>
                            <th>Recipients</th>
                            <th>Status</th>
                            <th>Scheduled At</th>
                            <th>Created At</th>
{{--                            <th>Action</th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Email Content</th>
                            <th>Recipients</th>
                            <th>Status</th>
                            <th>Scheduled At</th>
                            <th>Created At</th>
{{--                            <th>Action</th>--}}
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

<!--  Extra Large modal example -->
<div class="modal fade recipientsModal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Campaign Recipients</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="recipients-body">

            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-link link-success fw-medium" data-bs-dismiss="modal">
                    <i class="ri-close-line me-1 align-middle"></i> Close
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

    <input id="ajaxRoute" value="{{ route('campaign.index') }}" hidden/>
@endsection


@push('header_scripts')
@endpush

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('backend/modules/campaign/index.js') }}"></script>
@endpush
