<a href="{{ route('campaign.template.show', $row->slug) }}" target="_blank" class="btn btn-sm btn-warning btn-icon waves-effect waves-light">
    <i class="mdi mdi-eye"></i>
</a>
