@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Campaign Templates</h4>
                </div>
                <div class="card-body">
                    <table id="templates-data-table" class="table table-bordered table-striped align-middle table-nowrap mb-0"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Basic</td>
                            <td>
                                <a target="_blank" href="{{ route('campaign.template.show', ['slug' => 'basic']) }}" class="btn btn-sm btn-warning btn-icon waves-effect waves-light">
                                    <i class="mdi mdi-eye"></i>
                                </a>
                            </td>
                        </tr>
                        {{-- <tr>
                            <td>UAE</td>
                            <td>
                                <a target="_blank" href="{{ route('campaign.template.show', ['slug' => 'uae']) }}" class="btn btn-sm btn-warning btn-icon waves-effect waves-light">
                                    <i class="mdi mdi-eye"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Akhuwat</td>
                            <td>
                                <a target="_blank" href="{{ route('campaign.template.show', ['slug' => 'akhuwat']) }}" class="btn btn-sm btn-warning btn-icon waves-effect waves-light">
                                    <i class="mdi mdi-eye"></i>
                                </a>
                            </td>
                        </tr> --}}

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
