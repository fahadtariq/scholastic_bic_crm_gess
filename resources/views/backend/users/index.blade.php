@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Users</h4>
            @permission('add-user')
            <div class="flex-shrink-0">
                <a href="{{ route('user.create') }}" class="btn btn-success btn-label btn-sm">
                    <i class="ri-add-fill label-icon align-middle fs-16 me-2"></i> Add New
                </a>
            </div>
            @endpermission
        </div>
        <div class="card">
            <div class="card-body">
                <table id="users-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            @if(Auth::user()->hasRole('super_admin'))
                                <th>Not Active / Active</th>
                            @endif
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            @if(Auth::user()->hasRole('super_admin'))
                                <th>Not Active / Active</th>
                            @endif
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="user-profile-modal-area"></div>

@endsection


@push('header_scripts')


@endpush

@push('footer_scripts')

<script type="text/javascript">
    $(document).ready(function() {

        $.extend($.fn.dataTableExt.oStdClasses, {
            "sFilterInput": "form-control",
            "sLengthSelect": "form-control"
        });

        $('#users-table').dataTable({
            searching: true,
            processing: true,
            serverSide: true,
            responsive: true,
            bLengthChange: false,
            ordering: true,
            pageLength: 10,
            scrollX: true,
            language: {search: "", searchPlaceholder: "Search..."},
            ajax:
            {
                url: "{{ route('users.index') }}",
            },
            columns: [
                {   data: 'name', name: 'name'  },
                {   data: 'email', name: 'email'  },
                {   data: 'role', name: 'role'  },
                    @if(Auth::user()->hasRole('super_admin'))
                         {   data: 'is_active', name: 'is_active', orderable: false, searchable: false,  width: "15%", },
                    @endif
                {   data: 'action', name: 'action', orderable: false, searchable: false,  width: "5%",  sClass: 'text-center'   }
            ]
        });
    });

    // $(document).on('change', '.filter', function() {
    //     $('#users-table').DataTable().ajax.reload(null, false);
	// });



    $(document).on("keyup",'#mySearch', function() {
			var value = $(this).val().toLowerCase();
			if(value.length > 0 || value.length == 0){
				$('#users-table').DataTable().ajax.reload(null, false);
			}
		});

    $(document).on('click', '.user-profile-modal', function () {
        let url = $(this).attr('data-url');
        let id = $(this).attr('data-id');

        $.ajax({
            type : 'POST',
            url : url,
            data : {
              _token : "{{ csrf_token() }}",
                id
            },
            success:function (response) {
                $('.user-profile-modal-area').html(response);
                $('#userProfileModal').modal('show');
            }
        })
    });

    $(document).on('change', '.is_active', function () {
        const id = $(this).attr('data-id');
        const url = $(this).attr('data-url');
        const is_activate = $(this).is(':checked') ? 'Activate' : 'Deactivate';
        const status = $(this).is(':checked') ? 1 : 0;

        Swal.fire({
            html: '<div class="mt-3">' +
                '<lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>' +
                '<div class="mt-4 pt-2 fs-15 mx-5">' +
                '<h4>Are you sure?</h4>' +
                `<p class="text-muted mx-4 mb-0">Are you Sure You want to ${is_activate} this user ?</p>` +
                '</div>' +
                '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-primary w-xs me-2 mb-1',
            confirmButtonText: `Yes, ${is_activate} It!`,
            cancelButtonClass: 'btn btn-danger w-xs mb-1',
            buttonsStyling: false,
            showCloseButton: true
        }).then(function(result) {

            if (result.isConfirmed) {
                $.ajax({

                    url: url,
                    type: "PATCH",
                    // data : filters,
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    data :{
                        id : id,
                        is_active : status
                    },
                    cache: false,
                    success: function(data) {
                        /*$('#' + table).DataTable().ajax.reload(null, false);*/
                    },
                    error: function() {

                    },
                    beforeSend: function() {

                    },
                    complete: function() {

                    }
                });
            }else{
                $(`.is_active[data-id=${id}]`).prop('checked', !$(this).is(':checked'));
            }
        });

    });


</script>

@endpush
