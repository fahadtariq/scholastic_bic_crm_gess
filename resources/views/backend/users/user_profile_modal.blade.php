<div id="userProfileModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zoomInModalLabel">User Profile</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row mt-5">
                    <div class="col-xxl-3">
                        <div class="card mt-n5">
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <div class="profile-user position-relative d-inline-block mx-auto  mb-4">

                                            <img src="{{ asset('user-dummy-img.jpg') }}" class="rounded-circle avatar-xl img-thumbnail user-profile-image" alt="user-profile-image">



                                    </div>
                                    <h5 class="fs-16 mb-1">{{ $user->name ?? '' }}</h5>
                                    <p class="text-muted mb-0">Department</p>
                                </div>
                            </div>
                        </div><!--end card-->

                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center mb-4">
                                    <div class="flex-grow-1">
                                        <h5 class="card-title mb-0">Official Info</h5>
                                    </div>
                                </div>
                                <div class="mb-3 d-flex">
                                    <div class="avatar-xs d-block flex-shrink-0 me-3">
                                        <span class="avatar-title rounded-circle fs-16 bg-dark text-light">
                                            <i class="ri-mail-fill"></i>
                                        </span>
                                    </div>
                                    <strong class="text-muted mt-1">{{ $user->email ?? '' }}</strong>
                                </div>
                                <div class="mb-3 d-flex">
                                    <div class="avatar-xs d-block flex-shrink-0 me-3">
                                        <span class="avatar-title rounded-circle fs-16 bg-primary">
                                            <i class="ri-phone-fill"></i>
                                        </span>
                                    </div>
                                    <strong class="text-muted mt-1">Number</strong>
                                </div>
                                <div class="mb-3 d-flex">
                                    <div class="avatar-xs d-block flex-shrink-0 me-3">
                                        <span class="avatar-title rounded-circle fs-16 bg-success">
                                            <i class="ri-building-4-fill"></i>
                                        </span>
                                    </div>
                                    <strong class="text-muted mt-1">Company</strong>
                                </div>
                                <div class="d-flex">
                                    <div class="avatar-xs d-block flex-shrink-0 me-3">
                                        <span class="avatar-title rounded-circle fs-16 bg-danger">
                                            <i class="ri-home-4-fill"></i>
                                        </span>
                                    </div>
                                    <strong class="text-muted mt-1">Branch Name</strong>
                                </div>
                            </div>
                        </div><!--end card-->
                    </div><!--end col-->
                    <div class="col-xxl-9">
                        <div class="card mt-xxl-n5">
                            <div class="card-header">
                            </div>
                            <div class="card-body p-4">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="basic" role="tabpanel">
                                        <div class="row">
                                            <div class="col-6 col-md-4">
                                                <div class="d-flex mt-4">
                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                            <i class="ri-user-star-fill"></i>
                                                        </div>
                                                    </div>
                                                    <div class="flex-grow-1 overflow-hidden">
                                                        <p class="mb-1">Name :</p>
                                                        <h6 class="text-truncate mb-0">{{ $user->name ?? '' }}</h6>
                                                    </div>
                                                </div>
                                            </div><!--end col-->

                                            <div class="col-6 col-md-4">
                                                <div class="d-flex mt-4">
                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                            <i class="ri-user-follow"></i>
                                                        </div>
                                                    </div>
                                                    <div class="flex-grow-1 overflow-hidden">
                                                        <p class="mb-1">Roles :</p>
                                                        @forelse($user->roles as $role)
                                                            <span class="badge bg-success">{{ $role->display_name }}</span>
                                                        @empty
                                                        @endforelse
                                                    </div>
                                                </div>
                                            </div><!--end col-->

                                        </div><!--end row-->
                                    </div><!--end tab-pane-->

                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div>
        </div>
    </div>
</div>
