@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Add User</h4>
            </div>

            <div class="card-body">
                <form class="row g-3 needs-validation" action="{{ route('user.store') }}" method="POST" enctype="multipart/form-data" novalidate>
                @csrf
                <div class="col-md-4 col-sm-12">
                    <div class="form-label-group in-border">
                        <input type="text" class="form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" placeholder="Name" value="{{ old('name') }}"  required>
                        <label for="firstName" class="form-label">Name</label>
                        <div class="invalid-tooltip">
                            @if($errors->has('name'))
                                {{ $errors->first('name') }}
                            @else
                                Name is required!
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="form-label-group in-border">
                        <input type="email" class="form-control @if($errors->has('email')) is-invalid @endif" id="EmpEmail" name="email" placeholder="email@domain.com" value="{{ old('user.email') }}" required>
                        <label for="EmpEmail" class="form-label">Email</label>
                        <div class="invalid-tooltip">
                            @if($errors->has('email'))
                                {{ $errors->first('email') }}
                            @else
                                Email is required!
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="form-label-group in-border">
                        <input type="password" class="form-control @if($errors->has('password')) is-invalid @endif" id="password" name="password" placeholder="Password" value="{{ old('password') }}" required>
                        <label for="password" class="form-label">Password</label>
                        <div class="invalid-tooltip">
                            @if($errors->has('password'))
                                {{ $errors->first('password') }}
                            @else
                                Password is required!
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="form-label-group in-border">
                        <select class="roles-select2 form-select @if($errors->has('role_ids')) is-invalid @endif" id="role_ids" name="role_ids[]" aria-label="Role select" required>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                            @endforeach
                        </select>
                        <label for="role_ids" class="form-label">Roles</label>
                        <div class="invalid-tooltip">
                            @if($errors->has('role_ids'))
                                {{ $errors->first('role_ids') }}
                            @else
                                Role is required!
                            @endif
                        </div>
                    </div>
                </div>


                <div class="col-12 text-end">
                    <button class="btn btn-primary" type="submit">Submit form</button>
                    <a href="{{ route('users.index') }}" type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('footer_scripts')
    <script>
        $(document).ready(function () {
            $('.roles-select2').select2();
        })
    </script>
@endpush
