@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Update User</h4>
                </div>

                <div class="card-body">
                    <form class="user-profile-form row g-3 needs-validation" action="{{ route('user.profile.update') }}" method="POST" enctype="multipart/form-data" novalidate>
                        @csrf
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" placeholder="Name" value="{{ $user->name ?? '' }}"  required>
                                <label for="firstName" class="form-label">Name</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @else
                                        Name is required!
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="email" class="form-control @if($errors->has('email')) is-invalid @endif" id="EmpEmail" name="email" placeholder="email@domain.com" value="{{ $user->email ?? '' }}" readonly>
                                <label for="EmpEmail" class="form-label">Email</label>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="password" class="form-control @if($errors->has('old_password')) is-invalid @endif" id="old_password" name="old_password" placeholder="Password">
                                <label for="old_password" class="form-label">Old Password</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('old_password'))
                                        {{ $errors->first('old_password') }}
                                    @else
                                        Old password is required!
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="password" class="form-control" id="password" name="password" placeholder="New Password">
                                <label for="password" class="form-label">New Password</label>
                            </div>
                        </div>
                        <div class="col-12 text-end">
                            <button class="btn btn-primary" type="submit">Update</button>
                            <a href="{{ url('/') }}" type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
<script>
    $(document).on('submit', '.user-profile-form', function (e) {
        let password = $(this).find('#password').val();
        let old_password = $(this).find('#old_password').val();

        if (password.length > 0 && old_password.length == 0) {
            e.preventDefault();
            $(this).find('#old_password').attr('required', true);
        }
    })

    $(document).on('keydown', '#password', function () {
        let input = $(this).val();
        if (input.length <= 1) {
            $('#old_password').removeAttr('required');
        }
    });
</script>
@endpush
