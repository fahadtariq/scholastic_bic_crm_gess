@permission('edit-user')
<button type="button" class="btn btn-sm btn-info btn-icon waves-effect waves-light user-profile-modal" data-id="{{ $row->id}}" data-url="{{ route('user.show') }}"><i class="mdi mdi-account"></i></button>
<a href="{{ route('user.edit', $row->id) }}" type="button" class="btn btn-sm btn-info btn-icon waves-effect waves-light"><i class="mdi mdi-pencil"></i></a>
@endpermission

{{--@permission('delete-user')
<a href="{{ route('user.destroy') }}" type="button" data-rowid="{{ $row->id }}" data-table="users-table" class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method"><i class="ri-delete-bin-5-line"></i></a>
@endpermission--}}

@if (!auth()->user()->hasPermission('edit-user') &&
    !auth()->user()->hasPermission('delete-user'))
    <span>N/A</span>
@endif
