@foreach($branch->courses as $course)
    <tr>
        <td>{{ $course->name }}</td>
        <td>
            @php
                $courseTarget = $course->targets()->where(['branch_id' => $branch->id, 'session_id' => $sessionID])->first();
            @endphp
            <input type="number" min="1" id="course_target" value="{{ isset($courseTarget) ? $courseTarget->target : 0 }}">
            <a href="javascript:;" type="button" data-route="{{ route('branch.course.target.save') }}" data-branch-id="{{ $branch->id }}" data-course-id="{{ $course->id }}" class="btn btn-sm btn-info btn-icon waves-effect waves-light save_course_target_btn"><i class="ri-save-2-line"></i></a>
        </td>
    </tr>
@endforeach
