<option value="">Please select a state</option>
@forelse($states as $state)
    <option value="{{ $state->id }}" data-country-id="{{ $countryID }}">{{ $state->name }}</option>
@empty
@endforelse
