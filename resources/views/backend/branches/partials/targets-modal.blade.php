<div id="branchTargetsModal" class="modal fade zoomIn" tabindex="-1" aria-labelledby="zoomInModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="zoomInModalLabel">Targets</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-0">
                <div class="row">
                    <div class="col-xxl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body" style="max-height: 450px;overflow: auto;">
                                <form class="row g-3 needs-validation branch-target-form" data-edit-target-route="{{ route('branch.target.edit') }}" action="javascript:;" data-route="{{ route('branch.target.store') }}" method="POST" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <input type="hidden" id="target_id" value="" name="id">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input type="text" class="name form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" placeholder="Name" value="{{ old('name') }}" >
                                            <label for="firstName" class="form-label">Name</label>
                                            <div class="invalid-tooltip">
                                                @if($errors->has('name'))
                                                    {{ $errors->first('name') }}
                                                @else
                                                    Name is required!
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input type="number" min="1" class="target form-control @if($errors->has('target')) is-invalid @endif" id="target" name="target" placeholder="Target" >
                                            <label for="target" class="form-label">Target</label>
                                            <div class="invalid-tooltip">
                                                @if($errors->has('target'))
                                                    {{ $errors->first('target') }}
                                                @else
                                                    Target is required!
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-label-group in-border">
                                            <input type="text" class="date_range form-control @if($errors->has('date_range')) is-invalid @endif" id="date_range" name="date_range" placeholder="Date Range">
                                            <label for="date_range" class="form-label">Date Range</label>
                                            <div class="invalid-tooltip">
                                                @if($errors->has('date_range'))
                                                    {{ $errors->first('date_range') }}
                                                @else
                                                    Select the date range!
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-end">
                                        <button class="btn btn-warning clear_target_btn" type="button">Clear</button>
                                        <button class="btn btn-primary target_submit_btn" type="submit">Add</button>
                                    </div>
                                </form>
                                <hr>
                                <table class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Target</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="branch_targets_listing">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
