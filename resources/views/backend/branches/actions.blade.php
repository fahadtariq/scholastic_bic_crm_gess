<a href="javascript:;" data-target-default-route="{{ route('branch.target.default.status') }}" data-edit-target-route="{{ route('branch.target.edit') }}" data-route="{{ route('branch.targets') }}" data-branch-id="{{ $row->id }}" data-toggle="tooltip" title="Targets" type="button" class="btn btn-sm btn-warning btn-icon waves-effect waves-light branch_targets-btn"><i class="ri-arrow-up-circle-line"></i></a>
@permission('edit-branch')
<a href="{{ route("branch.edit", $row->id) }}" type="button" class="btn btn-sm btn-info btn-icon waves-effect waves-light"><i class="mdi mdi-pencil"></i></a>
@endpermission
@permission('delete-branch')
<a href="javascript:;" type="button" class="delete-branch btn btn-sm btn-danger btn-icon waves-effect waves-light" data-id="{{ $row->id }}" data-url="{{ route("branch.delete")}}"><i class="mdi mdi-delete"></i></a>
@endpermission

@if (!auth()->user()->hasPermission('edit-branch') &&
    !auth()->user()->hasPermission('delete-branch'))
    <span>N/A</span>
@endif
