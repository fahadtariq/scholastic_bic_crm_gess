@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Add Branch</h4>
                </div>

                <div class="card-body">
                    <form class="row g-3 needs-validation" onsubmit="disableSubmitBtn(this)" action="{{ route('branch.store') }}" method="POST" enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="col-md-4">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if ($errors->has('beams_id')) is-invalid @endif" name="beams_id" id="beams_id"
                                       placeholder="BEAMS ID" value="{{ old('beams_id') }}" required>
                                <label for="beams_id" class="form-label">BEAMS ID</label>
                                <div class="invalid-tooltip">
                                    @if ($errors->has('beams_id'))
                                        {{ $errors->first('beams_id') }}
                                    @else
                                        Beams id is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" placeholder="Name" value="{{ old('name') }}"  required>
                                <label for="firstName" class="form-label">Name</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @else
                                        Name is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select data-url="{{ route('branch.country.states') }}" class="countries form-select @if($errors->has('country_id')) is-invalid @endif" id="country_id" name="country_id" aria-label="Country select" required>
                                    <option value="">Please select a country</option>
                                    @foreach ($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                                <label for="country_id" class="form-label">Country</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('country_id'))
                                        {{ $errors->first('country_id') }}
                                    @else
                                        Country is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select data-url="{{ route('branch.state.cities') }}" class="states form-select @if($errors->has('state_id')) is-invalid @endif" id="state_id" name="state_id" aria-label="State select" required>
                                    <option value="">Please select a state</option>
                                </select>
                                <label for="state_id" class="form-label">State</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('state_id'))
                                        {{ $errors->first('state_id') }}
                                    @else
                                        State is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="cities form-select @if($errors->has('city_id')) is-invalid @endif" id="city_id" name="city_id" aria-label="City select" required>
                                    <option value="">Please select a city</option>
                                </select>
                                <label for="city_id" class="form-label">City</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('city_id'))
                                        {{ $errors->first('city_id') }}
                                    @else
                                        City is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select @if($errors->has('contact_code_id')) is-invalid @endif" id="contact_code_id" name="contact_code_id" aria-label="Contact code select" required>
                                    <option value="">Please select a contact code</option>
                                    @foreach ($contactCodes as $contactCode)
                                        <option value="{{ $contactCode->id }}">{{ $contactCode->code }}</option>
                                    @endforeach
                                </select>
                                <label for="contact_code_id" class="form-label">Contact Code</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('contact_code_id'))
                                        {{ $errors->first('contact_code_id') }}
                                    @else
                                        Contact code is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="number" class="form-control @if($errors->has('contact_number')) is-invalid @endif" id="contact_number" name="contact_number" placeholder="Contact Number" required>
                                <label for="contact_number" class="form-label">Contact Number</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('contact_number'))
                                        {{ $errors->first('contact_number') }}
                                    @else
                                        Contact number is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-label-group in-border">
                                <textarea class="form-control" name="address"></textarea>
                                <label for="address" class="form-label">Address</label>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select multiple="multiple" class="select2 form-select" id="course_ids" name="course_ids[]" aria-label="Course select" required>
                                    @forelse($courses as $course)
                                        <option value="{{ $course->id }}">{{ $course->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <label for="course_ids" class="form-label">Courses</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('course_ids'))
                                        {{ $errors->first('course_ids') }}
                                    @else
                                        Course is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <input type="text" class="form-control @if($errors->has('link')) is-invalid @endif" id="link" name="link" placeholder="Link" value="{{ old('link') }}">
                                <label for="link" class="form-label">Link</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('link'))
                                        {{ $errors->first('link') }}
                                    @else
                                        Link is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-label-group in-border">
                                <select class="form-select" id="status_id" name="status_id" aria-label="Status select" required>
                                    @forelse($statuses as $status)
                                        <option value="{{ $status->id }}">{{ $status->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                                <label for="status_id" class="form-label">Status</label>
                                <div class="invalid-tooltip">
                                    @if($errors->has('status_id'))
                                        {{ $errors->first('status_id') }}
                                    @else
                                        Status is required!
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-12 text-end">
                            <button class="btn btn-primary" type="submit">Submit form</button>
                            <a href="{{ route('branches.index') }}" type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('backend/modules/branches.js') }}"></script>
@endpush
