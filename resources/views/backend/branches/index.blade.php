@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')

    <div class="row">
        <div class="col-lg-12">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Branches</h4>

                @permission('add-branch')
                    <div class="flex-shrink-0">
                        <a href="{{ route('branch.create') }}" class="btn btn-success btn-label btn-sm">
                            <i class="ri-add-fill label-icon align-middle fs-16 me-2"></i> Add New
                        </a>
                    </div>
                @endpermission

            </div>
            <div class="card">
                <div class="card-body">
                    <table id="branches-table" class="table table-bordered table-striped align-middle table-nowrap mb-0" style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ route('branches.index') }}" id="branch_list_url">
    @include('backend.branches.partials.targets-modal')
@endsection

@push('footer_scripts')
    <script src="{{asset('theme/dist/default/assets/js/pages/flatpickr.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/modules/branches.js') }}"></script>
@endpush
