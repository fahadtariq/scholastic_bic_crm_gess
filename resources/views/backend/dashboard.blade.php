@extends('layouts.master')

@push('header_scripts')
<link rel="stylesheet" href="{{ asset('theme/dist/default/assets/libs/dragula/dragula.min.css') }}" />
@endpush

@section('content')
<div class="row">
   <div class="col">
      <div class="h-100">
         <div class="row">
            {{-- @permission('show-leads-widget')
            <div class="col-xl-3 col-md-6">
               <div class="card card-animate">
                  <div class="card-body">
                     <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                           <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Leads</p>
                        </div>
                     </div>
                     <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                           <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value"
                                 data-target="{{ $leads ?? '' }}">0</span></h4>
                           <a href="{{ route('leads.index') }}" class="text-decoration-underline">View leads</a>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                           <span class="avatar-title bg-soft-success rounded fs-3">
                              <i class="ri-customer-service-2-line text-success"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endpermission

            @permission('show-branches-widget')
            <div class="col-xl-3 col-md-6">
               <div class="card card-animate">
                  <div class="card-body">
                     <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                           <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Branches</p>
                        </div>
                     </div>
                     <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                           <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value"
                                 data-target="{{ $branchesCount ?? '' }}">0</span></h4>
                           <a href="{{ route('branches.index') }}" class="text-decoration-underline">View branches</a>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                           <span class="avatar-title bg-soft-info rounded fs-3">
                              <i class="bx bx-shopping-bag text-info"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endpermission

            @permission('show-users-widget')
            <div class="col-xl-3 col-md-6">
               <div class="card card-animate">
                  <div class="card-body">
                     <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                           <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Users</p>
                        </div>
                     </div>
                     <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                           <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value"
                                 data-target="{{ $users ?? '' }}">0</span></h4>
                           <a href="{{ route('users.index') }}" class="text-decoration-underline">View users</a>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                           <span class="avatar-title bg-soft-warning rounded fs-3">
                              <i class="bx bx-user-circle text-warning"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endpermission

            @permission('show-employees-widget')
            <div class="col-xl-3 col-md-6">
               <div class="card card-animate">
                  <div class="card-body">
                     <div class="d-flex align-items-center">
                        <div class="flex-grow-1 overflow-hidden">
                           <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total Employees</p>
                        </div>
                     </div>
                     <div class="d-flex align-items-end justify-content-between mt-4">
                        <div>
                           <h4 class="fs-22 fw-semibold ff-secondary mb-4"><span class="counter-value"
                                 data-target="{{ $employees ?? '' }}">0</span></h4>
                           <a href="{{ route('employees.index') }}" class="text-decoration-underline">View employees</a>
                        </div>
                        <div class="avatar-sm flex-shrink-0">
                           <span class="avatar-title bg-soft-primary rounded fs-3">
                              <i class="bx bx-wallet text-primary"></i>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            @endpermission --}}
            @if(!auth()->user()->hasRole('super_admin|social_media_manager|reviewer'))
            <div class="row">
               <div class="col-md-2 col-sm-12">
                  <div class="form-label-group in-border">
                        <select disabled class="filter form-select branch_filter" name="branch_id" placeholder="Branch">
                           <option value="">Please select</option>
                           @forelse($branches as $branch)
                              <option value="{{ $branch->id }}" {{isset($userEmployee->employee->branch_id) && !empty($userEmployee->employee->branch_id) && $userEmployee->employee->branch_id == $branch->id ? 'selected' : ''}}>{{ $branch->name }}</option>
                           @empty
                           @endforelse
                        </select>
                        <label for="branch_id" class="form-label">Branch</label>
                  </div>
               </div>
              <div class="col-md-2 col-sm-12">
               <div class="form-label-group in-border">
                   <select class="filter form-select source_select" id="source_id" name="source_id" placeholder="Source">
                       <option value="">Please select</option>
                       @forelse($sources as $source)
                           <option value="{{ $source->id }}">{{ $source->name }}</option>
                       @empty
                       @endforelse
                   </select>
                   <label for="source_id" class="form-label">Source</label>
               </div>
           </div>
           <div class="col-md-2 col-sm-12">
            <button onclick="filterFunnelData('{{route("funnel.data.filter")}}')" type="button" class="filter-lead-table-btn btn btn-primary btn-label rounded-pill"><i class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i> Filter</button>
           </div>
           <hr/>
           <div id="chartdiv" style="width: 100%; height: 600px; font-size: 11px;"></div>
            {{-- For Islamabad:  Branch : 1 dashboard --}}
            {{-- @if ($userEmployee->employee->branch_id == 1)
            <div class="col-xl-12 col-md-12">
               <div class="card">
                  <div class="card-body">
                     <div class="row">

                        <div class="col-md-12 col-sm-12">
                           <div class="form-label-group in-border session_select">
                              <iframe width="1200" height="1300" src="https://lookerstudio.google.com/embed/reporting/07ff8e44-e54b-46f3-adf4-6eec6a0f766e/page/p_57zqcb5t7c" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
            @endif --}}

            {{-- For Lahore:  Branch : 3 dashboard --}}
            {{-- @if ($userEmployee->employee->branch_id == 3)
            <div class="col-xl-12 col-md-12">
               <div class="card">
                  <div class="card-body">
                     <div class="row">

                        <div class="col-md-12 col-sm-12">
                           <div class="form-label-group in-border session_select">
                              <iframe width="1200" height="1300" src="https://lookerstudio.google.com/embed/reporting/84ef6477-4154-4020-8230-cb63cc8f53ac/page/p_57zqcb5t7c" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
            @endif --}}

            {{-- For Potohar : Branch : 2 dashboard --}}
            {{-- @if ($userEmployee->employee->branch_id == 2)
            <div class="col-xl-12 col-md-12">
               <div class="card">
                  <div class="card-body">
                     <div class="row">

                        <div class="col-md-12 col-sm-12">
                           <div class="form-label-group in-border session_select">
                              <iframe width="1200" height="1300" src="https://lookerstudio.google.com/embed/reporting/3352cacc-a327-4966-83d4-0b2dc87e0668/page/p_57zqcb5t7c" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
            @endif --}}

            {{-- For Faisalabad : Branch : 4 dashboard --}}
            {{-- @if ($userEmployee->employee->branch_id == 4)
            <div class="col-xl-12 col-md-12">
               <div class="card">
                  <div class="card-body">
                     <div class="row">

                        <div class="col-md-12 col-sm-12">
                           <div class="form-label-group in-border session_select">
                              <iframe width="1200" height="1300" src="https://lookerstudio.google.com/embed/reporting/0088b828-7813-4ffa-a19e-1f3361463df7/page/p_57zqcb5t7c" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
            @endif --}}
            @endif

            @if(auth()->user()->hasRole('super_admin|social_media_manager|reviewer'))
            <div class="col-xl-12 col-md-12">
               <div class="card">
                  <div class="card-body">
                     <div class="row">
                        <div class="col-md-2 col-sm-12">
                           <div class="form-label-group in-border">
                                 <select class="filter form-select branch_filter" name="branch_id" placeholder="Branch">
                                    <option value="">Please select</option>
                                    @forelse($branches as $branch)
                                       <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                    @empty
                                    @endforelse
                                 </select>
                                 <label for="branch_id" class="form-label">Branch</label>
                           </div>
                        </div>
                       <div class="col-md-2 col-sm-12">
                        <div class="form-label-group in-border">
                            <select class="filter form-select source_select" id="source_id" name="source_id" placeholder="Source">
                                <option value="">Please select</option>
                                @forelse($sources as $source)
                                    <option value="{{ $source->id }}">{{ $source->name }}</option>
                                @empty
                                @endforelse
                            </select>
                            <label for="source_id" class="form-label">Source</label>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12">
                     <button onclick="filterFunnelData('{{route("funnel.data.filter")}}')" type="button" class="filter-lead-table-btn btn btn-primary btn-label rounded-pill"><i class="ri-filter-2-line label-icon align-middle rounded-pill fs-16 me-2"></i> Filter</button>
                     </div><hr/>
                        <div id="chartdiv" style="width: 100%; height: 600px; font-size: 12px;"></div>
                           {{-- <div class="container-fluid">
                           <div class="row text-center" style="overflow:hidden;">
                                 <div class="col-sm-3" style="float: none !important;display: inline-block;">
                                    <label class="text-left">Angle:</label>
                                    <input class="chart-input" data-property="angle" type="range" min="0" max="60" value="40" step="1"/>
                                 </div>

                                 <div class="col-sm-3" style="float: none !important;display: inline-block;">
                                    <label class="text-left">Depth:</label>
                                    <input class="chart-input" data-property="depth3D" type="range" min="1" max="120" value="100" step="1"/>
                                 </div>
                              </div>
                           </div>	 --}}
                        {{-- <div class="col-md-4 col-sm-12">
                           <div class="form-label-group in-border">
                              <select class="form-select branch_filter"
                                 data-session-route="{{ route('branch.sessions') }}" id="branch_id" name="branch_id[]"
                                 aria-label="Branch select" multiple>
                                 @foreach ($branches as $branch)
                                 <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                 @endforeach
                              </select>
                              <label for="branch_id" class="form-label">Branch</label>
                           </div>
                        </div> --}}

                        {{-- <div class="col-md-12 col-sm-12">
                           <div class="form-label-group in-border session_select">
                              <iframe width="1200" height="1300" src="https://lookerstudio.google.com/embed/reporting/bbd9e819-4ccf-49c7-a2e1-0f2b9f2e07b3/page/p_57zqcb5t7c" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                        </div> --}}
                     </div>

                  </div>
               </div>
            </div>
            @endif

         </div>
      </div>
   </div>
</div>

{{--    @include('backend.dashboard.partials.content')--}}

<div id="load-content"></div>

<input type="hidden" value="{{ route('dashboard.content.load') }}" id="dashboard_content_url">

@endsection

@push('footer_scripts')
<script src="{{ asset('theme/dist/default/assets/libs/dragula/dragula.min.js') }}"></script>
<script src="{{ asset('theme/dist/default/assets/libs/dom-autoscroller/dom-autoscroller.min.js') }}"></script>
<script src="{{ asset('theme/dist/default/assets/libs/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('backend/modules/leads.js') }}"></script>
{{-- <script src="{{ asset('backend/modules/dashboard.js') }}"></script> --}}
<script>
   var funnelColor = [ "#2E5495", "#8DA9DB", "#BCD6EF", "#D9E2F3", "#DDE9F5", "#C3E0B0" ];
   var funnelDesc = [];
   funnelDesc['totalLeadsDescription'] = "All data sets that we have provided our campuses for outreach including BSS data, Engineering Board, etc. Any incoming calls or Walk-ins will automatically be added, if they aren't already part of our data.";
   funnelDesc['totalAssignLeadsDescription'] = "All leads that have been assigned to an admission advisor to call for attempted conversion. Ideally, total leads should be assigned. Any incoming calls or Walk-in will automatically be added here";
   funnelDesc['totalContactedLeadsDescription'] = "Students who have been contacted and have had a conversation with our admission advisors. If there is no response, they would NOT be considered contacted. Any Walk-ins in response to inbound/outbound calls will be added here";
   funnelDesc['totalInterestedLeadsDescription'] = "Students who have been contacted and shown a potential interest will be included here. Any Walk-ins in response to inbound/outbound calls will be added here Walk-Ins in response to outreach (via QR) will automatically be added here.";
   funnelDesc['totalRegisteredLeadsDescription'] = "Students who have registered will reflect here.";
   funnelDesc['totalPaidLeadsDescription'] = "Students who have paid will reflect here.";
   $(document).ready(function() {
      // Fetch data using AJAX
      if($(".branch_filter").val()){
         $('.filter-lead-table-btn').trigger('click');
      }else{
         getFunnelData();
      }
   });
   function filterFunnelData(url){
      var branch = $(".branch_filter").val();
      var sendRequest = true;
      // if(branch){
      //    sendRequest = true;
      // }else{
      //    alert('Please Select Branch');
      // }
      if(sendRequest){
         $.ajax({
            url: url,
            data: {
               branch_id: $(".branch_filter").val(),
               source_id: $(".source_select").val()
            },
            type: 'POST',
            dataType: 'json',
            headers: {
               "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function(response) {
               var data = response.data;
               // Calculate rounded percentages
               var totalLeads = data.totalLeads;
               var assignedLeadsPercentage = (data.totalAssignLeads / totalLeads) * 100;
               var contactedLeadsPercentage = (data.totalContactedLeads / totalLeads) * 100;
               var interestedLeadsPercentage = (data.totalInterestedLeads / totalLeads) * 100;
               var registeredLeadsPercentage = (data.totalRegisteredLeads / totalLeads) * 100;
               var paidLeadsPercentage = (data.totalPaidLeads / totalLeads) * 100;

               var funnelData = [
                  { "title": "Total Leads", "value": data.totalLeads, "percentage": 100, "newdescription": funnelDesc.totalLeadsDescription },
                  { "title": "Assigned Leads", "value": data.totalAssignLeads, "percentage": assignedLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalAssignLeadsDescription },
                  { "title": "Contacted Leads", "value": data.totalContactedLeads, "percentage": contactedLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalContactedLeadsDescription },
                  { "title": "Walk-in Leads", "value": data.totalInterestedLeads, "percentage": interestedLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalInterestedLeadsDescription },
                  { "title": "Registered Leads", "value": data.totalRegisteredLeads, "percentage": registeredLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalRegisteredLeadsDescription },
                  { "title": "Paid Leads", "value": data.totalPaidLeads, "percentage": paidLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalPaidLeadsDescription }
               ]
               var chart = AmCharts.makeChart("chartdiv", {
                  "fontSize": 16,
                  "fontWeight": 'bold',
                  "type": "funnel",
                  "theme": "light",
                  "dataProvider": funnelData,
                  "labelText": "[[title]] - [[value]] ([[percentage]])%",
                  "balloon": {
                     "fixedPosition": true
                  },
                  "valueField": "value",
                  "titleField": "title",
                  "marginRight": 240,
                  "marginLeft": 50,
                  "startX": -500,
                  "depth3D": 100,
                  "angle": 40,
                  "outlineAlpha": 1,
                  "outlineColor": "#FFFFFF",
                  "outlineThickness": 2,
                  "labelPosition": "right",
                  "balloonText": "[[title]] - [[value]] ([[percentage]])%",
                  "export": {
                     "enabled": true
                  },
                  "colors": funnelColor
               });
            }
         });
      }
   }
   function getFunnelData(){
      $.ajax({
         url: '/get-funnel-data', // Update with the correct URL
         type: 'POST',
         dataType: 'json',
         headers: {
            "X-CSRF-TOKEN": "{{ csrf_token() }}"
         },
         success: function(response) {
            var data = response.data;
            // Calculate rounded percentages
            var totalLeads = data.totalLeads;
            var assignedLeadsPercentage = (data.totalAssignLeads / totalLeads) * 100;
            var contactedLeadsPercentage = (data.totalContactedLeads / totalLeads) * 100;
            var interestedLeadsPercentage = (data.totalInterestedLeads / totalLeads) * 100;
            var registeredLeadsPercentage = (data.totalRegisteredLeads / totalLeads) * 100;
            var paidLeadsPercentage = (data.totalPaidLeads / totalLeads) * 100;

            var funnelData = [
               { "title": "Total Leads", "value": data.totalLeads, "percentage": 100, "newdescription": funnelDesc.totalLeadsDescription },
               { "title": "Assigned Leads", "value": data.totalAssignLeads, "percentage": assignedLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalAssignLeadsDescription },
               { "title": "Contacted Leads", "value": data.totalContactedLeads, "percentage": contactedLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalContactedLeadsDescription },
               { "title": "Walk-in Leads", "value": data.totalInterestedLeads, "percentage": interestedLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalInterestedLeadsDescription },
               { "title": "Registered Leads", "value": data.totalRegisteredLeads, "percentage": registeredLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalRegisteredLeadsDescription },
               { "title": "Paid Leads", "value": data.totalPaidLeads, "percentage": paidLeadsPercentage.toFixed(2), "newdescription": funnelDesc.totalPaidLeadsDescription }
            ]
            var chart = AmCharts.makeChart("chartdiv", {
               "fontSize": 16,
               "fontWeight": 'bold',
               "type": "funnel",
               "theme": "light",
               "dataProvider": funnelData,
               "labelText": "[[title]] - [[value]] ([[percentage]])%",
               "balloon": {
                  "fixedPosition": true
               },
               "valueField": "value",
               "titleField": "title",
               "marginRight": 240,
               "marginLeft": 50,
               "startX": -500,
               "depth3D": 100,
               "angle": 40,
               "outlineAlpha": 1,
               "outlineColor": "#FFFFFF",
               "outlineThickness": 2,
               "labelPosition": "right",
               "balloonText": "[[title]] - [[value]] ([[percentage]])%", // Adjusted balloonText
               "export": {
                  "enabled": true
               },
               "colors": funnelColor
            });
         },
         error: function(xhr, status, error) {
            console.error('Error fetching data:', error);
         }
      });
   }
</script>
@endpush
