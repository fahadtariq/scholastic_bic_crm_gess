@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')
    <div class="row">
        @if (isset($class))
            @permission('edit-class')
            @include('backend.class_grades.edit')
            @endpermission
        @else
             @permission('add-class')
                @include('backend.class_grades.create')
             @endpermission
        @endif

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Classes</h4>
                </div><!-- end card header -->
                <div class="card-body">
                    <table id="class_grades_table" class="table table-bordered table-striped align-middle table-nowrap mb-0"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Abbreviation</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Abbreviation</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input id="ajaxRoute" value="{{ route('class-grades.index') }}" hidden />
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('backend/modules/class_grades.js') }}"></script>
@endpush
