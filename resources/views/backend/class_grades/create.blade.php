<div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Create New Class</h4>
            <!-- <div class="flex-shrink-0">
                <div class="form-check form-switch form-switch-right form-switch-md">
                    <label for="FormVaidationCustom" class="form-label text-muted">Show Code</label>
                    <input class="form-check-input code-switcher" type="checkbox" id="FormVaidationCustom">
                </div>
            </div> -->
        </div><!-- end card header -->

        <div class="card-body">
            <div class="live-preview">
                <form class="row g-3 needs-validation" novalidate action="{{ route('class-grade.store') }}" method="post">
                    @csrf
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('beams_id')) is-invalid @endif" name="beams_id" id="beams_id"
                                   placeholder="BEAMS ID" value="{{ old('beams_id') }}" required>
                            <label for="beams_id" class="form-label">BEAMS ID</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('beams_id'))
                                    {{ $errors->first('beams_id') }}
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control @if ($errors->has('name')) is-invalid @endif"
                                   id="name" name="name" placeholder="Please enter class name"
                                   value="{{ old('name') }}" required>
                            <label for="name" class="form-label">Class name</label>
                            <div class="invalid-tooltip">
                                @if ($errors->has('name'))
                                    {{ $errors->first('name') }}
                                @else
                                    Class name is required!
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <input type="text" class="form-control" name="abbreviation" id="classNameAbbr"
                                   placeholder="Please enter abbreviation" value="{{ old('abbreviation') }}">
                            <label for="classNameAbbr" class="form-label">Abbreviation</label>
                            <div class="invalid-tooltip">Class name abbreviation is required!</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-label-group in-border">
                            <select class="form-select mb-3" name="status_id" required>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status->id }}"
                                    @if (old('status_id') == $status->id) {{ 'selected' }} @endif>
                                        {{ $status->name }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="statusID" class="form-label">Status</label>
                            <div class="invalid-tooltip">Select the status!</div>
                        </div>
                    </div>

                    <div class="col-12 text-end">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                        <button type="button" class="btn btn-light bg-gradient waves-effect waves-light">Cancel</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
