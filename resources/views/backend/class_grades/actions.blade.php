@permission('edit-class')
<a href="{{ route('class-grade.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
<i class="mdi mdi-lead-pencil"></i>
</a>
@endpermission

@permission('delete-class')
<a href="{{ route('class-grade.delete') }}" data-rowid="{{ $row->id }}" data-table="class_grades_table"
class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
<i class="ri-delete-bin-5-line"></i>
</a>
@endpermission

@if (!auth()->user()->hasPermission('edit-class') &&
!auth()->user()->hasPermission('delete-class'))
 <span>N/A</span>
@endif
