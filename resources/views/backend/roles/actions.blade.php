@permission('edit-role')
<a class="btn btn-sm btn-success btn-icon waves-effect waves-light" href="{{ route("roles.edit",  $row->id) }}"><i class="mdi mdi-lead-pencil"></i></a>
@endpermission
{{--TEMPORARILY COMMENTED ROLES DELETE BUTTON--}}
{{--<a class="btn btn-sm btn-danger btn-icon waves-effect delete-record" href="'.route("roles.destroy",  $row->id).'" data-table="roles-table"><i class="ri-delete-bin-5-line"></i></a>/*--}}
{{-- N/A --}}
@if (!auth()->user()->hasPermission('edit-role') &&
    !auth()->user()->hasPermission('delete-role'))
    <span>N/A</span>
@endif
