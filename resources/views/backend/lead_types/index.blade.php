@extends('layouts.master')

@section('content')
    @include('backend.components.flash_message')
    <div class="row">
        @if (isset($lead_type))
            @permission('edit-lead-type')
                @include('backend.lead_types.edit')
            @endpermission
        @else
             @permission('add-lead-type')
                @include('backend.lead_types.create')
             @endpermission
        @endif

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Lead Types</h4>
                </div><!-- end card header -->
                <div class="card-body">
                    <table id="lead_type_table" class="table table-bordered table-striped align-middle table-nowrap mb-0"
                        style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Abbreviation</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Abbreviation</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input id="ajaxRoute" value="{{ route('lead-type.index') }}" hidden />
@endsection

@push('footer_scripts')
    <script type="text/javascript" src="{{ asset('backend/modules/lead_types.js') }}"></script>
@endpush
