@permission('edit-lead-type')
<a href="{{ route('lead-type.edit', $row->id) }}" class="btn btn-sm btn-success btn-icon waves-effect waves-light">
<i class="mdi mdi-lead-pencil"></i>
</a>
@endpermission

@permission('delete-lead-type')
<a href="{{ route('lead-type.delete') }}" data-rowid="{{ $row->id }}" data-table="lead_type_table"
class="btn btn-sm btn-danger btn-icon waves-effect waves-light delete-record-post-method">
<i class="ri-delete-bin-5-line"></i>
</a>
@endpermission

@if (!auth()->user()->hasPermission('edit-lead-type') &&
!auth()->user()->hasPermission('delete-lead-type'))
 <span>N/A</span>
@endif
