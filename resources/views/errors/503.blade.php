<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg">

    <head>
        
        <meta charset="utf-8" />
        <title>Maintenance | Scholastic</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="503 error page" name="description" />
        <meta content="Scholastic" name="author" />
        <link rel="shortcut icon" href="{{ asset('theme/dist/default/assets/images/favicon.ico')}}">
        <script src="{{ asset('theme/dist/default/assets/js/layout.js')}}"></script>
        <link href="{{ asset('theme/dist/default/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/dist/default/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/dist/default/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/dist/default/assets/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
    </head>

    <body>

        <div class="auth-page-wrapper pt-5">
            <div class="auth-one-bg-position auth-one-bg" id="auth-particles">
                <div class="bg-overlay"></div>
        
                <div class="shape">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1440 120">
                        <path d="M 0,36 C 144,53.6 432,123.2 720,124 C 1008,124.8 1296,56.8 1440,40L1440 140L0 140z"></path>
                    </svg>
                </div>
            </div>
        
            <div class="auth-page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center mt-sm-5 pt-4">
                                <div class="mb-5 text-white-50">
                                    <h1 class="display-5 coming-soon-text">Site is Under Maintenance</h1>
                                    <p class="fs-14">Please check back in sometime</p>
                                </div>
                                <div class="row justify-content-center mb-2">
                                    <div class="col-xl-4 col-lg-8">
                                        <div>
                                            <img src="{{ asset('theme/dist/default/assets/images/maintenance.png')}}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <p class="mb-0 text-muted">&copy; <script>document.write(new Date().getFullYear())</script> © Scholastic CRM by Beaconhouse Technology</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <script src="{{ asset('theme/dist/default/assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ asset('theme/dist/default/assets/libs/simplebar/simplebar.min.js')}}"></script>
        <script src="{{ asset('theme/dist/default/assets/libs/node-waves/waves.min.js')}}"></script>
        <script src="{{ asset('theme/dist/default/assets/libs/feather-icons/feather.min.js')}}"></script>
        <script src="{{ asset('theme/dist/default/assets/js/pages/plugins/lord-icon-2.1.0.js')}}"></script>
        <script src="{{ asset('theme/dist/default/assets/js/plugins.js')}}"></script>
        <script src="{{ asset('theme/dist/default/assets/libs/particles.js/particles.js')}}"></script>
        <script src="{{ asset('theme/dist/default/assets/js/pages/particles.app.js')}}"></script>
    </body>
</html>