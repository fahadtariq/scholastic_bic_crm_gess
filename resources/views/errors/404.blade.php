<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg">

    <head>
        <meta charset="utf-8" />
        <title>404 Error | Scholastic</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="404 error page" name="description" />
        <meta content="Scholastic" name="author" />
        <link rel="shortcut icon" href="{{ asset('theme/dist/default/assets/images/favicon.ico')}}">
        <script src="{{ asset('theme/dist/default/assets/js/layout.js')}}"></script>
        <link href="{{ asset('theme/dist/default/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/dist/default/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/dist/default/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/dist/default/assets/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div class="auth-page-wrapper py-5 d-flex justify-content-center align-items-center min-vh-100">
            <div class="auth-page-content overflow-hidden p-0">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-7 col-lg-8">
                            <div class="text-center">
                                <img src="{{ asset('theme/dist/default/assets/images/error400-cover.png')}}" alt="error img" class="img-fluid">
                                <div class="mt-3">
                                    <h3 class="text-uppercase">Sorry, Page not Found 😭</h3>
                                    <p class="text-muted mb-4">The page you are looking for not available!</p>
                                    <a href="{{route('index')}}" class="btn btn-success"><i class="mdi mdi-home me-1"></i>Back to home</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>