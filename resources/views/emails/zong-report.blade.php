@extends('emails.layout')

@section('main-content')
    <div class="row">
        Following is the "Zong Calls" report for {{$currentDate}}<br><br>
        @php
            if($branchId == 0){
                $branchLists = $branches;
            }
            else{
                $branchLists = $branches->where('id', $branchId);
            }
        @endphp
        @foreach ($branchLists as $branch) 
            @php
                $branchNowId = $branch->id;
                $hangup = $busy = $userbusy = $usernotatt = $conn = $miss = $ans = 0;
                $extentions = [];

                if($branchNowId == 1){
                    $extentions = ['03178222171', '03178222172'];
                }
                elseif($branchNowId == 2){
                    $extentions = ['03178222177'];
                }
                elseif($branchNowId == 3){
                    $extentions = ['03178222173', '03178222174'];
                }
                elseif($branchNowId == 4){
                    $extentions = ['03178222175', '03178222176'];
                }

                foreach ($outgoings as $outgoing) {
                    if(!empty($outgoing->ext) && in_array($outgoing->ext, $extentions)){
                        if($outgoing->status == 'Agent Hang Up'){
                            $hangup++;
                        }
                        else if($outgoing->status == 'Busy'){
                            $busy++;
                        }
                        else if($outgoing->status == 'User Busy'){
                            $userbusy++;
                        }
                        else if($outgoing->status == 'User Not Attended'){
                            $usernotatt++;
                        }
                        else if($outgoing->status == 'Connected'){
                            $conn++;
                        }
                    }
                }

                foreach ($incomings as $incoming) {
                    if($incoming->status == 'Missed'){
                        $miss++;
                    }
                    if(!empty($incoming->ext) && in_array($incoming->ext, $extentions)){
                        if($incoming->status == 'Answered'){
                            $ans++;
                        }
                    }
                }
            @endphp

            <div class="row" style="border: solid 2px grey; padding: 10px;margin-bottom:10px">
                <div class="col-12">
                    <h2>{{$branch->name}} ({{implode(', ',$extentions)}})</h2>

                    <h2 style="margin-bottom: 0px !important">Outgoing Calls</h2>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td class="table-data-head">Agent Hang Up</td>
                                <td class="table-data-head">Network Error</td>
                                <td class="table-data-head">User Busy</td>
                                <td class="table-data-head">User Not Attended</td>
                                <td class="table-data-head">Connected</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="table-data">{{$hangup}}</td>
                                <td class="table-data">{{$busy}}</td>
                                <td class="table-data">{{$userbusy}}</td>
                                <td class="table-data">{{$usernotatt}}</td>
                                <td class="table-data">{{$conn}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <h2 style="margin-bottom: 0px !important">Incoming Calls</h2>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                {{-- <td class="table-data-head">Missed</td> --}}
                                <td class="table-data-head">Answered</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                {{-- <td class="table-data">{{$miss}}</td> --}}
                                <td class="table-data">{{$ans}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> 
        @endforeach

        @if($branchId == 0)
            @php
                $hangup = $busy = $userbusy = $usernotatt = $conn = $miss = $ans = 0;
                $extentions = ['03178222178','03178222179','03178222180'];

                foreach ($outgoings as $outgoing) {
                    if(!empty($outgoing->ext) && in_array($outgoing->ext, $extentions)){
                        if($outgoing->status == 'Agent Hang Up'){
                            $hangup++;
                        }
                        else if($outgoing->status == 'Busy'){
                            $busy++;
                        }
                        else if($outgoing->status == 'User Busy'){
                            $userbusy++;
                        }
                        else if($outgoing->status == 'User Not Attended'){
                            $usernotatt++;
                        }
                        else if($outgoing->status == 'Connected'){
                            $conn++;
                        }
                    }
                }

                foreach ($incomings as $incoming) {
                    if($incoming->status == 'Missed'){
                        $miss++;
                    }
                    if(!empty($incoming->ext) && in_array($incoming->ext, $extentions)){
                        if($incoming->status == 'Answered'){
                            $ans++;
                        }
                    }
                }
            @endphp
            <div class="row" style="border: solid 2px grey; padding: 10px;margin-bottom:10px">
                <div class="col-12">
                    <h2>Others ({{implode(', ',$extentions)}})</h2>

                    <h2 style="margin-bottom: 0px !important">Outgoing Calls</h2>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td class="table-data-head">Agent Hang Up</td>
                                <td class="table-data-head">Network Error</td>
                                <td class="table-data-head">User Busy</td>
                                <td class="table-data-head">User Not Attended</td>
                                <td class="table-data-head">Connected</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="table-data">{{$hangup}}</td>
                                <td class="table-data">{{$busy}}</td>
                                <td class="table-data">{{$userbusy}}</td>
                                <td class="table-data">{{$usernotatt}}</td>
                                <td class="table-data">{{$conn}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <h2 style="margin-bottom: 0px !important">Incoming Calls</h2>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td class="table-data-head">Answered</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="table-data">{{$ans}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <h2 style="margin-bottom: 0px !important">Incoming Calls (Overall)</h2>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td class="table-data-head">Overall Missed</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="table-data">{{$miss}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        <br>
        Regards,
    </div>
@endsection