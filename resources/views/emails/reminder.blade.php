@extends('emails.layout')

@section('main-content')
    <div class="row">
            Hi, {{ $user->name }}<br>

            This is an reminder email set on specific lead.<br><br>

            Lead ID: {{$lead->id ?? ''}}<br>
            Lead Code: {{$lead->code ?? ''}}<br>
            Message: {{$reminder->message}}<br><br>

            Regards,
    </div>
@endsection