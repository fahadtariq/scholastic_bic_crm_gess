@extends('emails.layout')

@section('main-content')
    <div class="row">
        @if($employee->type == 1)
            Hi, {{ $employee->first_name }} {{ $employee->last_name }}<br>
            Welcome to Beaconhouse Internation College - CRM, Kindly use below credentials to logged in into the system.<br><br>
            Url: <a href="{{ route('login') }}">Login</a><br>
            Email: {{ $employee->user->email }}<br>
            Password: {{$password}}<br><br>
            Regards,
        @else
            Hi, {{ $employee->name }}<br>
            Welcome to Beaconhouse Internation College - CRM, Kindly use below credentials to logged in into the system.<br><br>
            Url: <a href="{{ route('login') }}">Login</a><br>
            Email: {{ $employee->email }}<br>
            Password: {{$password}}<br><br>
            Regards,
        @endif
    </div>
@endsection
