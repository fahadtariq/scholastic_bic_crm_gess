<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
        .main-content {
            text-align: left;
            font-size: 16px;
            color: #333333;
        }

        .footer {
            background-color: #f3f3f3;
            text-align: center;
            padding: 10px !important;
        }
        .footer-header {
            font-size: 24px;
            font-weight: 600;
            margin: 0;
            color: #333333;
        }
        .tag-line {
            font-size: 16px;
            color: #a2a2a2;
        }
        .copyright-text {
            color: #0861ce;
            font-size: 14px;
        }
        .copyright-text a, .copyright-text a:visited  {
            color: #0861ce !important;
            font-size: 14px;
            font-weight: 700;
        }
        .copyright-text a:hover {
            color: #0861ce !important;
        }
        .footer-links a{
            color: #0861ce !important;
            font-size: 14px;
            padding: 0 5px;
        }
        .footer-links a:hover {
            color: #0861ce !important;
            font-size: 14px;
        }
        .f-phone {
            border-left: 1px solid #0861ce;
        }
        .social-links img {
            padding: 10px 5px;
        }
        .email-btn {
            min-width: 200px;
            background: #ffc700;
            border: 2px solid #ffc700;
            border-radius: 100px;
            color: #525f7f;
            font-size: 14px;
            font-weight: 600;
            padding: 10px 15px;
            text-decoration: none;
        }

        .email-btn:hover {
            background: transparent;
            color: #525f7f;
            text-decoration: none;
        }

        .table-data{
            padding: 4px !important;
            border: solid 1px black !important;
            color: black !important;
        }
        .table-data-head{
            padding: 4px !important;
            border: solid 1px black !important;
            background: darkgray !important;
            color: white !important;
        }
    </style>
</head>
<body>
<table class="wrapper" width="100%">
    <tr>
        <td class="wrapper-inner" align="center">
            <table class="main" align="center" style="width: 100%; background: #f3f3f3;">
                <tr style="display: block; margin: 0 auto; max-width: 700px;">
                    <td class="header" style="text-align: center; background: #fff; display: block; padding-top: 25px; margin-top: 30px; margin-left: 10px; margin-right: 10px; padding-left: 30px; padding-right: 30px;">
                        <a class="logo" href="https://www.bic.edu.pk/">
                            <img src="https://crm.bt-ho.com/bic-logo.png" alt="" height="40" />
                        </a>
                    </td>
                </tr>
                <tr style="display: block; margin: 0 auto; max-width: 700px;">
                    <td class="main-content" style="background: #fff;display: block; padding-left: 30px; padding-right: 30px; margin-left: 10px; margin-right: 10px; margin-bottom:20px;padding-bottom:15px">
                        @yield('main-content')
                    </td>
                </tr>
                <tr>
                    <td class="footer">
                        <span class="copyright-text">Copyright © {{ date('Y') }} <a href="https://www.bic.edu.pk/">Beaconhouse Internationl College</a>. All Rights Reserved.</span>
                        <br>
                        <span class="footer-links">
                                    <a class="f-email" href="mailto:info@bic.edu.pk">info@bic.edu.pk</a>

                                </span>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
