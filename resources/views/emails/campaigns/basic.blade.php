{{-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>

    <style>
        .card-body p {
            margin: 0px !important;
        }

        td,
        div {
            justify-content: center !important;
            margin-right: auto !important;
            margin-left: auto !important;
        }

        td.top-td,
        .top-td {
            margin: -160px auto 0 !important;
        }
    </style>

</head>

<body style=" padding: 0; max-width: 650px; width: 100%; margin: 0 auto; background: #ffffff;">
    <table cellpadding="0" class="email-template"
        style="max-width: 700px; width: 100%; margin: 0 auto; background: #e4e4e4">
        <tbody>
            <tr>
                <td>
                    <img alt="img" src="{{ asset('assets/images/email/hb-banner.png') }}"
                        style="box-sizing: border-box; display: block; height: auto; border: 0; max-width: 100%; width: 100%;" />
                </td>
            </tr>
            <tr>
                <td align="center" class="top-td"
                    style="text-align: left;background: white;padding: 20px 10px;max-width: 435px;margin: -160px auto 0 !important;box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);z-index: 1;position: relative;display: flex;justify-content: center;align-items: center;min-height: 200px;">
                    <div class="card">
                        <div class="card-body">
                            {!! $content ?? 'Content Here' !!}
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="line-height: inherit;text-align: center;max-width: 500px;margin: 30px auto;">
                        Homebridge by Beaconhouse proudly stands as Pakistan's pioneering institution, offering the
                        prestigious Cambridge Certified A Level Programme. With an unwavering commitment to academic
                        excellence, Homebridge equips students with the knowledge and skills to excel on a global scale.
                        As the vanguard of education, it sets a remarkable precedent for Pakistan's
                        educational landscape.
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <img src="{{ asset('assets/images/email/Homebridge-Logo.png') }}" alt=""
                        style="max-width: 100%; height: auto; margin: 0 auto; display: block;" />
                </td>
            </tr>

            <tr>
                <td align="center" style="text-align: center">
                    <div
                        style="display: flex; justify-content: center !important; align-items: center; text-align: center; gap: 5px; margin: 60px auto 20px;">
                        <a href="https://www.facebook.com/homebridgepk" target="_blank"
                            style="margin: 0 15px; display: flex">
                            <img alt="Facebook" height="32" src="{{ asset('assets/images/email/facebook-f.png') }}"
                                style="display: block; border: 0" title="Facebook" />
                        </a>

                        <a href="https://twitter.com/Homebridgepk" target="_blank"
                            style="margin: 0 15px; display: flex">
                            <img alt="Instagram" height="32" src="{{ asset('assets/images/email/twitter.png') }}"
                                style="display: block; border: 0" title="Instagram" />
                        </a>
                        <a href="https://www.linkedin.com/company/homebridgepk/" target="_blank"
                            style="margin: 0 15px; display: flex">
                            <img alt="LinkedIn" height="32" src="{{ asset('assets/images/email/linkedin.png') }}"
                                style="display: block; border: 0" title="LinkedIn" />
                        </a>
                        <a href="https://www.youtube.com/channel/UCifboFbDFmsbvWH2jvB34Dg" target="_blank"
                            style="margin: 0 15px; display: flex">
                            <img alt="Blogger" height="32" src="{{ asset('assets/images/email/youtube.png') }}"
                                style="display: block; border: 0" title="Blogger" />
                        </a>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="max-width: 500px; text-align: center; margin: 0 auto; border-top: 2px solid #fff;">
                        <p>
                            © 2023 Homebridge by Beaconhouse. All rights reserved. 10/11
                            Gurumangat Road, Lahore Pakistan
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html> --}}



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>

    <style>
        .card-body p {
            margin: 0px !important;
        }

        td,
        div {
            justify-content: center !important;
            margin-right: auto !important;
            margin-left: auto !important;
        }
    </style>

</head>

<body style=" padding: 0; max-width: 650px; width: 100%; margin: 0 auto; background: #ffffff;">
    <table cellpadding="0" class="email-template"
        style="max-width: 700px; width: 100%; margin: 0 auto; background: #e4e4e4">
        <tbody>

            <tr>
                <td align="center">
                    <img alt="img" src="https://crm.bic.edu.pk/bic-logo.png"
                        style="height: auto; max-width: 100%;" />
                </td>
            </tr>
            <tr>
                <td>
                    <div class="card-body"
                        style="
                        text-align: left;
                        background: white;
                        padding: 20px 10px;
                        min-height: 200px;
                      ">
                        {!! $content ?? 'Content Here' !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p
                        style="
                    line-height: inherit;
                    text-align: center;
                    max-width: 500px;
                    margin: 50px auto 30px;
                  ">
                        Beaconhouse International College proudly stands as Pakistan's pioneering institution, offering the
                        prestigious Cambridge Certified A Level Programme. With an unwavering commitment to academic
                        excellence, Beaconhouse International College equips students with the knowledge and skills to excel on a global scale.
                        As the vanguard of education, it sets a remarkable precedent for Pakistan's
                        educational landscape.
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <img src="https://crm.bic.edu.pk/theme/dist/default/assets/images/favicon.ico" alt=""
                        style="max-width: 100%; height: auto; margin: 0 auto; display: block;" />
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr>
                <td align="center">
                    <table>
                        <tbody>
                            <tr align="center">
                                <td scope="row">
                                    <span style="margin: 0 15px">
                                        <a href="https://www.facebook.com/bicbeaconhouse" target="_blank">
                                            <img alt="Facebook" height="32"
                                                src="https://crm.bic.edu.pk/assets/images/email/facebook-f.png"
                                                style="display: inline; border: 0" title="Facebook" />
                                        </a>
                                    </span>
                                </td>
                                <td>
                                    <span style="margin: 0 15px">
                                        <a href="https://twitter.com/beaconhousec?lang=en" target="_blank">
                                            <img alt="Twitter" height="32"
                                                src="https://crm.bic.edu.pk/assets/images/email/twitter.png"
                                                style="display: inline; border: 0" title="Twitter" />
                                        </a>
                                    </span>
                                </td>
                                <td>
                                    <span style="margin: 0 15px">
                                        <a href="https://www.linkedin.com/company/beaconhouse-international-college/" target="_blank">
                                            <img alt="LinkedIn" height="32"
                                                src="https://crm.bic.edu.pk/assets/images/email/linkedin.png"
                                                style="display: inline; border: 0" title="LinkedIn" />
                                        </a>
                                    </span>
                                </td>
                                <td>
                                    <span style="margin: 0 15px">
                                        <a href="https://www.youtube.com/@beaconhouseinternationalco7177"
                                            target="_blank" style="margin: 0 15px">
                                            <img alt="Youtube" height="32"
                                                src="https://crm.bic.edu.pk/assets/images/email/youtube.png"
                                                style="display: inline; border: 0" title="Youtube" />
                                        </a>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>

            <tr>
                <td>
                    <div style="max-width: 500px; text-align: center; margin: 0 auto; border-top: 2px solid #fff;">
                        <p>
                            © 2023 Beaconhouse International College. All rights reserved.
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
