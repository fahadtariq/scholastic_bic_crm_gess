<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        body {width: 700px;margin: 0 auto;}
        table {border-collapse: collapse;}
        table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
        img {-ms-interpolation-mode: bicubic;}
    </style>
    <![endif]-->

    <style type="text/css">
        body,
        p,
        div {
            font-family: arial;
            font-size: 14px;
        }

        body {
            color: #000000;
        }

        body a {
            color: #1188E6;
            text-decoration: none;
        }

        p {
            margin: 0;
            padding: 0;
        }

        table.wrapper {
            width: 100% !important;
            table-layout: fixed;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: 100%;
            -moz-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        img.max-width {
            max-width: 100% !important;
        }

        .column.of-2 {
            width: 50%;
        }

        .column.of-3 {
            width: 33.333%;
        }

        .column.of-4 {
            width: 25%;
        }

        @media screen and (max-width:480px) {

            .preheader .rightColumnContent,
            .footer .rightColumnContent {
                text-align: left !important;
            }

            .preheader .rightColumnContent div,
            .preheader .rightColumnContent span,
            .footer .rightColumnContent div,
            .footer .rightColumnContent span {
                text-align: left !important;
            }

            .preheader .rightColumnContent,
            .preheader .leftColumnContent {
                font-size: 80% !important;
                padding: 5px 0;
            }

            table.wrapper-mobile {
                width: 100% !important;
                table-layout: fixed;
            }

            img.max-width {
                height: auto !important;
                max-width: 480px !important;
            }

            a.bulletproof-button {
                display: block !important;
                width: auto !important;
                font-size: 80%;
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .columns {
                width: 100% !important;
            }

            .column {
                display: block !important;
                width: 100% !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
                margin-left: 0 !important;
                margin-right: 0 !important;
            }
        }
    </style>
    <!--user entered Head Start-->

    <!--End Head user entered-->
</head>

<body>
<center class="wrapper" data-link-color="#1188E6"
        data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
    <div class="webkit">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
            <tr>
                <td valign="top" bgcolor="#ffffff" width="100%">
                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0"
                           cellspacing="0" border="0">
                        <tr>
                            <td width="100%">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <!--[if mso]>
                                            <center>
                                                <table><tr><td width="700">
                                            <![endif]-->
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                   style="width: 100%; max-width:700px;" align="center">
                                                <tr>
                                                    <td role="modules-container"
                                                        style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;"
                                                        bgcolor="#ffffff" width="100%" align="left">

                                                        <table class="module preheader preheader-hide" role="module"
                                                               data-type="preheader" border="0" cellpadding="0"
                                                               cellspacing="0" width="100%"
                                                               style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                            <tr>
                                                                <td role="module-content">
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="wrapper" role="module" data-type="image"
                                                               border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;"
                                                                    valign="top" align="center">
                                                                    <img class="max-width"
                                                                         style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:100% !important;width:100%;height:auto !important;"
                                                                         src="https://d375w6nzl58bw0.cloudfront.net/uploads/7b7b342a1ba4ab2b3b919788128f2bfb77a0f2611e8a8722f799958a3e7803ee.jpg"
                                                                         alt="" width="700" border="0">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text"
                                                               border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:18px 0px 5px 0px;line-height:22px;text-align:inherit;background-color:#ffffff;"
                                                                    height="100%" valign="top" bgcolor="#ffffff">
                                                                        {!! $content ?? 'Content Here' !!}
                                                                    <br>
                                                                            <div><span
                                                                                    style="font-family:verdana,geneva,sans-serif;">For
                                                                                        details, visit our website, <a
                                                                                        href="http://www.homebridge.pk">homebridge.pk</a>,
                                                                                        or call the helpline at (042)
                                                                                        111 462 111 to speak to a parent
                                                                                        relations representative.</span>
                                                                            </div>

                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="spacer"
                                                               border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 30px 0px;"
                                                                    role="module-content" bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="divider"
                                                               border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 0px 0px;"
                                                                    role="module-content" height="100%" valign="top"
                                                                    bgcolor="">
                                                                    <table border="0" cellpadding="0"
                                                                           cellspacing="0" align="center" width="100%"
                                                                           height="3px"
                                                                           style="line-height:3px; font-size:3px;">
                                                                        <tr>
                                                                            <td style="padding: 0px 0px 3px 0px;"
                                                                                bgcolor="#361E54"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="spacer"
                                                               border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 10px 0px;"
                                                                    role="module-content" bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="module" role="module" data-type="code"
                                                               border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td height="100%" valign="top">
                                                                    <div
                                                                        style="text-align:center!important; padding:20px; padding-top:10px;">
                                                                        <a href="https://www.facebook.com/homebridgepk"
                                                                           target="_blank"
                                                                           style="margin-right: 15px;"><img
                                                                                src="https://legacy-image-prod.s3.amazonaws.com/uploads/57bcf43371a08938629e353ac30b0c3109df579465270521c8ea54149d0c552dc42d488691a4f5c591d3b1fd2f997643ee120e7b3366bbc0ccd4f6ed1ceef52b.png"
                                                                                alt="Freepik Facebook"
                                                                                style="width: 32px; height:auto;"></a>


                                                                        <a href="https://twitter.com/Homebridgepk"
                                                                           target="_blank"
                                                                           style="margin-right: 15px;"><img
                                                                                src="https://legacy-image-prod.s3.amazonaws.com/uploads/5663cda8a8bd42683fcf7e789929c230898e01071f0f52df926983e4b18a6a36010445bef331bab667e02ca8b33f6404b5c437c3ead827c9f33650d4a04c8667.png"
                                                                                alt="Freepik Twitter"
                                                                                style="width: 32px; height:auto;"></a>


                                                                        <a href="https://www.instagram.com/homebridgepk/"
                                                                           target="_blank"
                                                                           style="margin-right: 15px;"> <img
                                                                                src="https://legacy-image-prod.s3.amazonaws.com/uploads/aecab77da29465dd695b12101dd5bfaac8f76a5509eb5fc9c7a73ed9a53e4f570ae509d2e9daf771d8bd4b5f79bb186246dd6b5d4b14b81b69eda9b82ac593de.png"
                                                                                alt="Freepik Instagram"
                                                                                style="width: 32px; height:auto;"></a>

                                                                        <a href="https://www.linkedin.com/company/homebridgepk"
                                                                           target="_blank"
                                                                           style="margin-right: 15px;"><img
                                                                                src="https://legacy-image-prod.s3.amazonaws.com/uploads/10ad6f1b12e0837aa01b17e94f57f58fb69a3d1c6da050ff5bf27222c8fe76c05acac6cfb728d0402749b16de18bc6f368770df92d3d04001984f98102bbfa55.png"
                                                                                alt="Freepik linkedin"
                                                                                style="width: 32px; height:auto;"></a>


                                                                        <a href="https://www.youtube.com/channel/UCifboFbDFmsbvWH2jvB34Dg"
                                                                           target="_blank"
                                                                           style="margin-right: 15px;">
                                                                            <img src="https://legacy-image-prod.s3.amazonaws.com/uploads/6d85df7bad5736bc5c8c110003785da3d94437caa1c2d17f24a4f1da4adbed38dda25956c4fce06af53933f5c69f7e9e56dcf3cce2216fe43239b1ae7754633f.png"
                                                                                 alt="Freepik youtube"
                                                                                 style="width: 32px; height:auto;">
                                                                        </a>

                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="module" role="module" data-type="divider"
                                                               border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 0px 0px;"
                                                                    role="module-content" height="100%" valign="top"
                                                                    bgcolor="">
                                                                    <table border="0" cellpadding="0"
                                                                           cellspacing="0" align="center" width="100%"
                                                                           height="3px"
                                                                           style="line-height:3px; font-size:3px;">
                                                                        <tr>
                                                                            <td style="padding: 0px 0px 3px 0px;"
                                                                                bgcolor="#361E54"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="spacer"
                                                               border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 30px 0px;"
                                                                    role="module-content" bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div data-role="module-unsubscribe"
                                                             class="module unsubscribe-css__unsubscribe___2CDlR"
                                                             role="module" data-type="unsubscribe"
                                                             style="background-color:#012037;color:#ffffff;font-size:12px;line-height:20px;padding:16px 16px 16px 16px;text-align:center">
                                                            <div class="Unsubscribe--addressLine">
                                                                <p class="Unsubscribe--senderName"
                                                                   style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px">
                                                                    HomeBridge by Beaconhouse</p>
                                                                <p
                                                                    style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px">
                                                                        <span class="Unsubscribe--senderAddress">10-11
                                                                            Gurumangat road, Gulbeg III</span>, <span
                                                                        class="Unsubscribe--senderCity">Lahore</span>,
                                                                    <span
                                                                        class="Unsubscribe--senderState">Punjab</span>
                                                                </p>
                                                            </div>
                                                            <p
                                                                style="font-family:Arial,Helvetica, sans-serif;font-size:12px;line-height:20px">
                                                                <a class="Unsubscribe--unsubscribeLink"
                                                                   href="<%asm_group_unsubscribe_raw_url%>">Unsubscribe</a>
                                                                - <a class="Unsubscribe--unsubscribePreferences"
                                                                     href="<%asm_preferences_raw_url%>">Unsubscribe
                                                                    Preferences</a>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]>
                                            </td></tr></table>
                                            </center>
                                            <![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</center>
</body>

</html>
