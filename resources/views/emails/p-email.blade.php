<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>

    <style>
        .card-body p {
            margin: 0px !important;
        }

        td,
        div {
            justify-content: center !important;
            margin-right: auto !important;
            margin-left: auto !important;
        }
    </style>

</head>

<body style=" padding: 0; max-width: 650px; width: 100%; margin: 0 auto; background: #ffffff;">
Dear concern, 
Please process admission of below student
<p>Name: {{$lead->student->first_name}}</p>
<p>Phone: {{$lead->student->contact_number}}</p>
<p>Registration ID: {{$lead->beams_registration_id}}</p>
<p>Visitor ID: {{$lead->beams_id}}</p>
<p>Campus: {{$lead->branch->name}}</p>
<p>Session: {{$lead->session->name}}</p>
<p>Remarks: {{$remarks}}</p>
<p>Attachment Link: <a target="_blank" href="{{$attachment_link}}">{{$attachment_link}}</a></p>
</body>

</html>
