<?php
if(isset($type) && $type != ''){
    $type = $type;
}
else{
    $type = '';
}

if(isset($module) && $module != ''){
    $module = $module;
}
else{
    $module = '';
}
?>
<div class="row mt-3">
    {{$pagination->links('layouts.pagination', ['type' => $type, 'module' => $module])}}
</div>
