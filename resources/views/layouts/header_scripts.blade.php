<!-- App favicon -->
<link rel="shortcut icon" href="{{ asset('theme/dist/default/assets/images/favicon.ico') }}">

<!-- aos css -->
<link rel="stylesheet" href="{{ asset('theme/dist/default/assets/libs/aos/aos.css') }}" />
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- Layout config Js -->
<script src="{{ asset('theme/dist/default/assets/js/layout.js') }}"></script>

<!-- Bootstrap Css -->
<link href="{{ asset('theme/dist/default/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/datatables/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/datatables/fixedColumns.dataTables.css') }}" />
<link href="{{ asset('assets/css/datatables/buttons.dataTables.min.css') }}">

<link href="{{ asset('theme/dist/default/assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Icons Css -->
<link href="{{ asset('theme/dist/default/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />

<!-- App Css-->
<link href="{{ asset('theme/dist/default/assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

<!-- custom Css-->
<link href="{{ asset('theme/dist/default/assets/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/labels/floating-labels.min.css') }}" media="screen">
<link href="{{ asset('theme/dist/default/assets/libs/fullcalendar/main.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Select2 -->
<link href="{{ asset('assets/css/select2/select2.min.css') }}" rel="stylesheet" />
<!-- Custom CSS -->
<style type="text/css">
	.invalid-tooltip {
	    padding: 0.1rem 0.4rem;
	    right: 0;
	}
    .dt-buttons {
        float: right !important;
    }
    #loading-overlay {
        background: rgba(1,1,1,.5);
        color: white;
        position: fixed;
        height: 100%;
        width: 100%;
        z-index: 5000;
        top: 0;
        left: 0;
        float: left;
        text-align: center;
        padding-top: 18%;
    }
</style>
