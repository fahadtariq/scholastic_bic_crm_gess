@if ($paginator->hasPages())
    <?php
        if(isset($type) && $type != ''){
            $type= $type;
            $param_type= '&type='.$type;
        }
        else{
            $type= '';
            $param_type= '';
        }

        if(isset($module) && $module != ''){
            $module= $module;
            $param_module= '&module='.$module;
        }
        else{
            $module= '';
            $param_module= '';
        }
    ?>
    <div class="col-12">
        <ul class="pagination pagination-rounded justify-content-end mb-3">
            @if ($paginator->onFirstPage())
                <li class="page-item disabled">
                    <a data-type="{{$type}}" data-module="{{$module}}"  class="page-link" href="javascript: void(0);" aria-label="Previous">
                        <span aria-hidden="true">«</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a data-type="{{$type}}" data-module="{{$module}}"  class="page-link" href="{{ $paginator->previousPageUrl().$param_type.$param_module }}" aria-label="Previous">
                        <span aria-hidden="true">«</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @endif
            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="page-item disabled"><a data-type="{{$type}}" data-module="{{$module}}"  class="page-link" href="javascript: void(0);">{{ $element }}</a></li>
                @endif
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active"><a data-type="{{$type}}" data-module="{{$module}}"  class="page-link" href="javascript: void(0);">{{ $page }}</a></li>
                        @else
                            <li class="page-item"><a data-type="{{$type}}" data-module="{{$module}}"  class="page-link" href="{{ $url.$param_type.$param_module }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if ($paginator->hasMorePages())
                <li class="page-item"><a data-type="{{$type}}" data-module="{{$module}}"  class="page-link" href="{{ $paginator->nextPageUrl().$param_type.$param_module }}">&rsaquo;</a></li>
            @else
                <li class="page-item disabled"><a data-type="{{$type}}" data-module="{{$module}}"  class="page-link" href="javascript: void(0);">&rsaquo;</a></li>
            @endif
        </ul>
    </div>
@endif
