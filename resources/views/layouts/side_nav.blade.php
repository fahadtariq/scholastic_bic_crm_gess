<div class="app-menu navbar-menu">
    <div class="navbar-brand-box">
        <a href="{{ route('dashboard') }}" class="logo logo-dark">
            <span class="logo-sm">
                <img src="{{asset('assets/images/bic-logo.svg')}}" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{asset('assets/images/bic-logo.svg')}}" alt="" height="40">
            </span>
        </a>

        <a href="{{ route('dashboard') }}" class="logo logo-light">
            <span class="logo-sm">
                <img src="{{asset('assets/images/bic-logo.svg')}}" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{asset('assets/images/bic-logo.svg')}}" alt="" height="40">
            </span>
        </a>

        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover"
                id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">
            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title"><span data-key="t-menu">Menu</span></li>

                <li class="nav-item">
                    <a class="nav-link menu-link {{ Request::is('dashboard') ? 'active' : '' }}"
                       href="{{ route('dashboard') }}" role="button">
                        <i class="ri-home-smile-line"></i> <span data-key="t-dashboards">Dashboard</span>
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link menu-link {{ Request::is('sales-funnel') ? 'active' : '' }}"
                        href="{{ route('sales.funnel') }}" role="button">
                        <i class="ri-home-smile-line"></i> <span data-key="t-dashboards">Dashboard</span>
                    </a>
                </li> --}}
                @permission('show-data-insights')
                <li class="nav-item">
                    <a class="nav-link menu-link {{ Request::is('data-insights') ? 'active' : '' }}"
                       href="{{ route('data-insights') }}" role="button">
                        <i class="ri-database-line"></i> <span data-key="t-dashboards">Data Insights</span>
                    </a>
                </li>
                @endpermission
                @if (auth()->user()->hasRole(['super_admin', 'reviewer']))
                    <li class="nav-item">
                        <a class="nav-link menu-link {{ Request::is('call-center-insights') ? 'active' : '' }}"
                           href="{{ route('call-center-insights') }}" role="button">
                            <i class="ri-database-line"></i> <span data-key="t-dashboards">Call Center Insights</span>
                        </a>
                    </li>
                @endif
                @permission('show-branches')
                <li class="nav-item">
                    <a class="nav-link menu-link  @if (Request::is('branch/*') || Request::is('branch/edit/*') || Request::is('employee/*')) active @endif"
                       href="#branchModule"
                       data-bs-toggle="collapse" role="button"
                       aria-expanded="@if (Request::is('branch/*') || Request::is('branch/edit/*') || Request::is('employee/*')) true @else false @endif"
                       aria-controls="sidebarDashboards">
                        <i class="ri-stackshare-line"></i> <span
                            data-key="t-dashboards">{{ auth()->user()->hasRole('super_admin')? 'Branches': 'Branch' }}</span>
                    </a>
                    <div
                        class="collapse menu-dropdown @if (Request::is('branch/*') || Request::is('branch/edit/*') || Request::is('employee/*')) show @endif"
                        id="branchModule">
                        <ul class="nav nav-sm flex-column">
                            @if (auth()->user()->hasRole('super_admin'))
                                <li class="nav-item ">
                                    <a href="{{ route('branches.index') }}"
                                       class="nav-link @if (Request::is('branch/*')) active @endif"
                                       data-key="t-analytics"> Branch List
                                    </a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a href="{{ route('branch.edit', auth()->user()->employee->branch->id) }}"
                                       class="nav-link @if (Request::is('branch/edit/*')) active @endif"
                                       data-key="t-analytics"> Branch
                                    </a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a href="{{ route('employees.index') }}"
                                   class="nav-link @if (Request::is('employee/*')) active @endif"
                                   data-key="t-analytics">
                                    Employees</a>
                            </li>
                        </ul>
                    </div>
                </li>
                @endpermission

                @permission('show-leads')
                <li class="nav-item">
                    <a class="nav-link menu-link @if (Request::is('lead/*') || Request::is('lead-type/*') || Request::is('lead-status/*') || Request::is('list-duplicate-leads')) active @endif"
                       href="#leadModule"
                       data-bs-toggle="collapse" role="button"
                       aria-expanded="@if (Request::is('lead/*') || Request::is('lead-type/*') || Request::is('lead-status/*') || Request::is('list-duplicate-leads')) true @else false @endif"
                       aria-controls="sidebarDashboards">
                        <i class="ri-customer-service-2-line"></i> <span data-key="t-dashboards">Lead Management</span>
                    </a>
                    <div
                        class="collapse menu-dropdown @if (Request::is('lead/*') || Request::is('lead-type/*') || Request::is('lead-status/*') ||Request::is('list-duplicate-leads')) show @endif"
                        id="leadModule">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('leads.index') }}"
                                   class="nav-link @if (Request::is('lead/*')) active @endif"
                                   data-key="t-analytics">
                                    Leads</a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('leads.duplicate-leads') }}"
                                   class="nav-link @if (Request::is('list-duplicate-leads')) active @endif"
                                   data-key="t-analytics">
                                    Duplicates Leads</a>
                            </li>

                            @permission('show-lead-type')
                            <li class="nav-item">
                                <a href="{{ route('lead-type.index') }}"
                                   class="nav-link @if (Request::is('lead-type/*')) active @endif"
                                   data-key="t-analytics"> Lead Types
                                </a>
                            </li>
                            @endpermission

                            @permission('show-lead-status')
                            <li class="nav-item">
                                <a href="{{ route('lead-status.index') }}"
                                   class="nav-link @if (Request::is('lead-status/*')) active @endif"
                                   data-key="t-analytics">
                                    Lead Statuses</a>
                            </li>
                            @endpermission

                        </ul>
                    </div>
                </li>
                @endpermission
                @if (auth()->user()->hasRole(['super_admin', 'campus_head', 'reviewer']))
                    <li class="nav-item">
                        <a class="nav-link menu-link  @if (Request::is('reports/*')) active @endif" href="#reportModule"
                           data-bs-toggle="collapse" role="button"
                           aria-expanded="@if (Request::is('reports/*')) true @else false @endif"
                           aria-controls="sidebarDashboards">
                            <i class="ri-file-text-line"></i> <span
                                data-key="t-dashboards">Reports</span>
                        </a>
                        <div class="collapse menu-dropdown @if (Request::routeIs(['reports.*'])) show @endif"
                             id="reportModule">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="{{ route('reports.withdrawals') }}"
                                       class="nav-link @if (Request::routeIs('reports.withdrawals')) active @endif"
                                       data-key="t-analytics">
                                        Withdrawals</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('reports.walk-in-sources') }}"
                                       class="nav-link @if (Request::routeIs('reports.walk-in-sources')) active @endif"
                                       data-key="t-analytics">
                                        Sources</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('reports.mis') }}"
                                       class="nav-link @if (Request::routeIs('reports.mis')) active @endif"
                                       data-key="t-analytics">
                                        MIS</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif

                @if (auth()->user()->hasRole(['super_admin', 'reviewer']))
                    <li class="nav-item">
                        <a class="nav-link menu-link {{ Request::is('auditor-review') ? 'active' : '' }}"
                           href="{{ route('auditor-review.index') }}" role="button">
                            <i class="ri-edit-box-line"></i> <span data-key="t-dashboards">CRM Review</span>
                        </a>
                    </li>
                @endif

                @if (auth()->user()->hasRole(['super_admin']))
                    <li class="nav-item">
                        <a class="nav-link menu-link {{ Request::is('promotional-admissions') ? 'active' : '' }}"
                           href="{{ route('promotional-admissions.index') }}" role="button">
                            <i class="ri-group-line"></i> <span data-key="t-dashboards">Continue Admissions</span>
                        </a>
                    </li>
                @endif

                @if (auth()->user()->hasRole(['super_admin', 'campus_head']))
                    {{-- <li class="nav-item">
                        <a class="nav-link menu-link"  href="#dataStory"
                            data-bs-toggle="collapse" role="button"
                            aria-expanded="false"
                            aria-controls="sidebarDashboards">
                            <i class="ri-database-2-line"></i> <span
                                data-key="t-dashboards">Data Stories</span>
                        </a>
                        <div class="collapse menu-dropdown " id="dataStory">
                            <ul class="nav nav-sm flex-column">
                                @if (auth()->user()->hasRole(['super_admin']))
                                    <li class="nav-item">
                                        <a target="_blank" href="https://datastudio.google.com/reporting/e133662d-947e-41b6-ad9a-de2b5a59a37b"
                                            class="nav-link "
                                            data-key="t-analytics">
                                            Scholastic Overview</a>
                                    </li>
                                    <li class="nav-item">
                                        <a target="_blank" href="https://datastudio.google.com/reporting/1086f2aa-d795-495a-bf89-7fc20cd27ca9"
                                            class="nav-link "
                                            data-key="t-analytics">
                                            Target Analysis</a>
                                    </li>
                                @endif
                                @if (auth()->user()->hasRole(['campus_head']))
                                    @php
                                        $employeeBranch = auth()->user()->employee->branch;
                                    @endphp
                                    @if(!empty($employeeBranch->link))
                                        <li class="nav-item">
                                            <a target="_blank" href="{{$employeeBranch->link}}" class="nav-link" data-key="t-analytics">
                                                {{$employeeBranch->name}}
                                            </a>
                                        </li>
                                    @endif
                                @endif
                            </ul>
                        </div>
                    </li> --}}
                @endif

                @if(auth()->user()->hasRole('super_admin'))
                    <li class="nav-item">
                        <a class="nav-link menu-link @if (Request::is('class/*') || Request::is('courses/*') || Request::is('source/*') || Request::is('tag/*') || Request::is('activities/*') || Request::is('system-modules') || Request::is('user/*') || Request::is('roles') || Request::is('roles/*') || Request::is('permissions') || Request::is('country/*') || Request::is('state/*') || Request::is('city/*') || Request::is('contact-code/*')) active @endif"
                           href="#CourseModule" data-bs-toggle="collapse" role="button"
                           aria-expanded="@if (Request::is('class/*') || Request::is('courses/*') || Request::is('source/*') || Request::is('tag/*') || Request::is('activities/*') || Request::is('system-modules') || Request::is('user/*') || Request::is('roles') || Request::is('roles/*') || Request::is('permissions') || Request::is('country/*') || Request::is('state/*') || Request::is('city/*') || Request::is('contact-code/*')) true @else false @endif"
                           aria-controls="sidebarDashboards">
                            <i class="ri-settings-line"></i> <span data-key="t-dashboards">System Settings</span>
                        </a>
                        <div
                            class="collapse menu-dropdown @if (Request::is('class/*') || Request::is('courses/*') || Request::is('source/*') || Request::is('tag/*') || Request::is('activities/*') || Request::is('system-modules') || Request::is('user/*') || Request::is('roles') || Request::is('roles/*') || Request::is('permissions') || Request::is('country/*') || Request::is('state/*') || Request::is('city/*') || Request::is('contact-code/*')) show @endif"
                            id="CourseModule">
                            <ul class="nav nav-sm flex-column">
                                @permission('show-roles-and-permissions')
                                <li class="nav-item">
                                    <a class="nav-link menu-link @if (Request::is('system-modules') || Request::is('user/*') || Request::is('roles') || Request::is('roles/*') || Request::is('permissions')) active @endif"
                                       href="#roleAndPermissions" data-bs-toggle="collapse" role="button"
                                       aria-expanded="@if (Request::is('system-modules') || Request::is('user/*') || Request::is('roles') || Request::is('roles/*') || Request::is('permissions')) true @else false @endif"
                                       aria-controls="sidebarDashboards">
                                        {{-- <i class="ri-eye-off-line"></i> --}}
                                        <span data-key="t-dashboards">Roles & Permissions</span>
                                    </a>
                                    <div
                                        class="collapse menu-dropdown @if (Request::is('system-modules') || Request::is('user/*') || Request::is('roles') || Request::is('roles/*') || Request::is('permissions')) show @endif"
                                        id="roleAndPermissions">
                                        <ul class="nav nav-sm flex-column">
                                            <li class="nav-item">
                                                <a href="{{ route('system-modules.index') }}"
                                                   class="nav-link @if (Request::is('system-modules')) active @endif"
                                                   data-key="t-analytics"> System Modules
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('users.index') }}"
                                                   class="nav-link @if (Request::is('user/*')) active @endif"
                                                   data-key="t-analytics">
                                                    Users</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('roles.index') }}"
                                                   class="nav-link @if (Request::is('roles/*') || Request::is('roles')) active @endif"
                                                   data-key="t-analytics">
                                                    Roles
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('permissions.index') }}"
                                                   class="nav-link @if (Request::is('permissions')) active @endif"
                                                   data-key="t-analytics"> Permissions
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                @endpermission

                                @permission('show-region-management')
                                <li class="nav-item">
                                    <a class="nav-link menu-link @if (Request::is('country/*') || Request::is('state/*') || Request::is('city/*') || Request::is('contact-code/*')) active @endif"
                                       href="#regionManagement" data-bs-toggle="collapse" role="button"
                                       aria-expanded="@if (Request::is('country/*') || Request::is('state/*') || Request::is('city/*') || Request::is('contact-code/*')) true @else false @endif"
                                       aria-controls="sidebarDashboards">
                                        {{-- <i class="ri-global-line"></i> --}}
                                        <span data-key="t-dashboards">Region Management</span>
                                    </a>
                                    <div
                                        class="collapse menu-dropdown @if (Request::is('country/*') || Request::is('state/*') || Request::is('city/*') || Request::is('contact-code/*')) show @endif"
                                        id="regionManagement">
                                        <ul class="nav nav-sm flex-column">
                                            <li class="nav-item">
                                                <a href="{{ route('country.index') }}"
                                                   class="nav-link @if (Request::is('country/*')) active @endif"
                                                   data-key="t-analytics">
                                                    Country</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('state.index') }}"
                                                   class="nav-link @if (Request::is('state/*')) active @endif"
                                                   data-key="t-analytics">
                                                    State
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('city.index') }}"
                                                   class="nav-link @if (Request::is('city/*')) active @endif"
                                                   data-key="t-analytics">
                                                    City
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route('contact-code.index') }}"
                                                   class="nav-link @if (Request::is('contact-code/*')) active @endif"
                                                   data-key="t-analytics">
                                                    Contact Code
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                @endpermission
                                <li class="nav-item">
                                    <a href="{{ route('class-grades.index') }}"
                                       class="nav-link @if (Request::is('class/*')) active @endif"
                                       data-key="t-analytics">
                                        Classes</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('course.index') }}"
                                       class="nav-link @if (Request::is('courses/*')) active @endif"
                                       data-key="t-analytics"> Courses
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('sources.index') }}"
                                       class="nav-link @if (Request::is('source/*')) active @endif"
                                       data-key="t-analytics">
                                        Sources</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('tags.index') }}"
                                       class="nav-link @if (Request::is('tag/*')) active @endif"
                                       data-key="t-analytics">
                                        Tags</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('activities.index') }}"
                                       class="nav-link @if (Request::is('activities/*')) active @endif"
                                       data-key="t-analytics">
                                        Activity&nbsp;Logs</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
                @permission('show-marketing-campaign')
                <li class="nav-item">
                    <a class="nav-link menu-link  @if (Request::is('compain/*')) active @endif"
                       href="#compainModule" data-bs-toggle="collapse" role="button"
                       aria-expanded="@if (Request::is('compain/*')) true @else false @endif"
                       aria-controls="sidebarDashboards">
                        <i class="ri-stackshare-line"></i> <span data-key="t-dashboards">Marketing Campaign</span>
                    </a>
                    <div class="collapse menu-dropdown @if (Request::is('campaign/*')) show @endif"
                         id="compainModule">
                        <ul class="nav nav-sm flex-column">
                            @permission('show-campaign-list')
                            <li class="nav-item ">
                                <a href="{{ route('campaign.index') }}"
                                   class="nav-link @if (Request::is(['campaign/list*', 'campaign/create*'])) active @endif"
                                   data-key="t-analytics"> Campaign List
                                </a>
                            </li>
                            @endpermission

                            <li class="nav-item ">
                                <a href="{{ route('campaign.template.index') }}"
                                   class="nav-link @if (Request::is('campaign/templates')) active @endif"
                                   data-key="t-analytics">Campaign Templates
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                @endpermission
                @if (auth()->user()->hasRole(['super_admin', 'campus_head']))
                    <li class="nav-item">
                        <a class="nav-link menu-link {{ Request::is('zong-portal/list') ? 'active' : '' }}"
                           href="{{ route('zong-portal.index') }}" role="button">
                            <i class="ri-phone-line"></i> <span data-key="t-dashboards">Zong Portal</span>
                        </a>
                    </li>
                @endif

                @permission('show-call-center')
                <li class="nav-item">
                    <a class="nav-link menu-link  @if (Request::is('call-center/*')) active @endif"
                       href="#callCenterModule" data-bs-toggle="collapse" role="button"
                       aria-expanded="@if (Request::is('call-center/*')) true @else false @endif"
                       aria-controls="sidebarDashboards">
                        <i class="ri-stackshare-line"></i> <span data-key="t-dashboards">Call Center</span>
                    </a>
                    <div class="collapse menu-dropdown @if (Request::is('call-center/*')) show @endif"
                         id="callCenterModule">
                        <ul class="nav nav-sm flex-column">
                            @permission('show-outreach-data')
                            <li class="nav-item ">
                                <a href="{{ route('outreach.index') }}"
                                   class="nav-link @if (Request::is('call-center/outreach-data/*')) active @endif"
                                   data-key="t-analytics"> Out Reach Data
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </div>
                </li>
                @endpermission
                {{-- @if (auth()->user()->hasRole(['super_admin', 'campus_head']))
                    <li class="nav-item">
                        <a class="nav-link menu-link {{ Request::is('zong-portal/local-live') ? 'active' : '' }}"
                            href="{{ route('zong-portal.live') }}" role="button">
                            <i class="ri-phone-line"></i> <span data-key="t-dashboards">Zong Portal Live</span>
                        </a>
                    </li>
                @endif --}}
                @permission('show-whats-app')
                <li class="nav-item">
                    <a class="nav-link menu-link  @if (Request::is('whatsapp/*')) active @endif"
                       href="#whatsappModule" data-bs-toggle="collapse" role="button"
                       aria-expanded="@if (Request::is('whatsapp/*')) true @else false @endif"
                       aria-controls="sidebarDashboards">
                        <i class="ri-whatsapp-line"></i> <span data-key="t-dashboards">WhatsApp</span>
                    </a>
                    <div class="collapse menu-dropdown @if (Request::is('whatsapp/*')) show @endif"
                         id="whatsappModule">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item ">
                                <a href="{{ route('whatsapp.chat.index') }}"
                                   class="nav-link @if (Request::is('whatsapp/*')) active @endif"
                                   data-key="t-analytics"> Chat
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a href="https://web.whatsapp.com/" target="_blank" class="nav-link"
                                   data-key="t-analytics"> Web Whatsapp</a>
                            </li>

                        </ul>
                    </div>
                </li>
                @endpermission
            </ul>
        </div>
    </div>
</div>
